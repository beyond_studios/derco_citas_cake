$(function(){
	var validator=$('#TabactivitytypeEditForm').validate({
		rules:{
			'data[Tabactivitytype][code]':{
				required:true
			},
			'data[Tabactivitytype][description]':{
				required:true
			}
		},
		errorPlacement: function(label, element)
		{  label.insertAfter(element);}  
	});
});