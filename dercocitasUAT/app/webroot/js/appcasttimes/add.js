/**
 * @author chiusac
 */
$(function(){
	var validator=$('#AppcasttimeAddForm').validate({
	rules:{
		'data[Appcasttime][inittime]':{
			required:true,
		},
		'data[Appcasttime][endtime]':{
			required:true,
		},
	},
	errorPlacement: function(label, element)
	{  label.insertAfter(element);}    
	});
});
$(function(){
	$('#AppcasttimeInittime').timepicker({
		hourGrid: 4,
		minuteGrid: 10

	});
	$('#AppcasttimeEndtime').timepicker({
		hourGrid: 4,
		minuteGrid: 10

	});
});
