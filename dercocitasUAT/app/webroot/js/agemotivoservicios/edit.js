/**
 * @author Lourdes
 */
$(function(){
	var validator=$('#AgemotivoservicioEditForm').validate({
	rules:{
		'data[Agemotivoservicio][description]':{
			required:true,
		},
		'data[Agemotivoservicio][codigo_sap]':{
			required:true,
		}
	},
	errorPlacement: function(label, element)
	{  label.insertAfter(element);}    
	});
});