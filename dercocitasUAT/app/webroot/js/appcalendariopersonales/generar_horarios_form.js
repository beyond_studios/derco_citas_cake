/**
 * @author chiusac
 */
$(function(){
	var validator=$('#ApppersonalcalendarGenerarHorariosFormForm').validate({
	rules:{
		'data[Apppersonalcalendar][lunes_cast_id]':{
			required:true,
		},
		'data[Apppersonalcalendar][martes_cast_id]':{
			required:true,
		},
		'data[Apppersonalcalendar][miercoles_cast_id]':{
			required:true,
		},
		'data[Apppersonalcalendar][jueves_cast_id]':{
			required:true,
		},
		'data[Apppersonalcalendar][viernes_cast_id]':{
			required:true,
		},
		'data[Apppersonalcalendar][sabado_cast_id]':{
			required:true,
		},
		'data[Apppersonalcalendar][domingo_cast_id]':{
			required:true,
		},
	},
	errorPlacement: function(label, element)
	{  label.insertAfter(element);}    
	});
});