/**
 * @author chiusac
 */
//search:true, stype:'text', searchoptions:{dataInit:datePick, attr:{title:'Select Date'}}
var url;
$(document).ready(function(){
	url = $('base').attr('href');
	inicializarFechas();
});

function inicializarFechas(){
		$('#TabreporteFechaInicio').datepicker({
			showOn: 'both',
			buttonImage: url+('/app/webroot/img/calendar.gif'),
			buttonImageOnly: true,
			buttonText: 'Calendario',
			dateFormat: 'dd-mm-yy'
		}, $.datepicker.regional['es']);
		$('#TabreporteFechaFin').datepicker({
			showOn: 'both',
			buttonImage: url+('/app/webroot/img/calendar.gif'),
			buttonImageOnly: true,
			buttonText: 'Calendario',
			dateFormat: 'dd-mm-yy'
		}, $.datepicker.regional['es']);		
	}	


function exportExcel()
	{
//		$("#TabreporteExcel").val( $("<div>").append( $("#reportes").eq(0).clone()).html());
//
//        document.forms[0].method='POST';
//        document.forms[0].action=url+'tabreportes/exportarExcel';	// send it to server which will open this contents in excel file
//        document.forms[0].target='_blank';
//        document.forms[0].submit();
		var fechaInicial = $('#TabreporteFechaInicio').val();
		if(fechaInicial == '') fechaInicial='0';
		var fechaFinal = $('#TabreporteFechaFin').val();
		if(fechaFinal == '') fechaFinal = '0';
		var persona = $('#TabreporteResponsable').val();
		if(persona == '') persona = '0';
		var parametros = persona+ '/'+ fechaInicial + '/' + fechaFinal
		var url1 = url + '/tabreportes/reportePorOtExcel/'+ parametros;
		
	    var w = window.open(url1 , 'exportalExcel', 
	        'scrollbars=yes,resizable=yes,width=100,height=100,status=no,location=no,toolbar=no');	
					

	}