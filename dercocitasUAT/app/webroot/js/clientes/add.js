$(function(){
	var validator=$('#ClienteAddForm').validate({
	rules:{
		'data[Cliente][documento_tipo]':{
			required:true
		},
		'data[Cliente][documento_numero]':{
			required:true
		},
		'data[Cliente][placa]':{
			required:true
		}
	},
	errorPlacement: function(label, element)
	{  label.insertAfter(element);}
	});
});