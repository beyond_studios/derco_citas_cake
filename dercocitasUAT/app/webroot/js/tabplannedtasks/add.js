var anio;
var mes;
var tarea;
var dia;
var url;
var hora;
var minuto;
var fecha;
var horatemp;
var minutotemp;
$(document).ready(function(){
		url = $('base').attr('href');
		tarea = $('#TabplannedtaskTabtaskId').val();
		dia = $('#TabplannedtaskDia').val();
		mes = $('#TabplannedtaskMes').val();
		anio=$('#TabplannedtaskAnio').val();
		hora = $('#TabplannedtaskHora').val();
		minuto=$('#TabplannedtaskMinuto').val();
		fecha=dia+'/'+mes+'/'+anio;
		horatemp=hora;
		minutotemp=minuto;
		initFecha();
		initHoras();
//		$("#fecha").hide();
//		$("#horaInicio").hide();
//		$("#horaFin").hide();
//		$('#TabplannedtaskFechaLabel').hide();
//		$('#TabplannedtaskHoraInicioLabel').hide();
//		$('#TabplannedtaskHoraFinLabel').hide();
	});
	
	$(function(){
		var validator=$('#TabplannedtaskAddForm').validate({
		rules:{
			'data[Tabplannedtask][secperson_id]':{
				required:true,
			},
			'data[Tabplannedtask][fecha]':{
				required:true,
			},
			'data[Tabplannedtask][initdatetime]':{
				required:true,
			},
			'data[Tabplannedtask][enddatetime]':{
				required:true,
			},			
		},
		errorPlacement: function(label, element)
		{  label.insertAfter(element);}    
		});
	});
	
	function initFecha(){
		var nuevomes=mes-1;
		$('#TabplannedtaskFecha').datepicker({
			showOn: 'both',
			buttonImage: url+('app/webroot/img/calendar.gif'),
			buttonImageOnly: true,
			buttonText: 'Calendario',
			minDate: new Date(parseInt(anio),nuevomes, parseInt(dia)),
			//minDate: new Date(2011, 07, 25),
			onSelect: function(dateText, inst){
				$('#TabplannedtaskInitdatetime').timepicker("destroy");
				$('#TabplannedtaskEnddatetime').timepicker("destroy");
				now=''+dateText
				if(now==fecha){
					horatemp=hora;
					minutotemp=minuto;
				}else{
					$('#TabplannedtaskInitdatetime').val('');
					$('#TabplannedtaskEnddatetime').val('');
					horatemp=8;
					minutotemp=0;
				}
				initHoras();				
			}
		});
	}
	function initHoras(){
		$('#TabplannedtaskInitdatetime').timepicker({
				hourGrid: 4,
				minuteGrid: 10,
				minDate: new Date(anio, (parseInt(mes)-1),dia , horatemp, minutotemp)
				//minDate: new Date(anio, (parseInt(mes)-1),dia , hora, minuto)
		})
		$('#TabplannedtaskEnddatetime').timepicker({
				hourGrid: 4,
				minuteGrid: 10,
				minDate: new Date(anio, (parseInt(mes)-1),dia , horatemp, minutotemp)
				//minDate: new Date(anio, (parseInt(mes)-1),dia , hora, minuto)
		});
	}