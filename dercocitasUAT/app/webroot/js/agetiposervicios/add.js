$(function(){
	var validator=$('#AgetiposervicioAddForm').validate({
	rules:{
		'data[Agetiposervicio][description]':{
			required:true,
		},
		'data[Agetiposervicio][codigo_sap]':{
			required:true,
		},
		'data[Agetiposervicio][agemotivoservicio_id]':{
			required:true,
		}
	},
	errorPlacement: function(label, element)
	{  label.insertAfter(element);}    
	});
});