var url;
$(document).ready(function(){
	url = $('base').attr('href');
	inicializarFechas();
	$('#rechazarSolicitud').validate();
});

function inicializarFechas(){
	$('.dialog_fecha').datepicker({
		showOn: 'button',
		buttonImage:url+'img/derco/defecto/aventura/calendar.gif',
		buttonImageOnly: true, 
		buttonText: 'Cal',
		dateFormat: 'dd-mm-yy',
		onSelect: function(dateText, inst){
			$(this).parent().find('label.error').html('');
		}
	},$.datepicker.regional['es']);
	
	$( ".dialog_fecha" ).datepicker( "option", "altFormat", 'dd-mm-yy' );
}