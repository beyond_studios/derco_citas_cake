/**
 * @author chiusac
 */
var url;
$(document).ready(function(){
	url = $('base').attr('href');
	inicializarFechas();
	$('#aprobarSolicitud').validate({
		submitHandler: function(form){
				form.submit();
		},
		errorPlacement: function(label, element){	
			if(element.hasClass('msg_error_1'))
				label.insertAfter(element.next());
			else if (element.hasClass('msg_error_2'))
				label.insertAfter(element.next().next());
			else if (element.hasClass('msg_error_3'))
				label.insertAfter(element.next().next().next());
			else
				label.insertAfter(element);
		}   
	});	
});


function inicializarFechas(){
	$("#CcpsolicitudservicioFechacomfirmacionentrega").datepicker({
		showOn: 'button', 
		buttonImage: getUrl('img/calendar.gif'), 
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		minDate: -0,
		dateFormat: 'dd/mm/yy'
	}, $.datepicker.regional['es']);
}
