delimiter $$
DROP TABLE IF EXISTS ayudas $$
CREATE TABLE `ayudas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(50) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `titulo` varchar(250) CHARACTER SET latin1 NOT NULL,
  `status` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'AC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 $$
delimiter ;