delimiter $$
DROP PROCEDURE IF EXISTS `set_marca_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `set_marca_id`()
BEGIN
  DECLARE done BOOLEAN DEFAULT FALSE;
  DECLARE v_modelo_id, v_marca_id int;

  DECLARE c_modelos CURSOR FOR
     select model.id as model_id, marca.id as marca_id
      from modelos model JOIN marcas marca ON (model.marca_codigo = marca.codigo)
      order by model.id desc;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN c_modelos;
  FETCH c_modelos INTO v_modelo_id, v_marca_id;

  WHILE NOT done
  DO
    update modelos set marca_id = v_marca_id where id = v_modelo_id;

    FETCH c_modelos INTO v_modelo_id, v_marca_id;
  END WHILE;
  CLOSE c_modelos;
END
$$
delimiter ;