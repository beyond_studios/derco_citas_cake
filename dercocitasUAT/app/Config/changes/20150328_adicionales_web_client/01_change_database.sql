delimiter $$
DROP TABLE IF EXISTS servicios $$
CREATE TABLE `servicios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(50) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `binario` longblob DEFAULT NULL,
  `anchura` smallint(6) NOT NULL default 200,
  `altura` smallint(6) NOT NULL default 80,
  `titulo` varchar(100) NOT NULL,
  `description` varchar(250) CHARACTER SET latin1 NOT NULL,
  `status` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'AC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 $$

delimiter $$
DROP TABLE IF EXISTS campanias $$
CREATE TABLE `campanias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(50) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `binario` longblob DEFAULT NULL,
  `anchura` smallint(6) NOT NULL default 200,
  `altura` smallint(6) NOT NULL default 80,
  `url` varchar(250),
  `description` varchar(250) CHARACTER SET latin1 NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `status` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'AC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 $$

delimiter $$
DROP TABLE IF EXISTS Agedetallecitaslostsmotives $$
CREATE TABLE `Agedetallecitaslostsmotives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(80) CHARACTER SET latin1 NOT NULL,
  `status` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT 'AC',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 $$

delimiter $$
ALTER TABLE Agedetallecitaslosts CHANGE motivonocita_id agedetallecitaslostsmotive_id INT $$
ALTER TABLE Agedetallecitaslosts ADD CONSTRAINT agedetallecitaslosts_agedetallecitaslostsmotives_id FOREIGN KEY(agedetallecitaslostsmotive_id) REFERENCES agedetallecitaslostsmotives(id) $$   


delimiter ;