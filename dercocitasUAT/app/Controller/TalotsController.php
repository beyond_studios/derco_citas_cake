<?php
App::uses('AppController', 'Controller');
/**
 * Talots Controller
 *
 * @property Talot $Talot
 */
class TalotsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Talot->recursive = 0;
		$this->set('talots', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Talot->id = $id;
		if (!$this->Talot->exists()) {
			throw new NotFoundException(__('Invalid talot'));
		}
		$this->set('talot', $this->Talot->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Talot->create();
			if ($this->Talot->save($this->request->data)) {
				$this->Session->setFlash(__('The talot has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The talot could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Talot->id = $id;
		if (!$this->Talot->exists()) {
			throw new NotFoundException(__('Invalid talot'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Talot->save($this->request->data)) {
				$this->Session->setFlash(__('The talot has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The talot could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Talot->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Talot->id = $id;
		if (!$this->Talot->exists()) {
			throw new NotFoundException(__('Invalid talot'));
		}
		if ($this->Talot->delete()) {
			$this->Session->setFlash(__('Talot deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Talot was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
	
	/**update the date in talot
	 * 
	 * @param object $dateId
	 * @return 
	 */
	public function updateDate($talotId, $dateId){
		Configure::write('debug',0);
		$this->layout = 'ajax';
		$this->autoRender = false;
		
		$dateIds = array(
			'1'=>'DocAseFchL',	// fecha presupuesto 
			'2'=>'DocAseFch'	// fecha aprobacion
		);
		
		$talot['Talot']['id'] = $talotId;
		$talot['Talot'][$dateIds[$dateId]] = $this->Talot->getDateFormatDB(null, 'dmY', '/', $now=1);
		$talot['Talot'][$dateIds[$dateId].'_update'] = true;
		
		$rpt = array(false, "ERROR AL ACTUALIZAR TALOT");
		if($this->Talot->save($talot)) $rpt = array(true, "TALOT ACTUALIZADO");
		
		echo json_encode(array('result'=>$rpt[0], 'msg'=>$rpt[1], 'date'=>$this->Talot->getDateFormatViewHours($talot['Talot'][$dateIds[$dateId]])));
	}
}
