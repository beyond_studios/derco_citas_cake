<?php
class MarcasController extends AppController {

	public $name = 'Marcas';
	public $helpers = array('Html', 'Form');
	
    public function beforeFilter() {
        parent::beforeFilter();

    }
	
	public function index(){
		
		$elementos = array('Marca.codigo'=>__('codigo', TRUE),
						   'Marca.description'=>__('Marca', TRUE),
						   'Marca.portal'=>__('Pagina Web', TRUE)
						   );
		$this->set('elementos',$elementos);		
		
		if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();		
		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Marca.status'=>'DE') :
								array('Marca.status'=>'AC');
		
		$conditions = $conditions + $conditionsActivos;		
		
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Marca.description' => 'asc'),
								'conditions' => $conditions
								);
		
		$marcas=$this->paginate('Marca');
		$this->set('marcas',$marcas);
	}

	/**
     * Permite ingresar una nueva marca.
     */    
	function add() 
	{
		$this->layout = 'contenido';
		
		if (!empty($this->request->data)) {
			$this->Marca->create();
			if ($this->Marca->save($this->request->data))
                        {
				$this->Session->setFlash(__('marcaGuardado'),'flash_success');
				$this->Session->write('actualizarPadre',true);	
				$this->Session->write($this->redirect(array('action'=>'view',$this->Marca->getInsertID())));
			} 
                        else 
                        {
				$this->Session->setFlash(__('marcaNoGuardado'),'flash_failure');
			}
		}
	}
	
	 /* 
	 *
     * Esta función permite modificar los datos de una marca.
	 Reglas: 
	 * 1. Una marca puede ser desactivado solo si no tiene datos asociados
	 * @param string $id :  id de la marca que se desea modificar
     */	
	 public function edit($id=true) 
        {
		 	$this->layout = 'contenido';
			if (!$id && empty($this->request->data)) {
				$this->Session->setflash(__('marcaNoValido', true),'flash_failure');
				$this->redirect(array('action'=>'index'));
			}
	        
	        //$this->pageTitle = $this->titulo('CAST_MODIFICAR');
	        $estadoActivo = 'AC';
			$estadoDesactivo = 'DE';
	
			
			if (empty($this->request->data['Marca'])) {
					$this->request->data = $this->Marca->read(null, $id);
			} else {
	            $id = $this->request->data['Marca']['id'];
				$nuevoEstado = $this->request->data['Marca']['status'];	
			if ($this->Marca->save($this->request->data['Marca'])) {			
					$this->Session->setFlash(__('marcaGuardado', true),'flash_success');	
					$this->Session->write('actualizarPadre', true);	
				    $this->Session->write($this->redirect(array('action'=>'view',$this->request->data['Marca']['id'])));				
			} else {
					$this->Session->setFlash(__('marcaNoGuardado', true),'flash_failure');
	        }
		}
	}
	
	/**
     * Mostrar Marca.
	 * @param string $id : id del calendario que se desea mostrar
     */    
	public function view($id=true) 
	{
	
		$this->layout = 'contenido';
		if (!$id) {
			$this->Session->setFlash(__('marcaNoValido',true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
		$this->set('marca', $this->Marca->read(null, $id));
		
	}

	/**
     * Elimina una marca.
     * Reglas: 
     * 1. Una marca es eliminada solo si no tiene datos asociados
	 * @param string $id : id de la marca que se desea eliminar
     */
	public function delete($id=null) {
		$estadoEliminado = 'EL';
		$estadoActivo = 'AC';
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
			//Si existen registros asociados no se peuden eliminar---Miguel Chiuyari
			$existSecprojects = $this->Marca->MarcasSecproject->find('count', array('conditions' => array('MarcasSecproject.status' => 'AC', 'MarcasSecproject.marca_id'=>$id)));
			$existMarcas = $this->Marca->AgegruposMarca->find('count', array('conditions' => array('AgegruposMarca.status' => 'AC', 'AgegruposMarca.marca_id'=>$id)));
			if($existSecprojects||$existMarcas)
				{
				$this->Session->setFlash(__('GENERALES_REGISTRO_ASOCIADO',true),'flash_failure');				
				}
				else{						
					$this->request->data['Marca']['id'] = $id;
					$this->request->data['Marca']['status'] = $estadoEliminado;
					if ($this->Marca->save($this->request->data['Marca'])) {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ELIMINADO', true),'flash_success');	
					} else {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ACTIVADO', true),'flash_failure');
					}
				}
			}
			$this->redirect(array('action'=>'index'));
	}
	
	public function getMarcasClientJson($clientId = 0){
		configure::write('debug',0);
		$this->layout = 'ajax';
		$marcasClient = $this->Marca->getMarcasClient($clientId);
		
		$responce->susses = empty($marcasClient)? false:true;
		$responce->errors = array('msg'=>__('NO_EXISTEN_MARCAS'));
		
		foreach($marcasClient as $key => $value){
			$responce->data[$value['AgeclientesVehiculo']['id']] = array(
				'marca_id'=>$value['Marca']['id'],
				'marca'=>$value['AgeclientesVehiculo']['marca'],
				'placa'=>$value['AgeclientesVehiculo']['placa'],
				'modelo'=>$value['AgeclientesVehiculo']['modelo'],
				'codigo'=>$value['AgeclientesVehiculo']['codigo_vehiculo']
			);
		}
		
		echo json_encode($responce);
		$this->autoRender = false;
	}
	
}	
?>