<?php
class AgeclientesVehiculosController extends AppController {
	var $name = 'AgeclientesVehiculos';
	
	function getVehicles(){
		configure::write('debug',0);
		app::import('Vendor','json');
		$json = new Services_JSON;
		
		$this->loadModel('Marca');
		$this->loadModel('Cliente');
		$this->layout = 'ajax';
		
		$filters =  null;
		if($this->params['url']['_search'] == 'true'){
			$filters = $json->decode($this->params['url']['filters']);
			$filters = $this->AgeclientesVehiculo->TransfObjectToArray($filters);
		}
		
		$cndGrid = $this->AgeclientesVehiculo->conditionsBuscadorGrid($this->params['url'], $filters, $this->_getDtLg());
		
		$this->paginate = array(
			'page'=>$this->params['url']['page'],
			'limit'=>$this->params['url']['rows'],
			'order'=>array($this->params['url']['sidx']=>$this->params['url']['sord']),
			'conditions'=>array('1'=>$cndGrid)
		);
		$vehicles = $this->paginate();
		
		foreach($vehicles as $key => $val){
			$marca = $this->Marca->find('first',array(
				'conditions'=>array('OR'=>array('Marca.description'=>trim($val['AgeclientesVehiculo']['marca']),
												'Marca.description_sap'=>trim($val['AgeclientesVehiculo']['marca'])
									)
				),
				'recursive'=>-1
			));
			$vehicles[$key]['Marca'] = empty($marca)?array('id'=>"0"):$marca['Marca'];
			$vehicles[$key]['Cliente']['str_cliente_tipo'] = $this->Cliente->getStrTipoCliente($vehicles[$key]['Cliente']['cliente_tipo']);
		}
		//$this->log($this->Marca->lastQuery(),'debug');
		$this->set('vehicles',$vehicles);
	}
	
	/**
	 * AUTHOR: VENTURA RUEDA, JOSE ANTONIO
	 * @return 
	 */
	public function getDtJson(){
		configure::write('debug',0);
		$clientVehicleId = empty($this->request->query['id'])?'0':$this->request->query['id'];
		$this->layout = false;
		
		$list = "select
			    AgeclienteVehiculo.id, Marca.id, AgeclienteVehiculo.modelo_id 
			from 
			ageclientes_vehiculos AgeclienteVehiculo
			JOIN marcas Marca ON Marca.description = AgeclienteVehiculo.marca
			WHERE AgeclienteVehiculo.id = $clientVehicleId"; 

		$list = $this->AgeclientesVehiculo->query($list);
		if (!isset($response)){
			$response = new stdClass();
		} 
		
		$response->_empty = empty($list);
		$response->cli_vehi_id = $list['0']['AgeclienteVehiculo']['id'];
		$response->marca_id = $list['0']['Marca']['id'];
		$response->modelo_id = empty($list['0']['AgeclienteVehiculo']['modelo_id'])?'0':$list['0']['AgeclienteVehiculo']['modelo_id'];
		
		echo json_encode($response);
		$this->autoRender = false;
		
	}
}
