<?php
class ModelosController extends AppController {

	public $name = 'Modelos';
	public $helpers = array('Html', 'Form');
	
    public function beforeFilter() {
        parent::beforeFilter();
    }
	
	public function index(){
		$elementos = array('Modelo.codigo'=>__('codigo', TRUE),
						   'Modelo.description'=>__('Modelo', TRUE),
						   'Modelo.portal'=>__('Pagina Web', TRUE)
						   );
		$this->set('elementos',$elementos);		
		
		if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();		
		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Modelo.status'=>'DE') :
								array('Modelo.status'=>'AC');
		
		$conditions = $conditions + $conditionsActivos;		
		
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Modelo.description' => 'asc'),
								'conditions' => $conditions
								);
		
		$modelos=$this->paginate('Modelo');
		$this->set('modelos',$modelos);
	}

	/**
     * Permite ingresar una nueva modelo.
     */    
	function add() 
	{
		$this->layout = 'contenido';
		
		if (!empty($this->request->data)) {
			$this->Modelo->create();
			if ($this->Modelo->save($this->request->data))
                        {
				$this->Session->setFlash(__('modeloGuardado'),'flash_success');
				$this->Session->write('actualizarPadre',true);	
				$this->Session->write($this->redirect(array('action'=>'view',$this->Modelo->getInsertID())));
			} 
                        else 
                        {
				$this->Session->setFlash(__('modeloNoGuardado'),'flash_failure');
			}
		}
	}
	
	 /* 
	 *
     * Esta función permite modificar los datos de una modelo.
	 Reglas: 
	 * 1. Una modelo puede ser desactivado solo si no tiene datos asociados
	 * @param string $id :  id de la modelo que se desea modificar
     */	
	 public function edit($id=true) 
        {
		 	$this->layout = 'contenido';
			if (!$id && empty($this->request->data)) {
				$this->Session->setflash(__('modeloNoValido', true),'flash_failure');
				$this->redirect(array('action'=>'index'));
			}
	        
	        //$this->pageTitle = $this->titulo('CAST_MODIFICAR');
	        $estadoActivo = 'AC';
			$estadoDesactivo = 'DE';
	
			
			if (empty($this->request->data['Modelo'])) {
					$this->request->data = $this->Modelo->read(null, $id);
			} else {
	            $id = $this->request->data['Modelo']['id'];
				$nuevoEstado = $this->request->data['Modelo']['status'];	
			if ($this->Modelo->save($this->request->data['Modelo'])) {			
					$this->Session->setFlash(__('modeloGuardado', true),'flash_success');	
					$this->Session->write('actualizarPadre', true);	
				    $this->Session->write($this->redirect(array('action'=>'view',$this->request->data['Modelo']['id'])));				
			} else {
					$this->Session->setFlash(__('modeloNoGuardado', true),'flash_failure');
	        }
		}
	}
	
	/**
     * Mostrar Modelo.
	 * @param string $id : id del calendario que se desea mostrar
     */    
	public function view($id=true) 
	{
	
		$this->layout = 'contenido';
		if (!$id) {
			$this->Session->setFlash(__('modeloNoValido',true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
		$this->set('modelo', $this->Modelo->read(null, $id));
		
	}

	/**
     * Elimina una modelo.
     * Reglas: 
     * 1. Una modelo es eliminada solo si no tiene datos asociados
	 * @param string $id : id de la modelo que se desea eliminar
     */
	public function delete($id=null) {
		$estadoEliminado = 'EL';
		$estadoActivo = 'AC';
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
			//Si existen registros asociados no se peuden eliminar---Miguel Chiuyari
			$existSecprojects = $this->Modelo->ModelosSecproject->find('count', array('conditions' => array('ModelosSecproject.status' => 'AC', 'ModelosSecproject.modelo_id'=>$id)));
			$existModelos = $this->Modelo->AgegruposModelo->find('count', array('conditions' => array('AgegruposModelo.status' => 'AC', 'AgegruposModelo.modelo_id'=>$id)));
			if($existSecprojects||$existModelos)
				{
				$this->Session->setFlash(__('GENERALES_REGISTRO_ASOCIADO',true),'flash_failure');				
				}
				else{						
					$this->request->data['Modelo']['id'] = $id;
					$this->request->data['Modelo']['status'] = $estadoEliminado;
					if ($this->Modelo->save($this->request->data['Modelo'])) {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ELIMINADO', true),'flash_success');	
					} else {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ACTIVADO', true),'flash_failure');
					}
				}
			}
			$this->redirect(array('action'=>'index'));
	}
	
	public function getJsonList(){
		configure::write('debug',0);
		$marcaId = empty($this->request->query['id'])?'0':$this->request->query['id'];
		$this->layout = false;

		$list = $this->Modelo->find('list', array(
			'conditions'=>array('marca_id'=>$marcaId, 'status'=>'AC'),
			'field'=>array('id', 'description')
		)); 

		if (!isset($response)){
			$response = new stdClass();
		} 
		
		$response->_empty = empty($list);
		$i = 0;
		foreach($list as $key=>$value){
			$response->rows[++$i] = array('key'=>$key, 'value'=>$value);
		}
		echo json_encode($response);
		$this->autoRender = false;
	}	
	
}	