<?php
class AgetiposerviciosController extends AppController 
{

	public $name = 'Agetiposervicios';
	public $helpers = array('Html', 'Form');

    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
    }
	
    function index() 
	{
		$this->Agetiposervicio->recursive = 0;
		$elementos = array('Agetiposervicio.description'=>__('AGE_TIPOSERVICIO_DESCRIPCION',true));
		$this->set('elementos',$elementos);
			
	    if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];	
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];		
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();		
						
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Agetiposervicio.status'=>'DE') :
								array('Agetiposervicio.status'=>'AC');
		
		$conditions = $conditions + $conditionsActivos;
		
		$fields=array('Agetiposervicio.id','Agetiposervicio.description','Agetiposervicio.status','Agetiposervicio.mostrar_mantenimiento','Agetiposervicio.codigo_sap','Agemotivoservicio.description');	
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Agetiposervicio.description' => 'asc'),
								'conditions' => $conditions,
								'fields' => $fields
								);

		$agetiposervicios=$this->paginate('Agetiposervicio');
		$this->set('agetiposervicios', $agetiposervicios);

    }
	
	public function add() 
	{
		$this->layout ='contenido';
		if (!empty($this->request->data)) 
		{
			$this->Agetiposervicio->create();
			if ($this->Agetiposervicio->save($this->request->data))
			 {
				$this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO', true),'flash_success');
				$this->Session->write('actualizarPadre', true);	
				$this->Session->write($this->redirect(array('action'=>'view',$this->Agetiposervicio->getInsertID())));
			} 
			else {
				$this->Session->setFlash(__('GENERAL_ERROR_GRABACION', true),'flash_failure');
			}
		}
		$agemotivoservicios = $this->Agetiposervicio->Agemotivoservicio->find('list',array('conditions'=>array('Agemotivoservicio.status'=>'AC')));
		
		$this->set('agemotivoservicios', $agemotivoservicios);
    }
	
	
	public function edit($id=true) 
        {
		 	$this->layout = 'contenido';
			if (!$id && empty($this->request->data)) {
				$this->Session->setflash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
				$this->redirect(array('action'=>'index'));
			}

			if (empty($this->request->data['Agetiposervicio'])) {
					$this->request->data = $this->Agetiposervicio->read(null, $id);
			} else {
	            $id = $this->request->data['Agetiposervicio']['id'];

			if ($this->Agetiposervicio->save($this->request->data['Agetiposervicio'])) {			
					$this->Session->setFlash(__('GENERAL_REGISTRO_MODIFICADO', true),'flash_success');	
					$this->Session->write('actualizarPadre', true);
					$this->Session->write($this->redirect(array('action'=>'view',$this->request->data['Agetiposervicio']['id'])));		
			} else {
					$this->Session->setFlash(__('GENERAL_ERROR_GRABACION', true),'flash_failure');
	        }
		}
		$agemotivoservicios = $this->Agetiposervicio->Agemotivoservicio->find('list',array('conditions'=>array('Agemotivoservicio.status'=>'AC')));
		
		$this->set('agemotivoservicios', $agemotivoservicios);
	}
	
	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}		
		else{
			$exist = 0;
			if($exist)
				{
				$this->Session->setFlash(__('GENERALES_REGISTRO_ASOCIADO_NO_PODER_DECTIVAR',true),'flash_failure');				
				}
			else{	$this->request->data['Agetiposervicio']['id']=$id;
					$this->request->data['Agetiposervicio']['status']='EL';
					if ($this->Agetiposervicio->save($this->request->data)) 
						{
						$this->Session->setFlash(__('GENERAL_REGISTRO_ELIMINADO', true),'flash_success');	
						}
					else 
						{
						$this->Session->setFlash(__('GENERAL_REGISTRO_ACTIVADO',true),'flash_failure');	
						}		
				}
		}
		$this->redirect(array('action'=>'index'));
	}

	function view($id = null) {
		$this->layout='contenido';
		if (!$id) {
			$this->Session->setFlash(__('TipoTareaNoValida', true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
		else {$this->set('agetiposervicio', $this->Agetiposervicio->read(null, $id));}
		}
		
		
	/**DEVUELVE LA LISTA DE TIPOS DE SERVICIO
	 * AUTOR: VENTURA RUEDA JOSE ANTONIO
	 * FEHCA: 2013-03-20
	 * @return 
	 */
	function getTiposerviciosJson($motivoId){
		configure::write('debug',0);
		$this->layout = 'ajax';
		$tipos = $this->Agetiposervicio->find('all', array(
			'conditions'=>array('1'=>empty($motivoId)?"1=1":"Agetiposervicio.agemotivoservicio_id = $motivoId", 'Agetiposervicio.status'=>'AC'),
			'recursive'=>-1
		));
		
		$responce->susses = empty($tipos)? false:true;
		$responce->errors = array('msg'=>__('NO_EXISTEN_TIPOS'));
		
		foreach($tipos as $key => $value){
			$responce->data[$value['Agetiposervicio']['id']] = array(
				'name'=>$value['Agetiposervicio']['description'],
				'maintenance'=>$value['Agetiposervicio']['mostrar_mantenimiento']
			);
		}
		
		echo json_encode($responce);
		$this->autoRender = false;
	}
}
?>
