<?php
class AppcasttimesController extends AppController {

	public  $name = 'Appcasttimes';
	public $helpers = array('Html', 'Form');
	
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
    }

	function index() 
	{
		$estadoEliminado = 'EL';
		$elementos = array('Appcast.description'=>__('Descripcion',true));
		$this->set('elementos',$elementos);	
		/*if(!empty($this->request->url['named']['valor']) || !empty($this->request->url['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->request->url['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->request->url['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->request->url['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].'  LIKE'=>' %'.trim($this->request->data['Buscar']['valor']).'%'):
						array();*/	

        if(!empty($this->request->url['named']['valor']) || !empty($this->request->url['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->request->url['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->request->url['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->request->url['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();

		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Appcast.status'=>'DE') :
								array('Appcast.status'=>'AC');
		$conditions = $conditions + $conditionsActivos;
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Appcast.description' => 'asc'),
								'conditions' => $conditions
								);

		$casts=$this->paginate('Appcast');
		$this->set('casts', $casts);
	}

	function modificarCronograma($cast_id) {
        //$this->pageTitle = $this->titulo('CAST_TIME_MODIFICAR_CRONOGRAMA');
		$this->layout = 'contenido';
        $estadoActivo = 'AC';
		
		$cast = $this->Appcasttime->Appcast->read(null,$cast_id); 
		$castDescription = $cast['Appcast']['description'];
		$this->set('castDescription',$castDescription);	

		if (empty($this->request->data['Appcasttime'])) {
			$castTimes = $this->Appcasttime->find('all',array('conditions'=>array('Appcasttime.appcast_id'=>$cast_id,'Appcasttime.status'=>$estadoActivo)));
			//pr($castTimes);
			$this->set('castTimes',$castTimes);
		}
		$this->set('cast_id',$cast_id);
	}

	function add($cast_id = null) {
		$this->layout = 'contenido';
		$estadoEliminado = 'EL';
		$estadoActivo = 'AC';

		if(empty($this->request->data['Appcasttime'])) {
			$this->request->data['Appcasttime']['appcast_id'] = $cast_id;
		} else {
			$cast_id = $this->request->data['Appcasttime']['appcast_id'];
			$castTimes = $this->Appcasttime->find('all',array('conditions'=>array('Appcasttime.appcast_id'=>$cast_id,
														'Appcasttime.status'=>$estadoActivo)));
			
			if(empty($this->request->data['Appcasttime']['initTimeMinute'])) { 
				$this->request->data['Appcasttime']['initTimeMinute']  = 0;
			}			
			//si no se ingresaron minutos de termino
			if(empty($this->request->data['Appcasttime']['endTimeMinute'])) { 
				$this->request->data['Appcasttime']['endTimeMinute']  = 0;
			}
			//validamos que la hora de termino sea mayor a la hora de inicio
			$initTimeNuevo = date('H:i',strtotime($this->request->data['Appcasttime']['inittime'].":00"));
			$endTimeNuevo = date('H:i',strtotime($this->request->data['Appcasttime']['endtime'].":00"));
		
			if($endTimeNuevo <= $initTimeNuevo) {
				$this->Session->setFlash(__('finMenorInicio', true),'flash_failure');
				return;
			}
			//validamos que los tiempo ingresados no se encuentre dentro de otros intervalos ya existentes
			foreach($castTimes as $castTime) {
				
				$initTimeExistente = $castTime['Appcasttime']['inittime'];
				$endTimeExistente = $castTime['Appcasttime']['endtime'];

				// el nuevo intervalo no debe estar dentro de ningún intervalo
				if(($initTimeNuevo >= $initTimeExistente) && ($endTimeNuevo <= $endTimeExistente)) {
					$this->Session->setFlash(__('nuevoIntervaloNoDebeEstarDentroNingunIntervalo', true),'flash_failure');
					return;
				} 
				
				// ningún intervalo debe estar dentro del nuevo intervalo
				if(($initTimeNuevo <= $initTimeExistente) && ($endTimeNuevo >= $endTimeExistente)) {
					$this->Session->setFlash(__('ningunIntervaloDebeEstarDentroNuevoIntervalo', true),'flash_failure');
					return;
				} 
				
				//la hora de inicio del nuevo intervalo no se debe encontrar en ningún otro intervalo
				if(($initTimeNuevo >= $initTimeExistente) && ($initTimeNuevo <= $endTimeExistente)) {
					$this->Session->setFlash(__('horaInicioNoDebeEstarNingunIntervalo', true),'flash_failure');
					return;
				}
				
				//la hora de termino del nuevo intervalo no se debe encontrar en ningún otro intervalo
				if(($endTimeNuevo >= $initTimeExistente) && ($endTimeNuevo <= $endTimeExistente)) {
					$this->Session->setFlash(__('horaFinNoDebeEstarNingunIntervalo', true),'flash_failure');
					return;
				}
				
			}
			
			$this->request->data['Appcasttime']['inittime'] = $initTimeNuevo;
			$this->request->data['Appcasttime']['endtime'] = $endTimeNuevo;
			//pr($this->data);/*
			if ($this->Appcasttime->save($this->request->data['Appcasttime'])) {
				$this->Session->setFlash(__('plantillaGuardado', true),'flash_success');
				$this->Session->write('actualizarPadre', true);	
			} else {
				$this->Session->setFlash(__('cronogramaNopuedeserAgregado', true),'flash_failure');
			}

		}
	}
	
	function edit($id=null) {
		$this->layout = 'contenido';
		$estadoActivo='AC';
		if(empty($this->request->data['Appcasttime'])) {
			$this->request->data = $this->Appcasttime->read(null, $id);
			$this->request->data['Appcasttime']['inittime']= date('H:i',strtotime($this->request->data['Appcasttime']['inittime']));
			$this->request->data['Appcasttime']['endtime']= date('H:i',strtotime($this->request->data['Appcasttime']['endtime']));
			//pr($this->data);
		} else  {
			//-------------
			//pr($this->data);
			$casttime_id = $this->request->data['Appcasttime']['id'];
			$cast_id = $this->request->data['Appcasttime']['appcast_id'];
			$castTimes = $this->Appcasttime->find('all',array('conditions'=>array('Appcasttime.appcast_id'=>$cast_id,
																				'Appcasttime.id !='=>$casttime_id,
																				'Appcasttime.status'=>$estadoActivo)));
			
			if(empty($this->request->data['Appcasttime']['initTimeMinute'])) { 
				$this->request->data['Appcasttime']['initTimeMinute']  = 0;
			}			
			//si no se ingresaron minutos de termino
			if(empty($this->request->data['Appcasttime']['endTimeMinute'])) { 
				$this->request->data['Appcasttime']['endTimeMinute']  = 0;
			}
			//validamos que la hora de termino sea mayor a la hora de inicio
			$initTimeNuevo = date('H:i',strtotime($this->request->data['Appcasttime']['inittime'].":00"));
			$endTimeNuevo = date('H:i',strtotime($this->request->data['Appcasttime']['endtime'].":00"));
		
			if($endTimeNuevo <= $initTimeNuevo) {
				$this->Session->setFlash(__('finMenorInicio', true),'flash_failure');
				return;
			}
			//validamos que los tiempo ingresados no se encuentre dentro de otros intervalos ya existentes
			foreach($castTimes as $castTime) {
				
				$initTimeExistente = $castTime['Appcasttime']['inittime'];
				$endTimeExistente = $castTime['Appcasttime']['endtime'];

				// el nuevo intervalo no debe estar dentro de ningún intervalo
				if(($initTimeNuevo >= $initTimeExistente) && ($endTimeNuevo <= $endTimeExistente)) {
					$this->Session->setFlash(__('nuevoIntervaloNoDebeEstarDentroNingunIntervalo', true),'flash_failure');
					return;
				} 
				
				// ningún intervalo debe estar dentro del nuevo intervalo
				if(($initTimeNuevo <= $initTimeExistente) && ($endTimeNuevo >= $endTimeExistente)) {
					$this->Session->setFlash(__('ningunIntervaloDebeEstarDentroNuevoIntervalo', true),'flash_failure');
					return;
				} 
				
				//la hora de inicio del nuevo intervalo no se debe encontrar en ningún otro intervalo
				if(($initTimeNuevo >= $initTimeExistente) && ($initTimeNuevo <= $endTimeExistente)) {
					$this->Session->setFlash(__('horaInicioNoDebeEstarNingunIntervalo', true),'flash_failure');
					return;
				}
				
				//la hora de termino del nuevo intervalo no se debe encontrar en ningún otro intervalo
				if(($endTimeNuevo >= $initTimeExistente) && ($endTimeNuevo <= $endTimeExistente)) {
					$this->Session->setFlash(__('horaFinNoDebeEstarNingunIntervalo', true),'flash_failure');
					return;
				}
				
			}
			
			$this->request->data['Appcasttime']['inittime'] = $initTimeNuevo;
			$this->request->data['Appcasttime']['endtime'] = $endTimeNuevo;
			$this->request->data['Appcasttime']['id']=$casttime_id;			
			if ($this->Appcasttime->save($this->request->data['Appcasttime'])) {
				$this->Session->setFlash(__('plantillaGuardado', true),'flash_success');
				$this->Session->write('actualizarPadre', true);	
			} else {
				$this->Session->setFlash(__('cronogramaNopuedeserAgregado', true),'flash_failure');
			}			
			//-------------


		}
	}

	function delete($id) {
		$estadoEliminado = 'EL';
		$castTime = $this->Appcasttime->read('appcast_id',$id);
		$cast_id = $castTime['Appcasttime']['appcast_id'];
		$this->request->data['Appcasttime']['id'] = $id;
		$this->request->data['Appcasttime']['status'] = $estadoEliminado;
		if ($this->Appcasttime->save($this->request->data['Appcasttime'])) {
			$this->Session->setFlash(__('cronogramaDesactivado', true),'flash_success');	
		} else {
			$this->Session->setFlash(__('cronogramaNoDesactivado', true),'flash_failure');
		}
		$this->redirect(array('controller'=>'appcasttimes','action'=>'modificarCronograma',$cast_id));
	}

	function view($id) {
		$this->layout='contenido';
		$castTime = $this->Appcasttime->read(null, $id);
		$this->set('castTime',$castTime);
	}
}
?>