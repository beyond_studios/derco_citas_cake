<?php
class AgedetallecitasController extends AppController
{
	public $name = 'Agedetallecitas';	
	public $helpers = array('Html', 'Form', 'Js'); //,'Excel','Numerosaletras', 'Xmlexcel');
	
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
		$this->Agedetallecita->status = array(
			'AC'=>__('Programado', true),
			//'DE'=>__('Desactivo'),
			'RE'=>__('Reprogramado'),
			'EL'=>__('Eliminado'),
			//'PR'=>__('Programado')
		);
		
		$this->Agedetallecita->statusSap = array(
			''=>__('enviado', true),
			'1'=>__('enviado', true),
			'0'=>__('noEnviado', true)
		);
		
		$this->Agedetallecita->tiposervicio = array(
			0=>__('Mantenimiento Menor', true),
			1=>__('Mantenimiento Mayor'),
		);
    }
	
	public function test(){
		var_dump($this->sendMailClient());
//		var_dump($this->sendMailClientUpdate());
		die;
	}
	
	public function index(){
		//BORRAMOS LAS VARIABLES DE SESSION DEL BUSCADOR PRIMER INGRESO
		@$this->setInitSessionConditions();
		//FORMAMOS LA DATA SI SE ESTA ENVIANDO LOS DATOS POR PAGINADOR
		if($this->request->is('get')){
			//RECUPERAMOS LAS CONDICIONES DE BUSQUEDA ALMACENADOS EN SESSION
			$bscCnd = $this->getSessionConditions();
			$fechaActual = $this->Agedetallecita->getDateFormatView($this->Agedetallecita->fechaHoraActual());
			$this->request->data['bsc']['crt'] = empty($bscCnd['bsc']['crt'])?'':$bscCnd['bsc']['crt'];
			$this->request->data['bsc']['vlr'] = (trim(isset($bscCnd['bsc']['vlr'])?$bscCnd['bsc']['vlr']:'') == '')?'':$bscCnd['bsc']['vlr'];
			$this->request->data['bsc']['std'] = empty($bscCnd['bsc']['std'])?'':$bscCnd['bsc']['std'];
			$this->request->data['bsc']['f_campo'] = empty($bscCnd['bsc']['f_campo'])?'Agedetallecita.fechadecita':$bscCnd['bsc']['f_campo'];
			$this->request->data['bsc']['f_ini'] = empty($bscCnd['bsc']['f_ini'])?(isset($bscCnd['bsc']['f_ini'])?'':$fechaActual):$bscCnd['bsc']['f_ini'];
			$this->request->data['bsc']['f_fin'] = empty($bscCnd['bsc']['f_fin'])?(isset($bscCnd['bsc']['f_fin'])?'':$fechaActual):$bscCnd['bsc']['f_fin'];
		}
		
		// INICIALIZACION POR DEFECTO
   		if(!isset($this->request->data['bsc']['f_campo'])) $this->request->data['bsc']['f_campo'] = 'Agedetallecita.fechadecita';
		
		//GUARDAMOS LAS CONDICIONES EN SESSION
		@$this->setSessionConditions($this->request->data);
		
		$f_campo = array('Agedetallecita.fechadecita'=>__('Cita'), 'Agedetallecita.fechaRegistro'=>__('Creacion'));
		
		$cnd = $this->Agedetallecita->getConditionsBuscador($this->request->data, $this->_getDtLg());
		
		// DATOS PARA LA BUSQUEDA
		$this->set('std', $this->Agedetallecita->status);
		$this->set('crt', array('Cliente.nombres'=>__('Cliente'), 'Agedetallecita.placa'=>__('Placa')));
		$this->set('f_campo',$f_campo);
		
		$this->paginate = array('limit' => 10,
			'page' => 1,
			'order' => array ('Agedetallecita.fechaRegistro' => 'ASC'),
			'conditions' => $cnd
		);
		
		$agedetallecitas=$this->paginate('Agedetallecita');
		
		// se obtiene el caledario  de los registros obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->recursive = -1;	
			$agecitacalendarios = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->find('first', array(
				'conditions' => array('Agecitacalendario.status' =>'AC','Agecitacalendario.id' => $row['Agecitacalendariodia']['agecitacalendario_id'])
			));
			$agedetallecitas[$key]['Agecitacalendario'] = $agecitacalendarios['Agecitacalendario'];
		}
		
		// se obtiene el grupo de los registros obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agegrupo->recursive = -1;	
			$agegrupos = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agegrupo->find('first', array(
				'conditions' => array('Agegrupo.status' =>'AC','Agegrupo.id' => $row['Agecitacalendario']['agegrupo_id'])
			));
			$agedetallecitas[$key]['Agegrupo'] = $agegrupos['Agegrupo'];
		}
		
		// se obtiene lña sucursal  de los Sucursal obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Secproject->recursive = -1;	
			$secprojects = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Secproject->find('first', array(
				'conditions' => array('Secproject.status' =>'AC','Secproject.id' => $row['Agecitacalendario']['secproject_id'])
			));
			$agedetallecitas[$key]['Secproject'] = $secprojects['Secproject'];
		}
		
		// se obtiene el servicio de los Sucursal obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agemotivoservicio->recursive = -1;	
			$agemotivoservicios = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agemotivoservicio->find('first', array(
				'conditions' => array('Agemotivoservicio.status' =>'AC','Agemotivoservicio.id' => $row['Agecitacalendario']['agemotivoservicio_id'])
			));
			$agedetallecitas[$key]['Agemotivoservicio'] = $agemotivoservicios['Agemotivoservicio'];
		}
		
		$this->set('agedetallecitas',$agedetallecitas);				
	}	

	//elimina/reprograma citas desde la web...
	function delete($detallecita_id = 0){
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		
		$this->set('detallecita_id',$detallecita_id);
		if (!$detallecita_id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}
		
		$this->loadModel('Agecitacalendariodia');
		
		if($this->request->is('post')){
			$dt_original = $this->request->data;
			$this->set('dt_original',$dt_original);
			
			$this->Agedetallecita->begin();
			$rpt = $this->Agedetallecita->setEliminar($this->request->data, $this->_getDtLg());
			
			if($rpt[0]){
				$this->Agedetallecita->commit();
				$this->Session->write('actualizarPadre', true);
				$this->redirect(array('action'=>'mostrarCita', $detallecita_id));
			}else{
				$this->Agedetallecita->rollback();
			}
			
			$this->Session->setFlash($rpt[1]);
		}
		$this->mostrarCitaTaller($detallecita_id);
	}
	
	/**
	 * 
	 * @param object $detalle_cita [optional]
	 * @return: elimina una cita desde la web del cliente 
	 */
	public function deleteClient($detallecita_id = 0){
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$this->set('bandejaReprogramar',true);
		// validando si existe algun cliente logueado
		$clientSession = $this->requestAction('clientes/verifySessionClient');
		
		$this->set('detallecita_id',$detallecita_id);
		if (!$detallecita_id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}
		
		$this->loadModel('Agecitacalendariodia');
		
		if($this->request->is('post')){
			$dt_original = $this->request->data;
			$this->set('dt_original',$dt_original);
			
			$this->Agedetallecita->begin();
			$rpt = $this->Agedetallecita->setEliminar($this->request->data, $this->_getDtLg());
			
			if($rpt[0]){
				$this->Agedetallecita->commit();
				$this->Session->write('actualizarPadre', true);
				$this->redirect(array('controller'=>'Clientes','action'=>'bandejaReprogramar'));
			}else{
				$this->Agedetallecita->rollback();
			}
			
			$this->Session->setFlash($rpt[1]);
		}
		$this->mostrarCitaTaller($detallecita_id,null,$clientSession);
	}
	
	/**@author: ronald Tineo Santamaria
	 * 
	 * @param object $detallecitaId [optional]
	 * @return AGREGA LA CITA MEDIANTE AJAX
	 */
	function agregarCitaAjax($detallecitaId=null){
		$this->layout = 'ajax';
		$this->autoRender = false;

		try{

			$responseAjax = array('Success'=>false,'Mensaje'=>__('MENSAJE_NUMERO_TELEFONICO'));
			$dtLg = $this->_getDtLg();
			
			Configure::write('debug',0);
			if($this->request->data){
				//validamos el usuario logueado sino se envia a login
				if($this->request->data['Agedetallecita']['citaweb']!='1'){
					$this->log('es usuario de call center ','debug');
					if(empty($dtLg['role'])){
						$this->log('pero se ha perdido la session, redirigiendo a login','debug');
						$responseAjax = array('Success'=>false,'Mensaje'=>__('USUARIO_NO_LOGUEADO'),'redirectLogin'=>true);
						echo json_encode($responseAjax);
						
						return;
					}
				}else{
					$this->log('es usuario clienteWeb','debug');
				}

				$this->Agedetallecita->begin();
				$rpt = $this->Agedetallecita->agregarCita($this->request->data, $this->_getDtLg(), $dtAud = null);
				

				// si es reprogramacion actualizo los datos de las citas por dia
				if(!empty($detallecitaId) && $rpt[0]){
					$detallecitaNewId = $rpt['id'];
					$rpt = $this->Agedetallecita->setReprogramar($detallecitaId, $this->request->data, $this->_getDtLg());
					$rpt['id'] = $detallecitaNewId;
				} 
				
				$responseAjax = array('Success'=>false,'Mensaje'=>'LA CITA HA SIDO GENERADA SIN OT-SAP');
				if($rpt[0]){
					$data = $this->Agedetallecita->findById($rpt['id']);
					//rtineo: se ha agregado la llamada al nuevo web service
					//$reponse = $this->registrarCitaSap($data);
					/**rtineo cambio04062017: se comenta la llamada al web service de agendamiento 04/06/2017**/
					//$reponse = $this->registrarCitaSapNew($data);
					$reponse['Tramaxml']=null;
					$reponse['Mensaje']="Envio a SAP deshabilitado 04-06-2017";
					$ot = null;
					$estadoSap = 0;
					/**rtineo cambio04062017**/
					/*if(!empty($reponse['Ot']) && isset($reponse['Ot'])){
						$ot = $reponse['Ot'];
						$estadoSap = 1;
					}*/
					
					/** validate data send of SAP **/
					if(!empty($estadoSap) || true){
						if($this->Agedetallecita->updateOTSAPTRAMAXML($rpt['id'],$ot,$reponse['Tramaxml'],$reponse['Mensaje'],$estadoSap)){
							$id = $rpt['id'];
							unset($rpt);
							$this->Agedetallecita->commit();
							// enviamos el mensaje
							if($this->sendMailClientMakeCita($id)){
								if(empty($estadoSap) && !empty($dtLg['role'])){
									/**rtineo cambio04062017**/
									//$responseAjax = array('Success'=>true,'Mensaje'=>'La cita ha sido registrada, pero no tiene numero de OT en SAP','id'=>$id);
									$responseAjax = array('Success'=>true,'Mensaje'=>'La cita ha sido registrada correctamente','id'=>$id);
								}elseif(!empty($estadoSap) && !empty($dtLg['role'])){
									/**rtineo cambio04062017**/
									//$responseAjax = array('Success'=>true,'Mensaje'=>'La cita ha sido registrada correctamente su numero de referencia es:'.$reponse['Ot'],'id'=>$id);
									$responseAjax = array('Success'=>true,'Mensaje'=>'La cita ha sido registrada correctamente','id'=>$id);
								}else{
									/**rtineo cambio04062017**/
									//$responseAjax = array('Success'=>true,'Mensaje'=>'La cita ha sido registrada correctamente su numero de referencia es:'.$reponse['Ot'],'id'=>$id);
									$responseAjax = array('Success'=>true,'Mensaje'=>'La cita ha sido registrada correctamente','id'=>$id);
								}
							}else{
								$responseAjax = array('Success'=>false,'Mensaje'=>__('ERROR AL ENVIAR EL MENSAJE A SU EMAIL'));
							}
							
						}else{
							$this->Agedetallecita->rollback();
							$responseAjax = array('Success'=>false,'Mensaje'=>__('MENSAJE_NUMERO_TELEFONICO'));
						}
					}else{
						$this->Agedetallecita->rollback();
						$responseAjax = array('Success'=>false,'Mensaje'=>"ERROR EN EL SERVIDOR DE SAP, LA OT NO PUEDE SER GENERADA");
					}
				}else{
					$this->Agedetallecita->rollback();
					$responseAjax = array('Success'=>false,'Mensaje'=>$rpt[1]);
				}
			}
		}catch(Exception $e){
			print_r($e);
		}
		
		echo json_encode($responseAjax);
	}
	
	/** ############################################################################
	 * ###################### ACCIONES MODIFICADAS #################################
	 * ########## AUTOR: VENTURA RUEDA, JOSE ANTONIO ###############################
	 * */
	
	/**GENERA UNA NUEVA CITA DE CLIENTE
	 * AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 */
	function agregarCita() {
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		$this->loadModel('Agecitacalendario');
		$this->loadModel('Cliente');
		$this->loadModel('Agemotivoservicio');
		$this->loadModel('Secproject');
		
		$talleres = $this->Secproject->find('all',array('conditions'=>array('status'=>'AC'), 'recursive'=>-1));
		$this->set('talleres',$talleres);
		$this->set('isPost',$this->request->is('post'));
		
		if($this->request->is('post')){
			$dt = $this->request->data;
			$this->loadModel('Marca');
			$this->loadModel('Secproject');
			$this->loadModel('Agemotivoservicio');
			$this->loadModel('Agetipomantenimiento');
			$this->loadModel('Agetiposervicio');
			
			// recuperamos las marcas
			$marcas = $this->Marca->getMarcasClient($dt['Agedetallecita']['cliente_id']);	
				
			$secprojectsArray = $this->Secproject->getSecprojecsMarca(0, $dt['Agedetallecita']['marca']);
			foreach($secprojectsArray as $value)
				$secprojects[$value['Secproject']['id']] = $value['Secproject']['name'];
			
			//recuperamos los motivos de servicio
			$motivoservicios = $this->Agemotivoservicio->getMotivoServicio($dt['Secproject']['id']);

			// recuperamos los tipo de mantenimiento
			$tipomantenimientos = $this->Agetipomantenimiento->find('list',array(
				'conditions'=>array('agemotivoservicio_id'=>$dt['Agemotivoservicio']['id'], 'status'=>'AC'),
				'recursive'=>-1
			));
			
			$tiposervicios = $this->Agetiposervicio->find('all', array(
				'conditions'=>array("Agetiposervicio.agemotivoservicio_id"=>$dt['Agemotivoservicio']['id'], 'Agetiposervicio.status'=>'AC'),
				'recursive'=>-1
			));
			
			$this->set('marcas',$marcas);
			$this->set('secprojects',$secprojects);
			$this->set('motivoservicios',$motivoservicios);
			$this->set('tipomantenimientos',$tipomantenimientos);
			$this->set('tiposervicios',$tiposervicios);
		}
		
		if($this->request->is('get')){
			$this->set('motivoservicios',array());
			$this->set('tipomantenimientos', array());
			$this->set('secprojects', array());
			$this->set('marcas', array());
			$this->set('tiposervicios',array());
			$this->set('horarios',0);
		}
	}
	
	/**GENERA UNA NUEVA CITA DE CLIENTE
	 * AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 */
	function setCitationClient($clientVehicleId) {
		$this->set('clientVehicleId',$clientVehicleId);
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		//$this->layout = 'modulo_taller'.DS.'default_grid';
		
		$this->loadModel('Agecitacalendario');
		$this->loadModel('Cliente');
		$this->loadModel('Agemotivoservicio');
		$this->loadModel('Secproject');
		$this->loadModel('AgeclientesVehiculo');
		
		$clientSession = $this->requestAction('clientes/verifySessionClient');
		$dt['ClienteLogin'] = $clientSession['Cliente'];
		
		$talleres = $this->Secproject->find('all',array('conditions'=>array('status'=>'AC'), 'recursive'=>-1));
		$this->set('talleres',$talleres);
		$this->set('isPost',$this->request->is('post'));
		$this->set('citaweb',true);
		$this->set('tallerIndex',true);
		if($this->request->is('get')){
			$this->loadModel('Marca');
			$this->loadModel('Secproject');
			
			//RECUPERAMOS LOS DATOS DEL CLIENTE
			$clientVehicle_db = $this->AgeclientesVehiculo->findById($clientVehicleId);
//			debug($clientVehicle_db);
			$dt['Cliente'] = $clientVehicle_db['Cliente'];
			$dt['Cliente']['str_cliente_tipo'] = $this->Cliente->getStrTipoCliente($clientVehicle_db['Cliente']['cliente_tipo']);
			$dt['Cliente']['apellidoPaterno'] = $dt['Cliente']['nombres'];
			
			$dt['Agedetallecita']['cliente_id'] = $clientVehicle_db['AgeclientesVehiculo']['cliente_id'];
			$dt['Agedetallecita']['codigo_unidad'] = $clientVehicle_db['AgeclientesVehiculo']['codigo_vehiculo'];
			$dt['Agedetallecita']['ageclientesVehiculo_id'] = $clientVehicle_db['AgeclientesVehiculo']['id'];
			$dt['Agedetallecita']['placa'] = $clientVehicle_db['AgeclientesVehiculo']['placa'];
			$dt['Agedetallecita']['marca'] = $clientVehicle_db['AgeclientesVehiculo']['marca'];
			$dt['Agedetallecita']['modelo'] = $clientVehicle_db['Modelo']['description'];
			
			// recuperamos las marcas getMarcasClient
			$marcas = $this->Marca->getMarcasClient('0',$clientVehicle_db['AgeclientesVehiculo']['id']);
			
			//recuperamos los motivos de servicio
			$marca = $this->Marca->find('first',array(
				'conditions'=>array('Marca.description'=>trim($clientVehicle_db['AgeclientesVehiculo']['marca'])),
				'recursive'=>-1
			));
			$dt['Marca']['id'] = $marca['Marca']['id'];
				
			$secprojectsArray = $this->Secproject->getSecprojecsMarca(0, $dt['Agedetallecita']['marca']);
			foreach($secprojectsArray as $value)
				$secprojects[$value['Secproject']['id']] = $value['Secproject']['name'];
			
			$this->set('marcas',$marcas);
			$this->set('secprojects', $secprojects);
			$this->set('marcas', $marcas);
			$this->set('motivoservicios',array());
			$this->set('tipomantenimientos', array());
			$this->set('tiposervicios',array());
			$this->set('horarios',0);
			$this->request->data = $dt;
		}
	}
	
	function reprogramarCita($detallecitaId) {
		$this->set('detallecitaId',$detallecitaId);
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		
		$this->loadModel('Agecitacalendario');
		$this->loadModel('Cliente');
		$this->loadModel('Marca');
		$this->loadModel('Secproject');
		$this->loadModel('Agemotivoservicio');
		$this->loadModel('Agetipomantenimiento');
		$this->loadModel('Agetiposervicio');
		
		$talleres = $this->Secproject->find('all',array('conditions'=>array('status'=>'AC'), 'recursive'=>-1));
		$this->set('talleres',$talleres);
		$this->set('isPost',$this->request->is('post'));
		
		$detallecita_db = $this->Agedetallecita->find('first',array('conditions'=>array('Agedetallecita.id'=>$detallecitaId)));
		
		if($this->request->is('get')){
			$this->set('isPost',true);
			$detallecita_db['Cliente']['apellidoPaterno'] = $detallecita_db['Cliente']['nombres'];
			$detallecita_db['Cliente']['str_cliente_tipo'] = $this->Cliente->getStrTipoCliente($detallecita_db['Cliente']['cliente_tipo']);
			$detallecita_db['Agedetallecita']['fecha'] = $this->Secproject->getDateFormatView(substr($detallecita_db['Agedetallecita']['fechadecita'], 0, 10));
			
			// recuperamos las marcas
			$marcas = $this->Marca->getMarcasClient($detallecita_db['Agedetallecita']['cliente_id']);	
				
			$secprojectsArray = $this->Secproject->getSecprojecsMarca(0, $detallecita_db['Agedetallecita']['marca']);
			foreach($secprojectsArray as $value)
				$secprojects[$value['Secproject']['id']] = $value['Secproject']['name'];
			
			//recuperamos los motivos de servicio
			$marca = $this->Marca->find('first',array(
				'conditions'=>array('Marca.description'=>trim($detallecita_db['Agedetallecita']['marca'])),
				'recursive'=>-1
			));
			$motivoservicios = $this->Agemotivoservicio->getMotivoServicio($detallecita_db['Secproject']['id'],$marca['Marca']['id']);
			$detallecita_db['Marca']['id'] = $marca['Marca']['id'];
			// recuperamos los tipo de mantenimiento
			$tipomantenimientos = $this->Agetipomantenimiento->find('list',array(
				'conditions'=>array('agemotivoservicio_id'=>$detallecita_db['Agemotivoservicio']['id'], 'status'=>'AC'),
				'recursive'=>-1
			));
			
			$tiposervicios = $this->Agetiposervicio->find('all', array(
				'conditions'=>array("Agetiposervicio.agemotivoservicio_id"=>$detallecita_db['Agemotivoservicio']['id'], 'Agetiposervicio.status'=>'AC'),
				'recursive'=>-1
			));
			
			$this->request->data = $detallecita_db;
			$this->set('marcas',$marcas);
			$this->set('secprojects',$secprojects);
			$this->set('motivoservicios',$motivoservicios);
			$this->set('tipomantenimientos',$tipomantenimientos);
			$this->set('tiposervicios',$tiposervicios);
		}
	}
	
	/**
	 * 
	 * @param object $detallecita_id [optional]
	 * @return reprogramar una cita desde la web del cliente 
	 */
	public function reprogramarCitaClient($detallecitaId = 0){
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$this->set('bandejaReprogramar',true);
		// validando si existe algun cliente logueado
		$clientSession = $this->requestAction('clientes/verifySessionClient');

		$this->set('detallecitaId',$detallecitaId);
		
		$this->loadModel('Cliente');
		$this->loadModel('Marca');
		$this->loadModel('Secproject');
		$this->loadModel('Agemotivoservicio');
		$this->loadModel('Agetipomantenimiento');
		$this->loadModel('Agetiposervicio');
		
		$talleres = $this->Secproject->find('all',array('conditions'=>array('status'=>'AC'), 'recursive'=>-1));
		$this->set('talleres',$talleres);
		$this->set('isPost',$this->request->is('post'));
		
		$detallecita_db = $this->Agedetallecita->find('first',array('conditions'=>array('Agedetallecita.id'=>$detallecitaId)));
		
		if($this->request->is('get')){
			$this->set('isPost',true);
			$detallecita_db['Cliente']['apellidoPaterno'] = $detallecita_db['Cliente']['nombres'];
			$detallecita_db['Cliente']['str_cliente_tipo'] = $this->Cliente->getStrTipoCliente($detallecita_db['Cliente']['cliente_tipo']);
			$detallecita_db['Agedetallecita']['fecha'] = $this->Secproject->getDateFormatView(substr($detallecita_db['Agedetallecita']['fechadecita'], 0, 10));
			
			// recuperamos las marcas
			$marcas = $this->Marca->getMarcasClient($detallecita_db['Agedetallecita']['cliente_id']);	
				
			$secprojectsArray = $this->Secproject->getSecprojecsMarca(0, $detallecita_db['Agedetallecita']['marca']);
			foreach($secprojectsArray as $value)
				$secprojects[$value['Secproject']['id']] = $value['Secproject']['name'];
			
			//recuperamos los motivos de servicio
			$marca = $this->Marca->find('first',array(
				'conditions'=>array('Marca.description'=>trim($detallecita_db['Agedetallecita']['marca'])),
				'recursive'=>-1
			));
			$motivoservicios = $this->Agemotivoservicio->getMotivoServicio($detallecita_db['Secproject']['id'],$marca['Marca']['id']);
			$detallecita_db['Marca']['id'] = $marca['Marca']['id'];
			// recuperamos los tipo de mantenimiento
			$tipomantenimientos = $this->Agetipomantenimiento->find('list',array(
				'conditions'=>array('agemotivoservicio_id'=>$detallecita_db['Agemotivoservicio']['id'], 'status'=>'AC'),
				'recursive'=>-1
			));
			
			$tiposervicios = $this->Agetiposervicio->find('all', array(
				'conditions'=>array("Agetiposervicio.agemotivoservicio_id"=>$detallecita_db['Agemotivoservicio']['id'], 'Agetiposervicio.status'=>'AC'),
				'recursive'=>-1
			));
			
			$detallecita_db['ClienteLogin'] = $clientSession['Cliente'];
			$this->request->data = $detallecita_db;
			$this->set('marcas',$marcas);
			$this->set('secprojects',$secprojects);
			$this->set('motivoservicios',$motivoservicios);
			$this->set('tipomantenimientos',$tipomantenimientos);
			$this->set('tiposervicios',$tiposervicios);
		}
	}
	
	/**RECUPERA LOS HORARIOS DE UN CLIENTE
	 * AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * @return 
	 */
	public function getHorariosCliente(){
		$this->layout = 'ajax';
		
		$this->loadModel('Agecitacalendario');
		$this->loadModel('Agecitacalendariodia');
		
		$horarios=array();
		if($this->request->is('post')){
			//asignandoles formato a las fechas para comparar
			$fecha = $this->Agecitacalendariodia->getDateFormatDB($this->request->data['fechaini']);
			$fecha =  date('Ymd',strtotime($fecha));
			$fechaActual = $this->Agecitacalendariodia->fechaHoraActual();
			$fechaActual = date('Ymd',strtotime($fechaActual));
			
			if($fecha >= $fechaActual) {			
				//devolviendo la fecha a su formato inicial
				$fecha = date('d-m-Y', strtotime($fecha));
				$fechaActual = $this->Agecitacalendariodia->fechaHoraActual();
				
				if($fecha == date('d-m-Y', strtotime($fechaActual))) {
					$condicion = "Agecitacalendariodia.agecitacalendario_id IN (".$this->request->data['agecitacalendario_id'].") 
								AND date_format(Agecitacalendariodia.initDateTime,'%d-%m-%Y') = '".$fecha."' 
								AND (
									(date_format(Agecitacalendariodia.initDateTime,'%Y-%m-%d %H:%i:%s') <= '".$fechaActual."')
									OR (date_format(Agecitacalendariodia.initDateTime,'%Y-%m-%d %H:%i:%s') >= '".$fechaActual."')
								)
								AND date_format(Agecitacalendariodia.endDateTime,'%Y-%m-%d %H:%i:%s') >= '".$fechaActual."'";
				} else {
					$condicion = "Agecitacalendariodia.agecitacalendario_id IN (".$this->request->data['agecitacalendario_id'].") AND 
								date_format(Agecitacalendariodia.initDateTime,'%d-%m-%Y') = '".$fecha."'";
				}
				$condicion .= ' AND Agecitacalendariodia.available > 0 AND Agecitacalendariodia.estado=\'AC\' AND Agecitacalendario.status=\'AC\' ORDER BY Agecitacalendariodia.initDateTime,Agecitacalendariodia.agecitacalendario_id';
				$horarios = $this->Agecitacalendariodia->find('all',array('conditions'=>$condicion));
				
				if(!empty($horarios)) {
					foreach($horarios as $i => $horario) {
						$horarios[$i]['Agecitacalendariodia']['initDateTime'] = date('H:i',strToTime($horario['Agecitacalendariodia']['initDateTime']));
						$horarios[$i]['Agecitacalendariodia']['endDateTime'] = date('H:i',strToTime($horario['Agecitacalendariodia']['endDateTime']));
						$horarios[$i]['Agecitacalendariodia']['estado'] = "Libre";
					}
				}
			}
		}
		
		$this->set('horarios', $horarios);
		$this->set('conHorario', empty($horarios)?false:true);
		$this->set('agecitacalendario_id', $this->request->data['agecitacalendario_id']);
	}
	
	/**AUTOR VENTURA RUEDA, JOSE ANTONIO
	 * TRAIDO DESDE: derwebsolicitudes
	 * @param object $calendarId
	 * @param object $fechaInicial
	 * @param object $fechaFinal
	 * @param object $marcaApoyo
	 * @return 
	 */
	function listarProgramacionCitaTaller() {
		$this->layout = 'ajax';
		
		$this->loadModel('Agecitacalendario');
		$this->loadModel('Secproject');
		
		$calendarId = $this->request->data['Buscador']['agecitacalendario_id'];
		$fechaInicial = $this->request->data['Buscador']['fechaInicial'];
		$fechaFinal = $this->request->data['Buscador']['fechaFinal'];
		
		
		// se obtienen el taller y los rangos de fecha
		$fechaActual = date('Ymd', strtotime($this->Agedetallecita->fechaHoraActual()));
		$fechaIni = date('Ymd', strtotime($fechaInicial));
		$fechaFin = date('Ymd', strtotime($fechaFinal));
		
		// se obtiene la programacion de cita de taller
		$fechas = (!isset($fechas)) ? $this->Agecitacalendario->obtenerProgramacionCitaTaller($calendarId, $fechaInicial, $fechaFinal) : array();
		
		$this->set('fechas', isset($fechas) && !empty($fechas) ? $fechas : array());
	}
	
	/**MUESTRA LA CITA
	 * autor: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $taldetallecita_id
	 * @return 
	 */
	function mostrarCita($taldetallecita_id) {
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$this->mostrarCitaTaller($taldetallecita_id);
	}

	/**MUESTRA LA CITA DESDE WEB
	 * autor: RONALD TINEO SANTAMARIA
	 * @param object $taldetallecita_id
	 * @return 
	 */
	function mostrarCitaWebCliente($taldetallecita_id) {
		$this->layout = 'modulo_taller'.DS.'web_cliente_ajax';
		// validando si existe algun cliente logueado
		$this->requestAction('clientes/verifySessionClient');
		$this->mostrarCitaTaller($taldetallecita_id);
	}
		
	/*
	 * Muestra la cita de taller generada por el cliente.
	 * @return 
	 * @param $taldetallecita_id Object
	 * @param $dermarca_id Object
	 * @param $dercliente_id Object
	 */
	function mostrarCitaTaller($taldetallecita_id, $dermarca_id=null,$clienteWeb = null) {
		$this->loadModel('Agecitacalendariodia');
		$this->loadModel('Secorganization');
		$this->loadModel('Agedetallecita');
		$this->loadModel('Secproject');
		$this->loadModel('Cliente');
		$this->loadModel('Marca');
		$this->loadModel('Agecitacalendario');
		
		$estadoActivo = 'AC';
		
		// obtenemos los datos de la cita de taller
		$dt = $this->Agedetallecita->findById($taldetallecita_id);

		// obteniendo los datos del cliente
		$cliente = $this->Cliente->findById($dt['Cliente']['id']);
		$dt['Cliente'] = $cliente['Cliente'];
		$dt['Cliente']['str_cliente_tipo'] = $this->Cliente->getStrTipoCliente($cliente['Cliente']['cliente_tipo']); 
		// obteniendo los datos de la fecha de la cita
		$this->Agecitacalendariodia->recursive=2;
		$talcitacalendarday = $this->Agecitacalendariodia->findById($dt['Agedetallecita']['agecitacalendariodia_id']);//, $campos);
		
		$dt['Agecitacalendariodia'] = $talcitacalendarday['Agecitacalendariodia'];
		$dt['Agemotivoservicio'] = $talcitacalendarday['Agecitacalendario']['Agemotivoservicio'];
		$dt['Secproject'] = $talcitacalendarday['Agecitacalendario']['Secproject'];
		
		// obteniendo el nombre de la organizacion de la sucursal
		$organizationName = $this->Secorganization->obtenerName($dt['Secproject']['secorganization_id']);
		
		// obteniendo los datos de los servicios solicitados
		$servicios = array();
		
		$this->set('servicios', $servicios);
		$this->set('organizationName', $organizationName);
		$this->set('dercliente_id', $dt['Cliente']['id']);
		
		//Solo para la web del cliente
		$this->set('tallerVehiculos', true);// para marcar la opcion seleccionada
		$dt['ClienteLogin'] = $clienteWeb['Cliente'];
		//----------------------------
		$this->request->data = $dt;
	}
	
	/* REGISTRA CITA EN SAP
	 * @Author: Ronald Tineo Santamaria
	 * @Fecha: 18/03/2013
	 * */
	function updateEmailPhoneClienteJson(){
		app::import('Model', 'Cliente');	$this->Cliente = new Cliente();
		$this->layout = 'ajax';
		$this->autoRender = false;
		$responseAjax = array('Success'=>false,'Mensaje'=>__('MENSAJE_NUMERO_TELEFONICO'));
		Configure::write('debug',0);
		$param = $this->request->data;
		if($this->request->is('post') && !empty($param) && !empty($param['Confirm']['clienteid'])){
			$cliente['Cliente']['id'] = $param['Confirm']['clienteid'];
			$cliente['Cliente']['email'] = $param['Confirm']['Email'];
			$cliente['Cliente']['telefono'] = $param['Confirm']['Telefono'];
			if($this->Cliente->save($cliente)){
				$responseAjax =  array('Success'=>true,'Mensaje'=>'');
			}else $responseAjax =  array('Success'=>false,'Mensaje'=>'Por favor contactese con su administrador');
		}else{
			$responseAjax =  array('Success'=>false,'Mensaje'=>'Por Favor Complete los datos');
		}
		echo json_encode($responseAjax);
	}
	
	function registrarCitaSap($param){
		$dataWebService = array();
		$return = array('Ot'=>null,'Mensaje'=>__('MENSAJE_NUMERO_TELEFONICO'),'Tramaxml'=>'');
		$dataWebService = $this->requestAction('webservicessaps/getAgendamientoCitas',$param);
		if(!empty($dataWebService) && isset($dataWebService)){
			if($dataWebService['Estado']=='OK'){
				return array('Ot'=>$this->obtenerOtFromSap($dataWebService['Mensaje']),'Mensaje'=>$dataWebService['Mensaje'],'Tramaxml'=>$dataWebService['Tramaxml']);
			}elseif($dataWebService['Estado']=='NOOK'){
				return array('Ot'=>null,'Mensaje'=>$dataWebService['Mensaje'],'Tramaxml'=>$dataWebService['Tramaxml']);
			}
		}
		return $return;
	}
	
	function registrarCitaSapNew($param){
		$dataWebService = array();
		$return = array('Ot'=>null,'Mensaje'=>__('MENSAJE_NUMERO_TELEFONICO'),'Tramaxml'=>'');
		$dataWebService = $this->requestAction('webservicessaps/getAgendamientoCitasNew',$param);
		if(!empty($dataWebService) && isset($dataWebService)){
			if($dataWebService['Estado']=='OK'){
				return array('Ot'=>$this->obtenerOtFromSap($dataWebService['Mensaje']),'Mensaje'=>$dataWebService['Mensaje'],'Tramaxml'=>$dataWebService['Tramaxml']);
			}elseif($dataWebService['Estado']=='NOOK'){
				return array('Ot'=>null,'Mensaje'=>$dataWebService['Mensaje'],'Tramaxml'=>$dataWebService['Tramaxml']);
			}
		}
		return $return;
	}
	
	function obtenerOtFromSap($mensaje){
		return substr($mensaje,6,10);
	}
	
	function getCitaClientJson($cliVehId){
		$this->layout = 'ajax';
		$this->autoRender = false;
		
		Configure::write('debug',0);
		
		$citas = $this->Agedetallecita->getCitaClient($cliVehId, $afterNow = true);
		$response->susses = true;
		$response->errors = array('SIN ERRORS');
		$response->cita = !empty($citas);
		
		if(!empty($citas)){
			$response->message = "El cliente: "
				.$citas[0]['Cliente']['nombres']
				." tiene una cita pendiente para el dia: "
				.substr($citas[0]['Agecitacalendariodia']['initDateTime'], 0 , 10)
				." [".substr($citas[0]['Agecitacalendariodia']['initDateTime'], 11, 5)
				." - ".substr($citas[0]['Agecitacalendariodia']['endDateTime'], 11, 5)
				."]";
				
		}
		echo json_encode($response);
	}
	
	/**UTOR: VENTURA RUEDA, JOSE ANTNIO
	 * FECHA: 2013-03-20
	 * @return 
	 */
	function getIndexExel(){
		$this->loadModel('Cliente');
		//BORRAMOS LAS VARIABLES DE SESSION DEL BUSCADOR PRIMER INGRESO
		@$this->setInitSessionConditions();
		//FORMAMOS LA DATA SI SE ESTA ENVIANDO LOS DATOS POR PAGINADOR
		if($this->request->is('get')){
			//RECUPERAMOS LAS CONDICIONES DE BUSQUEDA ALMACENADOS EN SESSION
			$bscCnd = $this->getSessionConditions();
			$this->request->data['bsc']['vlr'] = (trim(isset($bscCnd['bsc']['vlr'])?$bscCnd['bsc']['vlr']:'') == '')?'':$bscCnd['bsc']['vlr'];
			$this->request->data['bsc']['f_ini'] = empty($bscCnd['bsc']['f_ini'])?'':$bscCnd['bsc']['f_ini'];
			$this->request->data['bsc']['f_fin'] = empty($bscCnd['bsc']['f_fin'])?'':$bscCnd['bsc']['f_fin'];
		}
	
		//GUARDAMOS LAS CONDICIONES EN SESSION
		@$this->setSessionConditions($this->request->data);
		$cnd = $this->Agedetallecita->getConditionsBuscador($this->request->data, $this->_getDtLg());

		$agedetallecitas=$this->Agedetallecita->find('all',array(
			'conditions'=>$cnd,
			'order' => array ('Agedetallecita.fechaRegistro' => 'ASC','Agedetallecita.horaRegistro' => 'ASC')
		));

		// se obtiene el caledario  de los registros obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->recursive = -1;	
			$agecitacalendarios = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->find('first', array(
				'conditions' => array('Agecitacalendario.status' =>'AC','Agecitacalendario.id' => $row['Agecitacalendariodia']['agecitacalendario_id'])
			));
			$agedetallecitas[$key]['Agecitacalendario'] = $agecitacalendarios['Agecitacalendario'];
			$agedetallecitas[$key]['Cliente']['str_cliente_tipo'] = $this->Cliente->getStrTipoCliente($agedetallecitas[$key]['Cliente']['cliente_tipo']);
		}
		
		// se obtiene el grupo de los registros obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agegrupo->recursive = -1;	
			$agegrupos = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agegrupo->find('first', array(
				'conditions' => array('Agegrupo.status' =>'AC','Agegrupo.id' => $row['Agecitacalendario']['agegrupo_id'])
			));
			$agedetallecitas[$key]['Agegrupo'] = $agegrupos['Agegrupo'];
		}
		
		// se obtiene lña sucursal  de los Sucursal obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Secproject->recursive = -1;	
			$secprojects = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Secproject->find('first', array(
				'conditions' => array('Secproject.status' =>'AC','Secproject.id' => $row['Agecitacalendario']['secproject_id'])
			));
			$agedetallecitas[$key]['Secproject'] = $secprojects['Secproject'];
		}
		
		// se obtiene el servicio de los Sucursal obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agemotivoservicio->recursive = -1;	
			$agemotivoservicios = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agemotivoservicio->find('first', array(
				'conditions' => array('Agemotivoservicio.status' =>'AC','Agemotivoservicio.id' => $row['Agecitacalendario']['agemotivoservicio_id'])
			));
			$agedetallecitas[$key]['Agemotivoservicio'] = $agemotivoservicios['Agemotivoservicio'];
		}
		
		$this->set('f_campo',array('Agedetallecita.fechadecita'=>__('cita'), 'Agedetallecita.fechaRegistro'=>__('creacion')));	
		$this->set('std', $this->Agedetallecita->status);
		$this->set('crt', array('Cliente.nombres'=>__('Cliente'), 'Agedetallecita.placa'=>__('placa')));
		$this->set('dt', $this->request->data);
		$this->set('agedetallecitas',$agedetallecitas);	
		$this->set('statusSap', $this->Agedetallecita->statusSap);
		
		//IMPRESION EN EXEL
		$this->layout = 'modulo_taller'.DS.'excel';
		header('Content-Type: text/html');
		header("Content-Disposition: attachment; filename=Historial_de_Citas.xls");
		
		set_time_limit(1200);
		ini_set('memory_limit', '512M');
		$fecha = date('d/m/Y');
        $this->set('fecha', $fecha);
	}
	
	/**AUTOR: RONALD TINEO SANTAMARIA
	 * FECHA: 2013-03-20
	 * @return 
	 */
	public function getExcelResumen() {
		configure::write('debug', 0);
		$this->layout = 'modulo_taller'.DS.'excel';
		//only for test month Junio
		$this->loadModel('Secproject');
		$this->loadModel('Agecitacalendario');
		$this->loadModel('Agecitacalendariodia');
		$this->loadModel('Marca');
		$this->loadModel('Agetiposervicio');
		$resumen = array();$resumen2 = array();$resumen3= array();
		
		$fechaReporte = str_replace('/', '-', $this->request->data['bsc']['f_ini']);
		$mes = date('m',strtotime($fechaReporte));
		$anio = date('Y',strtotime($fechaReporte));

		//obtenemos las sucursales
		$secproject = $this->Secproject->find('all',array('conditions'=>array('status'=>'AC'), 'recursive'=>-1));
		$idsecprojects = array();
		foreach($secproject as $id => $item){
			$idsecprojects[] = $item['Secproject']['id'];
			$resumen[$item['Secproject']['id']]['Secproject']['id'] = $item['Secproject']['id'];
			$resumen[$item['Secproject']['id']]['Secproject']['name'] = $item['Secproject']['name'];
			$resumen2[$item['Secproject']['id']]['Secproject']['id'] = $item['Secproject']['id'];
			$resumen2[$item['Secproject']['id']]['Secproject']['name'] = $item['Secproject']['name'];
			$resumen3[$item['Secproject']['id']]['Secproject']['id'] = $item['Secproject']['id'];
			$resumen3[$item['Secproject']['id']]['Secproject']['name'] = $item['Secproject']['name'];
		}
		//obtenemos los agecitacalendario y motivo de servicio dia por ids de sucursal 
		$agecalendario = $this->Agecitacalendario->getCalendarioAndMotivoServicioBySecproject($idsecprojects);
		
		foreach($secproject as $id => $item){
			foreach($agecalendario as $id1 => $item1){
				if($item['Secproject']['id'] == $item1['Secproject']['id']){
					$resumen[$item['Secproject']['id']]['Agemotivoservicio'][$item1['Agemotivoservicio']['id']] = $item1['Agemotivoservicio'];
					$resumen[$item['Secproject']['id']]['Agemotivoservicio'][$item1['Agemotivoservicio']['id']]['Agecitacalendario'] = $item1['Agecitacalendario'];
				}
			}
		}
		
		$idagecitacalendario = array();
		foreach($agecalendario as $id1 => $item1){
			$idagecitacalendario[] = $item1['Agecitacalendario']['id'];
		}
		//obtenemos los cita calendario dia
		$agecitacalendariodia = $this->Agecitacalendariodia->getCitaCalendarioDia($idagecitacalendario,$mes,$anio);
		
		foreach($resumen as $id => $item){
			if(!empty($item['Agemotivoservicio']) && isset($item['Agemotivoservicio'])){
				$agemotivoservicio = $item['Agemotivoservicio'];
				foreach($agemotivoservicio as $id1 => $item1){
					foreach($agecitacalendariodia as $id2 => $item2){
						if($item1['Agecitacalendario']['id'] == $item2['Agecitacalendariodia']['agecitacalendario_id']){
							$resumen[$id]['Agemotivoservicio'][$id1]['Agecitacalendario']['Programed'] = $item2['0']['Programed'];
							$resumen[$id]['Agemotivoservicio'][$id1]['Agecitacalendario']['Available'] = $item2['0']['Available'];
						}
					}
					
				}
			}
		}
		$this->set('reporte',$resumen);
		
		//SEGUNDO REPORTE DE SUCURSALES POR MARCAS
		$agedetallecitasucursalmarca = $this->Agedetallecita->getCantidadCitasSucursalMarca($mes,$anio);
		$marcas = $this->Marca->find('all',array('conditions'=>array('status'=>'AC'),
												'order'=>array('description'=>'asc'),
												'fields'=>array('id','description'),
												'recursive'=>-1));
		//llenamos las marcas en resumen
		$totalesMarca = array();
		foreach($resumen2 as $id => $item){
			foreach($marcas as $id1 => $item1){
				$resumen2[$id]['Marcas'][$item1['Marca']['id']]['id'] = $item1['Marca']['id'];
				$resumen2[$id]['Marcas'][$item1['Marca']['id']]['description'] = $item1['Marca']['description'];
				$totalesMarca[$item1['Marca']['id']]['total'] = 0;
			}
		}
		
		foreach($resumen2 as $id => $item){
			foreach($agedetallecitasucursalmarca as $id1 => $item1){
				if($item['Secproject']['id'] == $item1['Agedetallecita']['secproject_id']){
					foreach($marcas as $id2 => $item2){
						if($item1['Agedetallecita']['marca'] == $item2['Marca']['description']){
							$resumen2[$id]['Marcas'][$item2['Marca']['id']]['total'] = $item1['0']['total'];
							$totalesMarca[$item2['Marca']['id']]['total'] += $item1['0']['total'];
						}
					}
				}
			}
		}
		$this->set('reporte2',$resumen2);
		$this->set('marcas',$marcas);
		$this->set('totalesMarca',$totalesMarca);
		
		//TERCER REPORTE DE SUCURSALES Y TIPO DE SERVICIO
		$agetiposervicio = $this->Agedetallecita->getCantidadCitasSucursalTipoMantenimiento($mes,$anio);
		$tiposervicio = $this->Agetiposervicio->find('all',array('conditions'=>array('status'=>'AC'),
																			'fields'=>array('id','description'),
																			'recursive'=>-1
																		));
		$totalesTipoServicio = array();
		foreach($resumen3 as $id => $item){
			foreach($tiposervicio as $id1 => $item1){
				$resumen3[$id]['Agetiposervicios'][$item1['Agetiposervicio']['id']]['id'] = $item1['Agetiposervicio']['id'];
				$resumen3[$id]['Agetiposervicios'][$item1['Agetiposervicio']['id']]['description'] = $item1['Agetiposervicio']['description'];
				$totalesTipoServicio[$item1['Agetiposervicio']['id']]['total'] = 0;
			}
		}
		//pr($agetiposervicio);
		foreach($resumen3 as $id => $item){
			foreach($agetiposervicio as $id1 => $item1){
				if($item['Secproject']['id'] == $item1['Agedetallecita']['secproject_id']){
					foreach($tiposervicio as $id2 => $item2){
						if($item1['Agedetallecita']['agetiposervicio_id'] == $item2['Agetiposervicio']['id']){
							$resumen3[$id]['Agetiposervicios'][$item2['Agetiposervicio']['id']]['total'] = $item1['0']['total'];
							$totalesTipoServicio[$item2['Agetiposervicio']['id']]['total'] += $item1['0']['total'];
						}
					}
				}
			}
		}
		//pr($resumen3);
		$this->set('reporte3',$resumen3);
		$this->set('tiposervicio',$tiposervicio);
		$this->set('totalesTipoServicio',$totalesTipoServicio);
		
		header('Content-Type: text/html');
		header("Content-Disposition: attachment; filename=Reporte_resumen.xls");
		set_time_limit(0);
	}
	
	/**UTOR: VENTURA RUEDA, JOSE ANTNIO
	 * FECHA: 2013-03-20
	 * @return 
	 */
	function getIndexExel2(){
		set_time_limit(60 * 30);
        ini_set('memory_limit', (512 * 4).'M');
		configure::write('debug', 0);
		$this->layout = 'ajax';
		
		//BORRAMOS LAS VARIABLES DE SESSION DEL BUSCADOR PRIMER INGRESO
		@$this->setInitSessionConditions();
		//FORMAMOS LA DATA SI SE ESTA ENVIANDO LOS DATOS POR PAGINADOR
		if($this->request->is('get')){
			//RECUPERAMOS LAS CONDICIONES DE BUSQUEDA ALMACENADOS EN SESSION
			$bscCnd = $this->getSessionConditions();
			$this->request->data['bsc']['vlr'] = (trim(isset($bscCnd['bsc']['vlr'])?$bscCnd['bsc']['vlr']:'') == '')?'':$bscCnd['bsc']['vlr'];
			$this->request->data['bsc']['f_ini'] = empty($bscCnd['bsc']['f_ini'])?'':$bscCnd['bsc']['f_ini'];
			$this->request->data['bsc']['f_fin'] = empty($bscCnd['bsc']['f_fin'])?'':$bscCnd['bsc']['f_fin'];
		}
	
		//GUARDAMOS LAS CONDICIONES EN SESSION
		@$this->setSessionConditions($this->request->data);
		$cnd = $this->Agedetallecita->getConditionsBuscador($this->request->data, $this->_getDtLg());
		
		$agedetallecitas=$this->Agedetallecita->find('all',array(
			'conditions'=>$cnd,
			'order' => array ('Agedetallecita.fechaRegistro' => 'ASC')
		));
	
		// se obtiene el caledario  de los registros obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->recursive = -1;	
			$agecitacalendarios = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->find('first', array(
				'conditions' => array('Agecitacalendario.status' =>'AC','Agecitacalendario.id' => $row['Agecitacalendariodia']['agecitacalendario_id'])
			));
			$agedetallecitas[$key]['Agecitacalendario'] = $agecitacalendarios['Agecitacalendario'];
		}
		
		// se obtiene el grupo de los registros obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agegrupo->recursive = -1;	
			$agegrupos = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agegrupo->find('first', array(
				'conditions' => array('Agegrupo.status' =>'AC','Agegrupo.id' => $row['Agecitacalendario']['agegrupo_id'])
			));
			$agedetallecitas[$key]['Agegrupo'] = $agegrupos['Agegrupo'];
		}
		
		// se obtiene lña sucursal  de los Sucursal obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Secproject->recursive = -1;	
			$secprojects = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Secproject->find('first', array(
				'conditions' => array('Secproject.status' =>'AC','Secproject.id' => $row['Agecitacalendario']['secproject_id'])
			));
			$agedetallecitas[$key]['Secproject'] = $secprojects['Secproject'];
		}
		
		// se obtiene el servicio de los Sucursal obtenidos
		foreach($agedetallecitas as $key => $row) {	
			$this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agemotivoservicio->recursive = -1;	
			$agemotivoservicios = $this->Agedetallecita->Agecitacalendariodia->Agecitacalendario->Agemotivoservicio->find('first', array(
				'conditions' => array('Agemotivoservicio.status' =>'AC','Agemotivoservicio.id' => $row['Agecitacalendario']['agemotivoservicio_id'])
			));
			$agedetallecitas[$key]['Agemotivoservicio'] = $agemotivoservicios['Agemotivoservicio'];
		}
		
		$this->set('f_campo',array('Agedetallecita.fechadecita'=>__('cita'), 'Agedetallecita.fechaRegistro'=>__('creacion')));	
		$this->set('std', $this->Agedetallecita->status);
		$this->set('crt', array('Cliente.nombres'=>__('Cliente'), 'Agedetallecita.placa'=>__('placa')));
		$this->set('dt', $this->request->data);
		$this->set('agedetallecitas',$agedetallecitas);	
	}
	
	function ajaxCountTipoServicios($secproject_id, $motivoservicio_id, $fechatoday){
		configure::write('debug', 0);
		$this->layout = 'ajax';
		$this->autoRender = false;
		$fechatoday = date('Y-m-d',strtotime($fechatoday));
		$result = $this->Agedetallecita->getCountTipoServicios($secproject_id, $motivoservicio_id,$fechatoday);
		$json = array(0=>array('descripcion'=>$this->Agedetallecita->tiposervicio[0],'cantidad'=>0),1=>array('descripcion'=>$this->Agedetallecita->tiposervicio[1],'cantidad'=>0));
		if(!empty($result) && isset($result)){
			foreach($result as $id => $item){
				if($item['Agetipomantenimiento']['tiposervicio']==0){
					$json[0]['cantidad'] = $item[0]['cantidad'];
				}
				if($item['Agetipomantenimiento']['tiposervicio']==1){
					$json[1]['cantidad'] = $item[0]['cantidad'];
				}
			}
		}
		echo json_encode($json);
	}
	
	function ajaxGetMensajeTipoServicios($motivoservicio_id){
		configure::write('debug', 0);
		$this->layout = 'ajax';
		$this->autoRender = false;
		$this->loadModel('Agemotivoservicio');
		$this->Agemotivoservicio->recursive = -1;
		$result = $this->Agemotivoservicio->findById($motivoservicio_id);
		//pr($result);
		$json = array('mensaje'=>$result['Agemotivoservicio']['mensaje']);
		/*if(!empty($result) && isset($result)){
			foreach($result as $id => $item){
				if($item['Agetipomantenimiento']['tiposervicio']==0){
					$json[0]['cantidad'] = $item[0]['cantidad'];
				}
				if($item['Agetipomantenimiento']['tiposervicio']==1){
					$json[1]['cantidad'] = $item[0]['cantidad'];
				}
			}
		}*/
		//pr($this->sendMailClientMakeCita(255042));
		echo json_encode($json);
	}
}
?>