<?php
class CcpsolicitudserviciosController extends AppController {
	public $name='Ccpsolicitudservicios';	
	public $helpers = array('Html', 'Form', 'Js', 'Avhtml');
	public $components = array('RequestHandler'); 
	
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
		$this->loadmodel('Ccptablero');
    }
	
	function mostrar($solicitud_id){
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		$this->loadModel('Talot');
		$this->loadModel('Ccptablero');
		
		$solicitud = $this->Ccpsolicitudservicio->find('first',array(
			'conditions'=>array('Ccpsolicitudservicio.id'=>$solicitud_id),
			'recursive'=>0
		));
		
		$ccptableros = $this->Ccptablero->find('list');
   		
		// recuperamos las aseguradoras de talot
   		$ciaAseguradoras = $this->Talot->find('list',array(
			'conditions'=>array("CodSapCiaAseguradora <> '.'"),
			'fields'=>array('CodSapCiaAseguradora', 'CodSapCiaAseguradora')
		));
   		$ciaAseguradoras = array_merge($ciaAseguradoras, $this->Datos->_getDato('aseguradoras'));
		$aseguradoras = array(''=>__('Seleccionar'),'1'=>'PACIFICO','2'=>'RIMAC','3'=>'POSITIVA','4'=>'MAPFRE','5'=>'PUBLICO','6'=>'MAGALLANES');
		
		$this->set('solicitud',$solicitud);
		$this->set('ccptableros',$ccptableros);
		$this->set('ciaAseguradoras',$ciaAseguradoras);
		$this->set('aseguradoras',$aseguradoras);
	}
	
	/** AVENTURA - impresion de la solicitud **/
	/*
	 * migrado al 2.2.5 por Miguel Chiuyari
	 */
   	function imprimirPdf($solicitud_id='000'){
		Configure::write('debug',0);
   		$this->response->header(array('Content-type: application/pdf'));
    	$this->response->type('pdf');
		$this->mostrar($solicitud_id);
	    $this->layout = 'pdf'; //this will use the pdf.ctp layout
    	$this->render();
	}
	
	
	/** AVENTURA
     *  MUESTRA LA LISTA DE TODAS LAS SOLICITUDES DE SERVICIOS
     *  return array();
     */  
	function index($exportar=null,$parametros=null){
		$this->pageTitle = __('Solicitud Servicio');
		
		//MODELOS CARS UTILIZADO
		$this->loadModel('Secproject');
		$this->loadModel('Ccptiposervicio');
		$this->loadModel('Ccpsolicitudservicioestado');
		$this->loadModel('Ccptabhistorial');
		$this->loadModel('Ccptabestado');
		$this->loadModel('SistemaPlanificadorCcp');
		
		//DATOS ESTATICOS
		$estadoCcp="1,2,13";//lavado y ccasesor
		$estadoEliminado = 'EL'; $estadoActivo = 'AC';
		$dtLg = $this->_getDtLg();
		
		$data = empty($exportar)?$this->request->query
			:$parametros;
		
		$this->set('data',$data);
		
		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$project = $this->Secproject->findById($dtLg['Secproject']['id'], null,null,-1);
		$dtLg['Secproject']['carscod'] = $project['Secproject']['carscod'];
		
		// RECUPERAMOS las CONDICIONES
		$cnd = $this->SistemaPlanificadorCcp->cndBsc_getTalotCarsNew($data, $dtLg);

 		$this->paginate = array(
			'SistemaPlanificadorCcp'=>array('limit' => 10,
				'page' => 1,
				'order' => array ('SistemaPlanificadorCcp.FECHA' => 'DESC'),
				'conditions' => $cnd
			)
		); 
		
		$otsCars = empty($exportar)?$this->paginate('SistemaPlanificadorCcp')
			:$this->SistemaPlanificadorCcp->find('all',array('conditions'=>$cnd, 'order'=>array('SistemaPlanificadorCcp.FECHA' => 'DESC')));
		$otsCars = $this->SistemaPlanificadorCcp->getTalotCars($otsCars);
		$this->set('otsCars',$otsCars);

		$this->listaFinalPDF = isset($otsCars) && !empty($otsCars) ? $otsCars : array();

		//RECUPERAMOS LOS COMBOS PARA EL BUSCADOR
   		$ccptiposervicios = $this->Ccptiposervicio->find('list');
   		$ccptiposervicios = empty($ccptiposervicios)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccptiposervicios;
		$ccpsolicitudservicioestados = $this->Ccpsolicitudservicioestado->find('list');
   		$ccpsolicitudservicioestados = empty($ccpsolicitudservicioestados)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccpsolicitudservicioestados;
		$asesoresCars = $this->Ccptabhistorial->getAsesoresCars($project['Secproject']['carscod']);
		$asesoresCars = empty($asesoresCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$asesoresCars;
		$estadoPorPresupuestos = array(''=>__('Seleccionar'),'1'=>'Sin Presupuesto','2'=>'Con Presupuesto','3'=>'Presupuesto listo','4'=>'Presupuesto aprobado');
		$this->set(compact('estadoPorPresupuestos','asesoresCars','ccptiposervicios','ccpsolicitudservicioestados','estadoOtCars'));
		
		//obtenemos los estados de las ots de cars
		$estadosOtCars = $this->Datos->_getDato('estadosOtCarsCcp');
		$estadosOtCarsTooltip = $this->Datos->_getDato('estadosOtCars');
		$estadosOtCarsSelect = empty($estadosOtCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$estadosOtCars;
		
		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>__('Seleccionar'))+$estadosCcp:array(''=>__('Seleccionar'));
		
		$this->set('estadosOtCarsSelect',$estadosOtCarsSelect);
		$this->set('estadosOtCars',$estadosOtCars);
		$this->set('estadosCcpSelect',$estadosCcpSelect);
		$this->set('estadosOtCarsTooltip',$estadosOtCarsTooltip);	
	} 

	public function indexExportarExcel() {
		$this->layout = 'excel_db';
		
		$this->index($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=index.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);
	}
	
	/**AVENTURA
	 * AGREGAR UNA NUEVA SOLICITUD DE SERVICIO
	 * @return 
	 */
	public function setSolicitud($nro_cars){
		set_time_limit(1200);
		ini_set('memory_limit', '512M');
				
		$this->set('nro_cars',$nro_cars);
		//MODELOS UTILIZADOS
		$this->loadModel('Ccptabhistorial');
		$this->loadModel('Talot');
		$this->loadModel('SistemaPlanificadorCcp');
		
		//RECUPERAMOS EL PRIMER PRESUPUESTO DE LA SOLICITUD
		$this->Talot->recursive = -1; 
		$talot = $this->Talot->findByNrocars($nro_cars);
		$this->set('talot',$talot);
		$primerPresupuesto = $this->SistemaPlanificadorCcp->getOtPresupuesto($nro_cars);
		$this->set('primerPresupuesto',$primerPresupuesto);

		//si el numero de ot es proveniente de un estado de ampliacion el archivo de orden aparecera deshabilitado
		$ampliacion = false;
		$otCCP=$this->Ccptabhistorial->obtenerEstadoCcpPotNumeroOt(trim($nro_cars));
		if(!empty($otCCP) && isset($otCCP)){
			if($otCCP[0]['Ccptabestado']['id']==13 || $this->Ccptabhistorial->validarEstadoAnterior(trim($nro_cars),13)) $ampliacion=true;
		}
		$this->set('ampliacion',$ampliacion);
		
		//SI LA SOLICITUD VIENE DE UN RECHAZO EL ARCHIVO A SUBIR DEBE  DEBE CONSERVARSE
		$this->set('rechazado',0);
		$solicitud_anterior = $this->Ccpsolicitudservicio->find('first',array(
			'conditions'=>array("ot_numero = '".$nro_cars."' AND ccpsolicitudservicioestado_id = 5"),
			'order'=>array('id'=>'DESC'),
			'recursive'=>-1
		));

		if(!empty($solicitud_anterior) && isset($solicitud_anterior)){
			//OBTENEMOS LOS NOMBRE DE LOS ARCHIVOS
			$this->set('archivoOrden',$solicitud_anterior['Ccpsolicitudservicio']['archivoorden']);
			$this->set('archivoSolicitud',$solicitud_anterior['Ccpsolicitudservicio']['archivopresupuesto']);
			$this->set('rechazado',1);
		}
		
		$dtLog = $this->_getDtLg();

		if(!empty($this->request->data)){
			// GUARDAMOS LOS DATOS ENVIADOS POR EL FORMULARIO
			$this->Ccpsolicitudservicio->begin();
			$this->request->data['ccpsolicitudservicios']['contrasenia_'] = $this->Auth->password($this->request->data['ccpsolicitudservicios']['contrasenia']);
			$respuesta = $this->Ccpsolicitudservicio->setSolicitudServicio($this->request->data,$dtLog);
			
			if(!$respuesta[0]){
				$this->Ccpsolicitudservicio->rollback();
			}else{
				$this->request->data['guardado'] = true;
				$this->Ccpsolicitudservicio->commit();
				$this->Session->write('actualizarPadre', true);
				$organizationMostrarUrl = "/Ccpsolicitudservicios/mostrar/".$respuesta['id'];
				$this->redirect($organizationMostrarUrl);
			}
			$this->Session->setFlash($respuesta[1],empty($respuesta[0])?'flash_failure':'flash_success');
		}
		
		
		$this->request->data['logueado'] = $dtLog['Secperson']['username'];
		$this->__datosVista();
	}
	/** AVENTURA
     *  MUESTRA LA LISTA DE TODAS LAS SOLICITUDES DE SERVICIOS
     *  return array();
     */    
	function indexSolicitudServicio($exportar=null,$parametros=null){
		$this->pageTitle = __('Solicitud Servicio');
		 
		//MODELOS CARS UTILIZADO
		$this->loadModel('Ccptiposervicio');
		$this->loadModel('Ccpsolicitudservicioestado');
		$this->loadModel('Ccptabestado');
		$this->loadModel('Talot');
		$this->loadModel('Ccpsolicitudserviciosjoin');
		$this->loadModel('Secproject');
		
		//DATOS ESTATICOS
		$estadoEliminado = 'EL';
		$estadoActivo = 'AC';
		$dtLg = $this->_getDtLg();
		
		$data = empty($exportar)?$this->request->query
			:$parametros;
			
		$this->set('data',$data);
		
		$cnd = $this->Ccpsolicitudserviciosjoin->getCndIndexSolService($data, $dtLg);

		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$project = $this->Secproject->findById($dtLg['Secproject']['id'], null,null,-1);
		$params['params']['carscod'] = $project['Secproject']['carscod'];
		
		$this->paginate = array(
			//'findType'=>'Ccpsolicitudserviciosjoin',
			'Ccpsolicitudserviciosjoin'=>array(
				'conditions' => $cnd,
				'order'=>array('Ccpsolicitudserviciosjoin.ot_numero'=>'ASC'),
				'limit'=>empty($exportar)?"10":"10000000000",
				'page'=> 1
			)
		); 
		
		$solicitud_servicios = $this->paginate('Ccpsolicitudserviciosjoin');

 		$this->set('solicitud_servicios',$solicitud_servicios);
		$this->listaFinalPDF = $solicitud_servicios;
		
		//RECUPERAMOS LOS COMBOS PARA EL BUSCADOR
   		$ccptiposervicios = $this->Ccptiposervicio->find('list');
   		$ccptiposervicios = empty($ccptiposervicios)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccptiposervicios;
		$ccpsolicitudservicioestados = $this->Ccpsolicitudservicioestado->find('list');
   		$ccpsolicitudservicioestados = empty($ccpsolicitudservicioestados)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccpsolicitudservicioestados;
		$this->set(compact('ccptiposervicios','ccpsolicitudservicioestados'));
		
		//obtenemos los estados de las ots de cars
		$estadosOtCars = $this->Datos->_getDato('estadosOtCarsCcp');
		$estadosOtCarsTooltip = $this->Datos->_getDato('estadosOtCars');
		$estadosOtCarsSelect = empty($estadosOtCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$estadosOtCars;
		
		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>__('Seleccionar'))+$estadosCcp:array(''=>__('Seleccionar'));
		
		$this->set('estadosOtCarsSelect',$estadosOtCarsSelect);
		$this->set('estadosOtCars',$estadosOtCars);
		$this->set('estadosCcpSelect',$estadosCcpSelect);
		$this->set('estadosOtCarsTooltip',$estadosOtCarsTooltip);
		
	}
	
	public function indexSolicitudServicioExcel() {
		$this->layout = 'excel_db';
		
		$this->indexSolicitudServicio($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=ReporteProduccion.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);
	}
	
	/** AVENTURA
     *  MUESTRA LA LISTA DE TODAS LAS SOLICITUDES DE SERVICIOS
     *  return array();
     */    
	function indexCoordinador($exportar=null, $parametros=null){ 
		$this->pageTitle = __('Solicitud Servicio');

		//MODELOS CARS UTILIZADO
		$this->loadModel('Secproject');
		$this->loadModel('Ccptiposervicio');
		$this->loadModel('Ccpsolicitudservicioestado');
		$this->loadModel('Ccptabestado');
		$this->loadModel('Ccpsolicitudserviciosjoin');
		
		//DATOS ESTATICOS
		$estadoEliminado = 'EL';
		$estadoActivo = 'AC';
		$dtLg = $this->_getDtLg();
		
		$data = empty($exportar)?$this->request->query
			:$parametros;
			
		if(!isset($data['fechaIni']) and !isset($data['fechaFin']) and !isset($data['ccptiposervicio_id']) and !isset($data['nrocars'])
			and !isset($data['fechaIniOt']) and !isset($data['fechaFinOt']) and !isset($data['carsestado_id'])
			and !isset($data['ccpsolicitudservicioestado_id']) and !isset($data['carsestado_id']) 
			and isset($data['placa']) and isset($data['estadootccp_id'])){
			$data['carsestado_id'] = '';
		}
		$this->set('data',$data);
		
		// RECUPERAMOS EL ARRAR DE CONDICIONES
		$cnd = $this->Ccpsolicitudserviciosjoin->getConditionsBsc($data, $dtLg);
		
		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$project = $this->Secproject->findById($dtLg['Secproject']['id'], null,null,-1);
		$params['params']['carscod'] = $project['Secproject']['carscod'];
		
		$this->paginate = array(
			//'findType'=>'Ccpsolicitudserviciosjoin',
			'Ccpsolicitudserviciosjoin'=>array(
				'conditions' => $cnd,
				'order'=>array('Ccpsolicitudserviciosjoin.ot_fecha_creacion'=>'DESC'),
				'limit'=>empty($exportar)?"10":"10000000000",
				'page'=> 1
			)
		); 
		
		$this->listaFinalPDF = isset($otsCars) && !empty($otsCars) ? $otsCars : array();
		$otsCars = $this->paginate('Ccpsolicitudserviciosjoin');
		
 		$this->set('otsCars',$otsCars);
		$this->listaFinalPDF = $otsCars;
		
		//RECUPERAMOS LOS COMBOS PARA EL BUSCADOR
   		$ccptiposervicios = $this->Ccptiposervicio->find('list', array());
		$ccptiposervicios = empty($ccptiposervicios)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccptiposervicios;
		$ccpsolicitudservicioestados = $this->Ccpsolicitudservicioestado->find('list', array('conditions'=>array()));
		$ccpsolicitudservicioestados = empty($ccpsolicitudservicioestados)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccpsolicitudservicioestados;
		$this->set(compact('ccptiposervicios','ccpsolicitudservicioestados'));
		
		$estadosOtCars = $this->Datos->_getDato('estadosOtCarsCcp');
		$estadosOtCarsTooltip = $this->Datos->_getDato('estadosOtCars');
		$estadosOtCarsSelect = empty($estadosOtCars)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$estadosOtCars;
		
		//obtenemos los estados de las ots en ccp
		$estadosCcp=$this->Ccptabestado->getEstados();
		$estadosCcpSelect =(!empty($estadosCcp) && isset($estadosCcp))?array(''=>__('Seleccionar'))+$estadosCcp:array(''=>__('Seleccionar'));

		$this->set('estadosCcpSelect',$estadosCcpSelect);
		$this->set('estadosOtCarsTooltip',$estadosOtCarsTooltip);
		$this->set('estadosOtCarsSelect',$estadosOtCarsSelect);
		$this->set('estadosOtCars',$estadosOtCars);
		
	}
	
	public function indexCoordinadorExportarExcel() {
		$this->layout = 'excel_db';
		
		$this->indexCoordinador($exportar=1,$parametros=$this->request->query);
		header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=coordinador.xls");
		
		$reporte = $this->listaFinalPDF;
		$this->set('reporte',$reporte);
	}
	
	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-04-22
	 * @return 
	 */
	function __datosVista(){
		$this->loadModel('Ccptipocliente');
		$this->loadModel('Ccptipocliente');
		$this->loadModel('Ccptiposervicio');
		$this->loadModel('Ccppedidorepuesto');
		$this->loadModel('Ccptablero');
		$this->loadModel('Talot');
   		
   		$ccptipoclientes = $this->Ccptipocliente->find('list', array('conditions'=>array('Ccptipocliente.estado'=>'AC')));
   		$ccptiposervicios = $this->Ccptiposervicio->find('list', array('conditions'=>array('Ccptiposervicio.estado'=>'AC')));
   		$ccppedidorespuestos = $this->Ccppedidorepuesto->find('list', array('conditions'=>array('Ccppedidorepuesto.estado'=>'AC')));
   		$ccptableros = $this->Ccptablero->find('list');
		
		// recuperamos las aseguradoras de talot
   		$ciaAseguradoras = $this->Talot->find('list',array(
			'conditions'=>array("CodSapCiaAseguradora <> '.'"),
			'fields'=>array('CodSapCiaAseguradora', 'CodSapCiaAseguradora')
		));
   		$ciaAseguradoras = array_merge($ciaAseguradoras, $this->Datos->_getDato('aseguradoras'));
		$aseguradoras = array(''=>__('Seleccionar'),'1'=>'PACIFICO','2'=>'RIMAC','3'=>'POSITIVA','4'=>'MAPFRE','5'=>'PUBLICO','6'=>'MAGALLANES');
   		$ccptipoclientes  = empty($ccptipoclientes)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccptipoclientes;
   		$ccptiposervicios  = empty($ccptiposervicios)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccptiposervicios;
   		$ccppedidorespuestos  = empty($ccppedidorespuestos)?array(''=>__('Seleccionar')):array(''=>__('Seleccionar'))+$ccppedidorespuestos;
   		
   		$this->set(compact('ccptipoclientes','ccptiposervicios','ccppedidorespuestos','ccptableros', 'aseguradoras'));
   	}
	
	/**AVENTURA
	 * VISTA PARA LA APROBACION DE LA SOLICITUD
	 */
   	public function aprobarSolicitud($solicitud_id){
   		//MODELOS UTILIZADOS
		$this->loadModel('Talot');
		$this->loadModel('Secproject');
//		$this->loadModel('Ccppresupuesto');
		$this->loadModel('Ccptablero');
		
		//DT HARD
		$dtLog = $this->_getDtLg();
		
		//RECUPERO EL ID DE LA SUCURSAR SEGUN CARS
		$project = $this->Secproject->findById($dtLog['Secproject']['id'], null,null,-1);
		$secproject_carscod = $project['Secproject']['carscod'];
		
		$solicitud = $this->Ccpsolicitudservicio->find('first',array('conditions'=>array('Ccpsolicitudservicio.id'=>$solicitud_id)));
		$ot_numero['0']['0']['ot'] = $solicitud['Ccpsolicitudservicio']['ot_numero'];
		
		//$ot_numero = $this->Ccppresupuesto->numeroOtPorPresupuesto($secproject_carscod, $solicitud['Ccpsolicitudservicio']['presupuesto_numero']);
		if(!empty($ot_numero)){
			$ot_numero = $ot_numero['0']['0']['ot'];
			$ot = $this->Talot->findByNrocars($ot_numero,array('Talot.id','Talot.nrocars'),null);
			$this->set('ot',$ot);
		}
		
		$ccptableros = $this->Ccptablero->find('list', array('conditions'=>array()));
		$this->set('ccptableros',$ccptableros);
		$this->set('solicitud',$solicitud);

		//pr($solicitud);
		if(!empty($this->request->data)){
			if(empty($ot)){
				$respuesta = array(false,'NO_EXISTE_LA_OT_RELACIONADA');
			}else{
				// GUARDAMOS LOS DATOS ENVIADOS POR EL FORMULARIO
				$this->Ccpsolicitudservicio->begin();
				$data = $this->request->data;
				$data['Ccpsolicitudservicio']['talot_id'] = $ot['Talot']['id'];
				$numero_ot = $solicitud['Ccpsolicitudservicio']['ot_numero'];
			
				$respuesta = $this->Ccpsolicitudservicio->aprobarSolicitud($data,$numero_ot,$dtLog);
				if(!$respuesta[0]){
					$this->Ccpsolicitudservicio->rollback();
					$respuesta[1] = $respuesta[1];// __('ERROR_AL_GUARDAR_EL_DOCUMENTO');
				}else{
					$this->request->data['guardado'] = true;
					$this->Ccpsolicitudservicio->commit();
					$this->Session->write('actualizarPadre', true);
					$respuesta[1] = __('DOCUMENTO_APROBADO');
				}
			}
			$this->Session->setFlash(__($respuesta[1]),empty($respuesta[0])?'flash_failure':'flash_success');
		}
   	}
	
	
	public function ampliarSolicitud($idsolicitudServicios=null){
		if(!empty($idsolicitudServicios) && isset($idsolicitudServicios)){
			if(!empty($this->request->data['Ccpsolicitudservicio'])){
				$this->Ccpsolicitudservicio->begin();
				//actualizamos la solicitud de servicio con el nuevo mensaje
				$this->Ccpsolicitudservicio->recursive=-1;
				$solicitud=$this->Ccpsolicitudservicio->findById($idsolicitudServicios);
				$this->request->data['Ccpsolicitudservicio']['comentario_ampliacion']=$this->request->data['Ccpsolicitudservicio']['comentario_ampliacion'].'</br> '.$solicitud['Ccpsolicitudservicio']['comentario_ampliacion'];
				if($this->Ccpsolicitudservicio->save($this->request->data['Ccpsolicitudservicio'])){
					$this->Ccpsolicitudservicio->commit();
					$this->Session->write('actualizarPadre', true);
					$this->redirect('/comun/cerrar');
				}else{
					$this->Session->setFlash(__('GENERAL_CORRIJA_ERRORES'));
					$this->Ccpsolicitudservicio->rollback();
				}
			}else{
				$this->request->data=$this->Ccpsolicitudservicio->read(null,$idsolicitudServicios);
				$this->request->data['Ccpsolicitudservicio']['comentario_ampliacion_antiguo']=$this->request->data['Ccpsolicitudservicio']['comentario_ampliacion'];
				$this->request->data['Ccpsolicitudservicio']['comentario_ampliacion']="";
			}
		}
		$this->set('idsolicitudServicios',$idsolicitudServicios);
	}
	
	/**AVENTURA : OBSERVA LA SOLICITUD
	 *  
	 */
	public function observarSolicitud($solicitud_id) {
		$this->set('solicitud_id',$solicitud_id);
		
		$this->layout = 'aventura';
		
		$solicitud = $this->Ccpsolicitudservicio->findById($solicitud_id);
		$this->__datosVista();
		$this->set('solicitud',$solicitud);
		
		//GUARDAMOS LA OBSERVACION DE LA SOLICITUD
		if(!empty($this->request->data)){
			$this->request->data['Ccpsolicitudservicio']['id'] = $solicitud_id;
	        $this->request->data['Ccpsolicitudservicio']['ccpsolicitudservicioestado_id'] = 3;
	        $this->request->data['Ccpsolicitudservicio']['fecha_observado'] = $this->Ccpsolicitudservicio->fechaHoraActual();
			  
	        if (!$this->Ccpsolicitudservicio->save($this->request->data['Ccpsolicitudservicio'])){
	        	$respuesta = array(false,'GENERAL_CORRIJA_ERRORES');
	        }else{
	        	$this->request->data['guardado'] = true;
				$this->Session->write('actualizarPadre', true);
				$respuesta = array(true,'SOLICITUD_OBSERVADA');
	        }
			$this->Session->setFlash(__($respuesta[1]),empty($respuesta[0])?'flash_failure':'flash_success');
		}
	}
	
	/**AVENTURA : HABILITAR LA SOLICITUD
	 *  
	 */
	public function habilitarSolicitud($solicitud_id) {
		$this->set('solicitud_id',$solicitud_id);
		
		$solicitud = $this->Ccpsolicitudservicio->findById($solicitud_id);
		if(!empty($solicitud)){
			$solicitud = $this->Ccpsolicitudservicio->cambiarFechasMysqlPhp($solicitud);
		}
		$this->__datosVista();
		$this->set('solicitud',$solicitud);
		
		//GUARDAMOS LA OBSERVACION DE LA SOLICITUD
		if(!empty($this->request->data)){
			$this->request->data['Ccpsolicitudservicio']['id'] = $solicitud_id;
	        $this->request->data['Ccpsolicitudservicio']['ccpsolicitudservicioestado_id'] = 4;
	        $this->request->data['Ccpsolicitudservicio']['fecha_habilitado'] = $this->Ccpsolicitudservicio->fechaHoraActual();
			
			if (!$this->Ccpsolicitudservicio->save($this->request->data['Ccpsolicitudservicio'])){
				$respuesta = array(false,'GENERAL_CORRIJA_ERRORES');
	        }else{
	        	$this->request->data['guardado'] = true;
				$this->Session->write('actualizarPadre', true);
				$respuesta = array(true,'SOLICITUD_HABILITADA');
	        }
			$this->Session->setFlash(__($respuesta[1]),empty($respuesta[0])?'flash_failure':'flash_success');
		}
	}
	
	/**AVENTURA
   	 * FORMULARIO PARA EL RECHAZO DE SOLICITUD
   	 */
   	public function rechazarSolicitud($solicitud_id){
   		//MODELOS UTILIZADOS
		$this->loadModel('Ccpmotivorechazo');
		
		//DT HARD
		$dtLog = $this->_getDtLg();
		
		if(!empty($this->request->data)){
			$this->Ccpsolicitudservicio->begin();
			// GUARDAMOS LOS DATOS ENVIADOS POR EL FORMULARIO
			$respuesta = $this->Ccpsolicitudservicio->rechazarSolicitud($this->request->data, $dtLog); 
			if(!$respuesta[0]){
				$this->Ccpsolicitudservicio->rollback();
			}else{
				$this->request->data['guardado'] = true;
				$this->Ccpsolicitudservicio->commit();
				$this->Session->write('actualizarPadre', true);
				$this->redirect('/comun/cerrar');
			}
			$this->Session->setFlash(__($respuesta[1]),empty($respuesta[0])?'flash_failure':'flash_success');
		}
		$this->__datosVista();
		$solicitud = $this->Ccpsolicitudservicio->findById($solicitud_id);
		$motivo_rechazo = $this->Ccpmotivorechazo->find_list();
   		$this->set('solicitud',$solicitud);
   		$this->set('motivo_rechazo',$motivo_rechazo);
   	}
}