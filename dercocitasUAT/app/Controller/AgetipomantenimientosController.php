<?php
class AgetipomantenimientosController extends AppController {

	public $name = 'Agetipomantenimientos';
	public $helpers = array('Html', 'Form');
	
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
		$this->Agetipomantenimiento->tiposervicio = array(
			0=>__('Mantenimiento Menor', true),
			1=>__('Mantenimiento Mayor'),
		);
    }
	
	public function index(){
		
		$this->Agetipomantenimiento->recursive = 0;
		$elementos = array('Agetipomantenimiento.description'=>__('AGE_TIPOMANTENIMIENTO_DESCRIPCION', TRUE),
						   'Agemotivoservicio.description'=>__('AGE_MOTIVOSERVICIO_DESCRIPCION', TRUE));
		$this->set('elementos',$elementos);		
		
		if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();		
		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Agetipomantenimiento.status'=>'DE') :
								array('Agetipomantenimiento.status'=>'AC');
		
		$conditions = $conditions + $conditionsActivos;		
		
		$fields=array('Agetipomantenimiento.id','Agetipomantenimiento.description','Agetipomantenimiento.status','Agemotivoservicio.description','Agetipomantenimiento.tiposervicio');
		
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Agetipomantenimiento.description' => 'asc'),
								'conditions' => $conditions,
								'fields' => $fields
								);
		
		$agetipomantenimientos=$this->paginate('Agetipomantenimiento');
		$this->set('agetipomantenimientos',$agetipomantenimientos);
		$this->set('tiposervicio',$this->Agetipomantenimiento->tiposervicio);
	}

	/**
     * Permite ingresar un nuevo tipo de mantenimiento.
     */ 
	function add() {
		$this->layout = 'contenido';
		if (!empty($this->request->data)) {
			if(!$this->Agetipomantenimiento->hasText($this->request->data['Agetipomantenimiento']['tiposervicio'])) 
				unset($this->request->data['Agetipomantenimiento']['tiposervicio']);
			$this->Agetipomantenimiento->create();
			if ($this->Agetipomantenimiento->save($this->request->data)) 
			{
				$this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO', true),'flash_success');
				$this->Session->write('actualizarPadre',true);	
				$this->Session->write($this->redirect(array('action'=>'view',$this->Agetipomantenimiento->getInsertID())));
			} else {
				$this->Session->setFlash(__('GENERAL_ERROR_GRABACION',true),'flash_failure');
			}
		}
		$agemotivoservicios = $this->Agetipomantenimiento->Agemotivoservicio->find('list',array('conditions'=>array('Agemotivoservicio.status'=>'AC')));
		$this->set('agemotivoservicios',$agemotivoservicios);
		$this->set('tiposervicio',$this->Agetipomantenimiento->tiposervicio);
	}
	
	 /* 
	 *
     * Esta función permite modificar los datos la descripcion del tipo mantenimientos.
     */	
	public function edit($id=true) 
        {
		 	$this->layout = 'contenido';
			if (!$id && empty($this->request->data)) {
				$this->Session->setflash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
				$this->redirect(array('action'=>'index'));
			}
	        
	      
	      if (!empty($this->request->data)) {
	      	
	      	if(!$this->Agetipomantenimiento->hasText($this->request->data['Agetipomantenimiento']['tiposervicio'])) {
	      		$this->request->data['Agetipomantenimiento']['tiposervicio']= null;
	      	}
	      		

			if ($this->Agetipomantenimiento->save($this->request->data)) 
				{			
					$this->Session->setFlash(__('GENERAL_REGISTRO_MODIFICADO', true),'flash_success');	
					$this->Session->write('actualizarPadre', true);	
				    $this->Session->write($this->redirect(array('action'=>'view',$this->request->data['Agetipomantenimiento']['id'])));				
			} 
			else 
			{
					$this->Session->setFlash(__('GENERAL_ERROR_GRABACION', true),'flash_failure');
	        }
		}
		if (empty($this->request->data)) {
			$this->request->data = $this->Agetipomantenimiento->read(null, $id);
		}
		
		$agemotivoservicios = $this->Agetipomantenimiento->Agemotivoservicio->find('list');
		$this->set('agemotivoservicios',$agemotivoservicios);
		$this->set('tiposervicio',$this->Agetipomantenimiento->tiposervicio);
	}
	
	/**
     * Mostrar Tipo mantenimienmto.
	 * @param string $id : id del tipo mantenimiento
     */    
	public function view($id=true) 
	{
	
		$this->layout = 'contenido';
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO',true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
		$this->set('agetipomantenimiento', $this->Agetipomantenimiento->read(null, $id));
		$this->set('tiposervicio',$this->Agetipomantenimiento->tiposervicio);
	}

	/**
     * Elimina un tipo de mantenimiento.
     * Reglas: 
     * 1. Un registro de tipo de mantenimiento. puede ser eliminada solo si no tiene datos asociados
	 * @param string $id : id del tipo de mantenimiento. que se desea eliminar
     */
	public function delete($id=null) {
		$estadoEliminado = 'EL';
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
				$this->request->data['Agetipomantenimiento']['id'] = $id;
				$this->request->data['Agetipomantenimiento']['status'] = $estadoEliminado;
				if ($this->Agetipomantenimiento->save($this->request->data['Agetipomantenimiento'])) {
					$this->Session->setFlash(__('GENERAL_REGISTRO_DESACTIVADO', true),'flash_success');	
				} else {
					$this->Session->setFlash(__('GENERAL_REGISTRO_ACTIVADO', true),'flash_failure');
				}
			}
			$this->redirect(array('action'=>'index'));
	}
	
	public function getTipoMantenimientoJson($motivoId){
		configure::write('debug',0);
		$this->layout = 'ajax';
		$tipos = $this->Agetipomantenimiento->find('all',array(
			'conditions'=>array('agemotivoservicio_id'=>$motivoId, 'status'=>'AC'),
			'recursive'=>-1
		));
		
		$responce->susses = empty($tipos)? false:true;
		$responce->errors = array('msg'=>__('NO_EXISTEN_TIPOS'));
		
		foreach($tipos as $key => $value){
			$responce->data[$value['Agetipomantenimiento']['id']] = array(
				'name'=>$value['Agetipomantenimiento']['description']
			);
		}
		
		echo json_encode($responce);
		$this->autoRender = false;
	}
}	
?>