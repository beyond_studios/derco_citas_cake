<?php
class CampaniasController extends AppController {
	public $name = 'Campanias';
    public $helpers = array('Html', 'Xhtml', 'Form', 'Idioma');
	var $components = array('RequestHandler', 'Imagenes');
	
	public function index() {
		$this->layout = "default_02";
		$this->paginate = array(
			'limit' => 4, 
			'page' => 1,
			'order' => array ('Campania.id' => 'desc'),
			'conditions'=>array('Campania.status'=>'AC')
		);
		
		$this->set('campanias',$this->paginate('Campania'));	
	}
	
	public function add() {
		$this->pageTitle = __('NUEVO SERVICIO');
		$this->layout='modulo_taller'.DS.'default_grid';
		
		if(!empty($this->request->data)){
			$dt = $this->request->data;
			
			/* GUARDAMOS LOS DATOS ENVIADOS POR EL FORMULARIO */
			$this->Campania->begin();
			
			$dt['Campania']['fileName'] = $this->Imagenes->subirImg($dt['Campania']['img'], 'img/Campanias/', 0);
			
			if((isset($dt['Campania']['img'])) && ($dt['Campania']['img'] !='') && !empty($dt['Campania']['fileName'])){
				//guardamos el archivo
				$dt['Campania']['extension'] = $dt['Campania']['img']['type'];
				$dt['Campania']['binario'] = '';
				
				if(!$this->Campania->save($dt['Campania'])){
					$this->Campania->rollback();
					$this->Session->setFlash($rpt['error'], 'flash_failure');
				}else{
					$this->Session->write('actualizarPadre', true);
					$this->Campania->commit();
					$this->Session->setFlash(__('datosGuardados'), 'flash_success');
				}
			}else{
				$this->Session->setFlash("VERIFIQUE LA IMAGEN ENVIADA", 'flash_failure');
			}
		}
	}
	
	function delete($campaniaId = null){
		$this->layout = 'modulo_taller'.DS.'default_grid';
		
		if (!$campaniaId) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
			$campania['Campania']['id'] = $campaniaId;
			$campania['Campania']['status'] = 'EL';
			
			$this->Campania->begin();
			if($this->Campania->save($campania)){
				$this->Campania->commit();
				$this->Session->write('actualizarPadre', true);
				$this->Session->setFlash("SERVICIO ELIMINADO", 'flash_failure');
			}else{
				$this->Campania->rollback();
				$this->Session->setFlash("NO SE PUDO ELIMINAR EL SERVICIO", 'flash_failure');
			}
		}
		
		$this->redirect(array('action'=>'index'));
	}
	
	public function view($id=0){
		$this->set('bodyClass','backRegister');
		$this->layout = 'modulo_taller'.DS.'web_cliente';
		$this->set('id',$id);
		$campanias = $this->Campania->find('all', array(
			'order'=>array('id'=>'desc'),
			'conditions'=>array('status'=>'AC'),
			'recursive'=>-1
		));
		
		$this->set('campanias' ,$campanias);
	}
}