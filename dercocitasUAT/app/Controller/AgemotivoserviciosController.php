<?php
class AgemotivoserviciosController extends AppController {

	public $name = 'Agemotivoservicios';
	public $helpers = array('Html', 'Form');
	
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
    }
	
	public function index(){
		
		$elementos = array('Agemotivoservicio.description'=>__('AGE_MOTIVOSERVICIO_DESCRIPCION', TRUE));
		$this->set('elementos',$elementos);		
		
		if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();		
		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Agemotivoservicio.status'=>'DE') :
								array('Agemotivoservicio.status'=>'AC');
		
		$conditions = $conditions + $conditionsActivos;		
		
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Agemotivoservicio.description' => 'asc'),
								'conditions' => $conditions
								);
		
		$agemotivoservicios=$this->paginate('Agemotivoservicio');
		$this->set('agemotivoservicios',$agemotivoservicios);
	}

	/**
     * Permite ingresar un nuevo motivo de servico.
     */    
	function add() 
	{
		$this->layout = 'contenido';
		
		if (!empty($this->request->data)) {
			$this->Agemotivoservicio->create();
			if ($this->Agemotivoservicio->save($this->request->data))
            {
				$this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO'),'flash_success');
				$this->Session->write('actualizarPadre',true);	
				$this->Session->write($this->redirect(array('action'=>'view',$this->Agemotivoservicio->getInsertID())));
			} 
             else 
            {
				$this->Session->setFlash(__('GENERAL_ERROR_GRABACION'),'flash_failure');
			}
		}
	}
	
	 /* 
	 *
     * Esta función permite modificar los datos de una marca.
	 Reglas: 
	 * 1. Una plantilla puede ser desactivado solo si no tiene cast_time activos
	 * @param string $id :  id del cast que se desea modificar
     */	
	 public function edit($id=true) 
        {
		 	$this->layout = 'contenido';
			if (!$id && empty($this->request->data)) {
				$this->Session->setflash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
				$this->redirect(array('action'=>'index'));
			}

			if (empty($this->request->data['Agemotivoservicio'])) {
					$this->request->data = $this->Agemotivoservicio->read(null, $id);
			} else {
	            $id = $this->request->data['Agemotivoservicio']['id'];

			if ($this->Agemotivoservicio->save($this->request->data['Agemotivoservicio'])) {			
					$this->Session->setFlash(__('GENERAL_REGISTRO_MODIFICADO', true),'flash_success');	
					$this->Session->write('actualizarPadre', true);
					$this->Session->write($this->redirect(array('action'=>'view',$this->request->data['Agemotivoservicio']['id'])));		
			} else {
					$this->Session->setFlash(__('GENERAL_ERROR_GRABACION', true),'flash_failure');
	        }
		}
	}
	
	/**
     * Mostrar Marca.
	 * @param string $id : id del calendario que se desea mostrar
     */    
	public function view($id=true) 
	{
	
		$this->layout = 'contenido';
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO',true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
		$this->set('agemotivoservicio', $this->Agemotivoservicio->read(null, $id));
		
	}

	/**
     * Elimina una marca.
     * Reglas: 
     * 1. Una plantilla puede ser eliminada solo si no tiene cast_times activos o desactivos
	 * @param string $id : id de la plantilla que se desea eliminar
     */
	public function delete($id=null) {
		$estadoEliminado = 'EL';
		$estadoActivo = 'AC';
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
			//Si existen registros asociados no se peuden eliminar---Miguel Chiuyari
			$existAgetiposervicios = $this->Agemotivoservicio->Agetiposervicio->find('count', array('conditions' => array('Agetiposervicio.status' => 'AC', 'Agetiposervicio.agemotivoservicio_id'=>$id)));
			$existAgetipomantenimientos = $this->Agemotivoservicio->Agetipomantenimiento->find('count', array('conditions' => array('Agetipomantenimiento.status' => 'AC', 'Agetipomantenimiento.agemotivoservicio_id'=>$id)));
			$existAgecitacalendarios = $this->Agemotivoservicio->Agecitacalendario->find('count', array('conditions' => array('Agecitacalendario.status' => 'AC', 'Agecitacalendario.agemotivoservicio_id'=>$id)));
			if($existAgetiposervicios||$existAgetipomantenimientos||$existAgecitacalendarios)
				{
				$this->Session->setFlash(__('GENERALES_REGISTRO_ASOCIADO',true),'flash_failure');				
				}
				else{	
					$this->request->data['Agemotivoservicio']['id'] = $id;
					$this->request->data['Agemotivoservicio']['status'] = $estadoEliminado;
					if ($this->Agemotivoservicio->save($this->request->data['Agemotivoservicio'])) {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ELIMINADO', true),'flash_success');	
					} else {
						$this->Session->setFlash(__('GENERAL_REGISTRO_ACTIVADO', true),'flash_failure');
					}
				}
			}
			$this->redirect(array('action'=>'index'));
	}
	
	public function getMotivoServicioJson($secprojectId,$marcaId=null){
		configure::write('debug',0);
		$this->layout = 'ajax';
		$motivos = $this->Agemotivoservicio->getMotivoServicio($secprojectId,$marcaId);
		
		$responce->susses = empty($motivos)? false:true;
		$responce->errors = array('msg'=>__('NO_EXISTEN_MOTIVOS'));
		
		foreach($motivos as $key => $value){
			$responce->data[$value['Agemotivoservicio']['id']] = array(
				'name'=>$value['Agemotivoservicio']['description'],
				'agecitacalendario_id'=>$value['Agecitacalendario']['id']
			);
		}
		
		echo json_encode($responce);
		$this->autoRender = false;
	}
}	
?>