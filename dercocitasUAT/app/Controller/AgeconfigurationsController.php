<?php
App::uses('AppController', 'Controller');
/**
 * Ageconfigurations Controller
 *
 * @property Ageconfiguration $Ageconfiguration
 */
class AgeconfigurationsController extends AppController {
	public $helpers = array('Js');

	public function agregarageconfiguration() {
		if ($this->request->is('post')) {
			if($this->Ageconfiguration->ageconfigurationRegistroAnterior($this->request->data))
			{
				$this->Ageconfiguration->create();
				if ($this->Ageconfiguration->save($this->request->data)) {
						$this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO'),'flash_success');
						$this->Session->write('actualizarPadre', true);	
						//$this->redirect(array('action' => 'agregarageconfiguration'));
				} else {
					$this->Session->setFlash(__('GENERAL_ERROR_GRABACION'),'flash_failure');
				}				
			}
			else {
				$this->Session->setFlash(__('GENERAL_REGISTRO_ANTERIOR_NO_ELIMINDADO'),'flash_failure');
			}

		}
		$this->Ageconfiguration->Secorganization->recursive = -1;	
		$organizations = $this->Ageconfiguration->Secorganization->find('all',array('conditions' => array('Secorganization.status' => 'AC')));
		foreach ($organizations as $value) {
			$secorganizations[$value['Secorganization']['id']] = $value['Secorganization']['name'];
		}
		$this->Ageconfiguration->Secrole->recursive = -1;	
		$roles = $this->Ageconfiguration->Secrole->find('all', array('conditions' => array('Secrole.status' => 'AC')));
		foreach ($roles as $value) {
			$secroles[$value['Secrole']['id']] = $value['Secrole']['name'];
		}
		$this->set(compact('secorganizations','secroles'));
	}

	function listroles()
	{
		$this->layout = "ajax";
		Configure::write('debug', '1');
		$roles = array();
			if(!empty($this->request->data)){
				$roles = $this->Ageconfiguration->Secrole->find('all',array('fields' => array('Secrole.id','Secrole.name'),
																'conditions' => array( 'Secrole.secorganization_id' => $this->request->data['Ageconfiguration']['secorganization_id'])));
			}
		foreach($roles as $value)
			$secroles[$value['Secrole']['id']] = $value['Secrole']['name'];
		
		$this->set('secroles',$secroles);
	}
}
