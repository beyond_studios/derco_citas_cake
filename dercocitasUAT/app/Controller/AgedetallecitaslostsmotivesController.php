<?php
class AgedetallecitaslostsmotivesController extends AppController {

	public $name = 'Agedetallecitaslostsmotives';
	public $helpers = array('Html', 'Form');
	
    public function beforeFilter() {
        parent::beforeFilter();
        //$this->Auth->allow();
    }
	
	public function index(){
		
		$elementos = array('Agedetallecitaslostsmotive.description'=>__('descipcion', TRUE));
		$this->set('elementos',$elementos);		
		
		if(!empty($this->params['named']['valor']) || !empty($this->params['named']['desactivo']))
		{
			$this->request->data['Buscar']['buscador'] = $this->params['named']['buscador'];
			$this->request->data['Buscar']['valor'] = $this->params['named']['valor'];
			$this->request->data['Buscar']['desactivo'] = $this->params['named']['desactivo'];
		}
		
		$valorDeBusqueda = isset($this->request->data['Buscar']['valor'])?trim($this->request->data['Buscar']['valor']):null;
		$conditions = !empty($valorDeBusqueda)?
						array($this->request->data['Buscar']['buscador'].' LIKE'=>'%'.trim($this->request->data['Buscar']['valor']).'%'):
						array();		
		
		$conditionsActivos = (!empty($this->request->data['Buscar']['desactivo']) == 1) ?
								array('Agedetallecitaslostsmotive.status'=>'DE') :
								array('Agedetallecitaslostsmotive.status'=>'AC');
		
		$conditions = $conditions + $conditionsActivos;		
		
		$this->paginate = array('limit' => 10,
								'page' => 1,
								'order' => array ('Agedetallecitaslostsmotive.description' => 'asc'),
								'conditions' => $conditions
								);
		
		$agedetallecitaslostsmotives=$this->paginate('Agedetallecitaslostsmotive');
		$this->set('agedetallecitaslostsmotives',$agedetallecitaslostsmotives);
	}

	/**
     * Permite ingresar un nuevo motivo de servico.
     */    
	function add() 
	{
		$this->layout = 'contenido';
		
		if (!empty($this->request->data)) {
			$this->Agedetallecitaslostsmotive->create();
			if ($this->Agedetallecitaslostsmotive->save($this->request->data))
            {
				$this->Session->setFlash(__('GENERAL_REGISTRO_AGREGADO'),'flash_success');
				$this->Session->write('actualizarPadre',true);	
				$this->Session->write($this->redirect(array('action'=>'view',$this->Agedetallecitaslostsmotive->getInsertID())));
			} 
             else 
            {
				$this->Session->setFlash(__('GENERAL_ERROR_GRABACION'),'flash_failure');
			}
		}
	}
	
	 /* 
	 *
     * Esta función permite modificar los datos de una marca.
	 Reglas: 
	 * 1. Una plantilla puede ser desactivado solo si no tiene cast_time activos
	 * @param string $id :  id del cast que se desea modificar
     */	
	 public function edit($id=true) 
        {
		 	$this->layout = 'contenido';
			if (!$id && empty($this->request->data)) {
				$this->Session->setflash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
				$this->redirect(array('action'=>'index'));
			}

			if (empty($this->request->data['Agedetallecitaslostsmotive'])) {
					$this->request->data = $this->Agedetallecitaslostsmotive->read(null, $id);
			} else {
	            $id = $this->request->data['Agedetallecitaslostsmotive']['id'];

			if ($this->Agedetallecitaslostsmotive->save($this->request->data['Agedetallecitaslostsmotive'])) {			
					$this->Session->setFlash(__('GENERAL_REGISTRO_MODIFICADO', true),'flash_success');	
					$this->Session->write('actualizarPadre', true);
					$this->Session->write($this->redirect(array('action'=>'view',$this->request->data['Agedetallecitaslostsmotive']['id'])));		
			} else {
					$this->Session->setFlash(__('GENERAL_ERROR_GRABACION', true),'flash_failure');
	        }
		}
	}
	
	/**
     * Mostrar Marca.
	 * @param string $id : id del calendario que se desea mostrar
     */    
	public function view($id=true) 
	{
	
		$this->layout = 'contenido';
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO',true),'flash_failure');
			$this->redirect(array('action'=>'index'));
		}
		$this->set('agedetallecitaslostsmotive', $this->Agedetallecitaslostsmotive->read(null, $id));
		
	}

	/**
     * Elimina una marca.
     * Reglas: 
     * 1. Una plantilla puede ser eliminada solo si no tiene cast_times activos o desactivos
	 * @param string $id : id de la plantilla que se desea eliminar
     */
	public function delete($id=null) {
		$estadoEliminado = 'DE';
		$estadoActivo = 'AC';
		if (!$id) {
			$this->Session->setFlash(__('GENERALES_VALOR_NO_VALIDO', true),'flash_failure');
		}else{
			$this->request->data['Agedetallecitaslostsmotive']['id'] = $id;
			$this->request->data['Agedetallecitaslostsmotive']['status'] = $estadoEliminado;
			if ($this->Agedetallecitaslostsmotive->save($this->request->data['Agedetallecitaslostsmotive'])) {
				$this->Session->setFlash(__('GENERAL_REGISTRO_ELIMINADO', true));	
			} else {
				$this->Session->setFlash(__('GENERAL_REGISTRO_ACTIVADO', true));
			}
			$this->redirect(array('action'=>'index'));
		}
	}
	
	public function getMotivoServicioJson($secprojectId,$marcaId=null){
		configure::write('debug',0);
		$this->layout = 'ajax';
		$motivos = $this->Agedetallecitaslostsmotive->getMotivoServicio($secprojectId,$marcaId);
		
		$responce->susses = empty($motivos)? false:true;
		$responce->errors = array('msg'=>__('NO_EXISTEN_MOTIVOS'));
		
		foreach($motivos as $key => $value){
			$responce->data[$value['Agedetallecitaslostsmotive']['id']] = array(
				'name'=>$value['Agedetallecitaslostsmotive']['description'],
				'agecitacalendario_id'=>$value['Agecitacalendario']['id']
			);
		}
		
		echo json_encode($responce);
		$this->autoRender = false;
	}
}	
?>