<!-- JS/CSS UTILIZADOS -->
<?php $this->layout = 'aventura';  
	echo $this->Html->script('jquery/jquery.alerts.js');
	echo $this->Html->css('jquery/jquery.alerts');
	echo $this->Html->script('ccpsolicitudservicios/set_solicitud.js');
?>
<!-- DIALOGOS A UTILIZAR -->
<div id="dialog_presupuesto" title="<?php echo __('Buscar_presupuesto'); ?>">
	<div id="containerpresupuestos">
		<table id="presupuestos"></table> 
		<div id="presupuestos-pager"></div>
	</div>
</div>

<!-- CONTENIDO DE LA PAGINA --> 
<?php echo $this->Session->flash(); ?>
<div id="flashMessage" class="message" style="display: none;"><?php echo __('GENERAL_SELECCIONAR_ITEM'); ?></div>
<?php  if(empty($talot)){ ?>
	<div style="text-align:center;" class="accion">
		<div id="flashMessage" class="message"><?php echo 'REGISTRO SIN ACTUALIZACION'; ?></div>
		<br/>
        <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()'));?>
	</div>
<?php }else{ ?>
	<h2><?php echo __('SOLICITUD_DE_SERVICIO-TIKET'); ?></h2>
	
	<!-- CREACION DEL FORMULARIO -->
	<form id="agregarForm"  action="<?php echo $this->Html->url('/ccpsolicitudservicios/setSolicitud/').$nro_cars?>" method="post" enctype="multipart/form-data">
	<?php echo $this->Form->hidden('ccpsolicitudservicios.presupuesto_fecha_aprobacion', array('value'=>$this->Avhtml->date_dmY($primerPresupuesto['pre_fecha_aprobacion']))); ?>
	<div class="span-23">
		<div id="reserva" class="span-11 prepend-1" style="float: left;">
			<fieldset>
				<legend style="font-weight: bold;"><?php echo __('Datos_Ot'); ?></legend>
				<table>
					<tbody>
						<tr>
							<td class="etiqueta span-5"><?php echo __('Ot'); ?></td>
							<td class="valor span-5 last">
								<?php echo $this->Form->input('ccpsolicitudservicios.ot_numero', array('class'=>'required readonly','value'=>$primerPresupuesto['ot_numero'], 'readonly'=>true, 'div'=>false, 'label'=>false)); ?>
								<!--<?php echo $this->Xhtml->image('aventura/search_icon.gif', array('onclick' => "abrirDialog('dialog_presupuesto')")); ?>--> 
								<span class="campoObligatorio">*</span>
								<div><label for="ccpsolicitudserviciosOtNumero" generated="true" class="error" style="display: none;"></label></div>
							</td>
						</tr>

						<tr>
							<td class="etiqueta span-5"><?php echo __('Cliente'); ?></td>
							<td class="valor span-5 last">
								<?php echo $this->Form->input('ccpsolicitudservicios.ot_cliente', array('class'=>'readonly','value'=>$primerPresupuesto['ot_cliente'],'size'=>'20', 'readonly'=>true, 'div'=>false, 'label'=>false)); ?>
							</td>
						</tr>
						<tr>
							<td class="etiqueta span-5"><?php echo __('Asesor'); ?></td>
							<td class="valor span-5 last"><?php echo $this->Form->input('ccpsolicitudservicios.ot_asesor', array('class'=>'readonly','value'=>$primerPresupuesto['ot_asesor'],'size'=>'20', 'readonly'=>true, 'div'=>false, 'label'=>false)); ?></td>
						</tr>
						<tr>
							<td class="etiqueta span-5"><?php echo __('Placa'); ?></td>
							<td class="valor span-5 last"><?php echo $this->Form->input('ccpsolicitudservicios.ot_placa', array('class'=>'readonly','value'=>$primerPresupuesto['ot_placa'],'size'=>'20', 'readonly'=>true, 'div'=>false, 'label'=>false)); ?></td>
						</tr>
						<tr>
							<td class="etiqueta span-5"><?php echo __('Fecha_Ot'); ?></td>
							<td class="valor span-5 last"><?php echo $this->Form->input('ccpsolicitudservicios.ot_fecha_creacion', array('class'=>'readonly','value'=>$primerPresupuesto['ot_fecha_creacion'],'size'=>'20', 'readonly'=>true, 'div'=>false, 'label'=>false)); ?></td>
						</tr>
						<tr>
							<td class="etiqueta span-5"><?php echo __('Fecha_cliente_entrega'); ?></td>
							<td class="valor span-5 last"><?php echo $this->Form->input('ccpsolicitudservicios.ot_fecha_entrega', array('class'=>'readonly','value'=>$primerPresupuesto['ot_fecha_entrega'],'size'=>'20', 'readonly'=>true, 'div'=>false, 'label'=>false)); ?></td>
						</tr>
					</tbody>
				</table>
			</fieldset>
			
			<div id="presupuesto">
				<fieldset>
					<legend style="font-weight: bold;"><?php echo __('CCP_PRESUPUESTO'); ?></legend>
					<table>
						<tbody>
							<tr>
								<td class="etiqueta span-5"><?php echo __('Presupuesto'); ?></td>
								<td class="valor span-5 last">
									<?php echo $this->Form->input('ccpsolicitudservicios.presupuesto_numero', array('class'=>'required digits', 'minlength'=>9, 'size'=>'20'
																,'value'=>empty($this->data['ccpsolicitudservicios']['presupuesto_numero'])?($ampliacion)?'':$primerPresupuesto['presupuesto_numero']:$this->data['ccpsolicitudservicios']['presupuesto_numero']
																,'onkeyUp'=>'verificarPresupuesto($(this))', 'div'=>false, 'label'=>false)); ?> 
									<span class="campoObligatorio">*</span>
									<span id="imgDetPresupuesto" class="hide">
										<?php 
											echo $this->Xhtml->imagelink('detalle.gif', __('Mano obra'),
							                	'javascript:;',array('onclick'=>"verDetalle()"),
							                	null, array('width'=>'16'));	
										 ?>
									</span>
									<div><label for="ccpsolicitudserviciosPresupuestoNumero" generated="true" class="error" style="display: none;"></label></div>																
								</td>
							</tr>
							<?php if ($ampliacion){?> 					
							<tr>
								<td class="etiqueta span-5"><?php echo __('CCP_PRESUPUESTO_FECHA_LISTO'); ?></td>
								<td class="valor span-5 last"><?php echo $this->Form->input('ccpsolicitudservicios.fechapresupuestolisto', array('class'=>'dialog_fecha required', 'div'=>false, 'label'=>false
															,'value'=>empty($this->data['ccpsolicitudservicios']['fechapresupuestolisto'])?($ampliacion)?'':$primerPresupuesto['FECHA_PRESUP']:$this->data['ccpsolicitudservicios']['fechapresupuestolisto'],'size'=>'20','maxlength'=>'10')); ?>
									<span class="campoObligatorio">*</span>
									<div><label for="ccpsolicitudserviciosFechapresupuestolisto" generated="true" class="error" style="display: none;"></label></div>
								</td>
							</tr>
							<tr>
								<td class="etiqueta span-5"><?php echo __('CCP_PRESUPUESTO_APROBACION_COMPANIA'); ?></td>
								<td class="valor span-5 last"><?php echo $this->Form->input('ccpsolicitudservicios.fechaaprobacioncompania', array('class'=>'dialog_fecha required', 'div'=>false, 'label'=>false
															,'value'=>empty($this->data['ccpsolicitudservicios']['fechaaprobacioncompania'])?($ampliacion)?'':$primerPresupuesto['ot_fecha_aprobacion_compania']:$this->data['ccpsolicitudservicios']['fechaaprobacioncompania'],'size'=>'20','maxlength'=>'10')); ?>
									<span class="campoObligatorio">*</span>
									<div><label for="ccpsolicitudserviciosFechaaprobacioncompania" generated="true" class="error" style="display: none;"></label></div>
								</td>
							</tr>
							<?php } else {?> 
							<tr>
								<td class="etiqueta span-5"><?php echo __('CCP_PRESUPUESTO_FECHA_LISTO'); ?></td>
								<td class="valor span-5 last"><?php echo $this->Form->input('ccpsolicitudservicios.fechapresupuestolisto', array('class'=>'', 'div'=>false, 'label'=>false
															,'value'=>empty($this->data['ccpsolicitudservicios']['fechapresupuestolisto'])?$primerPresupuesto['FECHA_PRESUP']:$this->data['ccpsolicitudservicios']['fechapresupuestolisto'],'size'=>'20','maxlength'=>'10','readonly'=>true)); ?>
									<!--<span class="campoObligatorio">*</span>
									<div><label for="ccpsolicitudserviciosFechapresupuestolisto" generated="true" class="error" style="display: none;"></label></div>
									-->
								</td>
							</tr>
							<tr>
								<td class="etiqueta span-5"><?php echo __('CCP_PRESUPUESTO_APROBACION_COMPANIA'); ?></td>
								<td class="valor span-5 last"><?php echo $this->Form->input('ccpsolicitudservicios.fechaaprobacioncompania', array('class'=>'', 'div'=>false, 'label'=>false
															,'value'=>empty($this->data['ccpsolicitudservicios']['fechaaprobacioncompania'])?$primerPresupuesto['ot_fecha_aprobacion_compania']:$this->data['ccpsolicitudservicios']['fechaaprobacioncompania'],'size'=>'20','maxlength'=>'10','readonly'=>true)); ?>
<!--									<span class="campoObligatorio">*</span>
									<div><label for="ccpsolicitudserviciosFechaaprobacioncompania" generated="true" class="error" style="display: none;"></label></div>
									-->
								</td>
							</tr>
							<?php }	?>						
						</tbody>
					</table>
				</fieldset>
			</div>
			
			<div id="horas">
				<fieldset>
					<legend style="font-weight: bold;"><?php echo __('horas'); ?></legend>
					<table>
						<tbody>
							<tr>
								<td class="etiqueta span-5"><?php echo __('horasPlanchado'); ?></td>
								<td class="valor span-5 last">
									<?php echo $this->Form->input('ccpsolicitudservicios.horas_planchado', array('class'=>'required number', 'size'=>'20', 'div'=>false, 'label'=>false
																,'value'=>empty($this->data['ccpsolicitudservicios']['horas_planchado'])?'':$this->data['ccpsolicitudservicios']['horas_planchado'])); ?> 
									<span class="campoObligatorio">*</span>
									<div><label for="ccpsolicitudserviciosHorasPlanchado" generated="true" class="error" style="display: none;"></label></div>																
								</td>
							</tr>		
							<tr>
								<td class="etiqueta span-5"><?php echo __('horasPanio'); ?></td>
								<td class="valor span-5 last">
									<?php echo $this->Form->input('ccpsolicitudservicios.horas_panio', array('class'=>'required number', 'size'=>'20', 'div'=>false, 'label'=>false
																,'value'=>empty($this->data['ccpsolicitudservicios']['horas_panio'])?'':$this->data['ccpsolicitudservicios']['horas_panio'])); ?> 
									<span class="campoObligatorio">*</span>
									<div><label for="ccpsolicitudserviciosHorasPanio" generated="true" class="error" style="display: none;"></label></div>																
								</td>
							</tr>	
						</tbody>
					</table>
				</fieldset>
			</div>
					
			<div id="autenticacion">
				<fieldset>
					<legend style="font-weight: bold;"><?php echo __('Autenticacion_asesor_comercial'); ?></legend>
					<table>
						<tbody>
							<tr>
								<td class="etiqueta span-4"><?php echo __('Usuario'); ?></td>
								<td class="valor span-6 last"><?php echo $this->Form->input('ccpsolicitudservicios.usuario', array('class'=>'readonly','value'=>$this->data['logueado'], 'size'=>'20', 'readonly'=>true, 'div'=>false, 'label'=>false)); ?>
								</td>
							</tr>
							<tr>
								<td class="etiqueta span-4"><?php echo __('Contrasenia'); ?></td>
								<td class="valor span-6 last"><?php echo $this->Form->input('ccpsolicitudservicios.contrasenia', array('class'=>'required','type'=>'password','size'=>'20', 'div'=>false, 'label'=>false)); ?>
									<span class="campoObligatorio">*</span>
									<div class="clear"></div>
									<label for="ccpsolicitudserviciosContrasenia" generated="true" class="error" style="display: none;"></label>
								</td>
							</tr>

						</tbody>
					</table>
				</fieldset>
			</div>
		</div>
		
		<div id="tipo" class="span-11 last" style="float: left;">
			<fieldset>
				<legend style="font-weight: bold;"><?php echo __('Servicio'); ?></legend>
				<table id="contacto">
					<tbody>
						<tr>
							<td class="etiqueta span-5"><?php echo __('aseguradora'); ?></td>
							<td class="valor span-5 last">
								<!--<?php echo $this->Form->select('Talot.CodSAPCiaAseguradora',$ciaAseguradoras,array('empty'=>__('seleccionar', true),'class'=>'span-4 last required'));?>-->
								<?php echo $this->Form->select('ccpsolicitudservicios.CodSAPCiaAseguradora',$aseguradoras,array('empty'=>__('seleccionar', true),'class'=>'span-4 last required'));?>
								<span class="campoObligatorio">*</span>
								<div class="clear"></div>
								<div><label for="ccpsolicitudserviciosCodSAPCiaAseguradora" generated="true" class="error" style="display: none;"></label></div>
							</td>
						</tr>
						<tr>
							<td class="etiqueta span-5"><?php echo __('Tipo_cliente'); ?></td>
							<td class="valor span-5 last">
								<?php echo $this->Form->select('ccpsolicitudservicios.ccptipocliente_id',$ccptipoclientes,array('empty'=>__('seleccionar', true),'class'=>'span-4 last required'));?>
								<span class="campoObligatorio">*</span>
								<div class="clear"></div>
								<div><label for="ccpsolicitudserviciosCcptipoclienteId" generated="true" class="error" style="display: none;"></label></div>
							</td>
						</tr>
						
						<tr>
							<td class="etiqueta span-5"><?php echo __('Tipo_servicio'); ?></td>
							<td class="valor span-5 last">
								<?php echo $this->Form->select('ccpsolicitudservicios.ccptiposervicio_id',$ccptiposervicios,array('empty'=>__('seleccionar', true),'class'=>'span-4 last required'));?>
								<span class="campoObligatorio">*</span>
								<div class="clear"></div>
								<div><label for="ccpsolicitudserviciosCcptiposervicioId" generated="true" class="error" style="display: none;"></label></div>
							</td>
						</tr>
						<tr>
							<td class="etiqueta span-5"><?php echo __('Pedido_repuestos'); ?></td>
							<td class="valor span-5 last">
								<?php echo $this->Form->select('ccpsolicitudservicios.ccppedidorepuesto_id',$ccppedidorespuestos,array('empty'=>__('seleccionar', true),'class'=>'span-4 last required'));?>
								<span class="campoObligatorio">*</span>
								<div class="clear"></div>
								<div><label for="ccpsolicitudserviciosCcppedidorepuestoId" generated="true" class="error" style="display: none;"></label></div>							</td>
						</tr>
						
						<tr>
							<td class="etiqueta span-5"><?php echo __('Servicio'); ?></td>
							<td class="valor span-5 last">
								<table id="tableros">
									<tbody><?php //debug($ccptableros); ?>
										<?php foreach($ccptableros as $key => $tablero): ?>
										<tr>
											<td><?php echo $this->Form->checkbox('ccptableros.servicio_'.$key, array('value'=>$key, 'options'=>$tablero)); ?></td>
											<td><?php echo $tablero; ?></td>
										</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td class="etiqueta span-5"><?php echo __('Fecha_propuesta_entrega'); ?></td>
							<td class="valor span-5 last"><?php echo $this->Form->input('ccpsolicitudservicios.fecha_propuesta_entrega', array('class'=>'dialog_fecha required','size'=>'20','maxlength'=>'10', 'div'=>false, 'label'=>false)); ?>
								<span class="campoObligatorio">*</span>
								<div><label for="ccpsolicitudserviciosFechaPropuestaEntrega" generated="true" class="error" style="display: none;"></label></div>
							</td>
						</tr>
					</tbody>
				</table>
			</fieldset>
			
			<fieldset>
				<legend style="font-weight: bold;"><?php echo __('CCP_DOCUMENTOS'); ?></legend>
				<table id="documentos">
					<tbody>
						<?php if(!empty($rechazado) && isset($rechazado)){?>
							<tr>
								<td class="etiqueta span-4"><?php echo __('CCP_DOCUMENTOS_PRESUPUESTO_AJUSTADO'); ?></td>
								<td class="valor span-6 last">
									<?php echo $this->Form->input('ccpsolicitudservicios.archivopresupuesto',array('size'=>'15','readonly'=>true,'value'=>$archivoSolicitud,'class'=>'required', 'div'=>false, 'label'=>false));?>
									<?php echo $this->Xhtml->imagelink('pdf.png', __('Imprimir_PDF'),'javascript:;',
																array('onclick'=>"verPDF('".$archivoSolicitud."')"),null, array('width'=>'16')); ?>									
									<?php echo $this->Xhtml->imagelink('retirar.png', __('CCP_ELIMINAR_ARCHIVO'),'javascript:;',
																array('onclick'=>"eliminarArchivoPresupuesto(this)"),null, array('width'=>'16')); ?>									
									<span class="campoObligatorio">*</span>
									<div class="clear"></div>
									<div><label for="ccpsolicitudserviciosCcparchivopresupuesto" generated="true" class="error" style="display: none;"></label></div>
								</td>
							</tr>
							<?php if(!$ampliacion){ ?>
							<tr>
								<td class="etiqueta span-4"><?php echo __('CCP_DOCUMENTOS_ORDEN_RECEPCION'); ?></td>
								<td class="valor span-6 last">
									<?php echo $this->Form->input('ccpsolicitudservicios.archivoorden',array('size'=>'15','readonly'=>true,'value'=>$archivoOrden,'class'=>'required', 'div'=>false, 'label'=>false));?>
									<?php echo $this->Xhtml->imagelink('pdf.png', __('Imprimir_PDF'),'javascript:;',
																array('onclick'=>"verPDF('".$archivoOrden."')"),null, array('width'=>'16')); ?>									
									<?php echo $this->Xhtml->imagelink('retirar.png', __('CCP_ELIMINAR_ARCHIVO'),'javascript:;',
																array('onclick'=>"eliminarArchivoOrden(this)"),null, array('width'=>'16')); ?>
									<span class="campoObligatorio">*</span>
									<div class="clear"></div>
									<div><label for="ccpsolicitudserviciosCcparchivoorden" generated="true" class="error" style="display: none;"></label></div>
								</td>
							</tr>
						<?php } ?>
						<?php }else{?>
							<tr>
								<td class="etiqueta span-4"><?php echo __('CCP_DOCUMENTOS_PRESUPUESTO_AJUSTADO'); ?></td>
								<td class="valor span-6 last">
									<?php echo $this->Form->input('ccpsolicitudservicios.ccparchivopresupuesto',array('type'=>'file','size'=>'15','class'=>'required', 'div'=>false, 'label'=>false));?>
									<span class="campoObligatorio">*</span>
									<div class="clear"></div>
									<div><label for="ccpsolicitudserviciosCcparchivopresupuesto" generated="true" class="error" style="display: none;"></label></div>
								</td>
							</tr>
							<?php if(!$ampliacion){ ?>
							<tr>
								<td class="etiqueta span-4"><?php echo __('CCP_DOCUMENTOS_ORDEN_RECEPCION'); ?></td>
								<td class="valor span-6 last">
									<?php echo $this->Form->input('ccpsolicitudservicios.ccparchivoorden',array('type'=>'file','size'=>'15','class'=>'required', 'div'=>false, 'label'=>false));?>
									<span class="campoObligatorio">*</span>
									<div class="clear"></div>
									<div><label for="ccpsolicitudserviciosCcparchivoorden" generated="true" class="error" style="display: none;"></label></div>
								</td>
							</tr>
							<?php } ?>						
						<?php } ?>
					</tbody>
				</table>
			</fieldset>
			
			<div id="comentario">
				<fieldset>
					<legend style="font-weight: bold;"><?php echo __('Comentario'); ?></legend>
					<TEXTAREA NAME="data[ccpsolicitudservicios][comentario_emitido]" COLS=45 ROWS=1><?php echo empty($this->data['ccpsolicitudservicios']['comentario_emitido'])?'':$this->data['ccpsolicitudservicios']['comentario_emitido']; ?></TEXTAREA>
				</fieldset>
			</div>
			
			
		</div>
		<div class="clear"></div>
		
		<div style="text-align:center;" class="accion">
	        <?php if(empty($this->data['guardado'])) echo $this->Form->submit(__('GENERAL_ENVIAR'), array('class'=>'guardar', 'div'=>false, 'label'=>false));?>
	        <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
		</div>
	</div>
	</form>
	<?php echo $this->element('actualizar_padre') ?>
	<!-- FIN FORMULARIO -->
<?php } ?>
<script>
	function verPDF(nombrePDF){
	    var url = "/solicitud/"+nombrePDF;
	    var w = window.open(url, 'verPDF', 
	        'scrollbars=yes,resizable=yes,width=940,height=500,top=115,left=200,status=no,location=no,toolbar=no');
	}
	
	function verDetalle(){
		var nroPresuspuesto = $('#ccpsolicitudserviciosPresupuestoNumero').val();
	    var url = "ccpsolicitudservicios/getDetallePresupuesto?n="+nroPresuspuesto;
	    var w = window.open(url, 'verDetalle', 
	        'scrollbars=yes,resizable=yes,width=540,height=400,top=115,left=200,status=no,location=no,toolbar=no');
	}
	
	function verificarPresupuesto(miThis){
		if(miThis.val() == ""){
			//$('#imgDetPresupuesto').addClass('hide');
		}else{
		//	$('#imgDetPresupuesto').removeClass('hide');
		}
	}
</script>