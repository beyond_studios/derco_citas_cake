<?php echo $this->Html->script('Agedetallecitaslostslosts/delete.js'); ?>

<?php echo $this->Session->flash();?>
<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th colspan="2" class="titulo">
                <?php echo __('ELIMINAR_CITA') ?>
            </th>
        </tr>
    </thead>
</table>

<?php echo $this->element('solicitud_mostrar_cita') ?>
<?php echo $this->Form->create('Agedetallecitaslost',array('id'=>'delete', 'url'=>"delete/$detallecita_id", 'type'=>'post')); ?>
<?php echo $this->Form->hidden('Agedetallecitaslost.id', array('value'=>$detallecita_id)) ?>
<div>
	<table id="formularioEdicion" border="0" cellspacing="0" cellpadding="0" align="center" width="90%">
		<tbody >
			<tr>
				<td class="etiqueta"><?php echo __('comentarioEliminar',true); ?></td>	
				<td class="valor">
					<textarea name="data[Agedetallecitaslost][deletecomment]" class="required msg_error_1" style="height: 50px;padding: 5px;width: 550px;"><?php echo empty($dt_original)?"":trim($dt_original['Agedetallecitaslost']['deletecomment']); ?></textarea>
					<span class="error">*</span>
				</td>				
			</tr>
			<tr>
				<td colspan="2" style="text-align:center;">
					<?php echo $this->Form->submit(__('AGE_DETALLE_CITA_CONFIRM_ACEPTAR'), array('label'=>false,'div'=>false,'class'=>'guardar', 'id'=>'buttonConfirmSubmit'));?>
					<?php echo "&nbsp;&nbsp;&nbsp;".$this->Form->button(__('AGE_DETALLE_CITA_CONFIRM_CANCELAR'), array('type'=>'button','div'=>false,'class'=>'cancelar', 'div'=>false, 'onclick'=>'window.close();'));?>
				</td>
	        </tr>
		</tbody>
	</table>
<br/>
<br/>
<?php echo $this->Form->end(); ?>
</div>