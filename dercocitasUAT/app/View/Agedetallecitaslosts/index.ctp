<?php echo $this->Html->script('Agedetallecitaslosts/index.js'); ?>
<?php echo $this->Html->script('jqueryUI/jquery-ui-1.8.custom.min.js'); ?>
<?php echo $this->Html->css('theme/redmond/jquery-ui-1.7.2.custom.css'); ?> 
		
<?php echo $this->Session->flash();  ?>
<br/><h3 id="tituloTable"><?php echo __('Citas de Taller - Call Center');?></h3>
	
<?php echo $this->Form->create('bsc',array('url' => 'index', 'id'=>'f_bsc'));	?>
<div class="box">	
	<?php if(false): ?>
	<div style="width:25%;float:left;margin-bottom:3px !important;" class="box">
		<div style="padding-bottom:4px;margin-bottom:4px;border-bottom: 1px dashed #369;">
			<div style="width:60%;float:left;">
				<?php echo __('AGE_FECHA', TRUE).':&nbsp;&nbsp;' ?>
				<?php echo $this->Form->radio('f_campo',$f_campo, array('legend'=>false,'div'=>false, 'separator'=>'&nbsp;&nbsp;&nbsp;')); ?>
			</div>
			<div style="width:37%; text-align:right; float:left;">
				<?php 	$url = $this->Html->url(array('action'=>'agregarCita'));  
						echo $this->Html->link($this->Html->image('agregar.png', array('alt' => 'Agregar')).__('Add', true), 'javascript:;',
										 array('onclick' => "add('".$url."')",'escape'=>false), null);
				?>
			</div>
			<div class="clear"></div>
		</div>	
		
		<div style="width:25%;float:left;"><?php echo __('desde', TRUE) ?></div>
		<div style="width:65%;float:left;">
			<?php echo $this->Form->input('f_ini', array('value'=>null ,'id'=>'f_ini',  'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
			<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_ini').val('');")); ?>
		</div>
		
		<div style="width:25%;float:left;"><?php echo __('hasta', TRUE) ?></div>
		<div style="width:65%;float:left;">
			<?php echo $this->Form->input('f_fin', array('value'=>null ,'id'=>'f_fin', 'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
			<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_fin').val('');")); ?>
		</div>
		<div class="clear"></div>
	</div>
	<?php endif; ?>
	
	<div style="width:98%;float:left;margin:0px 0px 3px 3px !important;height:81px; " class="box">
		<div style="width:100%">
			<table>
				<tbody>
					<tr>
						<td><strong><?php echo __('Documento', TRUE) ?></strong></td>
						<td><?php echo $this->Form->input('documento_numero', array('div'=>false, 'label'=>false, 'style'=>'width:150px;')); ?></td>
						
						<td><strong><?php echo __('TALLER_ETIQUETA_TALLER_SUCURSAL', TRUE) ?></strong></td>
						<td><?php echo $this->Form->input('secproject_id',array('type'=>'select', 'onChange'=>'getMotivoServicio(this)', 'options'=>$talleres, 'label' => false, 'empty'=>__('-- Todos --',true), 'div' => false,'style'=>'width:200px;')); ?></td>
						
						<td><strong><?php echo __('TALLER_ETIQUETA_TALLER_TIPO_MANTENIMIENTO', TRUE) ?></strong></td>
						<td>
							<?php echo $this->Form->input('agetipomantenimiento_id',array('type'=>'select', 'options'=>$agetipomantenimientos, 'id'=>'tipoMantenimiento','label' => false, 'empty'=>__('-- Todos --',true), 'div' => false,'style'=>'width:200px;')); ?>
							<?php echo $this->Html->image('derco/defecto/grid-loading.gif', array('class'=>"hide")); ?>
						</td>
					</tr>
					<tr>
						<td><strong><?php echo __('Cliente', TRUE) ?></strong></td>
						<td><?php echo $this->Form->input('apellidoPaterno', array('div'=>false, 'label'=>false, 'style'=>'width:200px;')); ?></td>
						
						<td><strong><?php echo __('AGE_MOTIVOSERVICIO_DESCRIPCION', TRUE) ?></strong></td>
						<td>
							<?php echo $this->Form->input('agemotivoservicio_id',array('type'=>'select', 'onChange'=>'getTipoMantenimiento(this)', 'options'=>$agemotivoservicios, 'id'=>'motivoServicio','label' => false, 'empty'=>__('-- Todos --',true), 'div' => false,'style'=>'width:200px;')); ?>
							<?php echo $this->Html->image('derco/defecto/grid-loading.gif', array('class'=>"hide")); ?>
						</td>
						
						<td><strong><?php echo __('Motivo no Registra Cita', TRUE) ?></strong></td>
						<td>
							<?php echo $this->Form->input('agedetallecitaslostsmotive_id',array('type'=>'select','options'=>$motivonocitas, 'id'=>'motivoNoRegistra','label' => false, 'empty'=>__('-- Todos --',true), 'div' => false,'style'=>'width:200px;')); ?>
							<?php echo $this->Html->image('derco/defecto/grid-loading.gif', array('class'=>"hide")); ?>
						</td>
						
					</tr>
					<tr>
						<td><strong><?php echo __('Placa', TRUE) ?></strong></td>
						<td><?php echo $this->Form->input('placa', array('div'=>false, 'label'=>false, 'style'=>'width:200px;')); ?></td>
						
						<td><strong><?php echo __('ORDEN_ESTANDAR_ETIQUETA_TIPO_SERVICIO', TRUE) ?></strong></td>
						<td>
							<?php echo $this->Form->input('agetiposervicio_id',array('type'=>'select', 'onChange'=>'verTipomantenimiento(this)', 'options'=>$agetiposervicios, 'id'=>'tipoServicio','label' => false, 'empty'=>__('-- Todos --',true), 'div' => false,'style'=>'width:200px;')); ?>
							<?php echo $this->Html->image('derco/defecto/grid-loading.gif', array('class'=>"hide")); ?>
						</td>
						
						<td>
							<?php 	$url = $this->Html->url(array('action'=>'agregarCita'));  
									echo $this->Html->link($this->Html->image('agregar.png', array('alt' => 'Agregar')).__('Add', true), 'javascript:;',
													 array('onclick' => "add('".$url."')",'escape'=>false), null);
							?>
						</td>
						<td style="text-align:right;"><?php echo $this->Form->submit(__('Search',true),array('style'=>'width:100px;','div'=>false)); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="clear"></div>
</div>	
<?php echo $this->Form->end(); ?>
		
<table cellpadding="0" cellspacing="0" class="table" >
	<thead>
	<tr>                     
        <th class="span-4"><?php echo $this->Paginator->sort(__('Apallidos y Nombres / Razón social',true));?></th>
		<th><?php echo $this->Paginator->sort('Agedetallecitaslost.placa',__('Placa',true));?></th>
		<th class="span-3"><?php echo $this->Paginator->sort('Secproject.name',__('Sucursal',true));?></th> 
		<th><?php echo $this->Paginator->sort('Agetiposervicio.description',__('Tipo Servicio',true));?></th> 
		<th><?php echo $this->Paginator->sort('Agetipomantenimiento.description',__('Tipo Mantenimiento',true));?></th> 
		<th><?php echo $this->Paginator->sort('Agemotivoservicio.description',__('Motivo Servicio',true));?></th> 
		<th class="actionsfijo"><?php  echo __('Actions');?></th>
	</tr>			
	</thead>
	
	<tbody>
	<?php  foreach ($agedetallecitaslosts as $agedetallecitaslost):?>
	<tr>
		 <td>
			<?php 	if(!empty($agedetallecitaslost['Cliente']['nombres']))
						echo $agedetallecitaslost['Cliente']['nombres']; 					
					else
						echo $agedetallecitaslost['Cliente']['razonSocial']; ?>	
		</td>
		<td><?php 	echo $agedetallecitaslost['Agedetallecitaslost']['placa']; ?></td>
		<td><?php 	echo $agedetallecitaslost['Secproject']['name']; ?></td>
		<td><?php 	echo $agedetallecitaslost['Agetiposervicio']['description']; ?></td>
		<td><?php 	echo $agedetallecitaslost['Agetipomantenimiento']['description']; ?></td>
		<td><?php 	echo $agedetallecitaslost['Agemotivoservicio']['description']; ?></td>
		<td class="actionsfijo">
		<?php
			echo "&nbsp;";	
				$url = $this->Html->url(array('action'=>'mostrarCita/'.$agedetallecitaslost['Agedetallecitaslost']['id']));  
				echo $this->Html->link($this->Html->image('mostrar.png', array('alt' => 'Ver', 'title'=>'Ver')),
								 'javascript:;',
								 array('onclick' => "mostrar('".$url."')",'escape'=>false),
								 null);
								 
				echo (true)?'':$this->Html->link(
					$this->Html->image('eliminar.png', array('title'=>__('desactivar',true), "alt" => "Eliminar")), 
					array('action'=>'delete', $agedetallecitaslost['Agedetallecitaslost']['id']), array('escape'=>false), sprintf(__('estaSeguroEliminar %s?', $agedetallecitaslost['Agedetallecitaslost']['id']))
				); 	 
		?>	
		</td>
	</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>