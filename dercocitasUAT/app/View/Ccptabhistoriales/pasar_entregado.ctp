<!-- JS/CSS UTILIZADOS -->
<?php $this->Html->script('ccptabhistorial/enviar_deposito.js'); ?>
<!-- CONTENIDO DE LA PAGINA --> 
<?php 	$optionsCombo=array("style"=>"width:120px","0"=>true);//cambiar el 0 por disablesd
		$optionsInput=array('readonly'=>false);
		$optionsCheckBox=false;
		$optionsInputFecha=array('size'=>10,'readonly'=>false);
		$optionsInputDisabled=array('readonly'=>true);
		$optionsInputMedio=array('size'=>30);

?>
<!-- CREACION DEL FORMULARIO -->	
<div id="flashMessage" class="message" style="display: none;"><?php echo __('GENERAL_SELECCIONAR_ITEM'); ?></div>
<h2><?php echo __('CCP_PASAR_ENTREGADO'); ?></h2>

<form id="Ccptabhistorial" action="<?php echo $this->Html->url('pasarEntregado/').$ot_numero?>" method="post">
	<?php echo $this->Form->hidden('Ccptabhistorial.ot_numero',array('value'=>$ot_numero)); ?>
<div class="span-9">
	<div style="text-align:center;">
		<table align="center">
			<tbody>
				<tr>
					<td colspan="2" class="centrado" style="font-weight:bolder;" ><?php  echo 'OT '.$ot_numero?></td>
				</tr>
				<tr>
					<td class="etiqueta span-3"><?php echo __('CCP_ESTADO_ACTUAL'); ?></td>
					<td class="valor span-6 last">
						<?php echo $this->Form->input('Ccptabhistorial.ccptabestado_anterior',array('type'=>'select','options'=>$estadoAnterior,'disabled'=>true,'class'=>'required readonly','size'=>'', 'readonly'=>true, 'div'=>false, 'label'=>false)); ?>
						<?php foreach($estadoAnterior as $id=>$item){?>
							<?php echo $this->Form->hidden('Ccptabhistorial.ccptabestado_anterior',array('value'=>$id)); ?>
						<?php }?>						
						<span class="campoObligatorio">*</span>
						<div><label for="CcptabhistorialCcptabestadoAnterior" generated="true" class="error" style="display: none;"></label></div>
					</td>
				</tr>
				<tr>
					<td class="etiqueta span-3"><?php echo __('CCP_ESTADO_SIGUIENTE'); ?></td>
					<td class="valor span-6 last">
						<?php echo $this->Form->input('Ccptabhistorial.ccptabestado_id',array('type'=>'select','options'=>$estadoPasarA,'disabled'=>true,'class'=>'required readonly','readonly'=>true, 'div'=>false, 'label'=>false)); ?>
						<?php foreach($estadoPasarA as $id=>$item){?>
							<?php echo $this->Form->hidden('Ccptabhistorial.ccptabestado_id',array('value'=>$id)); ?>
						<?php }?>
						<span class="campoObligatorio">*</span>
						<div><label for="CcptabhistorialCcptabestadoId" generated="true" class="error" style="display: none;"></label></div>						
					</td>
				</tr>
				<tr>
					<td class="etiqueta span-3"><?php echo __('Comentario'); ?></td>
					<td class="valor span-6 last">
						<?php echo $this->Form->input('Ccptabhistorial.comentario', array('type'=>'textarea','cols'=>19,'rows'=>5, 'div'=>false, 'label'=>false)); ?>
						<div><label for="CcptabhistorialComentario" generated="true" class="error" style="display: none;"></label></div>						
					</td>
				</tr>
	
			</tbody>
		</table>
	</div>
	<div style="text-align:center;" class="accion">
        <?php if(empty($guardado)) echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'guardar' , 'div'=>false, 'label'=>false));?>
        <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar','onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
	</div>	
</div>
</form>
<script type="text/javascript">
actualizarPadre();
function actualizarPadre() {
    var guardado = "<?php echo $guardado?>";
	if(guardado=="1"){
		window.opener.location.href = window.opener.location.href;
	} 
    
}

</script>