<?php echo $this->Html->script('jqueryUI/jquery-ui-1.8.custom.min.js'); ?>
<?php echo $this->Html->css('theme/redmond/jquery-ui-1.7.2.custom.css'); ?> 
<?php echo $this->Html->css('modulo_taller/tooltip.css'); ?> 
<?php echo $this->Html->script('ccpsolicitudservicios/index.js'); ?>
		
<?php echo $this->Session->flash();  ?>
<br/><h3 id="tituloTable"><?php echo __('CCP_BANDEJA_ASESOR_SERVICIO_CLIENTE');?></h3>
<style>
	.Detener div {
		display: block;
		position: absolute;
		border: 1px solid black;
		z-index: 6;
		width: 525px;
		background-color: gray;
		color: white;
		text-align: center;
}
</style>
<!-- IMPLEMENTACION DEL BUSCADOR -->
<form id="principalForm" method="get" action="<?php echo $this->Html->url('indexAsesor') ?>">	
<div id="buscadorBasico_" class="box">
	<div id="criteriosBusqueda">
		<table id="formBuscador" border="0" cellspacing="0" cellpadding="0" align="center" style="width:100%">
			<input type="hidden" size=15 name="click" id="formBuscadorClick" value="<?php echo empty($data['click'])?'':$data['click']; ?>" size="10"/>
			<tbody>
	        	<tr>
	            	<td class="etiqueta"><?php echo __('FechaSol').':';?>&nbsp;<?php echo $this->Form->label('formBuscador.fechaIni', __('Desde'));?></td>
	            	<td class="valor">
	            		<?php echo $this->Form->input('fechaIni', array('name'=>'fechaIni','value'=>empty($data['fechaIni'])?'':$data['fechaIni'] ,'id'=>'f_ini',  'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
						<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_ini').val('');")); ?>
	            	</td>
					
	            	<td class="etiqueta"><?php echo $this->Form->label('formBuscador.fechaFin', __('Hasta'));?></td>
					<td class="valor">
	                	<?php echo $this->Form->input('fechaFin', array('name'=>'fechaFin','value'=>empty($data['fechaFin'])?'':$data['fechaFin'] ,'id'=>'f_fin', 'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
						<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_fin').val('');")); ?>
	            	</td>
					
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.asesor_id', __('Asesor'));?></td>
					<td  class="valor">
						<select id="formBuscadorAsesorId" value="<?php echo empty($data['asesor_id'])?'':$data['asesor_id']; ?>" style="width:125px" name="asesor_id">
							<?php foreach($asesoresCars as $id => $value): 
								if(empty($data)) echo '<option value="'.$id.'">'.$value.'</option>';
								else{
									if($data['asesor_id'] != $id) echo '<option value="'.$id.'">'.$value.'</option>';
									else echo '<option selected="selected" value="'.$id.'">'.$value.'</option>';
								}
							endforeach; ?>
						</select>					
					</td>
					
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.ccptiposervicio_id', __('Tipo_servicio'));?></td>
					<td  class="valor">
						<select id="formBuscadorCcptiposervicioId" value="<?php echo empty($data['ccptiposervicio_id'])?'':$data['ccptiposervicio_id']; ?>" style="width:125px" name="ccptiposervicio_id">
							<?php foreach($ccptiposervicios as $id => $value): 
								if(empty($data)) echo '<option value="'.$id.'">'.$value.'</option>';
								else{
									if($data['ccptiposervicio_id'] != $id) echo '<option value="'.$id.'">'.($value).'</option>';
									else echo '<option selected="selected" value="'.$id.'">'.($value).'</option>';
								}
							endforeach; ?>
						</select>					
					</td>
					
					<td class="centrado"><?php echo $this->Form->submit(__('Search'), array('style'=>'width:100px'));?></td>		
				</tr>
				
				<tr>
					<td class="etiqueta"><?php echo __('FechaOt').':';?>&nbsp;<?php echo $this->Form->label('formBuscador.fechaIniOt', __('Desde'));?></td>
	            	<td class="valor">
	            		<?php echo $this->Form->input('fechaIniOt', array('name'=>'fechaIniOt','value'=>empty($data['fechaIniOt'])?'':$data['fechaIniOt'] ,'id'=>'f_ini_2',  'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
						<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_ini_2').val('');")); ?>
	            	</td>
					
					
	            	<td class="etiqueta"><?php echo $this->Form->label('formBuscador.fechaFinOt', __('Hasta'));?></td>
	            	<td class="valor">
	                	<?php echo $this->Form->input('fechaFinOt', array('name'=>'fechaFinOt','value'=>empty($data['fechaFinOt'])?'':$data['fechaFinOt'] ,'id'=>'f_fin_2', 'div'=>false, 'label'=>false, 'readonly'=>true, 'style'=>'width:70%;')); ?>
						<?php echo $this->Html->image('derco/defecto/calendar_disabled.gif', array('style' => 'cursor: pointer;', 'onclick' => "javascript:$('#f_fin_2').val('');")); ?>
	            	</td>
					
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.carsestado_id', __('CCP_ESTADO_OT_CARS'));?></td>
					<td  class="valor">
						<select id="formBuscadorCarsestadoId" value="<?php echo empty($data['carsestado_id'])?'':$data['carsestado_id']; ?>" style="width:125px" name="carsestado_id">
							<?php foreach($estadosOtCarsSelect as $id => $value): 
								if(empty($data['carsestado_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
								else{
									if($data['carsestado_id'] != $id) echo '<option value="'.$id.'">'.utf8_encode($value).'</option>';
									else echo '<option selected="selected" value="'.$id.'">'.utf8_encode($value).'</option>';
								}
							endforeach; ?>
						</select>					
					</td>
					
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.ccpsolicitudservicioestado_id', __('estadoSolicitud'));?></td>
					<td  class="valor">
						<select id="formBuscadorCcpsolicitudservicioestadoId" value="<?php echo empty($data['ccpsolicitudservicioestado_id'])?'':$data['ccpsolicitudservicioestado_id']; ?>" style="width:125px" name="ccpsolicitudservicioestado_id">
							<?php foreach($ccpsolicitudservicioestados as $id => $value): 
								if(empty($data['ccpsolicitudservicioestado_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
								else{
									if($data['ccpsolicitudservicioestado_id'] != $id) echo '<option value="'.$id.'">'.$value.'</option>';
									else echo '<option selected="selected" value="'.$id.'">'.$value.'</option>';
								}
							endforeach; ?>
						</select>					
					</td>
					</td>
					<td class="centrado">
						<?php 
						//Exportar para Excel
			    		$iconoExportarExcel = $this->Xhtml->image('derco/defecto/MicrosoftExcelDocument.gif', array(
			    			'title'=>__('REPORTE_EXPORTAR_EXCEL'), 
							'border'=>'0',
							'width'=>'16px',
							'height'=>'16px'
						));
						if(!empty($otsCars)){
						echo $this->Html->link($iconoExportarExcel, 'javascript:;', array(
								'onmouseover' => "return cambiarStatusMsg('".__('REPORTE_EXPORTAR_EXCEL')."');", 
								'onmouseout' => "return cambiarStatusMsg('');", 
								'onclick'=>"exportarExcel()", 'escape'=>false), null, false);
						}
						?>						
					</td>	
				</tr>
				
				<tr>
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.nrocars', __('Nro_OT'));?></td>
	            	<td class="valor">
	            		<input type="text" size=15 name="nrocars" id="formBuscadorNrocars" value="<?php echo empty($data['nrocars'])?'':$data['nrocars']; ?>" size="10"/>
	            	</td>
					
					<!-- buscador por placa -->
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.placa', __('CCP_PLACA'));?></td>
	            	<td class="valor">
	            		<input type="text" size=15 name="placa" id="formBuscadorPlaca" value="<?php echo empty($data['placa'])?'':$data['placa']; ?>" size="10"/>
	            	</td>					
					
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.preestado_id', __('estadoPorPresupuesto'));?></td>
					<td  class="valor">
						<select id="formBuscadorPreestadoId" value="<?php echo empty($data['preestado_id'])?'':$data['preestado_id']; ?>" style="width:125px" name="preestado_id">
							<?php foreach($estadoPorPresupuestos as $id => $value): 
								if(empty($data['preestado_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
								else{
									if($data['preestado_id'] != $id) echo '<option value="'.$id.'">'.utf8_encode($value).'</option>';
									else echo '<option selected="selected" value="'.$id.'">'.utf8_encode($value).'</option>';
								}
							endforeach; ?>
						</select>					
					</td>
					<!-- buscador por estado -->
					<td class="etiqueta"><?php echo $this->Form->label('formBuscador.estadootccp_id', __('CCP_ESTADO_OT_CCP'));?></td>
	            	<td class="valor">
						<select id="formBuscadorEstadootccpId" value="<?php echo (!empty($data['estadootccp_id']) && isset($data['estadootccp_id']))?(($data['estadootccp_id']=='9,10,22')?'':$data['estadootccp_id']):''; ?>" style="width:125px" name="estadootccp_id">
							<?php foreach($estadosCcpSelect as $id => $value): 
								if(empty($data['estadootccp_id'])) echo '<option value="'.$id.'">'.$value.'</option>';
								else{
									if($data['estadootccp_id'] != $id) echo '<option value="'.$id.'">'.$value.'</option>';
									elseif($data['estadootccp_id']=='9,10,22') echo '<option value="'.$id.'">'.$value.'</option>';
										else echo '<option selected="selected" value="'.$id.'">'.$value.'</option>';
								}
							endforeach; ?>
						</select>							
	            	</td>	
					<td class="centrado" style="text-decoration: underline;">
						<strong>
							<?php
							//Link  para historial de  documentos
                          	  $url = $this->Html->url(array('controller'=>'Ccptabhistoriales','action'=>'indexDocumentos'));
                          	  echo $this->Html->link(__('Historial de Documentos',true), 'javascript:;',array('onclick' => "verDocumentosOts('".$url."')",'escape'=>false), null);                           
                			?> 
						</strong>	
					</td>	
				</tr>
			</tbody>
		</table>
	</div>
</div>
</form>
<!-- FIN - IMPLEMENTACION DEL BUSCADOR -->

<div id="listaCaja">
	<table id="listaPrincipal" cellpadding="0" cellspacing="0">
		<thead>
		    <tr>
				<th><?php echo $this->Paginator->sort('ot_numero', __('Nro_Ot'))?></th>
				<th><?php echo $this->Paginator->sort('CLIENTE', __('Cliente'))?></th>
				<th><?php echo $this->Paginator->sort('PLACA', __('CCP_PLACA'))?></th>
				<th><?php echo $this->Paginator->sort('FECHA', __('fechaAperturaOt'))?></th>
				<th><?php echo $this->Paginator->sort('FECHA_APROB_ASEG', __('Fecha_aprobacion_presupuesto'))?></th>
				<th><?php echo __('horasPlanchado') ?></th>
				<th><?php echo __('horasPanio') ?></th>
				<th><?php echo __('Fecha_solicitud') ?></th>
				<th><?php echo __('Tipo_servicio') ?></th>
				<th><?php echo __('EstadoPres.') ?></th>
				<th><?php echo __('CCP_ESTADO_CCP') ?></th>
				<th><?php echo __('GENERAL_ACCIONES')?></th>
		    </tr>
		</thead>
		<tbody>
			<?php $i=0;?>
		    <?php foreach ($otsCars as $otCars): ?>
		    <tr>
		        <td class="texto">
		        	<?php $i++; ?>
		        	<?php 
						if($i>15){
							$onmouseover = "tooltipArriba";
						}elseif($i>10){
							$onmouseover = "tooltipMedio";
						} else{
							$onmouseover = "tooltipAbajo";
						}
					?>
					
		        	<div class="ToolText" href="#"><?php  echo $otCars['Talot']['ot_numero']; ?></div>
					<div class="<?php echo "tooltip $onmouseover" ?>">
						<span id='<?php echo $otCars['Talot']['ot_numero'].'TTP'?>' style="margin: 5px; background-color: red;">  
							<?php if(!empty($otCars['tooltip'])): ?>
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tbody>
									<tr>
										<td class="detalles etiqueta" width="63px" style="border: hidden;"><?php echo __('OT_CAMPO_PROPIETARIO')?></td>
										<td class="detalles valor" width="285px" style="border: hidden;"><?php echo utf8_encode($otCars['tooltip']['Talot']['ot_cliente']); ?></td>
										<td class="detalles etiqueta" width="66px" style="border: hidden;"><?php echo __('OT_CAMPO_FECHA_ORDEN')?></td>
										<td class="detalles valor" width="100px" style="border: hidden;"><?php echo substr($otCars['tooltip']['Talot']['ot_fecha_creacion'],0,10); ?> <?php echo $otCars['tooltip']['Talot']['ot_hora_recibida']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_ETIQUETA_RESPONSABLE_ASESOR')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_nombre_asesor']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_ETIQUETA_FECHA_A_ENTREGA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo substr($otCars['tooltip']['Talot']['ot_fecha_entrega'],0,10); ?> <?php echo $otCars['tooltip']['Talot']['ot_hora_prometida']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;">
											<?php echo __('OT_CAMPO_DESCRIPCION')?>
											<b style="color: yellow;"><?php echo utf8_encode($estadosOtCarsTooltip[$otCars['tooltip']['Talot']['ot_estado']]); ?></b>
										</td>
										<td class="detalles valor" colspan="3" style="padding-bottom: 5px;">
											<div class="div_textarea" style="padding: 2px; overflow: auto; height: 90px; width: 405px;">
												<?php echo trim(utf8_encode($otCars['tooltip']['Talot']['ot_descripcion'])); ?>
							            	</div>
										</td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_MARCA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_marca']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_PLACA')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_placa']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_MODELO')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_modelo']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_NROCARS')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_numero']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_VERSION')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_version']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('OT_CAMPO_NROCONO')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_numero_cono']; ?></td>
									</tr>
									<tr>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('Tipo_OT')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['ot_tipo_ot']; ?></td>
										<td class="detalles etiqueta" style="border: hidden;"><?php echo __('CCPSOLICITUD_TABLERO_ETIQUETA_CHASIS')?></td>
										<td class="detalles valor" style="border: hidden;"><?php echo $otCars['tooltip']['Talot']['chasis_vin']; ?></td>
									</tr>
									<tr style="border: hidden;">
										<td class="detalles etiqueta" style="border: hidden;"></td>
										<td class="detalles valor" style="border: hidden;">
											<?php //echo $this->Form->button(__('MAQUINARIAS_ENVIAR_A_ENTREGA'), array('onclick'=>'enviar_a_entrega('.$otCars['tooltip']['Talot']['id'].', "'.$otCars['tooltip']['Talot']['nrocars'].'")'));?>							
										</td>
									</tr>
								</tbody>
							</table>
							<?php endif; ?>
						</span>
					</div>
				</td>
		        <td class="texto"><?php echo utf8_encode($otCars['Talot']['ot_cliente']); ?></td>
		        <td class="texto"><?php echo ($otCars['Talot']['ot_placa']); ?></td>
				<td style="text-align:center;background:<?php echo $otCars['Talot']['color']?>" class="texto"><?php echo substr($otCars['Talot']['ot_fecha_creacion'],0,10); ?></td>
		        <td style="text-align:center;" class="texto">
					<?php $fecha = ''; 
						if(substr($otCars['Talot']['pre_fecha_aprobacion'],6,10) >= 2000) $fecha = substr($otCars['Talot']['pre_fecha_aprobacion'],0,10);
					echo $fecha; ?>
				</td>
				<td style="text-align:center;" class="texto"><?php echo empty($otCars['Ccpsolicitudservicio'])?'':$otCars['Ccpsolicitudservicio']['horas_planchado']; ?></td>
				<td style="text-align:center;" class="texto"><?php echo empty($otCars['Ccpsolicitudservicio'])?'':$otCars['Ccpsolicitudservicio']['horas_panio']; ?></td>
		        <td style="text-align:center;" class="texto"><?php echo empty($otCars['Ccpsolicitudservicio'])?'':(date('d-m-Y',strtotime(substr($otCars['Ccpsolicitudservicio']['fecha_emitido'],0,10)))); ?></td>
		        <td class="centrado"><?php echo empty($otCars['Ccptiposervicio'])?'': ($otCars['Ccptiposervicio']['descripcion']); ?></td>
		        <td class="centrado"><?php echo $estadoPorPresupuestos[$otCars['Talot']['ot_pre_estado']]; ?></td>
				<!--<td class="centrado"><?php //echo empty($otCars['Ccpsolicitudservicioestado'])?'':$otCars['Ccpsolicitudservicioestado']['descripcion']; ?></td>-->
				<td class="centrado">
					<?php
					if(!empty($otCars['Ccpsolicitudservicio'])){
						if($otCars['Ccptabestado']['id']==3){
							if($otCars['Ccpsolicitudservicioestado']['id']==3 || $otCars['Ccpsolicitudservicioestado']['id']==4 || $otCars['Ccpsolicitudservicioestado']['id']==5){
								echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'].' - '.$otCars['Ccpsolicitudservicioestado']['descripcion'];
							}else echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'];
						}else{
							if($otCars['Ccpsolicitudservicioestado']['id']==5){
								echo 'Con Solicitud - Rechazado';
							}else echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'];
						}
					} else echo empty($otCars['Ccptabestado'])?'':$otCars['Ccptabestado']['descripcion'];
					?>
				</td>
		        <td class="accion">
		            <?php 
						if(!empty($otCars['tooltip']['Talot']['acta'])){
							echo $this->Xhtml->imagelink('derco/defecto/modificar.png', __('Crear_acta'),
					               	'javascript:;',array('onclick'=>"crearActa('".$otCars['Talot']['ot_numero']."')"),
					               	null, array('width'=>'16'));
							echo '&nbsp';
						}
						if(!empty($otCars['Ccpsolicitudservicio']['id'])){
	
							echo $this->Xhtml->imagelink('derco/defecto/mostrar.png', __('GENERAL_MOSTRAR'),
								                	'javascript:;',array('onclick'=>"mostrar('".$otCars['Ccpsolicitudservicio']['id']."')"),
								                	null, array('width'=>'16'));
							echo '&nbsp';
							if(!empty($otCars['Ccptabestado']) && isset($otCars['Ccptabestado'])){
								/*if($otCars['Ccptabestado']['id']==9){
									echo $this->Xhtml->imagelink('blue_check.png', __('CCP_PASAR_CC_ASESOR'),
						                	'javascript:;',array('onclick'=>"pasar_asesor('".$otCars['Talot']['ot_numero']."')"),
						                	null, array('width'=>'16'));
									echo '&nbsp';
								}*/

								/*
								 * RTINEO : //MODIFICADO EN LA SEGUNDA PARTE DEL TABLERO
								 */
								if($otCars['Ccptabestado']['id']==7 || $otCars['Ccptabestado']['id']==22){ 
									echo $this->Xhtml->imagelink('derco/defecto/ok_siguiente.png', __('CCP_PASAR_ENTREGADO'),
						                	'javascript:;',array('onclick'=>"pasar_entregado('".$otCars['Talot']['ot_numero']."')"),
						                	null, array('width'=>'16'));
									echo '&nbsp';
								}							
							}
						}
					?>
		        </td>
		    </tr>
		    <?php endforeach; ?>
		</tbody>
	</table>
</div>

<div class="clear"></div>
<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>

<script type="text/javascript">
	// create popups for each element with a title attribute
	colorearTabla('listaPrincipal');
	var click = false;
	function agregar(ot_nro) {
	    var url = "<?php echo $this->Html->url('/ccpsolicitudservicios/setSolicitud/')?>"+ot_nro;
	    var w = window.open(url, 'agregar', 
	        'scrollbars=yes,resizable=yes,width=940,height=500,top=115,left=200,status=no,location=no,toolbar=no');
	}
	
	function crearActa(ot_nro) {
	    var url = "<?php echo $this->Html->url('/talactas/agregar/')?>"+ot_nro;
	    var w = window.open(url, 'agregar', 
	        'scrollbars=yes,resizable=yes,width=650,height=500,top=115,left=200,status=no,location=no,toolbar=no');
	}
	
	function mostrar(id) {
	    var url = "<?php echo $this->Html->url('/ccpsolicitudservicios/mostrar')?>/"+id;
	    var w = window.open(url, 'mostrar', 
	        'scrollbars=yes,resizable=yes,width=940,height=480,top=115,left=200,status=no,location=no,toolbar=no');
	}
	
	function mostrar_presupuesto(url) {
		var w = window.open(url,'mostrar','scrollbars=yes,resizable=yes,width=450,height=300,top=150,left=300,status=no,location=no,toolbar=no');
	}
	
	function habilitarSolicitud(id) {
	    var url = "<?php echo $this->Html->url('/ccpsolicitudservicios/habilitarSolicitud')?>/"+id;
	    var w = window.open(url, 'mostrar', 
	        'scrollbars=yes,resizable=yes,width=940,height=600,top=100,left=200,status=no,location=no,toolbar=no');
	}
	
	
    function muestraOculta() {
		var id = (this.id).split('_')[1];
		
		$('contenidos_'+id).toggle();
		$('enlace_'+id).innerHTML = (!$('contenidos_'+id).visible()) ? 'Ocultar contenidos' : 'Mostrar contenidos';
	}
	
	function pasar_asesor(ot_numero){
	    var url = "<?php echo $this->Html->url('/ccptabhistoriales/pasarAsesor')?>/"+ot_numero;
	    var w = window.open(url, 'pasarAsesor', 
	        'scrollbars=yes,resizable=yes,width=400,height=300,top=500,left=800,status=no,location=no,toolbar=no');
		
	}
	function pasar_entregado(ot_numero){
	    var url = "<?php echo $this->Html->url('/ccptabhistoriales/pasarEntregado')?>/"+ot_numero;
	    var w = window.open(url, 'pasarEntregado', 
	        'scrollbars=yes,resizable=yes,width=400,height=300,top=500,left=800,status=no,location=no,toolbar=no');
		
	}
	
	
	function exportarExcel(){
		var actionExel = "<?php echo $this->Html->url('/ccptabhistoriales/indexAsesorExportarExcel')?>";
		
	    var w = window.open('' , 'exportalExcel', 'scrollbars=yes,resizable=yes,width=100,height=100,status=no,location=no,toolbar=no');
		var actionBsc = $('#principalForm').attr('action');
		$('#principalForm').attr('action', actionExel);
		$('#principalForm').attr('target', 'exportalExcel');
		$('#principalForm').submit();
		$('#principalForm').attr('action', actionBsc);	
		$('#principalForm').attr('target', '');
	}
	
	$('principalForm').onsubmit = function() {
		$('formBuscadorClick').value = 1;
		return true;
	}
	function verDocumentosOts(url){
	    var w = window.open(url, 'verhistorial', 
	        'scrollbars=yes,resizable=yes,width=940,height=500,top=115,left=200,status=no,location=no,toolbar=no');

	}
</script>