<script>
	window.close();
</script>
<div id="cuerpo">
    <table border="0" width="100%">
    	<tr>
    		<td colspan="12"><?php echo __('CCP_BANDEJA_RESPONSABLE_DEPOSITO')?></td>
        </tr>
		<tr>
    		<td colspan="2"><?php echo __('placa')?></td>
    		<td colspan="3"><?php echo empty($dt['buscador']['placa'])?'--':$dt['buscador']['placa'] ?></td>
        </tr>		
        <tr>
    		<td colspan="2"><?php echo __('nroOt')?></td>
    		<td colspan="3"><?php echo empty($dt['buscador']['nrocars'])?'--':$dt['buscador']['nrocars'] ?></td>
		</tr>
		<tr>
    		<td colspan="2"><?php echo __('chasis')?></td>
    		<td colspan="3"><?php echo empty($dt['buscador']['chasis'])?'--':$dt['buscador']['chasis'] ?></td>
		</tr>
		<tr>	
			<td colspan="2"><?php echo __('Fecha_reporte'); ?></td>
			<td colspan="3"><?php echo empty($fecha)?'--':$fecha; ?></td>
		</tr>
     </table>
	<div class="espacio"></div> 
	<table><tbody><tr><td></td></tr></tbody></table>
	
	<div id="tablaReporte">
		<?php if(!empty($ccptabhistoriales)){ ?>
		<table id="listaPrincipal" cellpadding="0" cellspacing="0">
			<thead>
				<tr>                     
					<th><?php echo __('Sucursal',true);?></th> 
	                <th><?php echo __('Fecha de la Cita');?></th>
	                <th><?php echo __('Hora cita',true);?></th>
	                <th><?php echo __('Apellidos y Nombres / Razon social',true);?></th>
	                <th><?php echo __('Marca',true);?></th>
	                <th><?php echo __('Modelo',true);?></th>
					<th>
						<?php echo __('Motivo Servicio',true);?>
					</th>
					<th><?php echo __('Placa',true);?></th>        
					<th><?php echo __('Telefono',true);?></th>        
					<th><?php echo __('Fecha de Registro');?></th>
					<th><?php echo __('CLIENTE_CAMPO_ESTADO_CLIENTE');?></th>
					<th><?php echo __('Estado Cita');?></th>
					
					<th><?php echo __('Usuario creacion');?></th>
					<th><?php echo __('Fecha creacion');?></th>
					
					<th><?php echo __('Usuario reprogramar');?></th>
					<th><?php echo __('Fecha reprogramar');?></th>
					<th><?php echo __('Comentario reprogramar');?></th>
					
					<th><?php echo __('Usuario eliminar');?></th>
					<th><?php echo __('Fecha eliminar');?></th>
					<th><?php echo __('Comentario eliminar');?></th>
					<th><?php echo __('statusSap', true);?></th>
				</tr>
			</thead>
			<tbody>
				<?php  foreach ($ccptabhistoriales as $ccptabhistoriale):?>
				<tr>
				<td>
					<?php 	echo $ccptabhistoriale['Ccptabhistorial']['ot_numero']; ?>	
				</td>


                <td>	
					<?php 	echo $ccptabhistoriale['Talot']['placa']; ?>	
				</td>


                <td>
					<?php 	echo $ccptabhistoriale['Talot']['marca']; ?>	
				</td>

                <td>
					<?php 	echo $ccptabhistoriale['Talot']['modelo']; ?>	
				</td>


                <td>
					<?php 	echo $ccptabhistoriale['Talot']['color']; ?>	
				</td>

                <td>
					<?php 	echo $ccptabhistoriale['Talot']['chassis']; ?>	
				</td>

				<td>
					<?php 	echo $ccptabhistoriale['Ccpsolicitudservicio']['horas_panio']; ?>	
				</td>

				<td>
					<?php 	echo $ccptabhistoriale['Ccpsolicitudservicio']['horas_planchado']; ?>	
				</td>
				
				<td>
					<?php 	echo $ccptabhistoriale['Ccptabestado']['descripcion']; ?>	
				</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
		</table>
		<?php } ?>
	</div>

	<div id="pie"></div>
</div>