<?php echo $this->Html->script('agecitacalendarios/add.js'); ?>
<div>
	<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
	    <thead>
	        <tr>
	            <th colspan="2" class="titulo">
	                <?php echo __('TALLER_CITACALENDAR_TITULO_AGREGAR', true)?>
	            </th>
	        </tr>
	    </thead>
	</table>
	<br/>
	<?php echo $this->Form->create('Agecitacalendario', array('url'=>"add/$citacalendarioId", 'id'=>"AgecitacalendarioForm"));?>
	<?php echo $this->Form->hidden('Agecitacalendario.id', array('value'=>$citacalendarioId)); ?>
	<table id="formularioEdicion" border="0" cellspacing="0" cellpadding="0" align="center">
		<tbody>
			<tr>
				<td class="etiqueta"><?php echo  $this->Form->label('Agecitacalendario.secproject_id', __('TALLER_CITACALENDAR_ETIQUETA_PROJECT', true)); ?><span class="error">*</span></td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendario.secproject_id',$secprojects,array('class'=>'required','style'=>'width:300px;'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta"><?php echo  $this->Form->label('Agecitacalendario.agemotivoservicio_id', __('AGE_MOTIVOSERVICIO_DESCRIPCION', true)); ?><span class="error">*</span></td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendario.agemotivoservicio_id',$agemotivoservicios,array('class'=>'required', 'style'=>'width:180px;'),false); ?></td>
			</tr>
			
			<tr>
				<td class="etiqueta"><?php echo  $this->Form->label('Agecitacalendario.agegrupo_id', __('TALGRUPOS_ETIQUETA_GRUPOS', true)); ?><span class="error">*</span></td>
				<td class="valor"><?php echo $this->Form->select('Agecitacalendario.agegrupo_id',$agegrupos,array('class'=>'required','style'=>'width:180px;'),false); ?></td>
			</tr>
			<tr>
				<td class="etiqueta"><?php echo  $this->Form->label('Agecitacalendario.maximo_citasprogramado', __('TALLER_CITACALENDAR_TITULO_PROGRAMADAS', true)); ?><span class="error">*</span></td>
				<td class="valor"><?php echo $this->Form->input('Agecitacalendario.maximo_citasprogramado',array('label'=>false,'class'=>'required','style'=>'width:175px;','error'=>false)); ?></td>
			</tr>
			<tr>
	            <td colspan="2" class="accion">
	                <?php echo $this->Form->submit(__('guardar',true), array('div'=>false, 'class'=>'guardar'));	?>
					<?php echo $this->Form->button(__('cerrar',true), array('class'=>'cerrar', 'type'=>'button','onClick' => 'javascript:window.close()')); ?>
	            </td>
	        </tr>
		</tbody>
	</table>
	<?php echo $this->Form->end(); ?>

<?php echo $this->element('actualizar_ver'); ?>
</div>