<div class="span-8" >
	<?php echo $this->Html->script('marcas/edit.js',false); ?>
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo __('Modificar Marca');?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Marca');?>
			<?php echo $this->Form->input('id', array('type'=>'hidden')); ?>
	
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('codigo',true)) ?>
				</label><span class="error"><?php echo " *";?></span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('codigo', array('label'=>'','class'=>'span-5','error'=>false)); 
					  echo $this->Form->error('codigo', array(															
													    'isUnique' =>  __('marcaCodigoUnico', true),
														'notEmpty' =>  __('marcaCodigoNoVacio', true),
														'maxLength' =>  __('marcaCodigoLongitud', true),
														'alphaNumeric' =>  __('marcaCodigoEspacios', true)										      
														), array('class' => 'input text required error'));				
				?>
			</div>
		</div>	
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('descripcion',true)) ?>
				</label><span class="error"><?php echo " *";?></span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('description', array('label'=>'','class'=>'span-5','error'=>false)); 
					  echo $this->Form->error('description', array(															
														'isUnique' =>  __('marcaDescripcionUnico', true),
														'notEmpty' =>  __('marcaDescripcionNoVacio', true),
														'maxLength' =>  __('marcaDescripcionLongitud', true)										      
														), array('class' => 'input text required error'));				
				?>
			</div>
		</div>	
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('Pagina Web',true)) ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('portal', array('label'=>'','class'=>'span-5','error'=>false)); 
					  echo $this->Form->error('portal', array(															
													   	'maxLength' =>  __('marcaLongitud', true)										      
														), array('class' => 'input text required error'));				
				?>
			</div>
		</div>
	
		<div id="rowLast" class="span-8" >
			<div class="span-3" ><label><?php echo (__('estado',true)) ?>
			</label></div>
			<div class="span-5 last" >
				<?php 
					$options=array('AC'=>__('Enable',true),'DE'=>__('Disable',true));		
					echo $this->Form->radio('status',$options,array('legend'=>'','default'=>'AC'));
				?>
			</div>
		</div>			
		
		<br/>
		<hr/>
		
		<div class=" span-8 botones" >
			<?php echo $this->Form->submit(__('Submit',true), array('div'=>false));	?>
			<?php echo $this->Form->button(__('Reset',true), array('type'=>'reset')); ?>				
			<?php echo $this->Form->button(__('Close',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end(); ?>

<?php echo $this->element('actualizar_ver'); ?>
</div>	