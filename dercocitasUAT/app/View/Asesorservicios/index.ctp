<?php echo $this->Html->script('asesorservicios/index.js'); ?>
<?php echo $this->Session->flash();?>
<br/>
<h3 id="tituloTable"><?php  echo __('ASESORSERVICIO_TITULO_INDEX'); ?></h3>
<div class="box">
	<div id="agregar" class="span-5" >
		<?php echo $this->element('agregar'); ?>	
	</div>
	<div id="buscador" class="">
		<?php echo $this->element('buscador', array('elementos' => $elementos, 'url' => 'index')); ?>
	</div>
</div>

<table cellpadding="0" cellspacing="0" class="table" >
	<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('Secperson.appaterno',__('apellidoNombre', true)); ?></th>
			<th><?php echo $this->Paginator->sort('Secperson.username', __('User', true)); ?></th>
			<th><?php echo __('organizacion', true); ?></th>
			<th><?php echo $this->Paginator->sort('Secproject.name',__('sucursal', true)); ?></th>
			<th><?php echo $this->Paginator->sort('Asesorservicio.codigo_sap',__('GENERAL_CODIGO_SAP', true)); ?></th>				
			<th><?php echo __('estado', true); ?></th>
			<th class="actions"><?php echo __('Actions', true); ?></th>
		</tr>			
	</thead>
	<tbody>
		<?php foreach ($asesorservicios as $asesorservicio):
		$status = $asesorservicio['Asesorservicio']['status']; ?>
		<tr>
			<td><?php 			
			echo $asesorservicio['Secperson']['apellidoNombre']; 
			?></td>
			<td><?php 			
			echo $asesorservicio['Secperson']['username']; 
			?></td>			
			<td><?php echo $asesorservicio['Secorganization']['name'];?></td>
			<td><?php echo $asesorservicio['Secproject']['name']; ?>
			</td>	
			<td><?php echo $asesorservicio['Asesorservicio']['codigo_sap']; ?>
			</td>			
			<td>			
				<?php echo $status == 'AC' ? __('Enable', true) : ($status == 'DE' ? __('Disable', true) : __('Limited', true)); ?>
			</td>
			<td class="actions">
				<?php echo $this->element('action', array(
					'id' => $asesorservicio['Asesorservicio']['id'], 
					'name' => $asesorservicio['Secperson']['appaterno'], 
					'estado' => $asesorservicio['Asesorservicio']['status']
				)); ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<div id ="paging" class="span-18">
	<?php echo $this->Paginator->options(array('url' =>$this->passedArgs)); ?>
	<?php 
	echo $this->element('paginador'); 	?>
</div>

<div class="clear"></div>
