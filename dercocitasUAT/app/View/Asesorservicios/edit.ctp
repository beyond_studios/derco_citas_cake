<div class="span-9" >	
	<?php echo $this->Html->script('asesorservicios/edit.js'); ?>	
	<?php echo $this->Session->flash();?>
	
	<div id="titulo" class="span-9" >
	<?php $this->Session->flash();?>
		<h3><?php echo __('ASESORSERVICIO_TITULO_EDITAR');?></h3>
	</div><div class="clear"></div>
	<br/>
	<?php echo $this->Form->create('Asesorservicio');
	?>
	<?php echo $this->Form->input('id', array('type'=>'hidden')); ?>
	<div class="span-9" ><hr/>
			<div class="span-3" >
				<label><?php echo(__('GENERAL_CODIGO_SAP')) ?><span class="error"><?php echo " *";?>	</span>
				</label>
			</div>
			<div class="span-6 last" >
				<?php 
					echo $this->Form->input('codigo_sap',array('class'=>'span-5','label'=>'' ));
				?>
			</div>
		</div>
		<div class="span-9" >
			<div class="span-3" >
				<label><?php echo $this->Form->label(__('organizacion')); ?><span class="error"><?php echo " *";?>	</span>
				</label>
			</div>
			<div class="span-6 last" >
				<?php echo $this->Form->select('secorganization_id',array($secorganizations),array('class'=>'span-5'),FALSE ); 
				
				?>	
				<?php echo $this->Js->get('#AsesorservicioSecorganizationId')->event('change',$this->Js->request(
				array('url' => array( 'action' => 'listrolpersonas'),'update' => 'AsesorservicioSecpersonId' )));?>					
			</div>
		</div>
		<div class="span-9" >
			<div class="span-3" >
				<label><?php echo(__('sucursales')) ?><span class="error"><?php echo " *";?></span>
				</label>
			</div>
			<div class="span-6 last" >
				<?php 
					echo $this->Form->input('secproject_id',array('class'=>'span-5','label'=>'' ));
				?>
			</div>
		</div>
		<div class="span-9" >
			<div class="span-3" >
				<label><?php echo __('MOTIVO_DE_SERVICIO') ?><span class="error"><?php echo " *";?>	</span>
				</label> 
			</div>
			<div class="span-6 last" >
				<?php 
					echo $this->Form->input('agemotivoservicio_id',array('type'=>'select','class'=>'span-5','label'=>'' ));
				?>
			</div>
		</div>
		<div class="span-9" >
			<div class="span-3" >
				<label><?php echo(__('usuarios')) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-6 last" >
				<?php 
					echo $this->Form->input('secperson_id', array('class'=>'span-5','label'=>'' ));
				?>
			</div>
		</div>
		<div  class="span-9" >
			<div class="span-3" >
				<label><?php echo(__('estado')) ?><span class="error"><?php echo " *";?>	</span>
				</label>
			</div>
			<div class="span-6 last" >
				<?php
					$options=array('AC'=>__('activo'),'DE'=>__('desactivo'));
					echo $this->Form->radio('status',$options,array('legend'=>'','default'=>'AC'));
				?>
			</div>
		</div>

		<div id="rowLast" class=" span-9 botones" >
			<hr/>
			<?php echo $this->Form->submit(__('guardar'), array('div'=>false));	?>
			<?php echo $this->Form->button(__('cancelar'), array('type'=>'reset')); ?>
			<?php  echo $this->Form->button(__('cerrar'), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>

	<?php
	echo $this->Form->end(); ?>
</div>


