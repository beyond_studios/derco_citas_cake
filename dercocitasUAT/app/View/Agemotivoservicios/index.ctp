<?php echo $this->Html->script('agemotivoservicios/index.js'); ?>
		<?php echo $this->Session->flash();  ?>
		<br/>
			<h3 id="tituloTable"><?php echo __('AGE_MOTIVOSERVICIO_TITULO');?></h3>
		
		<div class="box">
			<div id="agregar" class="span-5" >
			<?php echo $this->element('agregar'); ?>	
			</div>
			
			<div id="buscador" class="">
				<?php echo $this->element('buscador', array('elementos'=>$elementos,'url' => 'index')); ?>
			</div>
		</div>		
		<table cellpadding="0" cellspacing="0" class="table" >
			<thead>
			<tr>
				<th><?php echo $this->Paginator->sort('Agemotivoservicio.codigo_sap',__('GENERAL_CODIGO_SAP',true));?></th>  
				<th><?php echo $this->Paginator->sort('Agemotivoservicio.description',__('AGE_MOTIVOSERVICIO_DESCRIPCION'));?></th>       
				<th><?php echo $this->Paginator->sort('Agemotivoservicio.mensaje',__('mensajeMotivoServicio',true));?></th>
				<th><?php echo $this->Paginator->sort('Agemotivoservicio.status',__('estado',true));?></th>
				<th class="actionsfijo"><?php  echo __('Actions');?></th>
			</tr>			
			</thead>
			
			<tbody>
				<?php  foreach ($agemotivoservicios as $agemotivoservicio):?>
				<tr>
				<td>
					<?php
							echo  $agemotivoservicio['Agemotivoservicio']['codigo_sap']; 
					?>
				</td>
				<td>
					<?php 		
							$url = $this->Html->url(array('action'=>'view', $agemotivoservicio['Agemotivoservicio']['id']));
							echo $this->Html->link($agemotivoservicio['Agemotivoservicio']['description'], 'javascript:;',array('onclick' => "mostrar('".$url."')",'escape'=>false), null);	
					?>	
				</td>
				<td class="textl"><?php echo $agemotivoservicio['Agemotivoservicio']['mensaje']?></td>
				<td class="textc">
					<?php echo $agemotivoservicio['Agemotivoservicio']['status'] == 'AC' ? 
									__('Enable',true)
								:
									($agemotivoservicio['Agemotivoservicio']['status'] == 'DE'?
										__('Disable',true)
									:
										__('Limited',true))
								; ?>
				</td>
		
					<td class="actionsfijo">
				<?php echo $this->element('actionDerco', 
								array('id'=>$agemotivoservicio['Agemotivoservicio']['id'], 
										'name'=> $agemotivoservicio['Agemotivoservicio']['description'],
										'estado'=> $agemotivoservicio['Agemotivoservicio']['status'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>