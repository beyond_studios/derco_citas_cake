<div class='span-8'>
<?php echo $this->Session->flash();?>
<br/>
	<div id='titulo' class="span-8" >
		<h3><?php echo __('AGE_TIPOSERVICIO_DESCRIPCION', TRUE);?></h3>
	</div>
	<hr/>
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo __('AGE_MOTIVOSERVICIO_DESCRIPCION', TRUE) ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo ": "; ?><?php echo $agetiposervicio['Agemotivoservicio']['description']; ?>
			</div>
		</div>
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo __('description', TRUE) ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo ": "; ?><?php echo $agetiposervicio['Agetiposervicio']['description']; ?>
			</div>
		</div>
		
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo __('GENERAL_CODIGO_SAP', TRUE) ?>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo ": "; ?><?php echo $agetiposervicio['Agetiposervicio']['codigo_sap']; ?>
			</div>
		</div>
		
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo __('AGE_TIPOSERVICIO_MOSTRAR_MANTENIMIENTO', TRUE) ?>
				</label> 
			</div>
			<div class="span-5 last" >
		 		<?php echo ": "; ?><?php echo  $agetiposervicio['Agetiposervicio']['mostrar_mantenimiento'] == 1 ? 
							__('si',true):__('no',true); ?>
			</div>
		</div>
		
		<div class="span-8" id="rowLast">
			<div class="span-3" ><label><?php echo __('estado', TRUE) ?>
			</label></div>
			<div class="span-5 last" >
		 		<?php echo ": "; ?><?php echo  $agetiposervicio['Agetiposervicio']['status'] == 'AC' ? 
							__('Enable',true) :	($agetiposervicio['Agetiposervicio']['status'] == 'DE'? 	
							__('Disable',true):
							__('Limited',true))
						; ?>
			</div>
		</div>	
		<br/>		
		<hr/>
		
		<div class=" span-8 botones" >
			<?php  echo $this->Form->button(__('Close',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
			<?php echo $this->Form->end();?>
		</div>
	
	<?php echo $this->Form->end();?>
<?php echo $this->element('actualizar_ver'); ?>
</div>