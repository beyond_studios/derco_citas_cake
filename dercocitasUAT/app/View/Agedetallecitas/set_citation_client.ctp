<?php echo $this->element('dialog_message'); ?>
<?php 
		echo $this->Html->css('modulo_taller/derco/derco_ventana.css');
		echo $this->Html->script('jquery-validate/jquery.validate.js');
		echo $this->Html->script('jquery-validate/lib/jquery.form.js');
		echo $this->Html->script('clientes/taller_index.js');	
?>


<?php echo $this->element('dialog_message'); ?>
<?php echo $this->element('getBscVehicles'); ?>

<?php echo $this->Html->script('clientes/get_clientes.js'); ?>
<?php echo $this->Html->script('clientes/comun_clientes.js'); ?>
<?php echo $this->Html->script('Agedetallecitas/agregar_cita.js'); ?>


<div class="login width100">
	<h1 style="text-align: left;"><?php echo __('TALLER_TITULO_TALLER_CITA') ?></h1>
					
					
		<?php 
			echo $this->element('solicitud_agregar_cita', array(
				'action'=>array('controller'=>'agedetallecitas', 'action'=>'agregarCita'),
				'citarepogramarId'=>""
			)); 
		?>
				

		<div class="titleBar styleGriss" style="text-align: left;">
			<i class="fa fa-calendar-o" aria-hidden="true"></i><b><?php echo __('VENDEDOR_ETIQUETA_CONSULTA_DIAS_HABILES')?></b>
		</div>
		<?php echo $this->Form->create('Buscador', array('url'=>'listarProgramacionCitaTaller','class'=>'fomrLogin', 'id'=>'buscadorForm'));?>
		<?php echo $this->Form->hidden('agecitacalendario_id', array())?>
			<div class="formTable leftAling">
				<div class="groupFields col4">
					
					<div class="field">
						<div>
							<label class="leftAling"><?php echo __('AGE_REPORTE_FECHA_DESDE');?></label>
						</div>
						<div>										
							<?php echo $this->Form->input('fechaInicial', array('class'=>'required msg_error_2 soloLectura','readonly'=>true ,'label'=>false, 'div'=>false))?>
						</div>
					</div>

					<div class="field">
						<div>
							<label class="leftAling"><?php echo __('AGE_REPORTE_FECHA_HASTA');?></label>
						</div>
						<div>
							<?php echo $this->Form->input('fechaFinal', array('class'=>'required msg_error_2 soloLectura','readonly'=>true, 'label'=>false, 'div'=>false))?>
						</div>
					</div>

					<div class="field">
						<div>
							<label class="leftAling"><?php echo __('TALLER_ETIQUETA_TALLER_SUCURSAL');?></label>
						</div>
						<div>
							<select id="Talcitacalendar2Id" class="required msg_error_1 styled-select slate"  onchange="getMotivoServicioBuscador(this);" name="data[Talcitacalendar2][id]">
								<option value=""><?php echo __('Seleccionar', true) ?></option>
								<?php foreach($talleres as $taller){
									echo "<option direccion=\"".utf8_encode($taller['Secproject']['address'])."\" value=\"".$taller['Secproject']['id']."\">".utf8_encode($taller['Secproject']['name'])."</option>";
								} ?>
							</select>
						</div>
					</div>

					<div class="field">
						<div>
							<label class="leftAling"><?php echo __('AGE_MOTIVOSERVICIO_DESCRIPCION');?></label>
						</div>
						<div>
							<?php echo $this->Form->select('Talcitacalendar2.motivo_id', array(), array('empty'=>__('Seleccionar'), 'class'=>'required msg_error_1 styled-select slate'))?>
						</div>
					</div>
					<!--<div class='field'>
						<div>
							<label class="leftAling"><?php echo __('direccion');?></label>
						</div>
						<div>
							<?php echo $this->Form->input('Talcitacalendar2.direccion', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false)); ?>
						</div>
					</div>-->
					<div class='field' style='padding-top:26px; text-align:center; width:100%'>
						<?php echo $this->Form->button(__('GENERAL_BUSCAR'), array('class'=>'buscar buttomReset guinda', 'div'=>false));?>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
		
		<div class="tableData styleTable1">
			
		</div>

		<div class="logosFooter">
			<img src="dist/images/basLogos.png">
		</div>
	</div>
	<div id="dialog_cliente" title="Buscar cliente">
		<div id="containerclientes">
			<table id="clientes"></table> 
			<div id="clientes-pager"></div>
		</div>
	</div>


<div id="listaCaja">
		<div id="programacion" style="text-align: center;"></div>
		<div id="mensajeFinal" title="Mensaje" style="text-align: center;">
			<span id="mensajeFinaltexto"></span>
		</div>
		<div id="dialog_confirm" title="Confirmar Datos de la Cita">
			<?php echo $this->Form->create('Confirm',array('class'=>'fomrLogin','id'=>'ConfirmAgregarCitaForm'));?>
			<?php $url2 = $this->Html->url(array('controller'=>'clientes','action'=>'historialCitas')); ?>
			<?php echo $this->Form->hidden('urlAfterSave',array('value'=>$url2))?>
			<?php echo $this->Form->hidden('clienteid') ?>
				<table style='width:100%' class='formTable leftAling' style="text-align: left;">
				<tr>
					<td class="etiqueta"><label style="text-align: left;"><?php echo __('AGE_DETALLE_CITA_CONFIRM_SR');?></label></td>
					<td class="valor"><span id = "ConfirmNombre"></span></td>
				</tr>
				<tr>
					<td class="etiqueta"><label style="text-align: left;"><?php echo __('AGE_DETALLE_CITA_CONFIRM_NUMERO_DOCUMENTO');?></label></td>
					<td class="valor"><span id = "ConfirmNumeroDocumento"></span></td>
				</tr>
				<tr>
					<td class="etiqueta"><label style="text-align: left;"><?php echo __('AGE_DETALLE_CITA_CONFIRM_PLACA');?></label></td>
					<td class="valor"><span id = "ConfirmPlaca"></span></td>
				</tr>
				<tr>
					<td class="etiqueta"><label style="text-align: left;"><?php echo __('AGE_DETALLE_CITA_CONFIRM_LOCAL');?></label></td>
					<td class="valor"><span id = "ConfirmLocal"></span></td>
				</tr>
				<tr>
					<td class="etiqueta"><label style="text-align: left;"><?php echo __('AGE_DETALLE_CITA_CONFIRM_FECHA_HORA_CITA');?></label></td>
					<td class="valor"><span id = "ConfirmFechaHoraCita"></span></td>
				</tr>
				<tr>
					<td class="etiqueta"><label style="text-align: left;"><?php echo __('AGE_DETALLE_CITA_CONFIRM_EMAIL');?><span class="error">*</span></label><span class="error">*</span></td>
					<td class="valor"><?php echo $this->Form->input('Email',array('label'=>false,'class'=>'required')); ?></td>
				</tr>
				<tr>
					<td class="etiqueta"><label style="text-align: left;"><?php echo __('AGE_DETALLE_CITA_CONFIRM_TELEFONO');?><span class="error">*</span></label></td>
					<td class="valor"><?php echo $this->Form->input('Telefono',array('label'=>false,'class'=>'required')); ?></td>
				</tr>
				<tr>
					<td class="etiqueta" colspan='2' style='text-align:center'>
						<?php echo $this->Form->submit(__('AGE_DETALLE_CITA_CONFIRM_ACEPTAR'), array('label'=>false,'class'=>'guinda guardar', 'id'=>'buttonConfirmSubmit', 'div'=>false));?>
						<?php echo $this->Form->button(__('AGE_DETALLE_CITA_CONFIRM_CANCELAR'), array('type'=>'button','class'=>'buttomReset cancelar', 'div'=>false, 'onclick'=>'closeDialog()'));?>
					</td>
				</tr>
				</table>
			<?php echo $this->Form->end(); ?>
		</div>
</div> 
 	
</div>