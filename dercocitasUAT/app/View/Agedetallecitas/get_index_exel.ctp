<script>
	window.close();
</script>
<div id="cuerpo">
    <table border="0" width="100%">
    	<tr>
    		<td colspan="12"><?php echo __('Citas de Taller - Call Center')?></td>
        </tr>
		<tr>
    		<td colspan="2"><?php echo __('FechaDe')?></td>
    		<td colspan="3"><?php echo empty($dt['bsc']['f_campo'])?'--':$f_campo[$dt['bsc']['f_campo']] ?></td>
        </tr>		
        <tr>
    		<td colspan="2"><?php echo __('desde')?></td>
    		<td colspan="3"><?php echo empty($dt['bsc']['f_ini'])?'--':$dt['bsc']['f_ini'] ?></td>
    		<td colspan="2"><?php echo __('Estado')?></td>
    		<td colspan="5"><?php echo empty($dt['bsc']['std'])?'--':$std[$dt['bsc']['std']] ?></td>
		</tr>
		<tr>
    		<td colspan="2"><?php echo __('hasta')?></td>
    		<td colspan="3"><?php echo empty($dt['bsc']['f_fin'])?'--':$dt['bsc']['f_fin'] ?></td>
    		<td colspan="2"><?php echo empty($dt['bsc']['crt'])?'Todos':$crt[$dt['bsc']['crt']] ?></td>
    		<td colspan="5"><?php echo empty($dt['bsc']['vlr'])?'--':$dt['bsc']['vlr'] ?></td>
		</tr>
		<tr>	
			<td colspan="2"><?php echo __('Fecha_reporte'); ?></td>
			<td colspan="3"><?php echo empty($fecha)?'--':$fecha; ?></td>
		</tr>
     </table>
	<div class="espacio"></div> 
	<table><tbody><tr><td></td></tr></tbody></table>
	
	<div id="tablaReporte">
		<?php if(!empty($agedetallecitas)){ ?>
		<table id="listaPrincipal" cellpadding="0" cellspacing="0">
			<thead>
				<tr>                     
					<th><?php echo __('Sucursal',true);?></th> 
	                <th><?php echo __('Fecha de la Cita');?></th>
	                <th><?php echo __('Hora cita',true);?></th>
	                <th><?php echo __('Apellidos y Nombres / Razon social',true);?></th>
					<th><?php echo __('Tipo Cliente',true);?></th>
	                <th><?php echo __('Marca',true);?></th>
	                <th><?php echo __('Modelo',true);?></th>
					<th><?php echo __('Motivo Servicio',true);?></th>
					<th><?php echo __('Tipo Servicio',true);?></th>
					<th><?php echo __('Placa',true);?></th>        
					<th><?php echo __('Telefono',true);?></th>        
					<th><?php echo __('Fecha de Registro');?></th>
					<th><?php echo __('Hora de Registro');?></th>
					<th><?php echo __('Servicios Solicitados');?></th>
					<th><?php echo __('CLIENTE_CAMPO_ESTADO_CLIENTE');?></th>
					<th><?php echo __('Estado Cita');?></th>
					
					<th><?php echo __('Usuario creacion');?></th>
					<th><?php echo __('Fecha creacion');?></th>
					
					<th><?php echo __('Usuario reprogramar');?></th>
					<th><?php echo __('Fecha reprogramar');?></th>
					<th><?php echo __('Comentario reprogramar');?></th>
					
					<th><?php echo __('Usuario eliminar');?></th>
					<th><?php echo __('Fecha eliminar');?></th>
					<th><?php echo __('Comentario eliminar');?></th>
					<th><?php echo __('statusSap', true);?></th>
					<th><?php echo __('Numero OT');?></th>
					<th><?php echo __('Tipo de Cliente');?></th>
				</tr>
			</thead>
			<tbody>
				<?php  foreach ($agedetallecitas as $agedetallecita):?>
				<tr>
				<td>
					<?php 	echo $agedetallecita['Secproject']['name']; ?>	
				</td>
				
                <td>	
					<?php 	echo date('Y-m-d',strtotime($agedetallecita['Agedetallecita']['fechadecita'])); ?>
				</td>

                <td>
					<?php 	echo date("H:i",strtotime($agedetallecita['Agedetallecita']['fechadecita'])); ?>	
				</td>

                <td>
					<?php 	echo empty($agedetallecita['Cliente']['nombres'])?$agedetallecita['Cliente']['razonSocial']:$agedetallecita['Cliente']['nombres'];					
					?>	
				</td>

                <td>
					<?php 	echo $agedetallecita['Cliente']['cliente_tipo']; ?>	
				</td>

                <td>
					<?php 	echo $agedetallecita['Agedetallecita']['marca']; ?>	
				</td>

                <td>
					<?php 	echo $agedetallecita['Agedetallecita']['modelo']; ?>	
				</td>

				<td>
					<?php 	echo $agedetallecita['Agemotivoservicio']['description']; ?>	
				</td>
				
				<td>
					<?php 	echo $agedetallecita['Agetiposervicio']['description']; ?>	
				</td>

				<td>
					<?php 	echo $agedetallecita['Agedetallecita']['placa']; ?>	
				</td>
				
				<td>
					<?php 	echo $agedetallecita['Cliente']['telefono']; ?>	
				</td>
				<td>
					<?php 	echo $agedetallecita['Agedetallecita']['fechaRegistro']; ?>	
				</td>
				<td>
					<?php 	echo $agedetallecita['Agedetallecita']['horaRegistro']; ?>	
				</td>
				<td>
					<?php 	echo $agedetallecita['Agedetallecita']['otrosServicios']; ?>	
				</td>
				<td>
					<?php echo $agedetallecita['Cliente']['cliente_estado'] == 'PE' ? 
										__('GENERAL_PENDIENTE',true)
									:
										($agedetallecita['Cliente']['cliente_estado'] == 'CP'?
											__('GENERAL_COMPLETO',true)
										:
											__('Parcial',true))
					; ?>
				</td>
				<td>
					<?php echo $agedetallecita['Agedetallecita']['estado'] == 'AC' ? 
									__('Programado',true)
								:
									($agedetallecita['Agedetallecita']['estado'] == 'RE'?
										__('Reprogramado',true)
									:
										__('Eliminado',true))
								; ?>
				</td>
	
				<td><?php if(!empty($agedetallecita['Createdsecperson']['appaterno'])) echo $agedetallecita['Createdsecperson']['appaterno'].' '.$agedetallecita['Createdsecperson']['apmaterno'].', '.$agedetallecita['Createdsecperson']['firstname']; ?></td>
				<td><?php echo $agedetallecita['Agedetallecita']['fechaRegistro']; ?></td>
				
				<td><?php if(!empty($agedetallecita['Reschedulesecperson']['appaterno'])) echo $agedetallecita['Reschedulesecperson']['appaterno'].' '.$agedetallecita['Reschedulesecperson']['apmaterno'].', '.$agedetallecita['Reschedulesecperson']['firstname']; ?></td>
				<td><?php echo $agedetallecita['Agedetallecita']['rescheduledate']; ?></td>
				<td><?php echo $agedetallecita['Agedetallecita']['reschedulecomment']; ?></td>
				
				<td><?php if(!empty($agedetallecita['Deletesecperson']['appaterno'])) echo $agedetallecita['Deletesecperson']['appaterno'].' '.$agedetallecita['Deletesecperson']['apmaterno'].', '.$agedetallecita['Deletesecperson']['firstname']; ?></td>
				<td><?php echo $agedetallecita['Agedetallecita']['deletedate']; ?></td>
				<td><?php echo $agedetallecita['Agedetallecita']['deletecomment']; ?></td>
				<td><?php echo $statusSap[$agedetallecita['Agedetallecita']['estadoenviosap']]; ?></td>
				<td><?php echo $agedetallecita['Agedetallecita']['otsap']; ?></td>
				<td><?php echo $agedetallecita['Cliente']['str_cliente_tipo']; ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
		</table>
		<?php } ?>
	</div>

	<div id="pie"></div>
</div>