<?php echo $this->element('dialog_message'); ?>
<?php 
		echo $this->Html->css('modulo_taller/derco/derco_ventana.css');
		echo $this->Html->script('jquery-validate/jquery.validate.js');
		echo $this->Html->script('clientes/taller_index.js');	
?>
<div class="login width100">
<h1 style="text-align: left;"><?php echo __('TALLER_TITULO_TALLER_CITA');?></h1>
<table border="0" cellpadding="0" cellspacing="0" class="web">
	<tr>
		<td>
			<?php echo $this->element('getBscVehicles'); ?>
			
			<?php //echo $this->Html->script('clientes/get_clientes.js'); ?>
			<?php echo $this->Html->script('clientes/comun_clientes.js'); ?>
			<?php echo $this->Html->script('Agedetallecitas/reprogramar_cita.js'); ?>
			
			<!-- DIALOGOS A UTILIZAR -->
			<div id="dialog_cliente" title="Buscar cliente">
				<div id="containerclientes">
					<table id="clientes"></table> 
					<div id="clientes-pager"></div>
				</div>
			</div>
			<?php // echo $this->Session->flash();?>
			<ul class="acciones">
				<li>
					<?php //echo $this->Xhtml->imageTextLink('derco/defecto/cerrar.gif', __('GENERAL_CERRAR'),
						//'javascript:;',	array('onclick'=>'javascript:window.close()', 'escape'=>false), null, array('width'=>'16'))?>
			    </li>
			</ul>
			<br/>
			<?php 
				echo $this->element('solicitud_agregar_cita', array(
					'action'=>array('controller'=>'agedetallecitas', 'action'=>'reprogramarCita'),
					'citarepogramarId'=>$detallecitaId
				)); 
			?>
			<div id="listaCaja">
				<fieldset style="height: 220px;">
					<legend><?php echo __('VENDEDOR_ETIQUETA_CONSULTA_DIAS_HABILES')?></legend>
					<?php echo $this->Form->create('Buscador', array('url'=>'listarProgramacionCitaTaller', 'id'=>'buscadorForm'));?>
					<?php echo $this->Form->hidden('agecitacalendario_id', array())?>
					<div id="buscadorBasico">
						<table id="formularioEdicion" border="0" cellspacing="0" cellpadding="0" align="center" width="90%">
							<tbody>
								<tr>
					            	<td class="etiqueta">
					            		<?php echo __('AGE_REPORTE_FECHA_DESDE');?>
					            	</td>
									<td class="valor">
										<?php echo $this->Form->input('fechaInicial', array('class'=>'required msg_error_2 soloLectura','readonly'=>true ,'label'=>false, 'div'=>false,'style'=>'width:120px;'))?>
										<span class="error">*</span>
					            	</td>
					            	<td class="etiqueta">
					            		<?php echo __('AGE_REPORTE_FECHA_HASTA');?>
					            	</td>
									<td class="valor">
										<?php echo $this->Form->input('fechaFinal', array('class'=>'required msg_error_2 soloLectura','readonly'=>true, 'label'=>false, 'div'=>false,'style'=>'width:120px;'))?>
										<span class="error">*</span>
					            	</td>
								</tr>
								<tr>
					            	<td class="etiqueta">
					            		<?php echo __('TALLER_ETIQUETA_TALLER_SUCURSAL');?>
					            	</td>
									<td class="valor">
										<select id="Talcitacalendar2Id" class="required msg_error_1" style="width:70%;" class="required" onchange="getMotivoServicioBuscador(this);" name="data[Talcitacalendar2][id]">
											<option value=""><?php echo __('Seleccionar', true) ?></option>
											<?php foreach($talleres as $taller){
												echo "<option direccion=\"".utf8_encode($taller['Secproject']['address'])."\" value=\"".$taller['Secproject']['id']."\">".utf8_encode($taller['Secproject']['name'])."</option>";
											} ?>
										</select>
										<span class="error">*</span>
									</td>
									<td class="etiqueta">
					            		<?php echo __('direccion');?>
					            	</td>
									<td class="valor"><?php echo $this->Form->input('Talcitacalendar2.direccion', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>
								</tr>
								<tr>
									<td class="etiqueta">
					            		<?php echo __('AGE_MOTIVOSERVICIO_DESCRIPCION');?>
					            	</td>
									<td class="valor">
										<?php echo $this->Form->select('Talcitacalendar2.motivo_id', array(), array('empty'=>__('Seleccionar'), 'class'=>'required msg_error_1', 'style'=>'width:70%;'))?>
										<span class="error">*</span>
									</td>
									<td colspan=2 style="text-align:right;">
										<?php echo $this->Form->button(__('GENERAL_BUSCAR'), array('style'=>'width:80px', 'class'=>'buscar'));?>
									</td>
					        	</tr>
							</tbody>
						</table>
					</div>
					<?php echo $this->Form->end(); ?>
					<div id="programacion" style="text-align: center;"></div>
					<div id="mensajeFinal" title="Mensaje" style="text-align: center;">
						<span id="mensajeFinaltexto"></span>
					</div>
					<div id="dialog_confirm" title="Confirmar Datos de la Cita">
						<?php echo $this->Form->create('Confirm',array('id'=>'ConfirmReprogramarCitaForm'));?>
						<?php $url2 = $this->Html->url(array('controller'=>'clientes','action'=>'bandejaReprogramar')); ?>
						<?php echo $this->Form->hidden('urlAfterSave',array('value'=>$url2))?>
						<?php echo $this->Form->hidden('clienteid') ?>
						<table>
							<tr>
								<td class="etiqueta"><?php echo __('AGE_DETALLE_CITA_CONFIRM_SR');?></td>
								<td class="valor"><span id = "ConfirmNombre"></span></td>
					        </tr>
							<tr>
								<td class="etiqueta"><?php echo __('AGE_DETALLE_CITA_CONFIRM_NUMERO_DOCUMENTO');?></td>
								<td class="valor"><span id = "ConfirmNumeroDocumento"></span></td>
					        </tr>
							<tr>
								<td class="etiqueta"><?php echo __('AGE_DETALLE_CITA_CONFIRM_PLACA');?></td>
								<td class="valor"><span id = "ConfirmPlaca"></span></td>
					        </tr>
							<tr>
								<td class="etiqueta"><?php echo __('AGE_DETALLE_CITA_CONFIRM_LOCAL');?></td>
								<td class="valor"><span id = "ConfirmLocal"></span></td>
					        </tr>
							<tr>
								<td class="etiqueta"><?php echo __('AGE_DETALLE_CITA_CONFIRM_FECHA_HORA_CITA');?></td>
								<td class="valor"><span id = "ConfirmFechaHoraCita"></span></td>
					        </tr>
							<tr>
								<td class="etiqueta"><?php echo __('AGE_DETALLE_CITA_CONFIRM_EMAIL');?><span class="error">*</span></td>
								<td class="valor"><?php echo $this->Form->input('Email',array('label'=>false,'class'=>'required','style'=>'width: 200px;')); ?></td>
					        </tr>
							<tr>
								<td class="etiqueta"><?php echo __('AGE_DETALLE_CITA_CONFIRM_TELEFONO');?><span class="error">*</span></td>
								<td class="valor"><?php echo $this->Form->input('Telefono',array('label'=>false,'class'=>'required')); ?></td>
					        </tr>
							<tr>
								<td class="etiqueta">
									<?php echo $this->Html->image('loader.gif', array('id'=>'imgLoader','style' => 'display:none;'));?>
									<?php echo $this->Form->submit(__('AGE_DETALLE_CITA_CONFIRM_ACEPTAR'), array('label'=>false,'class'=>'guardar', 'id'=>'buttonConfirmSubmit'));?>
								</td>
								<td class="valor">
									<?php echo $this->Form->button(__('AGE_DETALLE_CITA_CONFIRM_CANCELAR'), array('type'=>'button','class'=>'cancelar', 'div'=>false, 'onclick'=>'closeDialog()'));?>
								</td>
					        </tr>
							
							
						</table>
						<?php echo $this->Form->end(); ?>
					</div>
				</fieldset>
</div> 
			
			
 		</td>
	</tr>
</table>
</div>
</div>