<?php
// se establece el titulo del worksheet
$title = strtoupper(__('citalTaller-callCenter', TRUE));
$xmlexcel->setTitle($title);

// se establecen los styles
$vertical = 'Center';
$fontName = 'Arial';
$family = 'Swiss';
$size = 10;

$sAlignmentLeft = $xmlexcel->getAlignment('Left', null, null, null, null, $vertical, null, 1);
$sAlignmentRight = $xmlexcel->getAlignment('Right', null, null, null, null, $vertical, null, 1);
$sAlignmentCenter = $xmlexcel->getAlignment('Center', null, null, null, null, $vertical, null, 1);

$sFont = $xmlexcel->getFont(1, '#000099', $fontName, null, null, null, $size, null, null, null, null, $family);
$sFont2 = $xmlexcel->getFont(0, null, $fontName, null, null, null, $size, null, null, null, null, $family);

$sBorders = $xmlexcel->getBorders(null, 'Continuous', 1);
$sInterior = $xmlexcel->getInterior('#F0F0F0', 'Solid');
$sNumberFormat = $xmlexcel->getNumberFormat('0.0000');

$xmlexcel->addStyle('sTitle', $sAlignmentLeft, array(), $sFont);
$xmlexcel->addStyle('sHeader', $sAlignmentCenter, $sBorders, $sFont, $sInterior);
$xmlexcel->addStyle('sLeft', $sAlignmentLeft, array(), $sFont2);
$xmlexcel->addStyle('sRight', $sAlignmentRight, array(), $sFont2);
$xmlexcel->addStyle('sRight2', $sAlignmentRight, array(), $sFont2, null, $sNumberFormat);
$xmlexcel->addStyle('sCenter', $sAlignmentCenter, array(), $sFont2);

// se establecen las columnas simples y las agrupadas de la primera fila
$fieldnames = array(
	array('fieldname' => __('sucursal', TRUE), 'width' => 100), 
	array('fieldname' => __('fechaDeCita', TRUE), 'width' => 100), 
	array('fieldname' => __('horaCita', TRUE), 'width' => 200), 
	array('fieldname' => __('apellidosNombres/razonSocial', TRUE), 'width' => 100), 
	array('fieldname' => __('marca', TRUE), 'width' => 100), 
	array('fieldname' => __('modelo', TRUE), 'width' => 100),   //fechaingresoplanta
	array('fieldname' => __('motivoServicio', TRUE), 'width' => 100), 
	
	array('fieldname' => __('placa', TRUE), 'width' => 100), 
	array('fieldname' => __('telefono', TRUE), 'width' => 100), 
	array('fieldname' => __('fechaRegistro', TRUE), 'width' => 200), 
	array('fieldname' => __('estadoCliente', TRUE), 'width' => 100)
);

// se agrega el filtro automatico
$initialRow = 5 + 1;// 6 => Parametros del Buscador, 1 => Cabecera
$initialColumn = 1;
$finalRow = count($registros) + $initialRow;
$finalColumn = count($fieldnames);
$xmlexcel->addNameRange($initialRow, $initialColumn, $finalRow, $finalColumn);

// se agrega la fila con el titulo del reporte
$cells = array();
$mergeAcross = ($finalColumn + 1) - 2;
$cells[] = $xmlexcel->getCell($xmlexcel->getData($title), null, null, null, null, $mergeAcross, null, 'sTitle');
$xmlexcel->addRow($cells);

// se agregan las filas con los parametros del buscador
$fechaHoraActual = date('d-m-Y H:i:s');
$fechaTipo = isset($fechaTipo) && !empty($fechaTipo) ? $fechaTipo : false;

$fechaDesde = empty($dt['bsc']['f_ini'])?"":sprintf("%s %s ",__('Del', TRUE), $dt['bsc']['f_ini']);
$fechaHasta = empty($dt['bsc']['f_fin'])?"":sprintf("%s %s ",__('Al', TRUE), $dt['bsc']['f_fin']);
$fechaRango = isset($fechaDesde) && !empty($fechaDesde) && isset($fechaHasta) && !empty($fechaHasta) ? $fechaDesde.$fechaHasta : '-';

$cells = array();
$cells[] = $xmlexcel->getCell($xmlexcel->getData(__('fechaHoraActual', TRUE)), null, null, null, null, null, null, 'sTitle');
$cells[] = $xmlexcel->getCell($xmlexcel->getData($fechaHoraActual), null, null, null, null, null, null, 'sCenter');
$xmlexcel->addRow($cells, null, null, null, 3);

$cells = array();
$cells[] = $xmlexcel->getCell($xmlexcel->getData(__('Fecha de cita', TRUE)), null, null, null, null, null, null, 'sTitle');
$cells[] = $xmlexcel->getCell($xmlexcel->getData($fechaRango), null, null, null, null, null, null, 'sCenter');
$cells[] = $xmlexcel->getCell($xmlexcel->getData(''));
$xmlexcel->addRow($cells, null, null, null, 4);

$cells = array();
$cells[] = $xmlexcel->getCell($xmlexcel->getData(__('Cliente', TRUE)), null, null, null, null, null, null, 'sTitle');
$cells[] = $xmlexcel->getCell($xmlexcel->getData($dt['bsc']['vlr']), null, null, null, null, null, null, 'sCenter');
$xmlexcel->addRow($cells, null, null, null, 5);

// se agrega la fila del encabezado del reporte (columnas simples)
$colIndex = 0;
$cells = array();
foreach($fieldnames as $columnData) {
	$colIndex++;
	$cell = $xmlexcel->getCellHeader($xmlexcel->getData($columnData['fieldname']), $columnData['width'], null, null, null, $colIndex, null, null, 'sHeader');
	$cells[] = $cell;
}
$xmlexcel->addRow($cells, null, null, null, 7);

// se agregan las filas del resultado del reporte
foreach($registros as $key => $value) {
	$cells = array();
	
	$cells[] = $xmlexcel->getCell($xmlexcel->getData($value['Secproject']['name']), null, null, null, 1, null, null, 'sLeft');
	$cells[] = $xmlexcel->getCell($xmlexcel->getData(date('Y-m-d',strtotime($value['Agedetallecita']['fechadecita']))), null, null, null, 2, null, null, 'sCenter');
	$cells[] = $xmlexcel->getCell($xmlexcel->getData(date("H:i",strtotime($value['Agedetallecita']['fechadecita']))), null, null, null, 3, null, null, 'sCenter');
	$cells[] = $xmlexcel->getCell($xmlexcel->getData(empty($value['Cliente']['nombres'])?$value['Cliente']['razonSocial']:$value['Cliente']['nombres']), null, null, null, 4, null, null, 'sLeft');
	$cells[] = $xmlexcel->getCell($xmlexcel->getData($value['Agedetallecita']['marca']), null, null, null, 5, null, null, 'sLeft');
	$cells[] = $xmlexcel->getCell($xmlexcel->getData($value['Agedetallecita']['modelo']), null, null, null, 6, null, null, 'sLeft');
	$cells[] = $xmlexcel->getCell($xmlexcel->getData($value['Agemotivoservicio']['description']), null, null, null, 7, null, null, 'sLeft');
	
	$cells[] = $xmlexcel->getCell($xmlexcel->getData($value['Agedetallecita']['placa']), null, null, null, 8, null, null, 'sLeft');
	$cells[] = $xmlexcel->getCell($xmlexcel->getData($value['Cliente']['telefono']), null, null, null, 9, null, null, 'sRight');
	$cells[] = $xmlexcel->getCell($xmlexcel->getData($value['Agedetallecita']['fechaRegistro']), null, null, null, 10, null, null, 'sLeft');
	$cells[] = $xmlexcel->getCell($xmlexcel->getData($value['Cliente']['cliente_estado']), null, null, null, 11, null, null, 'sLeft');
	$cells[] = $xmlexcel->getCell($xmlexcel->getData($item['cantidad'], 'Number'), null, null, null, 12, null, null, 'sRight');
		
			
	$xmlexcel->addRow($cells);
}

// se activa el freeze pane
$xmlexcel->addFreezePanes($initialRow);

// se establece el nombre del archivo a exportar
$filename = $title.'-'.date('Ymd_His');
$xmlexcel->render($filename);
?>