<?php if(isset($fechas) && !empty($fechas)) { ?>
<div id="listaCaja" style="overflow: auto; font-size: 8pt; text-align: center; height: 114px;">
	<table id="listaPrincipal" cellpadding="0" cellspacing="0">
		<thead>
		    <tr>
		        <th><?php echo __('VENDEDOR_ETIQUETA_FECHA');?></th>
		        <th><?php echo __('VENDEDOR_ETIQUETA_HORA');?></th>
		        <th><?php echo __('VENDEDOR_ETIQUETA_DISPONIBLE');?></th>
		    </tr>
		</thead>
		<tbody>
		    <?php foreach ($fechas as $key=>$fecha): $class = ($key%2==0) ? "par" : "impar"; ?>
		    <tr class="<?php echo $class; ?>">
		        <td class="centrado"><?php echo date('d-m-Y', strtotime($fecha['Agecitacalendariodia']['initDateTime'])); ?></td>
		        <td class="centrado">
		        	<?php echo date('H:i', strtotime($fecha['Agecitacalendariodia']['initDateTime'])); ?> - 
		        	<?php echo date('H:i', strtotime($fecha['Agecitacalendariodia']['endDateTime'])); ?>
		        </td>
				<?php if($fecha['Agecitacalendariodia']['available']==0){?>
		        	<td class="centrado"><FONT COLOR=red><?php echo $fecha['Agecitacalendariodia']['available']; ?></FONT></td>
				<?php }else{ ?>
					<td class="centrado"><?php echo $fecha['Agecitacalendariodia']['available']; ?></td>
				<?php }?>	
		    </tr>
		    <?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php } ?>