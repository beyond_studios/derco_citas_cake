<script>
	window.close();
</script>
<div id="cuerpo">
	<div class="espacio"></div> 
	<div id="tablaReporte">
		<table id="listaPrincipal" cellpadding="0" cellspacing="0" width="100%">
			<thead>
				<tr>                     
					<th rowspan="3"><?php echo __('Taller',true);?></th> 
	                <th colspan="9"><?php echo __('Tipo de Servicio');?></th>
				</tr>
				<tr>                     
					<th colspan="2"><?php echo __('Diurno',true);?></th> 
	                <th><?php echo __('%Citas');?></th>
					
					<th colspan="2"><?php echo __('Express',true);?></th> 
	                <th><?php echo __('%Citas');?></th>
					
					<th colspan="2"><?php echo __('Nocturno',true);?></th> 
	                <th><?php echo __('%Citas');?></th>
				</tr>
				<tr>                     
					<th><?php echo __('Disponible',true);?></th> 
	                <th><?php echo __('Programadas');?></th>
					<th><?php echo __('Programadas');?></th>
					
					<th><?php echo __('Disponible',true);?></th> 
	                <th><?php echo __('Programadas');?></th>
					<th><?php echo __('Programadas');?></th>
					
					<th><?php echo __('Disponible',true);?></th> 
	                <th><?php echo __('Programadas');?></th>
					<th><?php echo __('Programadas');?></th>
				</tr>
			</thead>
			<tbody>
				<?php
				//set variables total
				$diurnoTotalProgrammed = 0;$diurnoTotalAvailable = 0;
				$diurnoParcialProgrammed = 0;$diurnoParcialAvailable = 0;
				
				$nocturnoTotalProgrammed = 0;$nocturnoTotalAvailable = 0;
				$nocturnoParcialProgrammed = 0;$nocturnoParcialAvailable = 0;
				
				$expresTotalProgrammed = 0;$expresTotalAvailable = 0;
				$expresParcialProgrammed = 0;$expresParcialAvailable = 0; 
				foreach($reporte as $id =>$item){?>
				<tr>
					<td><?php echo $item['Secproject']['name'] ?></td>
					<?php if(!empty($item['Agemotivoservicio'])){?>
						<!--Diurno -->
						<td>
							<?php if(!empty($item['Agemotivoservicio']['1'])){
								$diurnoParcialProgrammed = $item['Agemotivoservicio']['1']['Agecitacalendario']['Programed'];
								$diurnoTotalProgrammed += $diurnoParcialProgrammed;
								echo $diurnoParcialProgrammed;
							}?>
						</td>
						<td>
							<?php if(!empty($item['Agemotivoservicio']['1'])){
								$diurnoParcialAvailable = $item['Agemotivoservicio']['1']['Agecitacalendario']['Available'];
								$diurnoTotalAvailable += $diurnoParcialAvailable;
								echo $diurnoParcialAvailable;
							}?>
						</td>
						<td>
							<?php if(!empty($item['Agemotivoservicio']['1'])){
								echo number_format((($diurnoParcialAvailable*100)/$diurnoParcialProgrammed),2).'%';
							}?>
						</td>
						
						<!--Nocturno -->
						<td>
							<?php if(!empty($item['Agemotivoservicio']['2'])){
								$nocturnoParcialProgrammed = $item['Agemotivoservicio']['2']['Agecitacalendario']['Programed'];
								$nocturnoTotalProgrammed += $nocturnoParcialProgrammed;
								echo $nocturnoParcialProgrammed;
							}?>
						</td>
						<td>
							<?php if(!empty($item['Agemotivoservicio']['2'])){
								$nocturnoParcialAvailable = $item['Agemotivoservicio']['2']['Agecitacalendario']['Available'];
								$nocturnoTotalAvailable += $nocturnoParcialAvailable;
								echo $nocturnoParcialAvailable;
							}?>
						</td>
						<td>
							<?php if(!empty($item['Agemotivoservicio']['2'])){
								echo number_format((($nocturnoParcialAvailable*100)/$nocturnoParcialProgrammed),2).'%';
							}?>
						</td>
						
						<!--Express -->
						<td>
							<?php if(!empty($item['Agemotivoservicio']['3'])){
								$expresParcialProgrammed = $item['Agemotivoservicio']['3']['Agecitacalendario']['Programed'];
								$expresTotalProgrammed += $expresParcialProgrammed;
								echo $expresParcialProgrammed;
							}?>
						</td>
						<td>
							<?php if(!empty($item['Agemotivoservicio']['3'])){
								$expresParcialAvailable = $item['Agemotivoservicio']['3']['Agecitacalendario']['Available'];
								$expresTotalAvailable += $expresParcialAvailable;
								echo $expresParcialAvailable;
							}?>
						</td>
						<td>
							<?php if(!empty($item['Agemotivoservicio']['3'])){
								echo number_format((($expresParcialAvailable*100)/$expresParcialProgrammed),2).'%';
							}?>
						</td>
					<?php }?>
				</tr>
				<?php }?>
				<!-- TOTAL GENERAL-->
				<tr>
					<td class="dato"><?php echo "Total General" ?></td>
					<td class="numerobold"><?php echo $diurnoTotalProgrammed ?></td>
					<td class="numerobold"><?php echo $diurnoTotalAvailable ?></td>
					<td class="numerobold"><?php echo number_format($diurnoTotalAvailable*100/$diurnoTotalProgrammed,2).'%' ?></td>
					<td class="numerobold"><?php echo $nocturnoParcialProgrammed ?></td>
					<td class="numerobold"><?php echo $nocturnoParcialAvailable ?></td>
					<td class="numerobold"><?php echo number_format($nocturnoParcialAvailable*100/$nocturnoParcialProgrammed,2).'%' ?></td>
					<td class="numerobold"><?php echo $expresTotalProgrammed ?></td>
					<td class="numerobold"><?php echo $expresParcialAvailable ?></td>
					<td class="numerobold"><?php echo number_format($expresParcialAvailable*100/$expresTotalProgrammed,2).'%' ?></td>
				</tr>
			</tbody>
		</table>
		<br><br><br><br>
		<table id="listaPrincipal" cellpadding="0" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th><?php echo __('TALLER');?></th>
					<?php foreach($marcas as $id => $item){?>
						<th><?php echo $item['Marca']['description']?></th>
					<?php }?>
				</tr>
			</thead>
			<tbody>
				<?php foreach($reporte2 as $id => $item){ ?>
					<tr>
						<td><?php echo $item['Secproject']['name'] ?></td>
						<?php foreach($item['Marcas'] as $id2 => $item2){?>
							<td class="numero"><?php if(!empty($item2['total'])) {
								echo $item2['total'];
							}?></td>
						<?php }?>
					</tr>
				<?php }?>
				<tr>
					<td class="dato"><?php echo "Totales";?></td>
					<?php foreach($totalesMarca as $id => $item){?>
						<td class="numerobold"><?php if(!empty($item['total'])) {
							echo $item['total'];
						}?></td>
					<?php }?>
				</tr>
			</tbody>
		</table>
		<br><br><br><br>
		<table id="listaPrincipal" cellpadding="0" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th><?php echo __('TALLER');?></th>
					<?php foreach($tiposervicio as $id => $item){?>
						<th><?php echo $item['Agetiposervicio']['description']?></th>
					<?php }?>
					<th><?php echo __('TOTAL');?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($reporte3 as $id => $item){ ?>
					<?php $totalxsucursal = 0; $totalxsucursalxtiposervicio = 0;?>
					<tr>
						<td><?php echo $item['Secproject']['name'] ?></td>
						<?php foreach($item['Agetiposervicios'] as $id2 => $item2){?>
							<td class="numero"><?php if(!empty($item2['total'])) {
								$totalxsucursal += $item2['total'];
								echo $item2['total'];
							}?></td>
						<?php }?>
						<td class="numerobold"><?php echo ($totalxsucursal!=0)?$totalxsucursal:''; ?></td>
					</tr>
				<?php }?>
				<tr>
					<td class="dato"><?php echo "Totales";?></td>
					<?php foreach($totalesTipoServicio as $id => $item){?>
						<td class="numerobold"><?php if(!empty($item['total'])) {
							$totalxsucursalxtiposervicio += $item['total'];
							echo $item['total'];
						}?></td>
					<?php }?>
					<td class="numerobold"><?php echo ($totalxsucursalxtiposervicio!=0)?$totalxsucursalxtiposervicio:''; ?></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div id="pie"></div>
</div>