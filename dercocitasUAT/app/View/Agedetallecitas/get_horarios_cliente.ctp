<?php if ($horarios){ ?>
<?php echo $this->Form->hidden('Agecitacalendario.id', array('value'=>$agecitacalendario_id))?> 
<div id="listaCaja" style="overflow: auto; height: 145px;">
	<center>
		<div class="tableData styleTable1">
			<table  id="HorariosClientes" class="listaPrincipal" >
				<thead>
					<tr>
						<th><?php echo __('TALLER_ETIQUETA_HORA')?></th>
						<th><?php echo __('TALLER_ETIQUETA_DISPOSICION')?></th>
						<th><?php echo __('GENERAL_SELECCIONAR')?></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($horarios as $key=>$horario): ?>
					<?php $class = ($key%2 == 0) ? "par" : "impar"; ?>
					<tr class="<?php echo $class; ?>">
						<td class="centrado">
							<?php echo date('H:i', strtotime($horario['Agecitacalendariodia']['initDateTime'])); ?> - 
							<?php echo date('H:i', strtotime($horario['Agecitacalendariodia']['endDateTime'])); ?>
						</td>
						<td class="centrado"><?php echo $horario['Agecitacalendariodia']['estado']; ?></td>
						<td class="centrado">
							<?php $nameRadio = "data[Agecitacalendariodia][radio".$horario['Agecitacalendariodia']['id']."]";
								$disabled = ($horario['Agecitacalendariodia']['available'] != 0) ? "" : "disabled='disabled'";
							?>
							<input id="AgecitacalendariodiaRadio<?php echo $horario['Agecitacalendariodia']['id']; ?>" 
									type="checkbox" name="<?php echo $nameRadio; ?>" <?php echo $disabled; ?> onclick="alternarChecks(this)" />
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</center>
</div>
<?php } else { ?>
<div id="listaPrincipal">
	<div id="flashMessage" style="width:98%; color: blue; text-align: center; font-size: 8pt;">
		Lo sentimos, no existen horarios para la fecha elegida</div>
	</div>
<?php } ?>