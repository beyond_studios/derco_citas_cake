<?php echo $this->element('dialog_message'); ?>
<?php echo $this->Html->script('clientes/actualizar_cliente_sap'); ?>
<?php echo $this->Html->script('clientes/comun_clientes'); ?>
<?php echo $this->Session->flash();?>
<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th colspan="2" class="titulo">
                <?php echo __('CLIENTE_TITULO_ACTUALIZAR_CLIENTE_SAP') ?>
            </th>
        </tr>
    </thead>
</table>
<br />
<div id="listaCaja">
	<table id="formularioEdicion" width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tbody>
			<tr>
				<td>
					<fieldset style="width:90%; height: auto;  margin-left: 20px; " >
						<legend><?php echo __('CLIENTE_ETIQUETA_DATOS_SISTEMA_SAP')?> </legend>
						<?php echo $this->Form->create('Clientesap', array('url'=>'#'));?>
						<?php echo $this->Form->hidden('codigosap', array())?>
						<div id="buscadorBasico">
							<table id="formularioEdicion" border="0" cellspacing="0" cellpadding="3"  align="center"  width="90%">
								<tbody>
									<tr >
										<td class="etiqueta" >
						            		<?php echo __('GENERAL_CODIGO_SAP');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('IdCliente', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
									<tr></tr>
									<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_APELLIDOS');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('Apellidos', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr></tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_NOMBRES');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('Nombres', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr></tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_TIPO_DOCUMETO_NUMERO');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('NumeroDoc', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('TIPO_DE_CLIENTE');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('StrTipoCliente', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_DIRECCION');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('Direccion', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_CIUDAD');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('Ciudad', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_DISTRITO');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('Distrito', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_TELEFONO');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('Telefono', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_CELULAR');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('Celular', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_EMAIL');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('Correo', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
								</tbody>
							</table>
						</div>
						<?php echo $this->Form->end(); ?>
					</fieldset>
				</td>
				<td>
					<fieldset style="width:90%; height: auto; margin-right: 20px; ">
						<legend><?php echo __('CLIENTE_ETIQUETA_DATOS_SISTEMA_WEB')?> </legend>
						<?php echo $this->Form->create('Cliente', array('url'=>'#'));?>
						<?php echo $this->Form->hidden('id', array())?>
						<div id="buscadorBasico">
							<table id="formularioEdicion" border="0" cellspacing="0" cellpadding="3" align="center" width="90%">
								<tbody>
									<tr>
										<td class="etiqueta">
						            		<?php echo __('GENERAL_CODIGO_SAP');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('codigo_sap', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
									
									<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_APELLIDOS');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('apellidoPaterno', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>
									
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_NOMBRES');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('nombres', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_TIPO_DOCUMETO_NUMERO');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('documento_numero', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('TIPO_DE_CLIENTE');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('str_cliente_tipo', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_DIRECCION');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('direccion', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_CIUDAD');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('ciudad', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_DISTRITO');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('distrito', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_TELEFONO');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('telefono', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_CELULAR');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('celular', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
						        	<tr>
										<td class="etiqueta">
						            		<?php echo __('CLIENTE_ETIQUETA_CLIENTE_EMAIL');?>
						            	</td>
										<td class="valor"><?php echo $this->Form->input('email', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>										
						        	</tr>
								</tbody>
							</table>
						</div>						
						<?php echo $this->Form->end(); ?>
					</fieldset>
				</td>
			</tr>
			<tr>
				<td>
					<fieldset style="width:90%; height: auto; margin-left: 20px; ">
						<legend><?php echo __('DATOS_DEL_VEHICULO')?></legend>
						<?php echo $this->Form->create('Vehiculo', array('url'=>'#'));?>
						<table>
							<tr>
								<td>
									<?php echo $this->Html->link(__('SELECCIONAR_VEHICULO',true), 'javascript:;',
									 array('onclick' => "openDialog()",'escape'=>false), null);?>
								</td>
							</tr>
							<tr>
								<td class="etiqueta">
						            <?php echo __('CLIENTE_ETIQUETA_PLACA');?>
						         </td>
								<td class="valor"><?php echo $this->Form->input('Placa', array('label'=>false,'type'=>'text','class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>
							</tr>
							<tr>
								<td class="etiqueta">
						            <?php echo __('Marca');?>
						         </td>
								<td class="valor"><?php echo $this->Form->input('Marca', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>
							</tr>
							<tr>
								<td class="etiqueta">
						            <?php echo __('Modelo');?>
						         </td>
								<td class="valor"><?php echo $this->Form->input('Modelo', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?></td>
							</tr>
						</table>
						<?php echo $this->Form->end(); ?>
					</fieldset>
				</td>
			</tr>
		</tbody>
	</table>	
</div>
<br/>
<div align="center">				
	<?php echo $this->Form->button(__('CLIENTE_ETIQUETA_ACTUALIZAR_CODIGO_CLIENTE_CARS'), array('id'=>'actualizarCliente','style'=>'width:190px'));?>
	&nbsp;
	<?php echo $this->Form->button(__('GENERAL_CERRAR'), array('style'=>'width:50px', 'type'=>'button','onClick' => 'javascript:window.close()'));?>			
</div>
<div id="dialog_placa" title="Seleccione Placa">
	<table style="margin:0 auto;">
		<body>
			<tr>
				<td class="etiqueta"><?php echo __('CLIENTE_ETIQUETA_PLACA');?></td>
				<td class="valor"><?php echo $this->Form->input('placas', array('label'=>false,'type'=>'select','options' => $placas,'error'=>false, 'style'=>'width:100%;')); ?></td>										
			</tr>
		</tbody>
	</table>
</div>
</div>