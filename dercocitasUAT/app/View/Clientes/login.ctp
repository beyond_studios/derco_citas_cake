<?php echo $this->element('nv_forgot_password');?>


<div class="login centerAll small">
<h1 class="titleCenter">INGRESO</h1>
<?php echo $this->Html->script('clientes/index'); ?>
<?php echo $this->Form->create('Cliente', array('class'=>'fomrLogin','url' => array('controller' => 'Clientes', 'action' =>'loginCliente')));  ?>
  
    <div class="formTable width70">
        <div id="camposLogin" class="groupFields">
            <div class="field">
                <label><?php echo $this->Form->label(__('N° Documento',true)); ?></label>
                <?php echo $this->Form->text('username',array('style' => '')); ?>
                <div class="errorValidacion"></div>
            </div>
        </div>

        <div class="groupFields">
            <div class="field">
                <?php echo $this->Form->label(__('Contraseña',true)); ?>
                <?php echo $this->Form->text('password',array('style' => '', 'type'=>'password')); ?>
                <div class="errorValidacion"></div>
            </div>
        </div>
        <div class="groupFields">
            <div class="field">
                <?php echo $this->Form->submit(__('Ingresar',true),array('div' => false, 'class'=>'guinda')); ?>
            </div>
        </div>
        <div class="groupFields col2" style="margin-top: 20px;">
            <div class="field">
                <?php
                    $url =  $this->Html->url(array('action'=>'forgot_password'));
                    echo $this->Html->link(__('¿Olvidó su contraseña?', true),$url,array('class'=>'linksForm',));
                ?>
            </div>
            <div class="field">
                <?php
                    $url =  $this->Html->url(array('action'=>'addWebCliente'));
                    echo $this->Html->link(__('Quiero registrarme', true), $url,array('class'=>'color_guinda linksForm'));
                ?>
            </div>
        </div>
    </div>
        <?php
    echo $this->Form->end();
?>
</div>

