<div class="login centerAll small">

	<?php echo $this->Html->script('clientes/add'); ?>
	<?php //echo $this->element('actualizar_padre');?>
	
			<h1 class="titleCenter"><?php echo (__('CAMBIAR CONTRASEÑA',true));?></h1>
				
            	<?php if(empty($guardadoExistoso)): ?>
			<?php echo $this->Form->create('Cliente', array('class'=>'fomrLogin'));?>
            <P class='forgot text'>
				Ingrese su Nro. documento y luego seleccione <span>ENVIAR,</span> en breves momentos le enviaremos un mensaje al <span>EMAIL</span> que ingresó al momento de registrarse. 
            </P>
                <div class="formTable ">
							<div class="groupFields forgot">
								<div class="field">
							        <label style='padding-bottom:20px'><?php echo(__('Número de Documento', true)) ?></label>
									 <?php
							echo $this->Form->input('documento_numero', array('placeholder'=>'Ingrese su número de documento', 'label' => false, 'class' => 'centerAll', 'error' => false));
							echo $this->Form->error('documento_numero', array(
								'isUnique' => __('empresaNombreUnico', true),
								'notEmpty' => __('empresaNombreNoVacio', true),
								'maxLength' => __('empresaNombreLongitud', true),
									), array('class' => 'input text required error'));
							?>
								</div>
							</div>
            <?php endif; ?>
            <div class='forgot text'>
						<?php echo $this->Session->flash();?>
						</div>
            	<?php if(empty($guardadoExistoso)): ?>
							<div class="groupFields ">
								<div class="field">
							    <?php echo $this->Form->submit(__('Enviar',true), array('div'=>false, 'class'=>'guinda'));	?>			
							    <?php echo $this->Form->button(__('Cancelar',true), array('div'=>false, 'class'=>'buttomReset', 'type'=>'reset'));	?>			
								</div>
							</div>
						</div>
            <?php echo $this->Form->end();?>
            <?php endif; ?>
</div>