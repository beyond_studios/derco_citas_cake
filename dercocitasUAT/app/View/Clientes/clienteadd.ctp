<div class="span-9" >
	
	<?php echo $this->Html->script('clientes/clienteadd.js'); ?>
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-9" >
		<h3><?php echo (__('Datos del Cliente',true));?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Cliente');?>
		<div class="span-10" >
            <div class="span-4" >
                <label>
                    <?php echo(__('Código SAP', true)) ?><span class="error"><?php echo " *"; ?>	</span>
                </label> 
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('codigo_sap', array('label' => false, 'class' => 'span-5 required', 'error' => false));
                echo $this->Form->error('codigo_sap', array(
                    'isUnique' => __('empresaNombreUnico', true),
                    'notEmpty' => __('empresaNombreNoVacio', true),
                    'maxLength' => __('empresaNombreLongitud', true),
                        ), array('class' => 'input text required error'));
                ?>
            </div>
        </div>	
		
		<div class="span-10" >
            <div class="span-4" >
                <label><?php echo (__('CLIENTE_ETIQUETA_CLIENTE_NOMBRES', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('nombres', array('label' => false, 'class' => 'span-5 required', 'error' => false));
                echo $this->Form->error('nombres', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>	
		
        <div class="span-10" >
            <div class="span-4" >
                <label>
                    <?php echo(__('CLIENTE_ETIQUETA_TIPO_DOCUMETO_NUMERO', true)) ?><span class="error"><?php echo " *"; ?>	</span>
                </label> 
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('documento_numero', array('label' => false, 'class' => 'span-5 required digits', 'error' => false, 'maxlength'=>8, 'minlenght'=>8));
                echo $this->Form->error('documento_numero', array(
                    'isUnique' => __('empresaNombreUnico', true),
                    'notEmpty' => __('empresaNombreNoVacio', true),
                    'maxLength' => __('empresaNombreLongitud', true),
                        ), array('class' => 'input text required error'));
                ?>
            </div>
        </div>	     
		
		<div class="span-10" >
            <div class="span-4" >
                <label>
                    <?php echo(__('Tipo Cliente', true)) ?>
                </label> 
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('cliente_tipo', array('label' => false, 'class' => 'span-5', 'error' => false));
                echo $this->Form->error('cliente_tipo', array(
                    'isUnique' => __('empresaNombreUnico', true),
                    'notEmpty' => __('empresaNombreNoVacio', true),
                    'maxLength' => __('empresaNombreLongitud', true),
                        ), array('class' => 'input text required error'));
                ?>
            </div>
        </div>	
		
	     <div class="span-10" >
            <div class="span-4" >
                <label><?php echo (__('Dirección', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('direccion', array('label' => false, 'class' => 'span-5 required', 'error' => false));
                echo $this->Form->error('direccion', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>	

        <div class="span-10" >
            <div class="span-4" >
                <label><?php echo (__('Ciudad', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('ciudad', array('label' => false, 'class' => 'span-5 required', 'error' => false));
                echo $this->Form->error('ciudad', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>  

        <div class="span-10" >
            <div class="span-4" >
                <label><?php echo (__('Distrito', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('distrito', array('label' => false, 'class' => 'span-5 required', 'error' => false));
                echo $this->Form->error('distrito', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>
        
        <div class="span-10" >
            <div class="span-4" >
                <label><?php echo (__('Teléfono', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('telefono', array('label' => false, 'class' => 'span-5 required', 'error' => false));
                echo $this->Form->error('telefono', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>
      
        <div class="span-10" >
            <div class="span-4" >
                <label><?php echo (__('Celular', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('celular', array('label' => false, 'class' => 'span-5 required', 'error' => false));
                echo $this->Form->error('celular', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>

        <div class="span-10" >
            <div class="span-4" >
                <label><?php echo (__('Email', true)) ?></label><span class="error"><?php echo " *"; ?>	</span>
            </div>
            <div class="span-5 last" >
                <?php
                echo $this->Form->input('email', array('label' => false, 'class' => 'span-5 required', 'error' => false));
                echo $this->Form->error('email', array(
                    'isUnique' => __('empresaCodigoUnico', true),
                    'notEmpty' => __('empresaCodigoNoVacio', true),
                    'maxLength' => __('empresaCodigoLongitud', true),
                    'alphaNumeric' => __('empresaCodigoEspacios', true)
                        ), array('class' => 'input text required error'));
                ?>	
            </div>
        </div>
		<br/>
		<hr/>
		
		<div class=" span-8 botones" >
			<?php if(!$guardadoExistoso){?>
				<?php echo $this->Html->image('loader.gif', array('id'=>'imgLoader','style' => 'display:none;'));?>
				<?php echo $this->Form->submit(__('Submit',true), array('id'=>'btnGuardar','div'=>false));	?>
				<?php echo $this->Form->button(__('Reset',true), array('id'=>'btnReset','type'=>'reset')); ?>
			<?php }?>
			<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end();?>

<?php echo $this->element('actualizar');?>
</div>