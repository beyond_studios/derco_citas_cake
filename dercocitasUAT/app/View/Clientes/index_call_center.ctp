<?php echo $this->Html->script('layouts/mijquery.js'); ?>
<?php echo $this->Html->script('clientes/index.js'); ?>
		<?php echo $this->Session->flash();  ?>
		<br/>
			<h3 id="tituloTable"><?php echo __('CLIENTE_TITULO_CLIENTES');?></h3>
		
		<div class="box">
			<div id="agregar" class="span-5" >
			<?php 	
					$url = isset($url)?$url:'addWebCliente';
					$url = $this->Html->url(array("action"=>$url));  
					echo $this->Html->link($this->Html->image('agregar.png', array('alt' => 'Agregar')),
									 'javascript:;',
									 array('onclick' => "add('".$url."')",'escape'=>false),
									 null); 
			?>
				&nbsp;
			<?php 
				echo $this->Html->link(__('Add', true), 'javascript:;',array('onclick' => "add('".$url."')")); 
			?>					
			</div>			
			<div id="buscador" class="">
				<?php
					echo $this->Form->create('bsc',array('url'=>'indexCallCenter'));
					echo $this->Form->label(__('Cliente'));
					echo '&nbsp;';
					echo $this->Form->input('nombres', array('class'=>'span-5 buscador-text','label'=>false, 'div'=>false ));	
					echo '&nbsp;'; 
					echo '&nbsp;'; 
					echo '&nbsp;';
					echo '&nbsp;';
					echo '&nbsp;';	
					echo '&nbsp;';
					echo $this->Form->label(__('Placa N°'));
					echo '&nbsp;';
					echo $this->Form->input('placa', array('class'=>'span-5 buscador-text','label'=>false, 'div'=>false));	
					echo '&nbsp';
					echo '&nbsp;';
					echo '&nbsp;';
					echo $this->Form->label(__('Estado'));
					echo '&nbsp;';
					echo $this->Form->select('std',$std ,array('class'=>'buscador','empty'=>'Seleccione'),false);
					echo '&nbsp;'; 
					echo $this->Form->submit(__('Search'),array('div'=>false));
					echo $this->Form->end();
				?>
			</div>
		</div>		
		<table cellpadding="0" cellspacing="0" class="table" >
			<thead>
			<tr>
                 
				<th><?php echo $this->Paginator->sort('Cliente.nombres',__('CLIENTE_CAMPO_APELLIDOS_NOMBRES_RAZONSOCIAL',true));?></th>        
				<th><?php echo $this->Paginator->sort('Cliente.documento_tipo',__('CLIENTE_CAMPO_TIPO_DOCUMETO',true));?></th>        
				<th><?php echo $this->Paginator->sort('Cliente.documento_numero',__('CLIENTE_CAMPO_TIPO_DOCUMETO_NUMERO',true));?></th>        
				<th><?php echo $this->Paginator->sort('Cliente.codigo_sap',__('GENERAL_CODIGO_SAP',true));?></th>				       
				<th><?php echo $this->Paginator->sort('Cliente.cliente_estado',__('estado',true));?></th>
				<th class="actionsfijo"><?php  echo __('Actions');?></th>
			
			</tr>			
			</thead>
			
			<tbody>
				<?php  foreach ($clientes as $cliente):?>
				<tr>
					<td>
						<?php 	echo $cliente['Cliente']['nombres']; ?>	
					</td>
					<td class="textc">
						<?php 	echo $cliente['Cliente']['documento_tipo']; ?>	
					</td>
					<td class="textc" >
						<?php echo $cliente['Cliente']['documento_numero'];	?>
					</td>
					<td class="textc">
						<?php 	echo $cliente['Cliente']['codigo_sap']; ?>	
					</td>
					
					<td class="textc">
						<?php echo $cliente['Cliente']['cliente_estado'] == 'PE' ? 
										__('GENERAL_PENDIENTE',true)
									:
										($cliente['Cliente']['cliente_estado'] == 'CP'?
											__('GENERAL_COMPLETO',true)
										:
											__('Parcial',true))
									; ?>
					</td>
			
					<td class="actionsfijo">						
					<?php 
					$url = $this->Html->url(array('controller'=>'Clientes','action'=>'actualizarClienteSap',$cliente['Cliente']['id']));
					$image = $this->Html->image('derco/defecto/lista.png', array('title'=>__('Actualizar datos',true), "alt" => "mostrar", 'width'=>'19'));
					echo $this->Html->link($image, 'javascript:;',array('onclick' => "actualizarClienteSap('".$url."')",'escape'=>false), null, false);
					
					if($cliente['Cliente']['cliente_estado']=='PE' || $cliente['Cliente']['cliente_estado']=='PA' || $cliente['Cliente']['cliente_estado']=='CP'){
						echo '&nbsp;';
						$url1 = $this->Html->url(array('controller'=>'Clientes','action'=>'actualizarVehiculo',$cliente['Cliente']['id']));
						$image = $this->Html->image('derco/defecto/actualizar.png', array('title'=>__('Actualizar datos Vehiculo',true), "alt" => "Actualizar", 'width'=>'19'));
						echo $this->Html->link($image, 'javascript:;',array('onclick' => "actualizarVehiculo('".$url1."')",'escape'=>false), null, false);
					}
					?>
					</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
		</table>

<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>