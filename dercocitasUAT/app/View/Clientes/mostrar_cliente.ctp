<?php 
		echo $this->Html->css('modulo_taller/derco/derco_ventana.css');
		echo $this->Html->script('jquery-validate/jquery.validate.js');
		echo $this->Html->script('clientes/mostrar_cliente.js');	
		if(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2)=='es'){
			echo $this->Html->script('jquery-validate/localization/messages_es.js');
		};
?>
	<?php echo $this->element('nv_forgot_password');?>

<div class="login width100 ">
	
		<h1 st><?php echo __('MOSTRAR_CLIENTE');?></h1>
		<div class="titleBar" style="text-align: left;">
			<?php echo __('INFORMACION_CLIENTE')?> 
		</div>
				<?php echo $this->Form->create('Cliente', array('class'=>'fomrLogin ')); ?>
				<?php echo $this->Form->hidden('id') ?>
						<div class="formTable leftAling">
							<div class="groupFields col4">
								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('GENERAL_CODIGO_SAP');?></label>
									</div>
									<?php echo $this->Form->input('codigo_sap', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?>
								</div>

								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('CLIENTE_ETIQUETA_CLIENTE_NOMBRES');?></label>
									</div>
									<?php echo $this->Form->input('nombres', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?>
								</div>

								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('CLIENTE_ETIQUETA_CLIENTE_TIPO_DOCUMENTO');?></label>
									</div>
									<div>
										<?php // echo $this->Form->input('documento_tipo', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false, 'style'=>'width:100%;')); ?>
										<select class="styled-select slate">
											<option value="DNI">DNI</option>
											<option value="saab">Carnet de Extranjeria</option>
											<option value="opel">Pasaporte</option>
											<option value="audi">Licencia de conducir</option>
										</select>
									</div>
								</div>
								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('CLIENTE_ETIQUETA_TIPO_DOCUMETO_NUMERO');?></label>
									</div>
									<?php echo $this->Form->input('documento_numero', array('label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false)); ?>
								</div>
							</div>
							<div class="groupFields col4">
								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('CLIENTE_ETIQUETA_CLIENTE_TELEFONO');?></label>
									</div>
									<?php echo $this->Form->input('telefono', array('label'=>false,'class'=>'required digits','error'=>false)); ?>
								</div>
								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('CLIENTE_ETIQUETA_CLIENTE_CELULAR');?></label>
									</div>
									<?php echo $this->Form->input('celular', array('label'=>false,'class'=>'required digits','error'=>false)); ?>
								</div>
								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('CLIENTE_ETIQUETA_CLIENTE_EMAIL');?></label>
									</div>
									<?php echo $this->Form->input('email', array('label'=>false,'class'=>'email required','error'=>false)); ?>
								</div>

								<div class="field">
									
								</div>

							</div>
							<div class="groupFields col4">
								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('CLIENTE_ETIQUETA_CLIENTE_CIUDAD');?></label>
									</div>
									<?php echo $this->Form->input('ciudad', array('label'=>false,'class'=>'required','error'=>false)); ?>
								</div>
								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('CLIENTE_ETIQUETA_CLIENTE_DISTRITO');?></label>
									</div>
									<?php echo $this->Form->input('distrito', array('label'=>false,'class'=>'required','error'=>false)); ?>
								</div>
								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('CLIENTE_ETIQUETA_CLIENTE_DIRECCION');?></label>
									</div>
									<?php echo $this->Form->input('direccion', array('label'=>false,'class'=>'required','error'=>false)); ?>
								</div>
								<div class="field">
									
								</div>

							</div>
							<div class='bottom-actions'>
								<div class="groupFields centerAling">
									<div class="field">
										<?php echo $this->Form->submit(__('Actualizar'), array('label'=>false,'class'=>'guinda', 'div'=>false));?>
									</div>
								</div>
							</div>
						</div>
					</form>

					<div class="titleBar styleGriss" style="text-align: left;">
						<?php echo __('ACCESO_SISTEMA_WEB')?> 
					</div>
					<form class="fomrLogin leftAling">
						<div class="formTable ">
							<div class="groupFields col4 dobleWidth">
								<div class="field">
									<div>
										<label class='leftAling'><?php echo __('CLIENTE_ETIQUETA_TIPO_DOCUMETO_NUMERO');?></label>
									</div>
									<div>
										<?php echo $this->Form->input('documento_numero', array('label'=>false,'type'=>'text','class'=>'','disabled'=>'disabled','error'=>false)); ?>
									</div>
								</div>
								<div class="field" style="text-align: left;"> 
									<div>
										<label class='leftAling'><?php echo __('Password');?></label>
									</div>
									<div>
										<?php echo $this->Form->input('password', array('value'=>'??????????','label'=>false,'class'=>'soloLectura','disabled'=>'disabled','error'=>false) ); ?>
									</div>
								</div>
								<div class="field" style="width: 50%;">
										<div style='padding-top:26px'>
										
										<?php 	
												//$url = $this->Html->url(array('action'=>'add', $cliente['Cliente']['id']));
											$url =  $this->Html->url(array('action'=>'forgot_password'));
											echo $this->Html->link('Generar Nueva Contraseña',$url,array('class'=>'linksForm nuevaClave'));																		               
										?>	
									</div>
								</div>
							</div>
						</div>
					<?php echo $this->Form->end();?>

					<div class="logosFooter">
						<img src="dist/images/basLogos.png">
					</div>
				</div>

