<?php echo $this->Html->script('Agedetallecitaslostsmotives/index.js'); ?>
		<?php echo $this->Session->flash();  ?>
		<br/>
			<h3 id="tituloTable"><?php echo __('MOTIVOS_CITAS_NO_ATENDIDAS');?></h3>
		
		<div class="box">
			<div id="agregar" class="span-5" >
			<?php echo $this->element('agregar'); ?>	
			</div>
			
			<div id="buscador" class="">
				<?php echo $this->element('buscador', array('elementos'=>$elementos,'url' => 'index')); ?>
			</div>
		</div>		
		<table cellpadding="0" cellspacing="0" class="table" >
			<thead>
			<tr>
				<th><?php echo $this->Paginator->sort('Agedetallecitaslostsmotive.description',__('descripcion'));?></th>       
				<th><?php echo $this->Paginator->sort('Agedetallecitaslostsmotive.status',__('estado',true));?></th>
				<th class="actionsfijo"><?php  echo __('Actions');?></th>
			</tr>			
			</thead>
			
			<tbody>
				<?php  foreach ($agedetallecitaslostsmotives as $agedetallecitaslostsmotive):?>
				<tr>
				<td>
					<?php 		
							$url = $this->Html->url(array('action'=>'view', $agedetallecitaslostsmotive['Agedetallecitaslostsmotive']['id']));
							echo $this->Html->link($agedetallecitaslostsmotive['Agedetallecitaslostsmotive']['description'], 'javascript:;',array('onclick' => "mostrar('".$url."')",'escape'=>false), null);	
					?>	
				</td>
				<td class="textc">
					<?php echo $agedetallecitaslostsmotive['Agedetallecitaslostsmotive']['status'] == 'AC' ? 
									__('Enable',true)
								:
									($agedetallecitaslostsmotive['Agedetallecitaslostsmotive']['status'] == 'DE'?
										__('Disable',true)
									:
										__('Limited',true))
								; ?>
				</td>
		
					<td class="actionsfijo">
				<?php echo $this->element('actionDerco', 
								array('id'=>$agedetallecitaslostsmotive['Agedetallecitaslostsmotive']['id'], 
										'name'=> $agedetallecitaslostsmotive['Agedetallecitaslostsmotive']['description'],
										'estado'=> $agedetallecitaslostsmotive['Agedetallecitaslostsmotive']['status'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<div id ="paging" class="span-18">
    <?php echo $this->element('paginador'); ?>
</div>
<div class="clear"></div>