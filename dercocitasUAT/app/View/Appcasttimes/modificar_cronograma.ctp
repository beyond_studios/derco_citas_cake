<?php echo $this->Html->script('layouts/mijquery.js'); ?>
<?php echo $this->Html->script('appcasttimes/modificarCronograma.js'); ?>
<div class='span-14'>
		<?php echo $this->Session->flash();?>
		<br/>
			<h3 id="tituloTable"><?php echo __('plantillaCronograma');?></h3>
		
		
			<div id="agregar" class="span-5" >
			<?php echo $this->element('agregar',array('url'=>'add/'.$cast_id)); ?>	
			</div>
		<div>
		</div>

<table cellpadding="0" cellspacing="0" class="table" >
<thead>
	<tr>
        <th colspan="2"><?php echo __('intervaloCronograma');?></th>
        <th colspan="1" style="width:90px"><?php echo __('Nro Programado'); ?></th>
        <th rowspan="2" style="width:90px"><?php echo __('Actions');?></th>
    </tr>
    <tr>
    	<th><?php echo __('horaInicioCronograma');?></th>
    	<th><?php echo __('horaFinCronograma');?></th>
    	<th><?php echo __('');?></th>
    </tr>
</thead>
<tbody>
	<?php foreach ($castTimes as $castTime): ?>
    <tr>
    	<td class="actionsfijo">
        	<?php echo $castTime['Appcasttime']['inittime']; ?>
        </td>
        <td class="actionsfijo">
        	<?php echo $castTime['Appcasttime']['endtime']; ?>
        </td>
        <td class="actionsfijo">
        	<?php echo $castTime['Appcasttime']['programed']; ?>
        </td>
		<td class="actionsfijo">
				<?php echo $this->element('action', 
								array('id'=>$castTime['Appcasttime']['id'], 
										'name'=> $castTime['Appcasttime']['inittime'].' - '.$castTime['Appcasttime']['endtime'],
										'estado'=> $castTime['Appcasttime']['status'])); ?>
			</td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
		<br/>
		<div align="center" >
			<?php  echo $this->Form->button(__('cerrar'), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
</div>