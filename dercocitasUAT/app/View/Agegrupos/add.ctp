<?php echo $this->Html->script('agegrupos/add.js'); ?>

<div class="span-8" >
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo(__('Agregar',true));?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Agegrupo');?>

		<div id="rowLast" class="span-8" >
			<div class="span-3" >
				<label><?php echo(__('descripcion',true))?></label><span class="error"><?php echo " *";?></span>
				</label> 
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('description', array('label'=>'','class'=>'span-5 required', 'error'=>false)); 
					  echo $this->Form->error('description', array(															
													   	'maxLength' =>  __('empresaTelefonoLongitud', true),
														'notEmpty' =>  __('empresaNombreNoVacio', true)											      
														), array('class' => 'input text required error'));				
				?>
			</div>
		</div>		
		<br/>
		<hr/>
		
		<div class=" span-8 botones" >
			<?php echo $this->Form->submit(__('Submit',true), array('div'=>false));	?>
			<?php echo $this->Form->button(__('Reset',true), array('type'=>'reset')); ?>				
			<?php echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end(); ?>

<?php echo $this->element('actualizar_ver'); ?>
</div>