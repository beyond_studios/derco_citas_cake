<?php echo $this->Html->script('agegrupos/add.js'); ?>

<div class="span-8" >	
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo __('Modificar Descripcion');?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Agegrupo');?>
			<?php echo $this->Form->input('id', array('type'=>'hidden')); ?>
		<div class="span-8" >
			<div class="span-3" >
				<label><?php echo (__('descripcion',true)) ?></label><span class="error"><?php echo " *";?>	</span>
			</div>
			<div class="span-5 last" >
				<?php echo $this->Form->input('description', array('label'=>false, 'class'=>'span-5','error'=>false));  
				 	  echo $this->Form->error('description', array(															
													    'isUnique' =>  __('empresaCodigoUnico', true),
														'notEmpty' =>  __('empresaCodigoNoVacio', true),
														'maxLength' =>  __('empresaCodigoLongitud', true),
														'alphaNumeric' =>  __('empresaCodigoEspacios', true)													      
														), array('class' => 'input text required error'));		
						
				?>	
			</div>
		</div>
	
		<div id="rowLast" class="span-8" >
			<div class="span-3" ><label><?php echo (__('estado',true)) ?>
			</label></div>
			<div class="span-5 last" >
				<?php
				    $options=array('AC'=>__('Enable',true),'DE'=>__('Disable',true)); 
					//$options=array('AC'=>__('Enable',true));		
					echo $this->Form->radio('status',$options,array('legend'=>'','default'=>'AC'));
				?>
			</div>
		</div>			
		
		<br/>
		<hr/>
		
		<div class=" span-8 botones" >
			<?php echo $this->Form->submit(__('Submit',true), array('div'=>false));	?>
			<?php echo $this->Form->button(__('Reset',true), array('type'=>'reset')); ?>				
			<?php echo $this->Form->button(__('Close',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end(); ?>

<?php echo $this->element('actualizar_ver'); ?>
</div>