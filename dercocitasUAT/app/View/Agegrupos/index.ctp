<?php echo $this->Html->script('agegrupos/index.js'); ?>

<br/>
<?php echo $this->Session->flash(); ?>
<h3 id="tituloTable"><?php echo __('Lista de Grupos'); ?></h3>
<div class="box">
	<div id="agregar" class="span-5" >
		<?php echo $this->element('agregar'); ?>
	</div>
	<div id="buscador" class="">
		<?php echo $this->element('buscador', array('elementos' => $elementos, 'url' => 'index')); ?>
	</div>
</div>
<?php if(isset($agegrupos) && !empty($agegrupos)): ?>
<table cellpadding="0" cellspacing="0" class="table" >
	<thead>
		<tr>
			<th><?php echo $this->Paginator->sort('Agegrupo.description',__('descripcion', true)); ?></th>
			<th><?php echo $this->Paginator->sort('Marca');?></th>
			<th><?php echo $this->Paginator->sort('Agegrupo.status',__('estado', true)); ?></th>			
			<th class="actions"><?php echo __('Actions', true); ?></th>
		</tr>			
	</thead>

	<tbody>
		<?php foreach($agegrupos as $agegrupo):
			$status = $agegrupo['Agegrupo']['status']; ?> 
		<tr>
			<td><?php echo $agegrupo['Agegrupo']['description']; ?></td>
			<td class="textc">
               <?php 						
						if(!empty($agegrupo['AgegruposMarca']['id']))
							echo $this->Html->image('correcto.gif', array('alt' => 'Agregar'));
						else
							echo '';
			   ?>

			</td>
			<td>
                <?php echo $status == 'AC' ? __('Enable', true) : ($status == 'DE' ? __('Disable', true) : __('Limited', true)); ?>
			</td>

            <td class="actionsfijo">						
					
                    <?php $url = $this -> Html -> url(array('controller' => 'Agegrupos', 'action' => 'asignar', $agegrupo['Agegrupo']['id']));
						echo $this -> Html -> link($this -> Html -> image('asignar.gif', array('alt' => 'Agregar')), 'javascript:;', array('onclick' => "asignar('" . $url . "')", 'escape' => false), null);

						echo $this -> element('actionGrupo', array('id' => $agegrupo['Agegrupo']['id'], 'description' => $agegrupo['Agegrupo']['description'], 'estado' => $agegrupo['Agegrupo']['status']));
					?>

					
		    </td>

		</tr>
		<?php endforeach; ?>
	</tbody>

</table>
<div id ="paging" class="span-18">
	<?php echo $this->element('paginador'); ?>
</div>
<?php endif; ?>
<div class="clear"></div>