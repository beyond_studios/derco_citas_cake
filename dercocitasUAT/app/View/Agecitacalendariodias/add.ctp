<div class="span-8" >
	<?php echo $this->Html->script('appcalendariopersonales/add.js',false);
	echo $this->Html->script('js-timepicker/jquery-ui-1.8.6.custom.min.js');
	echo $this->Html->script('js-timepicker/jquery-ui-timepicker-addon.js');
	echo $this->Html->script('js-timepicker/jquery.ui.datepicker-es.js');
	echo $this->Html->css('theme/redmond/jquery-ui-1.7.2.custom');
	?>
	
	<?php echo $this->Session->flash();?>
	<br/>
	<div id="titulo" class="span-8" >
		<h3><?php echo(__('plantillaAgregar',true));?></h3>
	</div>
	<hr/>
	
	<?php echo $this->Form->create('Apppersonalcalendar',array('url'=>array('controller'=>'appcalendariopersonales','action'=>'add')));?>
		<?php echo $this->Form->hidden('secperson_id')?>
		<?php echo $this->Form->hidden('anio')?>	
		<?php echo $this->Form->hidden('mes')?>	
		<?php echo $this->Form->hidden('dia')?>		
		<div class="span-8" >
				<div class="span-3" >
					<label><?php echo(__('horaInicioCronograma',true)) ?><span class="error"><?php echo " *";?>	</span>
					</label> 
				</div>
				<div class="span-5 last" >
					<?php echo $this->Form->input('initdatetime', array('label'=>false,'type'=>'text','class'=>'span-5','error'=>false));   
						  echo $this->Form->error('initdatetime', array('notEmpty' =>  __('empresaNombreNoVacio', true)), 
						  							array('class' => 'input text required error'));
					
					?>
				</div>
		</div>
		<div class="span-8" >
				<div class="span-3" >
					<label><?php echo(__('horaFinCronograma',true)) ?><span class="error"><?php echo " *";?>	</span>
					</label> 
				</div>
				<div class="span-5 last" >
					<?php echo $this->Form->input('enddatetime', array('label'=>false,'type'=>'text','class'=>'span-5','error'=>false));   
						  echo $this->Form->error('enddatetime', array('notEmpty' =>  __('empresaNombreNoVacio', true)), 
						  							array('class' => 'input text required error'));
					
					?>
				</div>
		</div>		


		<div id="rowLast" class="span-8" >
		</div>
		<br/>		
		<hr/>
		
		<div class=" span-8 botones" >
			<?php echo $this->Form->submit(__('Submit',true), array('div'=>false));	?>
			<?php echo $this->Form->button(__('Reset',true), array('type'=>'reset')); ?>				
			<?php  echo $this->Form->button(__('cerrar',true), array('type'=>'button','onClick' => 'javascript:window.close()')); ?>
		</div>
	
	<?php echo $this->Form->end(); ?>
<?php //echo $this->element('sql_dump'); ?>
<?php echo $this->element('actualizar'); ?>
</div>