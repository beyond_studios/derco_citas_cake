<table id="tituloVentana" border="0" cellspacing="0" cellpadding="0" align="center">
    <thead>
        <tr>
            <th colspan="2" class="titulo">
                <?php echo __('TALLER_CITACALENDARDAY_TITULO_AGREGAR')?>
            </th>
        </tr>
    </thead>
</table>
<form id="agregarForm" name="agregarForm" action="<?php echo $this->Html->url('/agecitacalendariodias/agregar/'.$anio.'/'.$mes.'/'.$dia.'/'.$talcitacalendar_id)?>" method="post">
	<?php echo $this->Form->hidden('Agecitacalendariodia.agecitacalendario_id')?>
	<?php echo $this->Form->hidden('Agecitacalendario.id', array('value'=>$talcitacalendar_id)); ?>
	<?php echo $this->Form->hidden('Agecitacalendariodia.anio')?>
	<?php echo $this->Form->hidden('Agecitacalendariodia.mes')?>
	<?php echo $this->Form->hidden('Agecitacalendariodia.dia')?>	
	<table id="formularioEdicion" border="0" cellspacing="0" cellpadding="0" align="center">
	    <tbody>
	    	<tr>
	            <td class="etiqueta">
	                <?php echo $this->Form->label('Agecitacalendariodia.initDateTime',__('TALLER_CITACALENDARDAY_CAMPO_INIT_DATE_TIME'))?>            </td>
	            <td class="valor">
					<?php echo $this->Form->select('Agecitacalendariodia.initTime', $horas, array('style'=>'width:50px', 'class'=>'required'), false)?>
				</td>
				<td class="centrado" style="width:30px">
					:
				</td>
	            <td class="valor">
					<?php echo $this->Form->select('Agecitacalendariodia.initTimeMinute', $minutos, array('style'=>'width:50px', 'class'=>'required'), false)?>
				</td>
	        </tr>
	        <tr>
	            <td class="etiqueta">
	                <?php echo $this->Form->label('Agecitacalendariodia.endDateTime',__('TALLER_CITACALENDARDAY_CAMPO_END_DATE_TIME'))?>           
				</td>
	            <td class="valor">
					<?php echo $this->Form->select('Agecitacalendariodia.endTime', $horas, array('style'=>'width:50px', 'class'=>'required'), false)?>
				</td>
				<td class="centrado">
					:
				</td>
	            <td class="valor">
					<?php echo $this->Form->select('Agecitacalendariodia.endTimeMinute', $minutos, array('style'=>'width:50px', 'class'=>'required'), false)?>
				</td>
	        </tr>
	        <tr>
	            <td class="etiqueta">
	                <?php echo $this->Form->label('Agecitacalendariodia.programed', __('TALLER_CITACALENDARDAY_CAMPO_PROGRAMED'));?>
	            </td>
	            <td class="valor" colspan="3">
	                <?php echo $this->Form->input('Agecitacalendariodia.programed', array('label'=>false, 'size'=>'17', 'maxlength'=>'1', 'class'=>'required'));?>
	            </td>
	        </tr>
	        <tr>
	            <td colspan="4" class="accion">
	                <?php echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'guardar', 'div'=>false));?>
	                <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar', 'div'=>false, 'onclick'=>'javascript:window.close()'));?>
	            </td>
	        </tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4">
	                <div class="centrado">
	                    <?php echo __('CAST_TIME_ETIQUETA_NOTA_FORMATO_HORAS')?>
	                </div>	
				</td>
			</tr>
	    </tbody>
	</table>
</form>
<?php echo $this->element('actualizar_ver'); ?>

<script type="text/javascript">
	function enviarFormulario() {
		var resultado = true;
		resultado = validarEnterosObligatorios() && resultado;
		resultado = validarEnterosOpcionales() && resultado;
		return resultado;
	}
	
	$('agregarForm').onsubmit = function() {
		return enviarFormulario();
	}
</script>