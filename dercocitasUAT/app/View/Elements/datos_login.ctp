<?php
if($this->Session->check('loginCliente')) {
	$cliente = $this->Session->read('loginCliente');

    $html_user_menu = '<div class="col-md-4"></div>';
    $html_user_menu .= '<div class="col-md-8 text-right" style="font-size:14px;padding:10px 15px;">';
    $html_user_menu .= '<span style="color:#545558; font-weight:bold;">HOLA, '. $cliente['Cliente']['nombres'].'</span>';
    $html_user_menu .= '<span style="margin-left:30px;">';
    $html_user_menu .= $this->Html->link(__('WEBSOLICITUD_ETIQUETA_SALIR'), '/clientes/logoutCliente', array('escape'=>false, 'style'=>'color:#b5b5b5;font-weight:bold;'), __('GENERAL_CONFIRMAR_SALIR'));
    $html_user_menu .= '</span>';
    $html_user_menu .= '</div>';
    echo $html_user_menu;
}
?>