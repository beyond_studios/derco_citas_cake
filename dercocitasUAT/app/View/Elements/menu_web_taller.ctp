<nav class="menu">
		<ul>
			<li class="<?php echo $this->params['action'] =='setCitationClient' || $this->params['action']=='tallerIndex' || $this->params['action']=='bandejaReprogramar' || $this->params['action']=='historialCitas'?'active':'' ?>" ><a href="javascript:void(0)">
            	<?php echo $this->Html->image('/dist/images/icon_car_white.png', array('alt' => '')); ?></a>
				<ul>
					<li>
					<label class="titleMenu">Taller de Servicios</label></li>
					<li>
						<?php 
							echo $this->Html->link(__('Solicitar Cita',true),
							array('controller' => 'clientes', 'action' => 'tallerIndex', 'full_base' => false),
							array('class'=>($this->params['action']=='tallerIndex'?'active':'')));
						?>	
					</li>
					<li>
						<?php
							echo $this->Html->link(__('Reprogramar y/o eliminar Citas ',true),array('controller' => 'clientes', 'action' => 'bandejaReprogramar', 'full_base' => false),
							array('class'=>($this->params['action']=='bandejaReprogramar'?'active':'')));
						?>
					</li>
					<li>
						<?php 
							echo $this->Html->link(__('Historial de Citas ',true),array('controller' => 'clientes', 'action' => 'historialCitas', 'full_base' => false),
							array('class'=>( $this->params['action'] =='setCitationClient' ||  $this->params['action']=='historialCitas'?'active':'')));
						?>
					</li>
				</ul>
			</li>
			<li class="<?php echo $this->params['action']=='mostrarCliente'?'active':'' ?>"><a href="javascript:void(0)">
            <?php echo $this->Html->image('/dist/images/icon_gear_white.png', array('alt' => '')); ?></a>
				<ul>
					<li><label class="titleMenu">Configuración</label></li>
					<li>
						<?php 
							echo $this->Html->link(__('Ver Datos Personales ',true),array('controller' => 'clientes', 'action' => 'mostrarCliente', 'full_base' => false),
							array('class'=>($this->params['action']=='mostrarCliente'?'active':'')));
						?>
					</li>
				</ul>
			</li>
			<li class="<?php echo $this->params['controller']=='servicios'?'active':'' ?>"><a href="javascript:void(0)">
            	<?php echo $this->Html->image('/dist/images/icon_tools_white.png', array('alt' => '')); ?></a>
				<ul>
					<li><label class="titleMenu">Servicios</label></li>
					<li>
						<?php 
							echo $this->Html->link(__('Servicios',true), array('controller' => 'servicios', 'action' => 'view', 'full_base' => false),
							array('class'=>($this->params['action']=='view' && $this->params['controller']=='servicios'?'active':'')));
						?>
					</li>
				</ul>
			</li>
			<li  class="<?php echo $this->params['controller']=='campanias'?'active':'' ?>"><a href="javascript:void(0)">
            	<?php echo $this->Html->image('/dist/images/icon_bocina_white.png', array('alt' => '')); ?></a>
				<ul>
					<li>
						<label class="titleMenu">Promociones</label>
					</li>
					<li>
						<?php 
							echo $this->Html->link(__('Promociones/Campañas',true), array('controller' => 'campanias', 'action' => 'view', 'full_base' => false),
							array('class'=>($this->params['action']=='view' && $this->params['controller']=='campanias'?'active':'')));
						?>
					</li>
				</ul>
			</li>
			<li class="<?php echo $this->params['controller']=='ayudas'?'active':'' ?>"><a href="javascript:void(0)">
            	<?php echo $this->Html->image('/dist/images/icon_help_white.png', array('alt' => '')); ?></a>
				<ul>
					<li>
						<label class="titleMenu">Ayuda</label>
					</li>
					<?php if(!empty($ayudas_menu)): 
						foreach($ayudas_menu as $ayuda_menu):
					?>
						<li>
							<?php
								echo $this->Html->link($ayuda_menu['Ayuda']['titulo'], array('controller' => 'ayudas', 'action' => 'view', $ayuda_menu['Ayuda']['id']),
							array('class'=>($this->params['action']=='view' && count($this->params['pass'])>0 && $this->params['pass'][0]==$ayuda_menu['Ayuda']['id'] ?'active':'')));	

							?>
						</li>
					<?php  endforeach;
						endif; ?>
				</ul>
			</li>
		</ul>
	</nav>


<!--
<script type="text/javascript">
	function enConstruccion() {
		alert('En Construccion...');
	}
	function registroCliente() {
		var url = "<?php echo $this->Html->url('/files/InstructivoRegistroWeb.pdf'); ?>";
   		var w = window.open(url, 'registrarcliente','scrollbars=no,resizable=yes,top=200,left=250,status=yes,location=no,toolbar=no,menubar=no');
	}
	function citasTaller() {
		var url = "<?php echo $this->Html->url('/files/InstructivoGeneracionCitasWeb.pdf'); ?>";
   		var w = window.open(url, 'registrarcliente','scrollbars=no,resizable=yes,top=200,left=250,status=yes,location=no,toolbar=no,menubar=no');
	}		
</script>
-->