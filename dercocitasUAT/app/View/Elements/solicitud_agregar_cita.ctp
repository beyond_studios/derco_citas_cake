<div id='formularioEdicion'>
<?php echo $this->Form->create('Agedetallecita', array('class'=>'fomrLogin','url'=>$action, 'id'=>'modificarForm'));?>
<?php echo $this->Form->hidden('isPost', array('value'=>empty($isPost)?'0':'1'))?>
<?php echo $this->Form->hidden('Base.citarepogramar_id', array('value'=>$citarepogramarId))?>
	
	<div class="formTable leftAling">
		<div class="titleBar styleGriss" style="text-align: left;">
			<i class="fa fa-user" aria-hidden="true"></i><b>DATOS DEL CLIENTE</b>
		</div>
		<div class="groupFields col4">
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_APELLIDOS_Y_NOMBRES');?></label>
				</div>
				<div>
					<?php echo $this->Form->hidden('cliente_id') ?>
                    <?php echo $this->Form->hidden('citaweb',array('value'=>isset($citaweb)?'1':'0')) ?>
                	<?php echo $this->Form->input('Cliente.apellidoPaterno', array('class'=>'required msg_error_2 soloLectura','readonly'=>true, 'readonly'=>'readonly','div'=>false, 'label'=>false)); ?>	
					<?php if(empty($citarepogramarId)){ if(!isset($citaweb))echo $this->Html->image('search_icon.gif', array('style' => 'cursor: pointer;', 'onclick' => "vehicle.open();"));} ?>
					<?php //echo $this->Html->image('search_icon.gif', array('style' => 'cursor: pointer;', 'onclick' => "abrirDialog('dialog_cliente')")); ?>
					<span class="error"></span>
				</div>
			</div>

			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('AGE_TIPO_DOCUMENTO');?></label>
				</div>
				<div>
					<?php echo $this->Form->input('Cliente.documento_tipo', array('label'=>false,'class'=>'soloLectura','readonly'=>true,'error'=>false,'div'=>false, 'label'=>false )); ?>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('AGE_NUMERO_DOCUMENTO');?></label>
				</div>
				<div>
					<?php echo $this->Form->input('Cliente.documento_numero', array('label'=>'','class'=>'soloLectura','readonly'=>true,'error'=>false, 'div'=>false, 'label'=>false)); ?>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TIPO_DE_CLIENTE');?></label>
				</div>
				<div>
					<?php echo $this->Form->hidden('Cliente.cliente_tipo') ?>
                	<?php echo $this->Form->input('Cliente.str_cliente_tipo', array('label'=>'','class'=>'soloLectura','readonly'=>true,'error'=>false, 'div'=>false, 'label'=>false)); ?>
				</div>
			</div>
		</div>
		<div class="groupFields col4">
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('distrito');?></label>
				</div>
				<div>
					<?php echo $this->Form->input('Cliente.distrito', array('label'=>'','class'=>'soloLectura','readonly'=>true,'error'=>false, 'label'=>false)); ?>
				</div>
			</div>

			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_TELEFONO');?></label>
				</div>
				<div>
					<?php echo $this->Form->input('Cliente.telefono', array('label'=>'','class'=>'soloLectura','readonly'=>true,'error'=>false, 'label'=>false)); ?>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_CELULAR');?></label>
				</div>
				<div>
					<?php echo $this->Form->input('Cliente.celular', array('label'=>'','class'=>'soloLectura','readonly'=>true,'error'=>false, 'label'=>false)); ?>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_CORREO');?></label>
				</div>
				<div>
					<?php echo $this->Form->input('Cliente.email', array('label'=>'','class'=>'soloLectura','readonly'=>true,'error'=>false, 'label'=>false)); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="titleBar styleGriss" style="text-align: left;">
		<i class="fa fa-car" aria-hidden="true"></i><b>DATOS DEL VEHÍCULO</b>
	</div>
	<div class="formTable leftAling">
		<div class="groupFields col4">
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_PLACA');?></label>
				</div>
				<div>
					<label class="leftAling colorTexto">
						<?php echo $this->Form->hidden('codigo_unidad') ?>
						<?php echo $this->Form->hidden('Agedetallecita.ageclientesVehiculo_id') ?>
						<?php echo $this->Form->input('placa', array('type'=>'text','label'=>false, 'div'=>false,'class'=>'required  soloLectura','readonly'=>true,'error'=>false, 'style'=>'width:170px;')); ?>
					</label>
				</div>
			</div>

			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_MARCA');?></label>
				</div>
				<div>
					<label class="leftAling colorTexto">
						<?php echo $this->Form->hidden('Marca.id');?>
                		<?php echo $this->Form->input('marca', array('type'=>'text','label'=>'','class'=>'soloLectura','readonly'=>true,'error'=>false, 'label'=>false)); ?>
					</label>
				</div>
			</div>
			<div class="field">
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_MODELO');?></label>
				</div>
				<div>
					<label class="leftAling colorTexto"><?php echo $this->Form->input('modelo', array('label'=>'','class'=>'soloLectura','readonly'=>true,'error'=>false, 'label'=>false)); ?></label>
				</div>
			</div>
			<div class="field">
				
			</div>
		</div>
	</div>
	<div class="titleBar styleGriss" style="text-align: left;">
		<i class="fa fa-wrench" aria-hidden="true"></i><b>DATOS DEL SERVICIO</b>
	</div>
	<div class="groupFields col4">
		<div class="field">
			<div>
				<label class="leftAling"><?php echo __('TALLER_ETIQUETA_TALLER_SUCURSAL');?></label>
			</div>
			<div>
				<?php echo $this->Form->select('Secproject.id', $secprojects, array('class'=>'required msg_error_2 styled-select slate', 'empty'=>__('Seleccionar'),'onchange'=>'getMotivoServicio(this)'))?>
			</div>
		</div><div class="field">
			<div>
				<label class="leftAling"><?php echo __('AGE_MOTIVOSERVICIO_DESCRIPCION');?></label>
			</div>
			<div>
				<select name="data[Agemotivoservicio][id]" onchange="getTipoMantenimiento(this);" class="required msg_error_2 valid styled-select slate"  id="AgemotivoservicioId">
					<option value="">Seleccionar</option>
					<?php foreach($motivoservicios as $value){
						$selected=($value['Agemotivoservicio']['id'] == $this->data['Agemotivoservicio']['id'])?"selected = selected":"";
						echo '<option '.$selected.' value="'.$value['Agemotivoservicio']['id'].'" agecitacalendario_id="'.$value['Agecitacalendario']['id'].'">'.$value['Agemotivoservicio']['description'].'</option>';
					} ?>
				</select>
			</div>
		</div><div class="field">
			<div>
				<label class="leftAling"><?php echo __('ORDEN_ESTANDAR_ETIQUETA_TIPO_SERVICIO');?></label>
			</div>
			<div>
				<select id="AgedetallecitaAgetiposervicioId"  class="required msg_error_2 styled-select slate" onchange="verTipomantenimiento(this);" name="data[Agedetallecita][agetiposervicio_id]">
					<option value=""><?php echo __('Seleccionar', true) ?></option>
					<?php foreach($tiposervicios as $value){
						$selected=($value['Agetiposervicio']['id'] == $this->data['Agedetallecita']['agetiposervicio_id'])?" selected=selected ":"";
						echo "<option $selected mantenimiento=\"".$value['Agetiposervicio']['mostrar_mantenimiento']."\" value=\"".$value['Agetiposervicio']['id']."\">".utf8_encode($value['Agetiposervicio']['description'])."</option>";
					} ?>
				</select>
			</div>
		</div><div class="field">
			<div>
				<div id="divTipomantenimientoEtiqueta" class="hide">
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_TALLER_TIPO_MANTENIMIENTO');?></td></label>
				</div>	
			</div>
			<div>
				<div id="divTipomantenimiento" class="hide">
					<?php echo $this->Form->select('Agetipomantenimiento.id', $tipomantenimientos, array('empty'=>__('Seleccionar'),'label'=>false, 'div'=>false,'class'=>"styled-select slate"))?>
				</div>	
			</div>
		</div>
	</div>
	<div class="groupFields">
			<div class="field" style='float:left;width:25%'>
				<div>
					<label class="leftAling"><?php echo __('servicioMenor');?></label>
				</div>
				<div>
					<?php echo $this->Form->input('serviciomenor', array('label'=>'','class'=>'soloLectura','readonly'=>true,'error'=>false, 'label'=>false)); ?>
				</div>
			</div>
			<div class="field" style='float:left;width:25%'>
				<div>
					<label class="leftAling"><?php echo __('servicioMayor');?></label>
				</div>
				<div>
					<?php echo $this->Form->input('serviciomayor', array('label'=>'','class'=>'soloLectura','readonly'=>true,'error'=>false, 'label'=>false)); ?>
				</div>
			</div>

			<div class="field" style='float:left;width:50%;'>
				<div>
					<label class="leftAling"><?php echo __('mensajeMotivoServicio');?></label>
				</div>
				<div>
					<?php echo $this->Form->input('mensajeMotivoServicio', array('label'=>'', 'rows'=>'1','type'=>'textarea','class'=>'soloLectura','readonly'=>true,'error'=>false,'div'=>false, 'label'=>false)); ?>
				</div>
			</div>
		</div>
		<div class="groupFields">
			<div class="field" style='float:left;width:50%; padding-right:30px'>
				<div>
					<label class="leftAling"><?php echo __('TALLER_ETIQUETA_OTROS_SERVICIOS');?></label>
				</div>
				<div>
					<?php echo $this->Form->textarea('Agedetallecita.otrosServicios', array('cols'=>'40', 'rows'=>'1', 'div'=>false, 'label'=>false));?>
				</div>
			</div>
			<div class="field" style='float:left;width:25%'>
				<div>
					<label class="leftAling"><?php echo __('AGE_FECHA_CITA');?></label>
				</div>
				<div>
					<?php echo $this->Form->input('fecha', array('readonly'=>true,'class'=>'required msg_error_2 soloLectura', 'label'=>false, 'div'=>false))?>
			
				</div>
			</div>
			
			<div class="field" style='padding-top:26px;float:left;width:25%; padding-left:10px; text-align:center'>
					<?php echo $this->Form->button(__('horarios'), array('type'=>'button','class'=>'buscar buttomReset guinda', 'div'=>false, 'onclick'=>'getHorariosCliente(); return false;'));?>
			</div>
		</div>
		<div class='groupFields col4'>
			<div class="field">
				<?php if(!empty($citarepogramarId)): ?>
					<div>
						<label class="leftAling"><?php echo __('comentarioReprogramar',true); ?></label>
					</div>
					<div>
						<textarea name="data[Agedetallecita][reschedulecomment]" class="required msg_error_1" style="height: 45px;padding: 2px;" cols="40"><?php echo empty($this->data)?"":trim($this->data['Agedetallecita']['reschedulecomment']); ?></textarea>
					</div>
				<?php endif; ?>
			</div>
		</div>
		
		<?php //echo __('VENDEDOR_ETIQUETA_HORARIOS_POR_TALLER_SERVICIOS')?>
		<div id="listaPrincipal"></div>
		<div class='bottom-actions'>
			<div class="groupFields centerAling">
				<div class="field" >
        			<?php echo $this->Form->submit(__('GENERAL_CONFIRMAR'), array('class'=>'guardar hide guinda', 'id'=>'buttonSubmit'));?>
				</div>
			</div>
		</div>
	
<?php echo $this->Form->end(); ?>
</div>