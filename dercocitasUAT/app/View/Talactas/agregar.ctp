<!-- JS/CSS UTILIZADOS -->
<?php echo $this->Html->script('Talactas/agregar.js'); ?>

<?php echo $this->Session->flash(); ?>
<?php if(empty($talot)){	?>
	<h2><?php echo __('NO EXISTE LA OT EN APOYO');?></h2>
	<br/>
	<?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar', 'onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
<?php }else{ ?>
	<h2><?php echo __('ACTA_TITULO');?></h2>
	<br/>
	
	<form id="modificarForm" action="<?php echo $this->Html->url('agregar/'.$ot); ?>" method="post">
		<?php echo $this->Form->hidden('Talacta.ot',array('value' => $ot, 'div'=>false, 'label'=>false))?>	
	<table border="0" >
		<tbody>
			<tr>			
				<td class="etiqueta">
					<?php echo __('ACTA_FECHA'); ?>				
				</td>
				<td>
					<?php echo $this->Form->input('Talacta.fechaUsuario', array('type'=>'text','class'=>'required msg_error_1', 'div'=>false, 'label'=>false))?>
				</td>
			</tr>
			<tr>	
				<td class="etiqueta">
					<?php echo __('ACTA_HORA'); ?>				
				</td>
				<td>
					 <?php echo $this->Form->select('Talacta.hora',$horas,array( 'class'=>'required', 'div'=>false, 'label'=>false))?><strong>:</strong>
	                 <?php echo $this->Form->select('Talacta.minuto',$minutos,array( 'class'=>'required', 'div'=>false, 'label'=>false))?>
				</td>
			</tr>	
			<tr>			
				<td class="etiqueta">
					<?php echo __('ACTA_DESCRIPCION'); ?>
				</td>
				<td>
					<?php echo $this->Form->input('Talacta.descripcion',array('type'=>'textarea', 'cols' => '50', 'rows' => '5','class'=>'required', 'div'=>false, 'label'=>false));?>			
				</td>
			</tr>	
			<tr>
				<td></td>
				<td>
		                <?php echo $this->Form->submit(__('GENERAL_GUARDAR'), array('class'=>'guardar', 'div'=>false, 'label'=>false));?>
		                <?php echo $this->Form->button(__('GENERAL_CERRAR'), array('class'=>'cerrar', 'onclick'=>'javascript:window.close()', 'div'=>false, 'label'=>false));?>
		        </td>
			</tr>
		</tbody>
	</table>
	</form>
	
	<br/>
	<fieldset>
		<legend><?php echo __('Historial_de_actas'); ?></legend>
		<table id="listaPrincipal">
			<thead>
				<tr>
					<!-- <th><?php echo __('ACTA_CLASIFICACION'); ?></th> -->
					<th><?php echo __('ACTA_FECHA'); ?></th>
					<th><?php echo __('ACTA_HORA'); ?></th>
					<th><?php echo __('CCP_TALACTAS_USUARIO'); ?></th>
					<th><?php echo __('ACTA_DESCRIPCION'); ?></th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach($actas as $acta): ?>
				<tr>
					<td><?php echo substr($acta['Talacta']['fechaUsuario'], 0,10); ?></td>
					<td><?php echo substr($acta['Talacta']['fechaUsuario'], 10,6); ?></td>
					<td><?php echo $acta[0]['Usuario'] ?></td>
					<td><?php echo $acta['Talacta']['descripcion'] ?></td>
				</tr>
				<?php endforeach; ?>
				<tr><td colspan=4>&nbsp;</td></tr>
			</tbody>	
		</table>	
	</fieldset>
<?php } ?>

<script type="text/javascript">
	colorearTabla('listaPrincipal');
</script>