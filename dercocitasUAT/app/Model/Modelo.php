<?php
class Modelo extends AppModel
{
	public $name = 'Modelo';
	public $displayField = 'description';
   
	public $validate = array(
		'description' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								),
						'isUnique' =>array(
								'rule'=>'isUnique',
								'last' => true
								),
						'maxLength' =>array(
            					'rule'    => array('maxLength', '60'),
								'last' => true
								)  
					)
    );

	public function getModelosClient($clientId = 0, $clientVehicleId = 0){
		if(empty($clientId) && empty($clientVehicleId)) return array();
		
		$cndClient = empty($clientId)?"1=1":"AgeclientesVehiculo.cliente_id = '$clientId'";
		$cndVehicle = empty($clientVehicleId)?"1=1":"AgeclientesVehiculo.id = '$clientVehicleId'";
		
		$modelosClient = $this->query(
			"select Modelo.id, AgeclientesVehiculo.id, AgeclientesVehiculo.codigo_vehiculo, AgeclientesVehiculo.modelo, AgeclientesVehiculo.modelo, AgeclientesVehiculo.placa
			from modelos AS Modelo
				JOIN ageclientes_vehiculos AgeclientesVehiculo ON AgeclientesVehiculo.modelo = Modelo.description
			WHERE $cndClient AND $cndVehicle"
		);
		return $modelosClient;
	}
	
	public function getAllModelosForRegistroClientes(){
		$modelos = array();
		$modelosClient = $this->query("select distinct(description) from modelos Modelo");
		foreach($modelosClient as $id=>$item){
			$modelos[$item['Modelo']['description']] = $item['Modelo']['description']; 
		}
		return $modelos;
	}
}
?>