<?php
class Ccptabhistorial extends AppModel{
	public $name='Ccptabhistorial';
	
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed		
	public $belongsTo = array(
		'Talot' => array(
			'className' => 'Talot',
			'foreignKey' => 'talot_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Ccptabestado' => array(
			'className' => 'Ccptabestado',
			'foreignKey' => 'ccptabestado_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Secperson' => array(
			'className' => 'Secperson',
			'foreignKey' => 'secperson_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Ccpsolicitudservicio' => array(
			'className' => 'Ccpsolicitudservicio',
			'foreignKey' => 'ccpsolicitudservicio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	/**
	 * MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $estadoccp_id
	 * @return 
	 */
	function obtenerOTPorEstadoCcp($estadoccp_id){
   	$sql="SELECT Ccptabhistorial.ot_numero
 		FROM (SELECT MAX(id) as id,ot_numero FROM ccptabhistoriales GROUP BY ot_numero) as CcptabhistorialTemp
  		INNER JOIN ccptabhistoriales Ccptabhistorial on(CcptabhistorialTemp.id=Ccptabhistorial.id)
   		WHERE Ccptabhistorial.ccptabestado_id IN(".$estadoccp_id.")";
		
//   	$sql="SELECT *from
// 		(select ot_numero,max(ccptabestado_id) as estadomaximo from ccptabhistoriales group by ot_numero) as Ccptabhistorial
//   		WHERE Ccptabhistorial.estadomaximo IN(".$estadoccp_id.")";
	$otCCP=$this->query($sql);
	$lista = array();
	foreach($otCCP as $id=>$item){
		$lista[$id]=$item['Ccptabhistorial']['ot_numero'];
	}
	return (!empty($lista) && isset($lista))?$lista:array();
   }
   
   /**
	 * MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * @return 
	 */
   function obtenerDiferenciaDias($fechaInicio,$FechaFinal){
		$int_nodias = floor(abs(strtotime($FechaFinal) - strtotime($fechaInicio))/86400);
		$color = "";
		if($int_nodias>=90){
			$color='red; color:white;border-bottom: 1px solid;';
		}elseif($int_nodias>=61){
			$color='orange;border-bottom-color:white;border-bottom: 1px solid;border-bottom-color:white;';
		}elseif($int_nodias>=31){
			$color='yellow;border-bottom-color:white;border-bottom: 1px solid;border-bottom-color:white;';
		}else $color='green; color:white;border-bottom: 1px solid;';
		
		return ($color);
	}
	
   /**
	 * MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * @return 
	 */
   function getSolicitudServicioAndCcpEstado($ot_numero){
		$sql="SELECT *FROM (SELECT
          Ccptabestado.id,
          Ccptabestado.descripcion,
          Ccptabhistorial.ot_numero
          FROM (select max(id) as id,ot_numero from ccptabhistoriales group by ot_numero) as CcptabhistorialTemp
          		INNER JOIN ccptabhistoriales Ccptabhistorial on(CcptabhistorialTemp.id=Ccptabhistorial.id)
          		INNER JOIN ccptabestados Ccptabestado on(Ccptabestado.id=Ccptabhistorial.ccptabestado_id)
          		WHERE Ccptabestado.estado='AC' and Ccptabhistorial.ot_numero='".$ot_numero."' limit 1) AS Ccptabestado
          LEFT JOIN ccpsolicitudservicios Ccpsolicitudservicio ON (Ccptabestado.ot_numero = Ccpsolicitudservicio.ot_numero)
          LEFT JOIN ccpsolicitudservicioestados Ccpsolicitudservicioestado ON (Ccpsolicitudservicio.ccpsolicitudservicioestado_id=Ccpsolicitudservicioestado.id)
          LEFT JOIN ccptiposervicios Ccptiposervicio ON(Ccpsolicitudservicio.ccptiposervicio_id=Ccptiposervicio.id)
        WHERE Ccptabestado.ot_numero='".$ot_numero."' order by Ccpsolicitudservicio.id desc limit 1";
		$lista=$this->query($sql);
		return ($lista);
	}
	
	
	/** funcion modificada
	 * autor: ventura rueda, jose antonio
	 * MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $secproject_carscod
	 * @return 
	 */
	function getAsesoresCars($secproject_carscod){
		$cndTaller = empty($secproject_carscod)?"1=1":"TALLER='$secproject_carscod'";
		
		$asesores = "SELECT miOt.USUARIO_OT FROM 
						(SELECT USUARIO_OT FROM sistema_planificador_ccps
						WHERE $cndTaller AND OT_ESTADO in ('0060','0070','0090','0095','0103','0999')
						GROUP BY USUARIO_OT ORDER BY USUARIO_OT) as miOt";

		$asesores = $this->query($asesores);
		if(empty($asesores)) return $asesores;
		
		foreach($asesores as $asesor)
			$asesor_tmp[$asesor['miOt']['USUARIO_OT']] = $asesor['miOt']['USUARIO_OT'];	
		
		return $asesor_tmp;
	}
	
	function verificarPasoDeEstados($estadoInicial,$estadoFinal){
		switch($estadoInicial){
			case 1://pendiente
				if(in_array($estadoFinal, array(2,3)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 2://deposito
				if(in_array($estadoFinal, array(1,3,4,13)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 3://con solicitud
				if(in_array($estadoFinal, array(3,4)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 4:// con solicitud aprobada
				if(in_array($estadoFinal, array(5)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 5://inicio ABP
				if(in_array($estadoFinal, array(6)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 6://fin ABP
				if(in_array($estadoFinal, array(7,12,13)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 7://control de calidad Aprobada
				if(in_array($estadoFinal, array(11,14,21)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 8://control de calidad Rechazada
				if(in_array($estadoFinal, array(12)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
		/*	case 9://lavado
				if(in_array($estadoFinal, array(10)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 10://control de calidad asesor => (entregado,Pendiente Entrega)
				if(in_array($estadoFinal, array(11,21)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;*/
			case 11://entregado
				if(in_array($estadoFinal, array(14)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 12://reprogramado
				if(in_array($estadoFinal, array(6)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 13://ampliacion de ampliacion pasa a estado deposito o con solicitud
				if(in_array($estadoFinal, array(2,3)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 14://entregado file
				if(in_array($estadoFinal, array(15)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 15://file observado
				if(in_array($estadoFinal, array(14)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 21://Pendiente entrega
				if(in_array($estadoFinal, array(22,14)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 22://Salida Unidad => pendiente entrega o entregado
				if(in_array($estadoFinal, array(21,11,14)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
						
			default: return array('respuesta'=>false,'msg'=>'Estado no Permitido');break;
		}
	}

	function obtenerEstadoCcpPotNumeroOt($ot_numero){
		$sql="SELECT 
			Ccptabestado.id,
			Ccptabestado.descripcion
	 		FROM (select max(id) as id,ot_numero from ccptabhistoriales group by ot_numero) as CcptabhistorialTemp
	  		INNER JOIN ccptabhistoriales Ccptabhistorial on(CcptabhistorialTemp.id=Ccptabhistorial.id)
	  		INNER JOIN ccptabestados Ccptabestado on(Ccptabestado.id=Ccptabhistorial.ccptabestado_id)
	 		WHERE Ccptabhistorial.ot_numero='".$ot_numero."' and Ccptabestado.estado='AC' LIMIT 1";
		
		$estadoCCP=$this->query($sql);
		return(!empty($estadoCCP) && isset($estadoCCP))?$estadoCCP:array();
   }
	
	function validarEstadoAnterior($ot_numero,$estado){
		$sql="SELECT id,ccptabestado_id 
			FROM ccptabhistoriales Ccptabhistoriale WHERE ot_numero='".$ot_numero."'
			ORDER BY id DESC";
		$ot=$this->query($sql);
		if(!empty($ot[1]['Ccptabhistorial']) && isset($ot[1]['Ccptabhistorial'])){
			if($ot[1]['Ccptabhistorial']['ccptabestado_id']==$estado){
				return true;
			}else return false;
		}else return false;
	}
	
	function getSolitudServicioId($ot_numero){
		//verificar si tiene un ampliacion para agregar el estado ampliacion		
		app::import('Model','Ccpsolicitudservicio');    		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		$this->Ccpsolicitudservicio->recursive=-1;
		$condition=array('Ccpsolicitudservicio.ot_numero'=>$ot_numero,
						'Ccpsolicitudservicio.ccpsolicitudservicioestado_id'=>2);
		$solicitudServicio= $this->Ccpsolicitudservicio->find('all',array('conditions'=>$condition,'order'=>'Ccpsolicitudservicio.id desc'));
		$comboAmpliacion=array();
		$solicitud_id=null;
		if(!empty($solicitudServicio) && isset($solicitudServicio)){
			$solicitud_id = $solicitudServicio[0]['Ccpsolicitudservicio']['id'];			
		}
		return 	$solicitud_id;
	}
	
	function cambiarEstado($estadoInicial,$estadoFinal,$numero_ot,$comentario=null,$usuario=null,$id_solicitud=null){		
		$estadoInicial=(!empty($estadoInicial) && isset($estadoInicial))?$estadoInicial:'';
		$estadoFinal=(!empty($estadoFinal) && isset($estadoFinal))?$estadoFinal:'';
		$numero_ot=(!empty($numero_ot) && isset($numero_ot))?$numero_ot:'';
		$usuario=(!empty($usuario) && isset($usuario))?$usuario:'';
		$respuesta=$this->verificarPasoDeEstados($estadoInicial,$estadoFinal);
		//validamos los Estados
		if(!$respuesta['respuesta']) return array('respuesta'=>false, 'msg'=>'Estado no permitido');
		
		$data['Ccptabhistorial']['fecha']=$this->getDateFormatDB('', null, null, $now=1);
		$data['Ccptabhistorial']['ot_numero']=$numero_ot;
		$data['Ccptabhistorial']['ccptabestado_id']=$estadoFinal;
		$data['Ccptabhistorial']['comentario']=$comentario;
		$data['Ccptabhistorial']['secperson_id']=$usuario;
		$data['Ccptabhistorial']['ccpsolicitudservicio_id']=(!empty($id_solicitud) && isset($id_solicitud))?$id_solicitud:null;
		$this->create();
		if(!$this->save($data['Ccptabhistorial'])) return array('respuesta'=>false, 'msg'=>"Error al cambiar estado");
		return array('respuesta'=>true, 'msg'=>"El Proceso se actualizó satisfactoriamente");
   }
   
   function getCantidadAmpliaciones($ot_numero){
		$sql="select count(*) as total from ccptabhistoriales where ot_numero='".$ot_numero."' and ccptabestado_id=13";
		$cantidad=$this->query($sql);
		return $cantidad[0][0]['total'];
	}

  function bloquearRegistro($ot_numero){
   	$sql="SELECT 
		Ccptabhistorial.id,
		Ccptabestado.id,
		Ccptabestado.descripcion
 		FROM (select max(id) as id,ot_numero from ccptabhistoriales group by ot_numero) as CcptabhistorialTemp
  		INNER JOIN ccptabhistoriales Ccptabhistori	al on(CcptabhistorialTemp.id=Ccptabhistorial.id)
  		INNER JOIN ccptabestados Ccptabestado on(Ccptabestado.id=Ccptabhistorial.ccptabestado_id)
 		WHERE Ccptabhistorial.ot_numero='".$ot_numero."' and Ccptabestado.estado='AC' LIMIT 1 LOCK IN SHARE MODE";
	$estadoCCP=$this->query($sql);
	if(!empty($estadoCCP) && isset($estadoCCP)){//entrara cuando viene del estado 1
	}else{
		//validamos que la ot exista en cars
		$estadoCCP[0]['Ccptabestado']['id']=1;
		$estadoCCP[0]['Ccptabestado']['descripcion']='Pendiente';
		}
  }
	
	function otpasoPorDeposito($ot_numero){
		$sql="SELECT id,ccptabestado_id 
			FROM ccptabhistoriales Ccptabhistoriale WHERE ot_numero='".$ot_numero."'
			ORDER BY id DESC";
		$ot=$this->query($sql);
		if(!empty($ot[1]['Ccptabhistoriale']) && isset($ot[1]['Ccptabhistoriale'])){
			if($ot[1]['Ccptabhistoriale']['ccptabestado_id']==2){
				return true;
			}else return false;
		}else return false;
	}
	
	function getComentarioDeposito($ot_numero){
		$sql="SELECT Descripcion.ot_numero,
      				GROUP_CONCAT(Descripcion.comentario SEPARATOR '<br>') as comentario
					FROM (SELECT ot_numero,CONCAT(Date_format(fecha,'%d-%m-%Y %h:%i:%s %p'),': ',comentario) AS comentario
     					FROM ccptabhistoriales WHERE ot_numero='".$ot_numero."' AND ccptabestado_id=2 ORDER BY id DESC) AS Descripcion
					GROUP BY Descripcion.ot_numero";
		$comentario = $this->query($sql);
		return $comentario;
	}
	
	/**MIGRADO POR VENTURA RUEDA, JOSE ANTONIO
	 * 
	 * @param object $data
	 * @param object $params
	 * @param object $login
	 * @param object $total [optional]
	 * @return 
	 */
	function getTalotCarsLavador($data, $params, $login, $total = false, $stdCars=array('0','2'), $conSeguimiento=false){
		$this->Ccplavadoseguimiento = new Ccplavadoseguimiento;
		
		// VALORES NECESARIOS
		$secproject_carscod = $params['params']['carscod'];
		// RECUPERAMOS DATOS PARA LA PAGINACION
		$sord = empty($params['params']['order'])?'':$params['params']['order'];
		$limit = empty($params['params']['limit'])?'':$params['params']['limit'];
		$page = empty($params['params']['page'])?'':$params['params']['page'];
 		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = '';

		//RECUPERAMOS las ots que se encuentran en ccpestado
		$ot_ccphistorial='';
		if(!empty($data['estadootccp_id']) && isset($data['estadootccp_id'])){
			$ot_ccphistorial = "AND ot.segestado_id = '".$data['estadootccp_id']."'";
		}
		
		//CONDICIONES DE BUSQUEDA POR FECHA
		$fechaOt = "";	
		if(!empty($data['fechaIni']) && !empty($data['fechaFin'])) {
			$fechaInicialOt = date("Y-m-d 00:00:00", strtotime($data['fechaIni']));
			$fechaFinalOt = date("Y-m-d 23:59:59", strtotime($data['fechaFin']));
			$fechaOt = " AND ot.fecha_lavador BETWEEN '" . $fechaInicialOt ."' AND '" . $fechaFinalOt ."' ";
		}
		elseif(!empty($data['fechaIni'])){
			$fechaInicialOt = date("Y-m-d 00:00:00", strtotime($data['fechaIni']));
			$fechaFinalOt = date("Y-m-d  23:59:59");
			$fechaOt = " AND ot.fecha_lavador  BETWEEN '" . $fechaInicialOt ."' AND '" . $fechaFinalOt ."' ";				
		}
		elseif(!empty($data['fechaFin'])){
			$fechaInicialOt = date("2009-01-01 00:00:00");
			$fechaFinalOt = date("Y-m-d  23:59:59", strtotime($data['fechaFin']));
			$fechaOt = " AND ot.fecha_lavador BETWEEN '" . $fechaInicialOt ."' AND '" . $fechaFinalOt ."' ";				
		}
		
		// SI ES NECESARIO QUE TENGA UN SEGUIMIENTO
		$conSeguimiento = $conSeguimiento?" AND ot.seg_id IS NOT NULL":"";
		
		//FILTROS DE CARS
		$condicion_cars = sprintf("ot.OT_ESTADO IN ('%s') AND ot.TALLER='$secproject_carscod'",implode("','", $stdCars));
		
		if(!empty($data['placa']) && isset($data['placa'])){
			$placa_= " AND ot.PLACA like '%".$data['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;

		if(!empty($data['chasis'])) $condicion_cars .= " AND ot.NRO_SERIE like '%".$data['chasis']."%'";
		if(!empty($data['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$data['nrocars'].'%\'';
		
		// RECUPERAMOS EL TOTAL DE REGISTROS
		//debug("$condicion_cars $ot_solicitudes");
		$total_query = "SELECT COUNT(OT) as total FROM viewccplavadoseguimientos as ot WHERE $condicion_cars $ot_solicitudes $ot_ccphistorial $conSeguimiento $fechaOt";

		$total_query = $this->query($total_query);
		
		if($total) return $total_query[0][0]['total'];
		
		$count = $total_query[0][0]['total'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		//paginacion en SQL 
		$offset=empty($page)?0:($page-1)*$limit;

		// RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		$presupuestos = "SELECT
								ot.OT AS ot_numero,         				-- Numero de OT generad
								ot.PRESUPUESTO AS presupuesto_numero,		-- Numero de presupuesto
								ot.FECHA AS ot_fecha_creacion,         		-- Fecha que se gener? el la OT  *
								ot.USUARIO_OT AS ot_asesor,  				-- Usuario que genero la OT(Asesor de Servicio del CCP)
								ot.FECHA_PROM AS ot_fecha_entrega,  		-- Fecha Prometida
								ot.CLIENTE AS ot_cliente,  					-- AQUI SERA EL CLIENTE
								ot.NRO_SERIE AS chasis_vin,					-- Numero de chasis
								ot.PLACA as ot_placa,							-- Nuemero de Placa
								ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,	-- fecha aprobacion de seguro * cia ceguros
								ot.OT_ESTADO AS ot_estado,						-- Estado de la ot
								ot.OT_PRE_ESTADO as ot_pre_estado,				-- Estado de la ot con respecto al presupuesto
								ot.MARCA AS ot_marca,         				-- Numero de OT generad
								ot.MODELO AS ot_modelo,         				-- Numero de OT generad
								ot.COLOR as ot_color,					-- Color								
								ot.VERSION AS ot_version,         				-- Numero de OT generad
								ot.DESCRIPCION AS ot_descripcion,         				-- Numero de OT generad
								ot.NOMBRE_ASESOR AS ot_nombre_asesor,         				-- Numero de OT generad
								ot.TIPO_OT AS ot_tipo_ot,         				-- Numero de OT generad
								ot.HORA_RECIBIDA AS ot_hora_recibida,         				-- Numero de OT generad
								ot.HORA_PROMETIDA AS ot_hora_prometida,         				-- Numero de OT generad
								ot.NRO_CONO AS ot_numero_cono,        				-- Numero de OT generad
								
								-- AVENTURA:
								ot.seg_id AS seg_id,   										-- id del seguimiento lavado generado
								ot.nro_seguimiento AS nro_seguimiento,   					-- 
								ot.segestado_id AS segestado_id,   							-- estado del seguimiento
								ot.segestado_descripcion AS segestado_descripcion,   		--
								
								ot.secperson_lavador_id AS secperson_lavador_id,
								ot.secperson_coordinador_id AS secperson_coordinador_id,
								ot.secperson_jefetaller_id AS secperson_jefetaller_id,
								
								ot.fecha_lavador AS fecha_lavador,
								ot.segmotivo_descripcion AS segmotivo_descripcion,
								ot.seg_comentario_lavado AS seg_comentario_lavado
						FROM 
							viewccplavadoseguimientos AS ot
						WHERE 
							$condicion_cars $ot_solicitudes $ot_ccphistorial $conSeguimiento $fechaOt
						ORDER BY ".$sord." LIMIT ".$limit." OFFSET ".$offset; 
		//debug($presupuestos);
		$presupuestos = $this->query($presupuestos);
				
		$this->Talot = new Talot();
		$this->Ccptabhistorial= new Ccptabhistorial;
		$this->Ccpsolicitudservicio= new Ccpsolicitudservicio;
		
		foreach($presupuestos as $key => $presupuesto){
			//aventura
			$presupuestos[$key]['Talot']['seg_id'] = trim($presupuesto['ot']['seg_id']);
			$presupuestos[$key]['Talot']['nro_seguimiento'] = trim($presupuesto['ot']['nro_seguimiento']);
			$presupuestos[$key]['Talot']['segestado_id'] = trim($presupuesto['ot']['segestado_id']);
			$presupuestos[$key]['Talot']['segestado_descripcion'] = trim($presupuesto['ot']['segestado_descripcion']);
			
			$presupuestos[$key]['Talot']['secperson_lavador_id'] = trim($presupuesto['ot']['secperson_lavador_id']);
			$presupuestos[$key]['Talot']['secperson_coordinador_id'] = trim($presupuesto['ot']['secperson_coordinador_id']);
			$presupuestos[$key]['Talot']['secperson_jefetaller_id'] = trim($presupuesto['ot']['secperson_jefetaller_id']);
			
			$presupuestos[$key]['Talot']['fecha_lavador'] = trim($presupuesto['ot']['fecha_lavador']);
			$presupuestos[$key]['Talot']['segmotivo_descripcion'] = trim($presupuesto['ot']['segmotivo_descripcion']);
			$presupuestos[$key]['Talot']['seg_comentario_lavado'] = trim($presupuesto['ot']['seg_comentario_lavado']);
			
			//AVENTURA: CALCULADOS
			$presupuestos[$key]['Talot']['nro_max_segimiento'] = $this->Ccplavadoseguimiento->getNroSeguimiento(trim($presupuesto['ot']['ot_numero']));
			$presupuestos[$key]['Talot']['nro_coordinador_aprobado'] = $this->Ccplavadoseguimiento->getLavadoCoordinadorAprobado(trim($presupuesto['ot']['ot_numero']));
			
			
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['ot']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['ot']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['ot']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['ot']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['ot']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['ot']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['ot']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['ot']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['ot']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['ot']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['ot']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['ot']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['ot']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_color'] = trim($presupuesto['ot']['ot_color']);
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['ot']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['ot']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['ot']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['ot']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['ot']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['ot']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['ot']['ot_numero_cono'];

			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($presupuesto['ot']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;

			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($presupuesto['ot']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			
			unset($presupuestos[$key]['ot']);
		} 
	
		return $presupuestos;
	}
	
	function getReporte($ot_numero){
		$sql="SELECT
				Ccptabhistoriale.id,
				Ccptabhistoriale.ot_numero,
				Ccptabhistoriale.ccptabestado_id,
				Ccptabestado.descripcion,
				Ccptabhistoriale.fecha,
				Ccptabhistoriale.ccpsolicitudservicio_id,
				concat(Secperson.firstname,' ',Secperson.appaterno) as nombreCompleto
				FROM ccptabhistoriales Ccptabhistoriale
				INNER JOIN ccptabestados Ccptabestado on(Ccptabhistoriale.ccptabestado_id = Ccptabestado.id)
				INNER JOIN secpeople Secperson ON(Ccptabhistoriale.secperson_id = Secperson.id)
				WHERE ot_numero='".$ot_numero."' ORDER BY Ccptabhistoriale.id";
		$lista=$this->query($sql);
		
		return (!empty($lista) && isset($lista))?$lista:array();
		
	}
	
	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-21
	 * @return 
	 */
	function getProcesos(){
		$proceso=array(1=>'Recepción',2=>'Depósito',3=>'Presupuesto',4=>'Producción',5=>'Control de Calidad',6=>'Entrega',7=>'Facturación');
		return $proceso;
	}
	
	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-21
	 * @return 
	 */
	function getProcesosPorCcpEstado($estadoccp_id){
		$procesos = $this->getProcesos();
		if(in_array($estadoccp_id, array(1)))return $procesos[1];
		if(in_array($estadoccp_id, array(2)))return $procesos[2];
		if(in_array($estadoccp_id, array(16,17,3,4,18,19,20)))return $procesos[3];
		if(in_array($estadoccp_id, array(5,6)))return $procesos[4];
		if(in_array($estadoccp_id, array(13,7,8,9,10,12)))return $procesos[5];
		if(in_array($estadoccp_id, array(11)))return $procesos[6];
		if(in_array($estadoccp_id, array(14,15)))return $procesos[7];
	}
	
	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-21
	 * @return 
	 */
	function getPrimerSolitudServicioIdByNroOt($ot_numero){
		//verificar si tiene un ampliacion para agregar el estado ampliacion
		app::import('Model', 'Ccpsolicitudservicio');		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		
		$this->Ccpsolicitudservicio->recursive=-1;
		$condition=array('Ccpsolicitudservicio.ot_numero'=>$ot_numero);
		$solicitudServicio= $this->Ccpsolicitudservicio->find('all',array(
			'conditions'=>$condition,
			'order'=>'Ccpsolicitudservicio.id asc'
		));
		$comboAmpliacion=array();
		$solicitud_id=null;
		//pr($solicitudServicio);
		if(!empty($solicitudServicio) && isset($solicitudServicio)){
			$solicitud_id = $solicitudServicio[0]['Ccpsolicitudservicio']['id'];			
		}
		return 	$solicitud_id;
	}
	
	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-21
	 * @return 
	 */
	function getSolicitudServicioPorSolicitud($solicitud_id){
		if(empty($solicitud_id)) return array();
		$sql = "SELECT Ccpsolicitudservicio.*,
				concat(Secperson.firstname,' ',Secperson.appaterno) as nombreCompleto
				FROM ccpsolicitudservicios Ccpsolicitudservicio
				INNER JOIN secpeople Secperson ON(Secperson.id=Ccpsolicitudservicio.secperson_id)
				WHERE Ccpsolicitudservicio.id=".$solicitud_id;
		$lista = $this->query($sql);
		return (!empty($lista) && isset($lista))?$lista:array();
	}
	
	/**MIGRADO POR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-05-21
	 * @return 
	 */
	function getEstadoProceso($fechaInicial,$fechaFin){
		if((!empty($fechaInicial) && isset($fechaInicial)) && (!empty($fechaInicial) && isset($fechaInicial))) return 'Finalizado';
		else return 'Pendiente';
	}
} 