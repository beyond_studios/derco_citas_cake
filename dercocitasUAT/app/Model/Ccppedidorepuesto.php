<?php 
class Ccppedidorepuesto extends AppModel{
	public $name='Ccppedidorepuesto';	
	public $displayField = 'descripcion';
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed		
	public $hasMany = array(
		'Ccpsolicitudservicio' => array(
			'className' => 'Ccpsolicitudservicio',
			'foreignKey' => 'ccppedidorepuesto_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
}
?>