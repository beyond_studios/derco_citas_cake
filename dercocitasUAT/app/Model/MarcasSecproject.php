<?php
App::uses('AppModel', 'Model');
class MarcasSecproject extends AppModel
{
	public $name = 'MarcasSecproject';
	
	public $validate = array(
		'secproject_id' => array('rule' => array('numeric')),
		'marca_id' => array('rule' => array('numeric')),
		'marca_id' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'message' => 'selecione 1 o mas marcas'
								),
						'numeric' =>array(
            					'rule'    => 'numeric',
								'message' => 'ingrese dato numerico'
								)      
					),
		'status' => array('rule' => array('notempty'))
	);
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Secproject' => array(
			'className' => 'Secproject',
			'foreignKey' => 'secproject_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Marca' => array(
			'className' => 'Marca',
			'foreignKey' => 'marca_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**ASIGNA LAS MARCAS A UNA DETERMINADA SUCURSAL
	 * AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013/03/11
	 * @param object $secprojectId REQUERIDO
	 * @param object $dt REQUERIDO
	 * @return 
	 */
	function setMarcas($secprojectId, $dt){
		// DESACTIVAMOS TODAS LAS ASIGNACIONES DE LAS MARCAS ANTERIORES
		if(!$this->updateAll(
			array('MarcasSecproject.status'=>"'DE'"),
			array('MarcasSecproject.secproject_id'=>$secprojectId, 'MarcasSecproject.status'=>'AC'))
		) return array(false, __('lasMarcasAsignadasNoSeActualizaron', true));
		
		// PROCEDEMOS A GUARDAR LAS NUEVAS MARCAS ASIGNADAS A LA SUCURSAL
		foreach($dt['MarcasSecprojects'] as $marca){
			$marca['MarcasSecproject']['secproject_id'] = $secprojectId;
			$marca['MarcasSecproject']['status'] = 'AC';
			
			$marca_db = $this->find('first', array(
				'conditions'=>array('MarcasSecproject.marca_id'=>$marca['MarcasSecproject']['marca_id'], 'MarcasSecproject.secproject_id'=>$secprojectId),
				'recursive'=>-1
			));
			
			if(empty($marca_db)) $this->create();
			else $marca['MarcasSecproject']['id'] = $marca_db['MarcasSecproject']['id'];
			
			if(!$this->save($marca)) return array(false, __("laMarcaNoSeGuardo", true));
			unset($marca_db);
		}
	
		return array(true, 'losDatosFueronGuardados');
	}
}
?>