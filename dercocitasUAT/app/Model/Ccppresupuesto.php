<?php
class Ccppresupuesto extends AppModel {
   public $name = 'Ccppresupuesto';
   //public $useDbConfig = 'mssql_apoyo';
   public $useTable = false;
   
   /**AVENTURA
	 * RECUPERA EL ARRAY DE REGISTROS PARA MOSTRAR
	 * @param object $condicion
	 * @return 
	 */
	function buscarTodos($parametros,$filters=null,$secproject_carscod){ //debug($parametros);
		$condicion ="1=1";
		if($parametros['_search'] == 'true'){
			
			foreach($filters['rules'] as $rule){
				switch($rule['op']){
					case "eq": $condicion .= " ".$filters['groupOp']." ".$rule['field']." = ".$rule['data']; break;
					case "ne": $condicion .= " ".$filters['groupOp']." ".$rule['field']." <> ".$rule['data']; break;
					case "lt": $condicion .= " ".$filters['groupOp']." ".$rule['field']." < ".$rule['data']; break;
					case "le": $condicion .= " ".$filters['groupOp']." ".$rule['field']." <= ".$rule['data']; break;
					case "gt": $condicion .= " ".$filters['groupOp']." ".$rule['field']." > ".$rule['data']; break;
					case "ge": $condicion .= " ".$filters['groupOp']." ".$rule['field']." >= ".$rule['data']; break;
					case "bw": //$condicion .= " ".$filters['groupOp']." ".$rule['field']." LIKE '%".$rule['data']."'"; break;
					case "ew": //$condicion .= " ".$filters['groupOp']." ".$rule['field']." LIKE '".$rule['data']."%'"; break;
					case "ce": 	if($rule['field'] == 'OT.DocFch')  $condicion .= " ".$filters['groupOp']." ".$rule['field']." = '".$this->fecha_Php_Mysql($rule['data'])."'";	
								else  $condicion .= " ".$filters['groupOp']." ".$rule['field']." LIKE '%".$rule['data']."%'";
								break;
				}
			}				
		}
			
		$page 	= $parametros['page']; // get the requested page 
		$limit 	= $parametros['rows']; // get how many rows we want to have into the grid 
		$sidx 	= $parametros['sidx']; // get index row - i.e. user click to sort 
		$sord 	= $parametros['sord']; // get the direction 
		
		if(!$sidx) $sidx =1; 
		
		//$secproject_carscod = 74;
		// RECUPERAMOS EL TOTAL DE REGISTROS
		$total = "SELECT COUNT(DocNro) FROM (
					SELECT
						ot.DocNro
							
					FROM 
						TORDCAB AS pre WITH (nolock) 
						INNER JOIN (
								SELECT miOt.*, cli.CliApe,au.AutNumSer, au.AutNumMat, ma.MarDsc, mo.ModAbv, mo.ModDsc 
								FROM TORDCAB as miOt WITH (nolock)
									INNER JOIN GCLIENTE AS cli WITH (nolock)ON miOt.CiaCod = cli.CiaCod and miOt.CliCod = cli.CliCod 
									LEFT JOIN GAUTOS AS au  WITH (nolock)  ON miOt.CiaCod = au.CiaCod AND miOt.AutNumId = au.AutNumId
									INNER JOIN GMARCAS AS ma WITH (nolock) ON au.AutMarCod = ma.MarCod AND au.CiaCod = ma.CiaCod
									INNER JOIN GMODELO AS mo WITH (nolock) ON au.AutModCiaC = mo.ModMarCiaC AND au.AutModMar = mo.ModMar 
											AND au.AutModCod = mo.ModCod
								 WHERE miOt.DocTipo <> 'P' and miOt.CiaCod in ('DP','DC') and miOt.DocLocId = $secproject_carscod
							) as ot ON ot.CiaCod=pre.CiaCod and ot.DocNro = pre.DocPreOtGe
					WHERE 
						(pre.DocActivo = 'S')  						-- Documento activado 
						AND (pre.DocTipo = 'P')    					-- Documento tipo presupuesto
						-- AND (ot.DocFch = '08/08/2011')
						AND $condicion
					GROUP BY 
						ot.DocNro) as DOC";
							
		$total = $this->query($total);
		$count = $total[0][0]['computed'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		$start = $limit*$page - $limit; 
		
		//paginacion en SQL
		$offset=($page-1)*$limit;
		
		// RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		$presupuestos = "SELECT TOP $limit
								ot.DocNro AS ot_numero,         		-- Numero de OT generad
								min(pre.DocNro) AS presupuesto_numero,	-- Numero de presupuesto
								ot.DocFch AS ot_fecha_creacion,         -- Fecha que se gener? el la OT
								ot.DocUsrAbr AS ot_asesor,  			-- Usuario que genero la OT(Asesor de Servicio del CCP)
								ot.DocFecProm AS ot_fecha_entrega,  	-- Fecha Prometida
								ot.CliApe AS ot_cliente,  		-- AQUI SERA EL CLIENTE
								ot.AutNumSer AS chasis_vin	,		-- Numero de chasis
								ot.AutNumMat as ot_placa,		       	-- Nuemero de Placa
								ot.DocAseFch AS pre_fecha_aprobacion    -- fecha aprobacion de seguro
						FROM 
							TORDCAB AS pre WITH (nolock) 
							INNER JOIN (
									SELECT miOt.*,cli.CliApe,au.AutNumSer, au.AutNumMat, ma.MarDsc, mo.ModAbv, mo.ModDsc 
									FROM TORDCAB as miOt WITH (nolock)
										INNER JOIN GCLIENTE AS cli WITH (nolock)ON miOt.CiaCod = cli.CiaCod and miOt.CliCod = cli.CliCod 
										LEFT JOIN GAUTOS AS au  WITH (nolock)  ON miOt.CiaCod = au.CiaCod AND miOt.AutNumId = au.AutNumId
										INNER JOIN GMARCAS AS ma WITH (nolock) ON au.AutMarCod = ma.MarCod AND au.CiaCod = ma.CiaCod
										INNER JOIN GMODELO AS mo WITH (nolock) ON au.AutModCiaC = mo.ModMarCiaC AND au.AutModMar = mo.ModMar 
												AND au.AutModCod = mo.ModCod
									 WHERE miOt.DocTipo <> 'P' and miOt.CiaCod='DP' and miOt.DocLocId = $secproject_carscod
								) as ot ON ot.CiaCod=pre.CiaCod and ot.DocNro = pre.DocPreOtGe
						WHERE 
							(pre.DocActivo = 'S')  						-- Documento activado 
							AND (pre.DocTipo = 'P')    					-- Documento tipo presupuesto
							-- AND (ot.DocFch = '08/08/2011')
							AND $condicion
							AND ot.DocNro NOT IN (
									SELECT TOP ".$offset."
											ot.DocNro
									FROM 
										TORDCAB AS pre WITH (nolock) 
										INNER JOIN (
												SELECT miOt.DocFch,miOt.DocNro,ot.DocUsrAbr,miOt.CiaCod, cli.CliApe,au.AutNumSer, au.AutNumMat, ma.MarDsc, mo.ModAbv, mo.ModDsc 
												FROM TORDCAB as miOt WITH (nolock)
													INNER JOIN GCLIENTE AS cli WITH (nolock)ON miOt.CiaCod = cli.CiaCod and miOt.CliCod = cli.CliCod 
													LEFT JOIN GAUTOS AS au  WITH (nolock)  ON miOt.CiaCod = au.CiaCod AND miOt.AutNumId = au.AutNumId
													INNER JOIN GMARCAS AS ma WITH (nolock) ON au.AutMarCod = ma.MarCod AND au.CiaCod = ma.CiaCod
													INNER JOIN GMODELO AS mo WITH (nolock) ON au.AutModCiaC = mo.ModMarCiaC AND au.AutModMar = mo.ModMar 
															AND au.AutModCod = mo.ModCod
												 WHERE miOt.DocTipo <> 'P' and miOt.CiaCod='DP' and miOt.DocLocId = $secproject_carscod
											) as ot ON ot.CiaCod=pre.CiaCod and ot.DocNro = pre.DocPreOtGe
									WHERE 
										(pre.DocActivo = 'S')  						-- Documento activado 
										AND (pre.DocTipo = 'P')    					-- Documento tipo presupuesto
										-- AND (miot.DocFch = '08/08/2011')
										AND $condicion
									GROUP BY 
										ot.DocFch,ot.DocNro,ot.AutNumSer, ot.DocUsrAbr,ot.CliApe,ot.AutNumSer, ot.AutNumMat, ot.MarDsc, ot.ModAbv, ot.ModDsc
									ORDER BY $sidx $sord
									)
						GROUP BY 
							ot.DocNro,ot.DocFch,ot.DocUsrAbr,ot.DocFecProm,ot.DocUsrAbr,ot.AutNumSer,ot.AutNumMat, ot.DocAseFch, ot.CliApe
						ORDER BY $sidx $sord"; 

		$presupuestos = $this->query($presupuestos);
		
		$responce->page = $page; 
		$responce->total = $total_pages; 
		$responce->records = $count; 
									
		foreach($presupuestos as $key => $presupuesto){
			$responce->rows[$key]['id']=trim($presupuesto['0']['ot_numero']);
			$responce->rows[$key]['cell'] = array(
												trim($presupuesto['0']['ot_numero']),
												trim($presupuesto['0']['presupuesto_numero']),
												$this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']),
												trim($presupuesto['0']['ot_asesor']),
												trim($presupuesto['0']['chasis_vin']),
												trim($presupuesto['0']['ot_cliente']),
												trim($presupuesto['0']['ot_placa']),
												$this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_entrega']),
												$this->fecha_Mysql_Php($presupuesto['0']['pre_fecha_aprobacion']));
		} 
		
		return $responce;
	}
	
	function getPresupuesto($numero_presupuesto){
		$condicion = " pre.DocNro = '$numero_presupuesto'";
		
		$presupuestos = "SELECT
								  pre.DocUsrAbr AS presupuesto_nombre_origina,-- Usuario que abrio el presupuesto
						          pre.DocFch AS presupuesto_fecha_creacion,    -- Fecha que se gener? el presupuesto
								  pre.DocAseFchL AS presupuesto_fecha_presupuesto_listo, --fecha presupuesto listp
								  pre.DocAseFch AS presupuesto_fecha_aprobacion_compania --fecha aprobacion compania								  
						FROM 
							dpcars75.dbo.TORDCAB AS pre WITH (nolock) 
						where $condicion";		
		return $this->query($presupuestos);
	}
	
	//RTINEO obtiene el presupuesto validado con el autnumId del vehiculo de la ot
	function getPresupuestoNew($numero_presupuesto,$ot_numero){
		$presupuestos = "SELECT 
							pre.DocUsrAbr AS presupuesto_nombre_origina,-- Usuario que abrio el presupuesto
 							pre.DocFch AS presupuesto_fecha_creacion,    -- Fecha que se gener? el presupuesto
 							pre.DocAseFchL AS presupuesto_fecha_presupuesto_listo, --fecha presupuesto listp
							pre.DocAseFch AS presupuesto_fecha_aprobacion_compania --fecha aprobacion compania								  
							FROM dpcars75.dbo.TORDCAB AS pre WITH (nolock) 
							INNER JOIN (SELECT AutNumId FROM dpcars75.dbo.TORDCAB AS pre WITH (nolock) where DocNro='".$ot_numero."') as AutNum -- numero de ot 
							ON(AutNum.AutNumId=pre.AutNumId)
							WHERE pre.DocTipo='P' AND pre.DocNro = '".$numero_presupuesto."'  -- numero ingresado por el usuario";		
		return $this->query($presupuestos);
	}
	
	/** AVENTURA - NOS RECUPERA TODOS LOS PRESUPUESTOS DE UNA OT **/
	function getPresupuestosPorOt($ot_id){
		$this->Talot = new Talot ;
		$ot = $this->Talot->findById($ot_id,null,null,-1);
		
		$presupuestos = "SELECT
								pre.DocNro AS numero,	-- Numero de presupuesto
								pre.DocFch AS created,
								ot.DocAseFch AS aprobacion    -- fecha aprobacion de seguro
						FROM 
							dpcars75.dbo.TORDCAB AS pre WITH (nolock)
							INNER JOIN  (Select * from dpcars75.dbo.TORDCAB WHERE DocTipo <> 'P' and CiaCod='DP') as ot ON ot.CiaCod=pre.CiaCod and ot.DocNro = pre.DocPreOtGe 
						WHERE 
							pre.DocActivo = 'S'  						-- Documento activado 
							AND pre.DocTipo = 'P'    					-- Documento tipo presupuesto
							AND ot.DocNro = '".$ot['Talot']['nrocars']."'";
		
		$presupuestos = $this->query($presupuestos);
		
		if(empty($presupuestos)) return $presupuestos;
		
		foreach($presupuestos as $key => $value){
			$presupuestosOt[$key]['Ccppresupuesto']['numero'] = $value['0']['numero'];
			$presupuestosOt[$key]['Ccppresupuesto']['created'] = $this->fecha_Mysql_Php($value['0']['created']);
			$presupuestosOt[$key]['Ccppresupuesto']['aprobacion'] = $this->fecha_Mysql_Php($value['0']['aprobacion']);
		}			
		return $presupuestosOt;					
	}
	
	/**AVENTURA
	 * $data = Array
(
    [url] => ccpsolicitudservicios/index
    [fechaIni] => 08-11-2011				--solicitud
    [fechaFin] => 10-11-2011				--solicitud
    [ccptiposervicio_id] => 1				--solicitud
    [ccpsolicitudservicioestado_id] => 1    --solicitud
    
    [asesor_id] => ABS  					-- cars -- solicitud    
    [fechaIniOt] => 15-11-2011 				-- cars
    [fechaFinOt] => 17-11-2011  			-- cars
    [carsestado_id] => 1					-- cars
)
	 * RECUPERA TODAS LAS OTS DE CARS
	 * @param object $condicion
	 * @return 
	 */
	function getTalotCars($data, $params, $login, $total = false){
		// VALORES NECESARIOS
		$secproject_carscod = $params['params']['carscod'];
		// RECUPERAMOS DATOS PARA LA PAGINACION
		$sord = empty($params['params']['order'])?'':$params['params']['order'];
		$limit = empty($params['params']['limit'])?'':$params['params']['limit'];
		$page = empty($params['params']['page'])?'':$params['params']['page'];
 		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$condicionSolicitud = 'Ccpsolicitudservicio.ot_numero is not null';
		$fechaSol = $this->condicionBuscadorFecha(empty($data['fechaIni'])?'':$data['fechaIni'], empty($data['fechaIni'])?'':$data['fechaFin'], $campoModelo ='Ccpsolicitudservicio.fecha_emitido');
		if(!empty($fechaSol)) $condicionSolicitud .= $fechaSol;
		if(!empty($data['ccptiposervicio_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccptiposervicio_id = '.$data['ccptiposervicio_id'];
		if(!empty($data['ccpsolicitudservicioestado_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccpsolicitudservicioestado_id = '.$data['ccpsolicitudservicioestado_id'];
		//if(!empty($data['asesor_id'])) $condicionSolicitud .= " AND Ccpsolicitudservicio.ot_asesor = '".trim($data['asesor_id'])."'";
		//debug($condicionSolicitud);	
		//RECUPERO LOS OT DE LAS SOLICITUDES
		$ot_solicitudes = '';
		if($condicionSolicitud != 'Ccpsolicitudservicio.ot_numero is not null'){
			$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
			$ot_solicitudes = $this->Ccpsolicitudservicio->generateList($condicionSolicitud);
			$ot_solicitudes = empty($ot_solicitudes)?'AND ot.OT IN(\'0\')':'AND ot.OT IN(\''.implode('\',\'', $ot_solicitudes).'\')';
		}
		//RECUPERAMOS las ots que se encuentran en estado
		$ot_ccphistorial='';
		if(!empty($data['estadootccp_id']) && isset($data['estadootccp_id'])){
			$this->Ccptabhistorial = new Ccptabhistorial;
			if(strlen($data['estadootccp_id']==6)){
				if(strpos('1', $data['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($data['estadootccp_id']==1){
					$ot_ccphistorial="";
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		//FILTROS DE CARS
		$condicion_cars=" ";
		$condicion_cars="ot.TALLER=".$secproject_carscod;
		if(!empty($data['placa']) && isset($data['placa'])){
			$placa_= " AND ot.PLACA like '%".$data['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;

		$fechaCars = $this->condicionBuscadorFecha(empty($data['fechaIniOt'])?'':$data['fechaIniOt'], empty($data['fechaFinOt'])?'':$data['fechaFinOt'], $campoModelo ='ot.FECHA');
		if(!empty($fechaCars)) $condicion_cars .= $fechaCars;
		if(!empty($data['asesor_id'])) $condicion_cars .= " AND ot.USUARIO_OT = '".trim($data['asesor_id'])."' ";
		if(!empty($data['carsestado_id'])){
			$estadoCars = $data['carsestado_id']-1; 
			$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO IN (0,2)"; 
		if(!empty($data['preestado_id'])) $condicion_cars .= ' AND ot.OT_PRE_ESTADO = '.$data['preestado_id'];
		if(!empty($data['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$data['nrocars'].'%\'';
		
		// RECUPERAMOS EL TOTAL DE REGISTROS
		//debug("$condicion_cars $ot_solicitudes");
		$total_query = "SELECT COUNT(OT) FROM dpcars75.Sistema_Planificador_CCP as ot WHERE $condicion_cars $ot_solicitudes $ot_ccphistorial";
		$total_query = $this->query($total_query);
		
		if($total) return $total_query[0][0]['computed'];
		
		$count = $total_query[0][0]['computed'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		//paginacion en SQL 
		$offset=empty($page)?0:($page-1)*$limit;

		//debug($ot_solicitudes);
		//debug($condicion_cars);
		// RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		$presupuestos = "SELECT TOP $limit
								ot.OT AS ot_numero,
								ot.MARCA AS ot_marca,
								ot.MODELO AS ot_modelo,
								ot.VERSION AS ot_version,
								ot.DESCRIPCION AS ot_descripcion,
								ot.NOMBRE_ASESOR AS ot_nombre_asesor,
								ot.TIPO_OT AS ot_tipo_ot,
								ot.HORA_RECIBIDA AS ot_hora_recibida,
								ot.HORA_PROMETIDA AS ot_hora_prometida,
								ot.NRO_CONO AS ot_numero_cono,
								ot.PRESUPUESTO AS presupuesto_numero,
								ot.FECHA AS ot_fecha_creacion,
								ot.USUARIO_OT AS ot_asesor,
								ot.FECHA_PROM AS ot_fecha_entrega,
								ot.CLIENTE AS ot_cliente,
								ot.NRO_SERIE AS chasis_vin,
								ot.PLACA as ot_placa,
								ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,
								ot.OT_ESTADO AS ot_estado,
								ot.OT_PRE_ESTADO as ot_pre_estado
						FROM 
							dpcars75.Sistema_Bandeja_CCP AS ot
						WHERE 
							$condicion_cars $ot_solicitudes $ot_ccphistorial
							AND ot.OT NOT IN (
									SELECT TOP $offset
											ot.OT AS ot_numero
									FROM 
										dpcars75.Sistema_Bandeja_CCP AS ot
									WHERE 
										$condicion_cars $ot_solicitudes $ot_ccphistorial
									-- GROUP BY ot.OT
									ORDER BY $sord
									)
						ORDER BY $sord"; 
		//pr($presupuestos);
		
		$presupuestos = $this->query($presupuestos);
		//pr($presupuestos);		
		$this->Talot = new Talot();
		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		$this->Ccptabhistorial = new Ccptabhistorial;
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['0']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['0']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['0']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['0']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['0']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['0']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['0']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['0']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['0']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['0']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['0']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['0']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['0']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['0']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['0']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['0']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['0']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['0']['ot_numero_cono'];
			
			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($presupuesto['0']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;

			//obtenemos el color de la ot
			$presupuestos[$key]['Talot']['color']=$this->Ccptabhistorial->obtenerDiferenciaDias(date('d-m-Y'),$presupuestos[$key]['Talot']['ot_fecha_creacion']);
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($presupuesto['0']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			unset($solicitudServicio[0]);
			unset($presupuestos[$key]['0']);
		} 
		
		return $presupuestos;
	}
/*
 * RTINEO
 * @param object $data
 * @param object $params
 * @param object $login
 * @param object $total [optional]
 * @return listado de ot desde cars 
 */

function getTalotCarsNew($data, $params, $login, $total = false,$exportar){
		// VALORES NECESARIOS
		//pr('aca');pr($data);
		$secproject_carscod = $params['params']['carscod'];
		// RECUPERAMOS DATOS PARA LA PAGINACION
		$sord = empty($params['params']['order'])?'':$params['params']['order'];
		$limit = empty($params['params']['limit'])?'':$params['params']['limit'];
		$page = empty($params['params']['page'])?'':$params['params']['page'];
 		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$condicionSolicitud = "";
		$condicionSolicitud = 'Ccpsolicitudservicio.ot_numero is not null';
		$fechaSol = $this->condicionBuscadorFecha(empty($data['fechaIni'])?'':$data['fechaIni'], empty($data['fechaIni'])?'':$data['fechaFin'], $campoModelo ='Ccpsolicitudservicio.fecha_emitido');
		if(!empty($fechaSol)) $condicionSolicitud .= $fechaSol;
		if(!empty($data['ccptiposervicio_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccptiposervicio_id = '.$data['ccptiposervicio_id'];
		if(!empty($data['ccpsolicitudservicioestado_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccpsolicitudservicioestado_id = '.$data['ccpsolicitudservicioestado_id'];
	
		//RECUPERO LOS OT DE LAS SOLICITUDES
		$ot_solicitudes = '';
		
		if($condicionSolicitud != 'Ccpsolicitudservicio.ot_numero is not null'){
			$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
			$ot_solicitudes = $this->Ccpsolicitudservicio->generateList($condicionSolicitud,null,null,"{n}.Ccpsolicitudservicio.id","{n}.Ccpsolicitudservicio.ot_numero");
			$ot_solicitudes = empty($ot_solicitudes)?'AND ot.OT IN(\'0\')':'AND ot.OT IN(\''.implode('\',\'', $ot_solicitudes).'\')';
			
		}
		//RECUPERAMOS las ots que se encuentran en estado
		$ot_ccphistorial='';
		if(!empty($data['estadootccp_id']) && isset($data['estadootccp_id'])){
			$this->Ccptabhistorial = new Ccptabhistorial;
			if(strlen($data['estadootccp_id']==7)){
				if(strpos('1', $data['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($data['estadootccp_id']==1){
					$ot_ccphistorial="";
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		//FILTROS DE CARS
		$condicion_cars=" ";
		$condicion_cars="ot.TALLER=".$secproject_carscod;
		if(!empty($data['placa']) && isset($data['placa'])){
			$placa_= " AND ot.PLACA like '%".$data['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;
		$fechaCars = $this->condicionBuscadorFecha(empty($data['fechaIniOt'])?'':$data['fechaIniOt'], empty($data['fechaFinOt'])?'':$data['fechaFinOt'], $campoModelo ='ot.FECHA');
		//pr(array('aca'=>$fechaCars));
		if(!empty($fechaCars)) $condicion_cars .= $fechaCars;
		if(!empty($data['asesor_id'])) $condicion_cars .= " AND  LTRIM(RTRIM(ot.USUARIO_OT)) = '".trim($data['asesor_id'])."'";
		if(!empty($data['carsestado_id'])){
			$estadoCars = $data['carsestado_id']-1; 
			$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		if(!empty($data['preestado_id'])) $condicion_cars .= ' AND ot.OT_PRE_ESTADO = '.$data['preestado_id'];
		if(!empty($data['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$data['nrocars'].'%\'';
		
		// RECUPERAMOS EL TOTAL DE REGISTROS
		//debug("$condicion_cars $ot_solicitudes");
		$total_query = "SELECT COUNT(OT) FROM dpcars75.Sistema_Planificador_CCP as ot WHERE $condicion_cars $ot_solicitudes $ot_ccphistorial";
		$total_query = $this->query($total_query);
		
		if($total) return $total_query[0][0]['computed'];
		
		$count = $total_query[0][0]['computed'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		//paginacion en SQL 
		$offset=empty($page)?0:($page-1)*$limit;

		//debug($ot_solicitudes);
		//debug($condicion_cars);
		//RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		if($exportar){
			$presupuestos = "SELECT TOP $limit
									ot.OT AS ot_numero,
									ot.MARCA AS ot_marca,
									ot.MODELO AS ot_modelo,
									ot.VERSION AS ot_version,
									ot.DESCRIPCION AS ot_descripcion,
									ot.NOMBRE_ASESOR AS ot_nombre_asesor,
									ot.TIPO_OT AS ot_tipo_ot,
									ot.HORA_RECIBIDA AS ot_hora_recibida,
									ot.HORA_PROMETIDA AS ot_hora_prometida,
									ot.NRO_CONO AS ot_numero_cono,
									ot.PRESUPUESTO AS presupuesto_numero,
									ot.FECHA AS ot_fecha_creacion,
									ot.USUARIO_OT AS ot_asesor,
									ot.FECHA_PROM AS ot_fecha_entrega,
									ot.CLIENTE AS ot_cliente, 
									ot.NRO_SERIE AS chasis_vin,
									ot.PLACA as ot_placa,
									ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,
									ot.OT_ESTADO AS ot_estado,
									ot.OT_PRE_ESTADO as ot_pre_estado
							FROM 
								dpcars75.Sistema_Planificador_CCP AS ot
							WHERE 
								$condicion_cars $ot_solicitudes $ot_ccphistorial
							ORDER BY $sord"; 			
		}else{
			$presupuestos = "SELECT TOP $limit
									ot.OT AS ot_numero,
									ot.MARCA AS ot_marca,
									ot.MODELO AS ot_modelo,
									ot.VERSION AS ot_version,
									ot.DESCRIPCION AS ot_descripcion,
									ot.NOMBRE_ASESOR AS ot_nombre_asesor,
									ot.TIPO_OT AS ot_tipo_ot,
									ot.HORA_RECIBIDA AS ot_hora_recibida,
									ot.HORA_PROMETIDA AS ot_hora_prometida,
									ot.NRO_CONO AS ot_numero_cono,
									ot.PRESUPUESTO AS presupuesto_numero,
									ot.FECHA AS ot_fecha_creacion,
									ot.USUARIO_OT AS ot_asesor,
									ot.FECHA_PROM AS ot_fecha_entrega,
									ot.CLIENTE AS ot_cliente,
									ot.NRO_SERIE AS chasis_vin,
									ot.PLACA as ot_placa,
									ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,
									ot.OT_ESTADO AS ot_estado,
									ot.OT_PRE_ESTADO as ot_pre_estado
							FROM 
								dpcars75.Sistema_Planificador_CCP AS ot
							WHERE 
								$condicion_cars $ot_solicitudes $ot_ccphistorial
								AND ot.OT NOT IN (
										SELECT TOP $offset
												ot.OT AS ot_numero
										FROM 
											dpcars75.Sistema_Planificador_CCP AS ot
										WHERE 
											$condicion_cars $ot_solicitudes $ot_ccphistorial
										ORDER BY $sord
										)
							ORDER BY $sord"; 
		}
		//DEBUG($presupuestos);
		//pr($presupuestos);
		
		$presupuestos = $this->query($presupuestos);
				
		$this->Talot = new Talot();
		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		$this->Ccptabhistorial= new Ccptabhistorial;
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['0']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['0']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['0']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['0']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['0']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['0']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['0']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['0']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['0']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['0']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['0']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['0']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['0']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['0']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['0']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['0']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['0']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['0']['ot_numero_cono'];
			
			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($presupuesto['0']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;
			
			//obtenemos el color de la ot
			$presupuestos[$key]['Talot']['color']=$this->Ccptabhistorial->obtenerDiferenciaDias(date('d-m-Y'),$presupuestos[$key]['Talot']['ot_fecha_creacion']);
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($presupuesto['0']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			
			unset($presupuestos[$key]['0']);
		}
		
		return $presupuestos;
	}

/*
 * RTINEO: Bandeja del resposable de deposito
 * @param object $data
 * @param object $params
 * @param object $login
 * @param object $total [optional]
 * @return 
 */
	function getTalotCarsResponsableDeposito($data, $params, $login, $total = false){
		// VALORES NECESARIOS
		$secproject_carscod = $params['params']['carscod'];
		// RECUPERAMOS DATOS PARA LA PAGINACION
		$sord = empty($params['params']['order'])?'':$params['params']['order'];
		$limit = empty($params['params']['limit'])?'':$params['params']['limit'];
		$page = empty($params['params']['page'])?'':$params['params']['page'];
 		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = '';

		//RECUPERAMOS las ots que se encuentran en ccpestado
		$ot_ccphistorial='';
		if(!empty($data['estadootccp_id']) && isset($data['estadootccp_id'])){
			$this->Ccptabhistorial = new Ccptabhistorial;
			if(strlen($data['estadootccp_id']==17)){
				if(strpos('1', $data['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($data['estadootccp_id']==1){
					$ot_ccphistorial="";
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		//pr($ot_ccphistorial);
		//FILTROS DE CARS
		$condicion_cars=" ";
		$condicion_cars="ot.TALLER=".$secproject_carscod;
		
		if(!empty($data['placa']) && isset($data['placa'])){
			$placa_= " AND ot.PLACA like '%".$data['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;

		//if(!empty($data['asesor_id'])) $condicion_cars .= " AND ot.USUARIO_OT = '".trim($data['asesor_id'])."'";
		if(!empty($data['carsestado_id'])){
			$estadoCars = $data['carsestado_id']-1; 
			//$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'";
			$condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		if(!empty($data['chasis'])) $condicion_cars .= " AND ot.NRO_SERIE like '%".$data['chasis']."%'";
		if(!empty($data['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$data['nrocars'].'%\'';
		
		// RECUPERAMOS EL TOTAL DE REGISTROS
		//debug("$condicion_cars $ot_solicitudes");
		$total_query = "SELECT COUNT(OT) FROM dpcars75.Sistema_Planificador_CCP as ot WHERE $condicion_cars $ot_solicitudes $ot_ccphistorial";

		$total_query = $this->query($total_query);
		
		if($total) return $total_query[0][0]['computed'];
		
		$count = $total_query[0][0]['computed'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		//paginacion en SQL 
		$offset=empty($page)?0:($page-1)*$limit;

		// RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		$presupuestos = "SELECT TOP $limit
								ot.OT AS ot_numero,
								ot.PRESUPUESTO AS presupuesto_numero,		-- Numero de presupuesto
								ot.FECHA AS ot_fecha_creacion,         		-- Fecha que se gener? el la OT  *
								ot.USUARIO_OT AS ot_asesor,  				-- Usuario que genero la OT(Asesor de Servicio del CCP)
								ot.FECHA_PROM AS ot_fecha_entrega,  		-- Fecha Prometida
								ot.CLIENTE AS ot_cliente,  					-- AQUI SERA EL CLIENTE
								ot.NRO_SERIE AS chasis_vin,					-- Numero de chasis
								ot.PLACA as ot_placa,							-- Nuemero de Placa
								ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,	-- fecha aprobacion de seguro * cia ceguros
								ot.OT_ESTADO AS ot_estado,						-- Estado de la ot
								ot.OT_PRE_ESTADO as ot_pre_estado,				-- Estado de la ot con respecto al presupuesto
								ot.MARCA AS ot_marca,         				-- Numero de OT generad
								ot.MODELO AS ot_modelo,         				-- Numero de OT generad
								ot.COLOR as ot_color,					-- Color								
								ot.VERSION AS ot_version,         				-- Numero de OT generad
								ot.DESCRIPCION AS ot_descripcion,         				-- Numero de OT generad
								ot.NOMBRE_ASESOR AS ot_nombre_asesor,         				-- Numero de OT generad
								ot.TIPO_OT AS ot_tipo_ot,         				-- Numero de OT generad
								ot.HORA_RECIBIDA AS ot_hora_recibida,         				-- Numero de OT generad
								ot.HORA_PROMETIDA AS ot_hora_prometida,         				-- Numero de OT generad
								ot.NRO_CONO AS ot_numero_cono        				-- Numero de OT generad
						FROM 
							dpcars75.Sistema_Planificador_CCP AS ot
						WHERE 
							$condicion_cars $ot_solicitudes $ot_ccphistorial
							AND ot.OT NOT IN (
									SELECT TOP $offset
											ot.OT
									FROM 
										dpcars75.Sistema_Planificador_CCP AS ot
									WHERE 
										$condicion_cars $ot_solicitudes $ot_ccphistorial
									-- GROUP BY ot.OT
									ORDER BY $sord
									)
						ORDER BY $sord"; 
		//DEBUG($presupuestos);
		//pr($presupuestos);
		$presupuestos = $this->query($presupuestos);
				
		$this->Talot = new Talot();
		$this->Ccptabhistorial= new Ccptabhistorial;
		$this->Ccpsolicitudservicio= new Ccpsolicitudservicio;
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['0']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['0']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['0']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['0']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['0']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['0']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['0']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['0']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['0']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['0']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['0']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_color'] = trim($presupuesto['0']['ot_color']);
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['0']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['0']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['0']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['0']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['0']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['0']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['0']['ot_numero_cono'];

			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($presupuesto['0']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;

			//recuperamos el comentario de los estados de deposito
			$comentarioDeposito = $this->Ccptabhistorial->getComentarioDeposito(trim($presupuesto['0']['ot_numero']));
			if(!empty($comentarioDeposito) && isset($comentarioDeposito)) $presupuestos[$key]['tooltip']['Talot']['comentarioDeposito']=$comentarioDeposito[0][0]['comentario'];
			else $presupuestos[$key]['tooltip']['Talot']['comentarioDeposito'] = null;
			//if(empty($comentarioDeposito[0]['Talot']) && isset($comentarioDeposito[0]['Talot'])) $presupuestos[$key]['tooltip']['Talot']['acta']=true;
			//else $presupuestos[$key]['tooltip']['acta'] = false;
			
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($presupuesto['0']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			
			unset($presupuestos[$key]['0']);
		} 
		
		return $presupuestos;
	}
		
	function getAsesoresCars($secproject_carscod){
		$asesores = "SELECT miOt.USUARIO_OT 
						FROM dpcars75.Sistema_Planificador_CCP as miOt 
						WHERE miOt.TALLER='$secproject_carscod' AND miOt.OT_ESTADO IN(0,2)
						GROUP BY miOt.USUARIO_OT ORDER BY miOt.USUARIO_OT";
		$asesores = $this->query($asesores);
		
		if(empty($asesores)) return $asesores;
		
		foreach($asesores as $asesor)
			$asesor_tmp[$asesor[0]['USUARIO_OT']] = $asesor[0]['USUARIO_OT'];	
		
		return $asesor_tmp;
	}
	
	function getOtPresupuesto($nro_cars){
		$otPresupuesto = "SELECT TOP 1
								ot.OT AS ot_numero,         				-- Numero de OT generad
								ot.PRESUPUESTO AS presupuesto_numero,		-- Numero de presupuesto
								ot.FECHA AS ot_fecha_creacion,         		-- Fecha que se gener? el la OT  *
								ot.USUARIO_OT AS ot_asesor,  				-- Usuario que genero la OT(Asesor de Servicio del CCP)
								ot.FECHA_PROM AS ot_fecha_entrega,  		-- Fecha Prometida
								ot.CLIENTE AS ot_cliente,  					-- AQUI SERA EL CLIENTE
								ot.NRO_SERIE AS chasis_vin,					-- Numero de chasis
								ot.PLACA as ot_placa,							-- Nuemero de Placa
								ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,	-- fecha aprobacion de seguro * cia ceguros
								ot.FECHA_PRESUP AS FECHA_PRESUP, 	-- Fecha que se envi� el presupuesto y fecha presupuesto listo
								ot.OT_ESTADO AS ot_estado,						-- Estado de la ot
								ot.OT_PRE_ESTADO as ot_pre_estado,				-- Estado de la ot con respecto al presupuesto
								ot.FECHA_APROB_ASEG as ot_fecha_aprobacion_compania -- fecha aprobacion compania
						FROM 
							dpcars75.Sistema_Planificador_CCP AS ot
						WHERE 
							ot.COMPANIA in ('DP', 'DC')
							AND ot.OT = $nro_cars -- AND ot.PRESUPUESTO is not null
						ORDER BY ot.FECHA_PRES asc";
		
		$otPresupuesto = $this->query($otPresupuesto);
		
		if(empty($otPresupuesto)) return $otPresupuesto;
		else{
			$otPresupuesto['0']['0']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($otPresupuesto['0']['0']['ot_fecha_entrega']);
			$otPresupuesto['0']['0']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($otPresupuesto['0']['0']['ot_fecha_creacion']);
			$otPresupuesto['0']['0']['FECHA_PRESUP'] = $this->fecha_Mysql_Php($otPresupuesto['0']['0']['FECHA_PRESUP']);
			$otPresupuesto['0']['0']['FECHA_PRESUP']= ($otPresupuesto['0']['0']['FECHA_PRESUP']=='31-12-1969' || $otPresupuesto['0']['0']['FECHA_PRESUP']=='01-01-1753')?'':$otPresupuesto['0']['0']['FECHA_PRESUP'];
			$otPresupuesto['0']['0']['ot_fecha_aprobacion_compania'] = $this->fecha_Mysql_Php($otPresupuesto['0']['0']['ot_fecha_aprobacion_compania']);
			$otPresupuesto['0']['0']['ot_fecha_aprobacion_compania']= ($otPresupuesto['0']['0']['ot_fecha_aprobacion_compania']=='31-12-1969' || $otPresupuesto['0']['0']['ot_fecha_aprobacion_compania']=='01-01-1753')?'':$otPresupuesto['0']['0']['ot_fecha_aprobacion_compania'];

		} 
		
		return $otPresupuesto['0']['0'];
	}
	
	function getTalotCarsProbador($data, $params, $login, $total = false){
		// VALORES NECESARIOS
		$secproject_carscod = $params['params']['carscod'];
		// RECUPERAMOS DATOS PARA LA PAGINACION
		$sord = empty($params['params']['order'])?'':$params['params']['order'];
		$limit = empty($params['params']['limit'])?'':$params['params']['limit'];
		$page = empty($params['params']['page'])?'':$params['params']['page'];
 		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = '';

		//RECUPERAMOS las ots que se encuentran en ccpestado
		$ot_ccphistorial='';
		if(!empty($data['estadootccp_id']) && isset($data['estadootccp_id'])){
			$this->Ccptabhistorial = new Ccptabhistorial;
			if(strlen($data['estadootccp_id']==6)){
				if(strpos('1', $data['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($data['estadootccp_id']==1){
					$ot_ccphistorial="";
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		//pr($ot_ccphistorial);
		//FILTROS DE CARS
		$condicion_cars=" ";
		$condicion_cars="ot.TALLER=".$secproject_carscod;
		
		if(!empty($data['placa']) && isset($data['placa'])){
			$placa_= " AND ot.PLACA like '%".$data['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;

		//if(!empty($data['asesor_id'])) $condicion_cars .= " AND ot.USUARIO_OT = '".trim($data['asesor_id'])."'";
		if(!empty($data['carsestado_id'])){
			$estadoCars = $data['carsestado_id']-1; 
			//$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'";
			$condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')";  
		}else  $condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		if(!empty($data['chasis'])) $condicion_cars .= " AND ot.NRO_SERIE like '%".$data['chasis']."%'";
		if(!empty($data['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$data['nrocars'].'%\'';
		
		// RECUPERAMOS EL TOTAL DE REGISTROS
		//debug("$condicion_cars $ot_solicitudes");
		$total_query = "SELECT COUNT(OT) FROM dpcars75.Sistema_Planificador_CCP as ot WHERE $condicion_cars $ot_solicitudes $ot_ccphistorial";

		$total_query = $this->query($total_query);
		
		if($total) return $total_query[0][0]['computed'];
		
		$count = $total_query[0][0]['computed'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		//paginacion en SQL 
		$offset=empty($page)?0:($page-1)*$limit;

		// RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		$presupuestos = "SELECT TOP $limit
								ot.OT AS ot_numero,         				-- Numero de OT generad
								ot.PRESUPUESTO AS presupuesto_numero,		-- Numero de presupuesto
								ot.FECHA AS ot_fecha_creacion,         		-- Fecha que se gener? el la OT  *
								ot.USUARIO_OT AS ot_asesor,  				-- Usuario que genero la OT(Asesor de Servicio del CCP)
								ot.FECHA_PROM AS ot_fecha_entrega,  		-- Fecha Prometida
								ot.CLIENTE AS ot_cliente,  					-- AQUI SERA EL CLIENTE
								ot.NRO_SERIE AS chasis_vin,					-- Numero de chasis
								ot.PLACA as ot_placa,							-- Nuemero de Placa
								ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,	-- fecha aprobacion de seguro * cia ceguros
								ot.OT_ESTADO AS ot_estado,						-- Estado de la ot
								ot.OT_PRE_ESTADO as ot_pre_estado,				-- Estado de la ot con respecto al presupuesto
								ot.MARCA AS ot_marca,         				-- Numero de OT generad
								ot.MODELO AS ot_modelo,         				-- Numero de OT generad
								ot.COLOR as ot_color,					-- Color								
								ot.VERSION AS ot_version,         				-- Numero de OT generad
								ot.DESCRIPCION AS ot_descripcion,         				-- Numero de OT generad
								ot.NOMBRE_ASESOR AS ot_nombre_asesor,         				-- Numero de OT generad
								ot.TIPO_OT AS ot_tipo_ot,         				-- Numero de OT generad
								ot.HORA_RECIBIDA AS ot_hora_recibida,         				-- Numero de OT generad
								ot.HORA_PROMETIDA AS ot_hora_prometida,         				-- Numero de OT generad
								ot.NRO_CONO AS ot_numero_cono        				-- Numero de OT generad
						FROM 
							dpcars75.Sistema_Planificador_CCP AS ot
						WHERE 
							$condicion_cars $ot_solicitudes $ot_ccphistorial
							AND ot.OT NOT IN (
									SELECT TOP $offset
											ot.OT
									FROM 
										dpcars75.Sistema_Planificador_CCP AS ot
									WHERE 
										$condicion_cars $ot_solicitudes $ot_ccphistorial
									-- GROUP BY ot.OT
									ORDER BY $sord
									)
						ORDER BY $sord"; 
		//DEBUG($presupuestos);
		//pr($presupuestos);
		$presupuestos = $this->query($presupuestos);
				
		$this->Talot = new Talot();
		$this->Ccptabhistorial= new Ccptabhistorial;
		$this->Ccpsolicitudservicio= new Ccpsolicitudservicio;
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['0']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['0']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['0']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['0']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['0']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['0']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['0']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['0']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['0']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['0']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['0']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_color'] = trim($presupuesto['0']['ot_color']);
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['0']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['0']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['0']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['0']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['0']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['0']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['0']['ot_numero_cono'];

			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($presupuesto['0']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;

			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($presupuesto['0']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			
			unset($presupuestos[$key]['0']);
		} 
		
		return $presupuestos;
	}
	
	function getTalotCarsLavador($data, $params, $login, $total = false){
		// VALORES NECESARIOS
		$secproject_carscod = $params['params']['carscod'];
		// RECUPERAMOS DATOS PARA LA PAGINACION
		$sord = empty($params['params']['order'])?'':$params['params']['order'];
		$limit = empty($params['params']['limit'])?'':$params['params']['limit'];
		$page = empty($params['params']['page'])?'':$params['params']['page'];
 		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = '';

		//RECUPERAMOS las ots que se encuentran en ccpestado
		$ot_ccphistorial='';
		if(!empty($data['estadootccp_id']) && isset($data['estadootccp_id'])){
			$this->Ccptabhistorial = new Ccptabhistorial;
			if(strlen($data['estadootccp_id']==3)){
				if(strpos('1', $data['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($data['estadootccp_id']==1){
					$ot_ccphistorial="";
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		//pr($ot_ccphistorial);
		//FILTROS DE CARS
		$condicion_cars=" ";
		$condicion_cars="ot.TALLER=".$secproject_carscod;
		
		if(!empty($data['placa']) && isset($data['placa'])){
			$placa_= " AND ot.PLACA like '%".$data['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;

		//if(!empty($data['asesor_id'])) $condicion_cars .= " AND ot.USUARIO_OT = '".trim($data['asesor_id'])."'";
		if(!empty($data['carsestado_id'])){
			$estadoCars = $data['carsestado_id']-1; 
			//$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'";
			$condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		if(!empty($data['chasis'])) $condicion_cars .= " AND ot.NRO_SERIE like '%".$data['chasis']."%'";
		if(!empty($data['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$data['nrocars'].'%\'';
		
		// RECUPERAMOS EL TOTAL DE REGISTROS
		//debug("$condicion_cars $ot_solicitudes");
		$total_query = "SELECT COUNT(OT) FROM dpcars75.Sistema_Planificador_CCP as ot WHERE $condicion_cars $ot_solicitudes $ot_ccphistorial";

		$total_query = $this->query($total_query);
		
		if($total) return $total_query[0][0]['computed'];
		
		$count = $total_query[0][0]['computed'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		//paginacion en SQL 
		$offset=empty($page)?0:($page-1)*$limit;

		// RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		$presupuestos = "SELECT TOP $limit
								ot.OT AS ot_numero,         				-- Numero de OT generad
								ot.PRESUPUESTO AS presupuesto_numero,		-- Numero de presupuesto
								ot.FECHA AS ot_fecha_creacion,         		-- Fecha que se gener? el la OT  *
								ot.USUARIO_OT AS ot_asesor,  				-- Usuario que genero la OT(Asesor de Servicio del CCP)
								ot.FECHA_PROM AS ot_fecha_entrega,  		-- Fecha Prometida
								ot.CLIENTE AS ot_cliente,  					-- AQUI SERA EL CLIENTE
								ot.NRO_SERIE AS chasis_vin,					-- Numero de chasis
								ot.PLACA as ot_placa,							-- Nuemero de Placa
								ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,	-- fecha aprobacion de seguro * cia ceguros
								ot.OT_ESTADO AS ot_estado,						-- Estado de la ot
								ot.OT_PRE_ESTADO as ot_pre_estado,				-- Estado de la ot con respecto al presupuesto
								ot.MARCA AS ot_marca,         				-- Numero de OT generad
								ot.MODELO AS ot_modelo,         				-- Numero de OT generad
								ot.COLOR as ot_color,					-- Color								
								ot.VERSION AS ot_version,         				-- Numero de OT generad
								ot.DESCRIPCION AS ot_descripcion,         				-- Numero de OT generad
								ot.NOMBRE_ASESOR AS ot_nombre_asesor,         				-- Numero de OT generad
								ot.TIPO_OT AS ot_tipo_ot,         				-- Numero de OT generad
								ot.HORA_RECIBIDA AS ot_hora_recibida,         				-- Numero de OT generad
								ot.HORA_PROMETIDA AS ot_hora_prometida,         				-- Numero de OT generad
								ot.NRO_CONO AS ot_numero_cono        				-- Numero de OT generad
						FROM 
							dpcars75.Sistema_Planificador_CCP AS ot
						WHERE 
							$condicion_cars $ot_solicitudes $ot_ccphistorial
							AND ot.OT NOT IN (
									SELECT TOP $offset
											ot.OT
									FROM 
										dpcars75.Sistema_Planificador_CCP AS ot
									WHERE 
										$condicion_cars $ot_solicitudes $ot_ccphistorial
									-- GROUP BY ot.OT
									ORDER BY $sord
									)
						ORDER BY $sord"; 
		//DEBUG($presupuestos);
		//pr($presupuestos);
		$presupuestos = $this->query($presupuestos);
				
		$this->Talot = new Talot();
		$this->Ccptabhistorial= new Ccptabhistorial;
		$this->Ccpsolicitudservicio= new Ccpsolicitudservicio;
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['0']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['0']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['0']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['0']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['0']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['0']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['0']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['0']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['0']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['0']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['0']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_color'] = trim($presupuesto['0']['ot_color']);
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['0']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['0']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['0']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['0']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['0']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['0']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['0']['ot_numero_cono'];

			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($presupuesto['0']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;

			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($presupuesto['0']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			
			unset($presupuestos[$key]['0']);
		} 
		
		return $presupuestos;
	}
	function getTalotCarsAsistente($data, $params, $login, $total = false){
		// VALORES NECESARIOS
		$secproject_carscod = $params['params']['carscod'];
		// RECUPERAMOS DATOS PARA LA PAGINACION
		$sord = empty($params['params']['order'])?'':$params['params']['order'];
		$limit = empty($params['params']['limit'])?'':$params['params']['limit'];
		$page = empty($params['params']['page'])?'':$params['params']['page'];
 		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = '';

		//RECUPERAMOS las ots que se encuentran en ccpestado
		$ot_ccphistorial='';
		if(!empty($data['estadootccp_id']) && isset($data['estadootccp_id'])){
			$this->Ccptabhistorial = new Ccptabhistorial;
			if(strlen($data['estadootccp_id']==11)){
				if(strpos('1', $data['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($data['estadootccp_id']==1){
					$ot_ccphistorial="";
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		//FILTROS DE CARS
		$condicion_cars=" ";
		$condicion_cars="ot.TALLER=".$secproject_carscod;
		
		if(!empty($data['placa']) && isset($data['placa'])){
			$placa_= " AND ot.PLACA like '%".$data['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;

		//if(!empty($data['asesor_id'])) $condicion_cars .= " AND ot.USUARIO_OT = '".trim($data['asesor_id'])."'";
		if(!empty($data['carsestado_id'])){
			$estadoCars = $data['carsestado_id']-1; 
			//$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'";
			$condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')";  
		}else  $condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		if(!empty($data['chasis'])) $condicion_cars .= " AND ot.NRO_SERIE like '%".$data['chasis']."%'";
		if(!empty($data['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$data['nrocars'].'%\'';
		
		// RECUPERAMOS EL TOTAL DE REGISTROS
		//debug("$condicion_cars $ot_solicitudes");
		$total_query = "SELECT COUNT(OT) FROM dpcars75.Sistema_Planificador_CCP as ot WHERE $condicion_cars $ot_solicitudes $ot_ccphistorial";

		$total_query = $this->query($total_query);
		
		if($total) return $total_query[0][0]['computed'];
		
		$count = $total_query[0][0]['computed'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		//paginacion en SQL 
		$offset=empty($page)?0:($page-1)*$limit;

		// RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		$presupuestos = "SELECT TOP $limit
								ot.OT AS ot_numero,         				-- Numero de OT generad
								ot.PRESUPUESTO AS presupuesto_numero,		-- Numero de presupuesto
								ot.FECHA AS ot_fecha_creacion,         		-- Fecha que se gener? el la OT  *
								ot.USUARIO_OT AS ot_asesor,  				-- Usuario que genero la OT(Asesor de Servicio del CCP)
								ot.FECHA_PROM AS ot_fecha_entrega,  		-- Fecha Prometida
								ot.CLIENTE AS ot_cliente,  					-- AQUI SERA EL CLIENTE
								ot.NRO_SERIE AS chasis_vin,					-- Numero de chasis
								ot.PLACA as ot_placa,							-- Nuemero de Placa
								ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,	-- fecha aprobacion de seguro * cia ceguros
								ot.OT_ESTADO AS ot_estado,						-- Estado de la ot
								ot.OT_PRE_ESTADO as ot_pre_estado,				-- Estado de la ot con respecto al presupuesto
								ot.MARCA AS ot_marca,         				-- Numero de OT generad
								ot.MODELO AS ot_modelo,         				-- Numero de OT generad
								ot.COLOR as ot_color,					-- Color								
								ot.VERSION AS ot_version,         				-- Numero de OT generad
								ot.DESCRIPCION AS ot_descripcion,         				-- Numero de OT generad
								ot.NOMBRE_ASESOR AS ot_nombre_asesor,         				-- Numero de OT generad
								ot.TIPO_OT AS ot_tipo_ot,         				-- Numero de OT generad
								ot.HORA_RECIBIDA AS ot_hora_recibida,         				-- Numero de OT generad
								ot.HORA_PROMETIDA AS ot_hora_prometida,         				-- Numero de OT generad
								ot.NRO_CONO AS ot_numero_cono        				-- Numero de OT generad
						FROM 
							dpcars75.Sistema_Planificador_CCP AS ot
						WHERE 
							$condicion_cars $ot_solicitudes $ot_ccphistorial
							AND ot.OT NOT IN (
									SELECT TOP $offset
											ot.OT
									FROM 
										dpcars75.Sistema_Planificador_CCP AS ot
									WHERE 
										$condicion_cars $ot_solicitudes $ot_ccphistorial
									-- GROUP BY ot.OT
									ORDER BY $sord
									)
						ORDER BY $sord"; 
		//DEBUG($presupuestos);
		$presupuestos = $this->query($presupuestos);
				
		$this->Talot = new Talot();
		$this->Ccptabhistorial= new Ccptabhistorial;
		$this->Ccptabhistorialdocumento= new Ccptabhistorialdocumento;
		$this->Ccpsolicitudservicio= new Ccpsolicitudservicio;
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['0']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['0']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['0']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['0']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['0']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['0']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['0']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['0']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['0']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['0']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['0']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_color'] = trim($presupuesto['0']['ot_color']);
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['0']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['0']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['0']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['0']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['0']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['0']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['0']['ot_numero_cono'];

			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($presupuesto['0']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;

			
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($presupuesto['0']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
					$estadodocumento=$this->Ccptabhistorialdocumento->obtenerEstadoCcpPotNumeroOt(trim($presupuesto['0']['ot_numero']));
					if(!empty($estadodocumento) && isset($estadodocumento)){
						$presupuestos[$key]['Ccptabestadodocumento'] = $estadodocumento[0]['Ccptabestadodocumento'];
					}
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			
			unset($presupuestos[$key]['0']);
		} 
		
		return $presupuestos;
	}
	
	function ValidarOT($ot_numero){
		$total_query = "SELECT ot.OT FROM dpcars75.Sistema_Planificador_CCP as ot WITH (nolock) WHERE ot.OT='".trim($ot_numero)."'";
		$total_query = $this->query($total_query);
		return (!empty($total_query) && isset($total_query))?true:false;
	}
	
	function getReporte($data, $params=null, $login=null, $total = false){
		//$data['nrocars'] = '401895';
		//VALORES NECESARIOS
		$secproject_carscod = $params['params']['carscod'];
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = " ";
		//RECUPERAMOS LAS OT QUE SE ENCUENTRAN EN CCPTABHISTORIAL
		$ot_ccphistorial = " ";
		//FILTROS DE CARS
		$condicion_cars = " ";

		if(!empty($data['carsestado_id'])){
			$estadoCars = $data['carsestado_id']-1; 
			//$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'";
			$condicion_cars .= " AND ot.OT_ESTADO IN ('0','1','2')";
		}else  $condicion_cars .= " AND ot.OT_ESTADO IN ('0','1','2')";
		
		//SI NO TIENE DATO EN NINGUNO DE LOS CAMPOS RETORNA VACIO 
		if((!empty($data['nrocars']) && isset($data['nrocars'])) || (!empty($data['placa']) && isset($data['placa']))){
			if(!empty($data['nrocars'])) $condicion_cars .= " AND ot.OT like '%".$data['nrocars']."%'";
			//if(!empty($data['nrocars'])) $condicion_cars .= " AND ot.OT = '".$data['nrocars']."'";
			if(!empty($data['placa'])) $condicion_cars .= " AND ot.PLACA like '%".$data['placa']."%'";
			//if(!empty($data['placa'])) $condicion_cars .= " AND ot.PLACA = '".$data['placa']."'";
		}else{
			return array();	
		}

		
		// RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		$presupuestos = "SELECT TOP 1
						ot.OT as ot_numero,												-- numero del ot
						ot.FECHA_PRESUP AS ot_fecha_presupuesto_listo, 					-- fecha presupuesto listo
						ot.OT_ESTADO AS ot_estado,										-- Estado de la ot
						ot.OT_PRE_ESTADO as ot_pre_estado,								-- Estado de la ot con respecto al presupuesto
						ot.FECHA AS ot_fecha_creacion,         							-- Fecha que se gener? el la OT  *
						ot.NOMBRE_ASESOR as ot_nombre_asesor,							-- Nombre del asesor
						ot.CLIENTE as ot_cliente,										-- Cliente de la OT
						ot.USUARIO_OT AS ot_asesor,  									-- Usuario que genero la OT(Asesor de Servicio del CCP)
						ot.PLACA as ot_placa,											-- Nuemero de Placa
						ot.FECHA_APROB_ASEG as ot_fecha_aprobacion_compania				-- fecha aprobacion compania
						FROM dpcars75.Sistema_Planificador_CCP as ot WITH (nolock) WHERE 1=1
						$condicion_cars $ot_solicitudes $ot_ccphistorial
						ORDER BY ot.FECHA DESC"; 
				
		//pr($presupuestos);
		$presupuestos = $this->query($presupuestos);
		
		if(empty($presupuestos)) return array();
		$this->Talot = new Talot();
		$this->Ccptabhistorial= new Ccptabhistorial;
		$this->Ccpsolicitudservicio= new Ccpsolicitudservicio;
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['0']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = trim($presupuesto['0']['ot_nombre_asesor']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['0']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['0']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['0']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['0']['ot_asesor']);
			$presupuestos[$key]['Talot']['ot_fecha_presupuesto_listo'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_presupuesto_listo']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_fecha_aprobacion_compania'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_aprobacion_compania']);
			
			//EL PRIMER ESTADO
			$keyestados=0;
			$presupuestos[$key]['Estados'][$keyestados]=array(
															'id'=>1,
															'descripcion'=>'OT Abierta',
															'responsable'=>trim($presupuesto['0']['ot_nombre_asesor']),
															'fechaInicio'=>$presupuestos[$key]['Talot']['ot_fecha_creacion'],
															'fechaFin'=>$presupuestos[$key]['Talot']['ot_fecha_creacion'],
															'duracion'=>'00:00',
															'proceso'=>$this->Ccptabhistorial->getProcesosPorCcpEstado(1),
															'estado'=>'Finalizado');
			
			$solicitudServicio=$this->Ccptabhistorial->getReporte(trim($presupuesto['0']['ot_numero']));
			
			//PARA SIMPLIFICAR OPERACIONES ASIGNAREMOS LA FECHA DE INICIO PARA ERL PROXIMO PROCESO COMO LA FECHA DE FIN DEL ANTERIOR PROCESO
			foreach($solicitudServicio as $id => $item){
				if(!empty($solicitudServicio[$id-1]['Ccptabhistoriale']['fecha']) && isset($solicitudServicio[$id-1]['Ccptabhistoriale']['fecha'])){
					$solicitudServicio[$id]['Ccptabhistoriale']['fechaInicio']=$solicitudServicio[$id-1]['Ccptabhistoriale']['fecha'];	
				}else{
					$solicitudServicio[$id]['Ccptabhistoriale']['fechaInicio']=$presupuestos[$key]['Talot']['ot_fecha_creacion'];
				}
			}

			$estado = array();
			$ampliacion = false;
			//SE AGREGAN TODAS LAS SOLICITUDES DE LA OT
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				//ANTES DE RECORRER EL ARRAY VERIFICAMOS SI TIENE EL ESTADO 2 AL INICIO
				/*
				 * RTINEO: MODIFICADO
				 * FECHA: 10/05/2012
				 */
				//anterior
				//$idsolicitud = $this->Ccptabhistorial->getSolitudServicioId($solicitudServicio[0]['Ccptabhistoriale']['ot_numero']);
				//nuevo
				$idsolicitud = $this->Ccptabhistorial->getPrimerSolitudServicioIdByNroOt($solicitudServicio[0]['Ccptabhistoriale']['ot_numero']);
				$datosSolicitud = $this->Ccptabhistorial->getSolicitudServicioPorSolicitud($idsolicitud);
				//pr($datosSolicitud);
				if($solicitudServicio[0]['Ccptabhistoriale']['ccptabestado_id']==2){
					$keyestados += 1;
					$estado['id'] = $solicitudServicio[0]['Ccptabhistoriale']['ccptabestado_id'];
					$estado['descripcion'] = $solicitudServicio[0]['Ccptabestado']['descripcion'];
					$estado['responsable'] = $solicitudServicio[0][0]['nombreCompleto'];
					$estado['fechaInicio'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']);
					$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($solicitudServicio[0]['Ccptabhistoriale']['fecha']);
					$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
					$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
					$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
					$presupuestos[$key]['Estados'][$keyestados]=$estado;
					unset($solicitudServicio[0]);
				}
				
				$keyestados += 1;
				$estado['id'] = 16;//temporal
				$estado['descripcion'] = 'Presupuesto Listo';
				$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
				$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['ot_fecha_creacion']);
				$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
				$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
				$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
				$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
				$presupuestos[$key]['Estados'][$keyestados]=$estado;
							
				$keyestados += 1;
				$estado['id'] = 17;//temporal
				$estado['descripcion'] = 'Presupuesto Aprobado Compania de Seguro';
				$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
				$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
				$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
				$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
				$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
				$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
				$presupuestos[$key]['Estados'][$keyestados]=$estado;				
				unset($datosSolicitud);
				$i=0;
				foreach($solicitudServicio as $id => $item){
					// SI HUBO AMPLIACION EN EL REGISTRO ANTERIOR Y SE FUE DEPOSITO
					if($ampliacion){
						if($item['Ccptabhistoriale']['ccptabestado_id'] == 2){
							//LLENAMOS CON ESTADO DEPOSITO
							$keyestados += 1;
							$estado['id'] = $item['Ccptabhistoriale']['ccptabestado_id'];
							$estado['descripcion'] = $item['Ccptabestado']['descripcion'];
							$estado['responsable'] = $item[0]['nombreCompleto'];
							$estado['fechaInicio'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']);
							$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
							$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
							$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
							$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
							$presupuestos[$key]['Estados'][$keyestados]=$estado;
							
							//LLENAMOS LOS DATOS PRESUPUESTO LISTO Y PRESUPUESTO COMPANIA DE LA SOLICITUD DE SERVICIO
							$ccpSiguiente = $this->Ccptabhistorial->getSiguienteCcpTabHistorial($item['Ccptabhistoriale']['id']);
							if(!empty($ccpSiguiente) && isset($ccpSiguiente)){
								$datosSolicitud = $this->Ccptabhistorial->getSolicitudServicioPorSolicitud($ccpSiguiente['Ccptabhistoriale']['ccpsolicitudservicio_id']);
								$keyestados += 1;
								$estado['id'] = 16;//temporal
								$estado['descripcion'] = 'Presupuesto Listo';
								$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['ot_fecha_creacion']);
								$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
								$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;
								
								$keyestados += 1;
								$estado['id'] = 17;//temporal
								$estado['descripcion'] = 'Presupuesto Aprobado Compania de Seguro';
								$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
								$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
								$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;
							}
							
						}elseif($item['Ccptabhistoriale']['ccptabestado_id'] == 3){
							//LLENAMOS LOS DATOS PRESUPUESTO LISTO Y PRESUPUESTO COMPANIA DE LA SOLICITUD DE SERVICIO
							$datosSolicitud = $this->Ccptabhistorial->getSolicitudServicioPorSolicitud($solicitudservicio_id);
							$keyestados += 1;
							$estado['id'] = 16;//TEMPORAL
							$estado['descripcion'] = 'Presupuesto Listo';
							$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
							$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['ot_fecha_creacion']);
							$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
							$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
							$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
							$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
							$presupuestos[$key]['Estados'][$keyestados]=$estado;
							
							$keyestados += 1;
							$estado['id'] = 17;//TEMPORAL
							$estado['descripcion'] = 'Presupuesto Aprobado Compania de Seguro';
							$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
							$estado['fechaInicio'] =$this->fecha_Mysql_Php_Hora( $datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
							$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
							$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
							$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
							$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
							$presupuestos[$key]['Estados'][$keyestados]=$estado;
							
							$keyestados += 1;
							$estado['id'] = $item['Ccptabhistoriale']['ccptabestado_id'];
							$estado['descripcion'] = $item['Ccptabestado']['descripcion'];
							$estado['responsable'] = $item[0]['nombreCompleto'];
							$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
							$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
							$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
							$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
							$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
							$presupuestos[$key]['Estados'][$keyestados]=$estado;
							
							//ACA ABAJO LLENAR LOS ESTADOS OBSERVADO Y RECHAZADO
							if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado'])){
								$keyestados += 1;
								$estado['id'] = 18;//temporal para solicitud de servicio aprobada
								$estado['descripcion'] = 'Solicitud Servicio Aprobada';
								$estado['responsable'] = $item[0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
								$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
								$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;								
							}
							if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado'])){
								$keyestados += 1;
								$estado['id'] = 19;//temporal para solicitud de servicio habilitada
								$estado['descripcion'] = 'Solicitud Servicio Habilitada';
								$estado['responsable'] = $item[0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
								$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']);
								$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;								
							}
							if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado'])){
								$keyestados += 1;
								$estado['id'] = 20;//temporal para solicitud de servicio rechazada
								$estado['descripcion'] = 'Solicitud Servicio Rechazada';
								$estado['responsable'] = $item[0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
								$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']);
								$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;								
							}
						}
					}else{
//-.-
						if($i>0){
							if($item['Ccptabhistoriale']['ccptabestado_id'] == 3){
								//LLENAMOS LOS DATOS PRESUPUESTO LISTO Y PRESUPUESTO COMPANIA DE LA SOLICITUD DE SERVICIO
								$solicitudservicio_id = $item['Ccptabhistoriale']['ccpsolicitudservicio_id'];
								$datosSolicitud = $this->Ccptabhistorial->getSolicitudServicioPorSolicitud($solicitudservicio_id);
								//pr($datosSolicitud);
								$keyestados += 1;
								$estado['id'] = 16;//TEMPORAL
								$estado['descripcion'] = 'Presupuesto Listo';
								$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['ot_fecha_creacion']);
								$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
								$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;
								
								$keyestados += 1;
								$estado['id'] = 17;//TEMPORAL
								$estado['descripcion'] = 'Presupuesto Aprobado Compania de Seguro';
								$estado['responsable'] = $datosSolicitud[0][0]['nombreCompleto'];
								$estado['fechaInicio'] =$this->fecha_Mysql_Php_Hora( $datosSolicitud[0]['Ccpsolicitudservicio']['fechapresupuestolisto']);
								$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
								$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;
								
								$keyestados += 1;
								$estado['id'] = $item['Ccptabhistoriale']['ccptabestado_id'];
								$estado['descripcion'] = $item['Ccptabestado']['descripcion'];
								$estado['responsable'] = $item[0]['nombreCompleto'];
								$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fechaaprobacioncompania']);
								$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
								$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
								$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
								$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
								$presupuestos[$key]['Estados'][$keyestados]=$estado;
								
								//ACA ABAJO LLENAR LOS ESTADOS OBSERVADO Y RECHAZADO
								if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado'])){
									$keyestados += 1;
									$estado['id'] = 18;//temporal para solicitud de servicio aprobada
									$estado['descripcion'] = 'Solicitud Servicio Observada';
									$estado['responsable'] = $item[0]['nombreCompleto'];
									$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
									$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
									$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
									$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
									$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
									$presupuestos[$key]['Estados'][$keyestados]=$estado;								
								}
								if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado'])){
									$keyestados += 1;
									$estado['id'] = 19;//temporal para solicitud de servicio habilitada
									$estado['descripcion'] = 'Solicitud Servicio Habilitada';
									$estado['responsable'] = $item[0]['nombreCompleto'];
									$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
									$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']);
									$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
									$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
									$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
									$presupuestos[$key]['Estados'][$keyestados]=$estado;								
								}
								if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado'])){
									$keyestados += 1;
									$estado['id'] = 20;//temporal para solicitud de servicio rechazada
									$estado['descripcion'] = 'Solicitud Servicio Rechazada';
									$estado['responsable'] = $item[0]['nombreCompleto'];
									$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
									$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']);
									$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
									$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
									$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
									$presupuestos[$key]['Estados'][$keyestados]=$estado;								
								}
							}else{
									$keyestados += 1;
									$estado['id'] = $item['Ccptabhistoriale']['ccptabestado_id'];
									$estado['descripcion'] = $item['Ccptabestado']['descripcion'];
									$estado['responsable'] = $item[0]['nombreCompleto'];
									$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
									$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fechaInicio']);
									$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
									$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
									$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
									$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
									$presupuestos[$key]['Estados'][$keyestados]=$estado;
									
								}						
						}else{
							if($i==0){
								if($item['Ccptabhistoriale']['ccptabestado_id'] == 3){
									//LLENAMOS LOS DATOS PRESUPUESTO LISTO Y PRESUPUESTO COMPANIA DE LA SOLICITUD DE SERVICIO
									$solicitudservicio_id = $item['Ccptabhistoriale']['ccpsolicitudservicio_id'];
									$datosSolicitud = $this->Ccptabhistorial->getSolicitudServicioPorSolicitud($solicitudservicio_id);
									//ACA ABAJO LLENAR LOS ESTADOS OBSERVADO Y RECHAZADO
									if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado'])){
										$keyestados += 1;
										$estado['id'] = 18;//temporal para solicitud de servicio aprobada
										$estado['descripcion'] = 'Solicitud de Servicio Observada';
										$estado['responsable'] = $item[0]['nombreCompleto'];
										$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
										$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
										$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
										$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
										$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
										$presupuestos[$key]['Estados'][$keyestados]=$estado;								
									}
									if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado'])){
										$keyestados += 1;
										$estado['id'] = 19;//temporal para solicitud de servicio habilitada
										$estado['descripcion'] = 'Solicitud Servicio Habilitada';
										$estado['responsable'] = $item[0]['nombreCompleto'];
										$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_observado']);
										$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_habilitado']);
										$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
										$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
										$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
										$presupuestos[$key]['Estados'][$keyestados]=$estado;								
									}
									if(!empty($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']) && isset($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado'])){
										$keyestados += 1;
										$estado['id'] = 20;//temporal para solicitud de servicio rechazada
										$estado['descripcion'] = 'Solicitud Servicio Rechazada';
										$estado['responsable'] = $item[0]['nombreCompleto'];
										$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
										$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($datosSolicitud[0]['Ccpsolicitudservicio']['fecha_rechazado']);
										$estado['duracion'] =  $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
										$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
										$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
										$presupuestos[$key]['Estados'][$keyestados]=$estado;								
									}										
								}
							}
							$keyestados += 1;
							$estado['id'] = $item['Ccptabhistoriale']['ccptabestado_id'];
							$estado['descripcion'] = $item['Ccptabestado']['descripcion'];
							$estado['responsable'] = $item[0]['nombreCompleto'];
							$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
							$estado['fechaInicio'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fechaInicio']);
							$estado['fechaFin'] = $this->fecha_Mysql_Php_Hora($item['Ccptabhistoriale']['fecha']);
							$estado['duracion'] = $this->getDiferenciaDeFechasEnMinutos($estado['fechaInicio'],$estado['fechaFin']);
							$estado['proceso'] = $this->Ccptabhistorial->getProcesosPorCcpEstado($estado['id']);
							$estado['estado'] = $this->Ccptabhistorial->getEstadoProceso($estado['fechaInicio'],$estado['fechaFin']);
							$presupuestos[$key]['Estados'][$keyestados]=$estado;
						}
					}

					if($item['Ccptabhistoriale']['ccptabestado_id']==13){
						$ampliacion = true;
						$solicitudservicio_id=$item['Ccptabhistoriale']['ccpsolicitudservicio_id'];
					}else $ampliacion = false;
					
					unset($estado);	
					$i++;
				}
			}
			unset($presupuestos[$key]['0']);
			
		} 
		return $presupuestos;		
		//----
	}
	
	function getTalotCarsGerente($data, $params, $login, $total = false){
		// VALORES NECESARIOS
		
		$secproject_carscod = $params['params']['carscod'];
		// RECUPERAMOS DATOS PARA LA PAGINACION
		$sord = empty($params['params']['order'])?'':$params['params']['order'];
		$limit = empty($params['params']['limit'])?'':$params['params']['limit'];
		$page = empty($params['params']['page'])?'':$params['params']['page'];
 		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$condicionSolicitud = "";
		$condicionSolicitud = 'Ccpsolicitudservicio.ot_numero is not null';
		$fechaSol = $this->condicionBuscadorFecha(empty($data['fechaIni'])?'':$data['fechaIni'], empty($data['fechaIni'])?'':$data['fechaFin'], $campoModelo ='Ccpsolicitudservicio.fecha_emitido');
		if(!empty($fechaSol)) $condicionSolicitud .= $fechaSol;
		if(!empty($data['ccptiposervicio_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccptiposervicio_id = '.$data['ccptiposervicio_id'];
		if(!empty($data['ccpsolicitudservicioestado_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccpsolicitudservicioestado_id = '.$data['ccpsolicitudservicioestado_id'];
	
		//RECUPERO LOS OT DE LAS SOLICITUDES
		$ot_solicitudes = '';
		
		if($condicionSolicitud != 'Ccpsolicitudservicio.ot_numero is not null'){
			$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
			$ot_solicitudes = $this->Ccpsolicitudservicio->generateList($condicionSolicitud,null,null,"{n}.Ccpsolicitudservicio.id","{n}.Ccpsolicitudservicio.ot_numero");
			$ot_solicitudes = empty($ot_solicitudes)?'AND ot.OT IN(\'0\')':'AND ot.OT IN(\''.implode('\',\'', $ot_solicitudes).'\')';
			
		}
		//RECUPERAMOS las ots que se encuentran en estado
		$ot_ccphistorial='';
		if(!empty($data['estadootccp_id']) && isset($data['estadootccp_id'])){
			$this->Ccptabhistorial = new Ccptabhistorial;
			if(strlen($data['estadootccp_id']==33)){
				if(strpos('1', $data['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($data['estadootccp_id']==1){
					$ot_ccphistorial="";
				}else{				
					$ot_ccphistorial = $this->Ccptabhistorial->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		//FILTROS DE CARS
		$condicion_cars=" ";
		$condicion_cars="ot.TALLER=".$secproject_carscod;
		if(!empty($data['placa']) && isset($data['placa'])){
			$placa_= " AND ot.PLACA like '%".$data['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;
		$fechaCars = $this->condicionBuscadorFecha(empty($data['fechaIniOt'])?'':$data['fechaIniOt'], empty($data['fechaFinOt'])?'':$data['fechaFinOt'], $campoModelo ='ot.FECHA');
		//pr(array('aca'=>$fechaCars));
		if(!empty($fechaCars)) $condicion_cars .= $fechaCars;
		if(!empty($data['asesor_id'])) $condicion_cars .= " AND ot.USUARIO_OT = '".trim($data['asesor_id'])."'";
		if(!empty($data['carsestado_id'])){
			$estadoCars = $data['carsestado_id']-1; 
			$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		if(!empty($data['preestado_id'])) $condicion_cars .= ' AND ot.OT_PRE_ESTADO = '.$data['preestado_id'];
		if(!empty($data['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$data['nrocars'].'%\'';
		
		// RECUPERAMOS EL TOTAL DE REGISTROS
		//debug("$condicion_cars $ot_solicitudes");
		$total_query = "SELECT COUNT(OT) FROM dpcars75.Sistema_Planificador_CCP as ot WHERE $condicion_cars $ot_solicitudes $ot_ccphistorial";
		$total_query = $this->query($total_query);
		
		if($total) return $total_query[0][0]['computed'];
		
		$count = $total_query[0][0]['computed'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		//paginacion en SQL 
		$offset=empty($page)?0:($page-1)*$limit;

		//debug($ot_solicitudes);
		//debug($condicion_cars);
		//RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		$presupuestos = "SELECT TOP $limit
								ot.OT AS ot_numero,         				-- Numero de OT generad
								ot.MARCA AS ot_marca,         				-- Numero de OT generad
								ot.MODELO AS ot_modelo,         				-- Numero de OT generad
								ot.VERSION AS ot_version,         				-- Numero de OT generad
								ot.DESCRIPCION AS ot_descripcion,         				-- Numero de OT generad
								ot.NOMBRE_ASESOR AS ot_nombre_asesor,         				-- Numero de OT generad
								ot.TIPO_OT AS ot_tipo_ot,         				-- Numero de OT generad
								ot.HORA_RECIBIDA AS ot_hora_recibida,         				-- Numero de OT generad
								ot.HORA_PROMETIDA AS ot_hora_prometida,         				-- Numero de OT generad
								ot.NRO_CONO AS ot_numero_cono,         				-- Numero de OT generad
								ot.PRESUPUESTO AS presupuesto_numero,		-- Numero de presupuesto
								ot.FECHA AS ot_fecha_creacion,         		-- Fecha que se gener? el la OT  *
								ot.USUARIO_OT AS ot_asesor,  				-- Usuario que genero la OT(Asesor de Servicio del CCP)
								ot.FECHA_PROM AS ot_fecha_entrega,  		-- Fecha Prometida
								ot.CLIENTE AS ot_cliente,  					-- AQUI SERA EL CLIENTE
								ot.NRO_SERIE AS chasis_vin,					-- Numero de chasis
								ot.PLACA as ot_placa,							-- Nuemero de Placa
								ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,	-- fecha aprobacion de seguro * cia ceguros
								ot.OT_ESTADO AS ot_estado,						-- Estado de la ot
								ot.OT_PRE_ESTADO as ot_pre_estado				-- Estado de la ot con respecto al presupuesto
						FROM 
							dpcars75.Sistema_Planificador_CCP AS ot
						WHERE 
							$condicion_cars $ot_solicitudes $ot_ccphistorial
							AND ot.OT NOT IN (
									SELECT TOP $offset
											ot.OT
									FROM 
										dpcars75.Sistema_Planificador_CCP AS ot
									WHERE 
										$condicion_cars $ot_solicitudes $ot_ccphistorial
									-- GROUP BY ot.OT
									ORDER BY $sord
									)
						ORDER BY $sord"; 
		//DEBUG($presupuestos);
		//pr($presupuestos);
		$presupuestos = $this->query($presupuestos);
				
		$this->Talot = new Talot();
		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		$this->Ccptabhistorial= new Ccptabhistorial;
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['0']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['0']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['0']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['0']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['0']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['0']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['0']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['0']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['0']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['0']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['0']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['0']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['0']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['0']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['0']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['0']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['0']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['0']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['0']['ot_numero_cono'];
			
			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($presupuesto['0']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;
			
			//obtenemos el color de la ot
			$presupuestos[$key]['Talot']['color']=$this->Ccptabhistorial->obtenerDiferenciaDias(date('d-m-Y'),$presupuestos[$key]['Talot']['ot_fecha_creacion']);
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($presupuesto['0']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			
			unset($presupuestos[$key]['0']);
		}
		
		return $presupuestos;
	}
	
	/*
	 * RTINEO : obtiene el tipo de mantenimiento en base a la ot, Necesaria para los reportes de taller de servicios
	 * FECHA : 08/05/2012
	 */
	function getTipoMantenimientoByOt($ot){
		$sql = "SELECT otServicio FROM TallerSeguimientoPostVenta WHERE OT = '$ot'";
		$tipomantenimiento = $this->query($sql);
		return (!empty($tipomantenimiento) && isset($tipomantenimiento))?$tipomantenimiento[0][0]['otServicio']:array();
	}
	
	/**
	 * os códigos, descripción y cantidad de horas de mano de obra de reparaciones,
	 * DESARROLLADO POR: VENTURA RUEDA, JOSE ANTONIO (AVENTURA)
	 * @param object $nroPresupuesto
	 * @param object $dtLog [optional]
	 * @return 
	 */
	function getDetallePresupuesto($nroPresupuesto, $dtLog=null){
		if(empty($nroPresupuesto)) return array();
		
		//AVENTURA: ARMAMOS LA CONDICION POR ORGANIZATION
		$cndOrganization = " 1=1 ";
		if(!empty($dtLog)){
			$this->Secorganization = new Secorganization;
			$organization = $this->Secorganization->findById($dtLog['organizationId']);
			
			
			if($organization['Secorganization']['code'] == 'DC'){
				$cndOrganization = " ot.CiaCod = '".$organization['Secorganization']['code']."' ";
			}else{
				$cndOrganization = "ot.DocFch >= '01/01/2011' AND ot.CiaCod = '".$organization['Secorganization']['code']."' ";
			}
		} 
		
		//AVENTURA RECUPERAMOS EL CIACOD DE LA EMPRESA
		$sql = "SELECT  ot.CiaCod,   ot.DocNro AS Presupuesto,    -- El numero de presupuesto
           orepa.DocRepNroC as Item,  -- Correlativo del Item      orepa.DocRepaFch, orepa.RepImp,
           orepa.REPACOD as Codigo,     -- codigo de Reparacion   
           repa.REPADES as Descripcion,      -- Descricpcion de la reparacion
           orepa.DocRepHo AS Horas, -- Horas cargadas
           orepa.DocRepPr as ValorHora,       -- Valor por cada hora
           -- ROUND(((ROUND(orepa.DocRepHo,0)+((ROUND(orepa.DocRepHo,2)- ROUND(orepa.DocRepHo,0))/0.6))* ROUND(orepa.DocRepPr,2)),2) AS Precio
           ROUND(((FLOOR(orepa.DocRepHo)+((ROUND(orepa.DocRepHo,2)- FLOOR(orepa.DocRepHo))/0.6))* ROUND(orepa.DocRepPr,2)),2) AS Precio -- Multiplicacion de los dos anteriores
                      -- ROUND(orepa.DocRepHo * orepa.DocRepPr, 2) AS Precio,
                      -- CASE repa.REPATIPO WHEN 1 THEN 'TERCEROS' ELSE 'MANO DE OBRA' END AS TIPO
		    FROM         dpcars75.dbo.TORDCAB AS ot WITH (nolock) INNER JOIN
                      dpcars75.dbo.GLOCALES AS lo WITH (nolock) ON ot.CiaCod = lo.CiaCod AND ot.DocLocId = lo.LocId INNER JOIN
                      dpcars75.dbo.TREPCAB AS repa WITH (nolock) INNER JOIN
                      dpcars75.dbo.TORDREPA AS orepa WITH (nolock) ON repa.CiaCod = orepa.CiaCod AND repa.REPACOD = orepa.REPACOD ON ot.DocNro = orepa.DocNro AND
                      ot.CiaCod = orepa.CiaCod /*INNER JOIN
  								dpcars75.dbo.TORDTEC AS tec WITH (nolock) ON orepa.CiaCod = tec.CiaCod AND orepa.DocNro = tec.DocNro AND orepa.REPACOD = tec.REPACOD AND
  								orepa.DocRepNroC = tec.DocRepNroC*/ LEFT
                       OUTER JOIN
                      dpcars75.dbo.GCLIENTE AS cli WITH (nolock) INNER JOIN
                      dpcars75.dbo.TTIPORD AS tip WITH (nolock) ON cli.CiaCod = tip.CiaCod ON ot.TpoOrdId = tip.TpoOrdId AND ot.CiaCod = tip.CiaCod AND ot.CiaCod = cli.CiaCod AND
                      ot.Clicod = cli.Clicod LEFT OUTER JOIN
                      dpcars75.dbo.GMODELO AS mo WITH (nolock) RIGHT OUTER JOIN
                      dpcars75.dbo.GAUTOS AS au WITH (nolock) ON mo.ModMarCiaC = au.AutModCiaC AND mo.ModMar = au.AutModMar AND
                      mo.ModCod = au.AutModCod LEFT OUTER JOIN
                      dpcars75.dbo.GMARCAS AS ma WITH (nolock) ON au.CiaCod = ma.CiaCod AND au.AutMarCod = ma.MarCod ON ot.CiaCod = au.CiaCod AND
                      ot.AutNumId = au.AutNumId
			WHERE  1=1
				-- AND ot.DocLocId = 74
				AND ot.DocNro = '$nroPresupuesto'
				AND $cndOrganization ";
				
		$sql = $this->query($sql);
		return $sql;
	}
}
?>