<?php
class Agetipomantenimiento extends AppModel
{
	public $name = 'Agetipomantenimiento';
	public $displayField = 'description';
   
	public $validate = array(
		'description' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								),
						/*'isUnique' =>array(
								'rule'=>'isUnique',
								'last' => true
								),*/
						'maxLength' =>array(
            					'rule'    => array('maxLength', '60'),
								'last' => true
								)   
					)
    );	
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Agemotivoservicio' => array(
			'className' => 'Agemotivoservicio',
			'foreignKey' => 'agemotivoservicio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
}
?>