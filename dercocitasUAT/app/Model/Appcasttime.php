<?php
class Appcasttime extends AppModel {
	public $name = 'Appcasttime';
	public $validate = array(
		'appcast_id' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								)
						),
		'initTime' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								)
						),
		'endTime' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								)
						),
		'programed' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								)
						),
		'status' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								)
						)
	);

	public $belongsTo = array(
		'Appcast' =>
			array('className' => 'Appcast',
				'foreignKey' => 'appcast_id',
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'counterCache' => ''
			),
	);	
}
?>