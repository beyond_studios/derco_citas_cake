<?php
class Agegrupo extends AppModel
{
	public $name = 'Agegrupo';  
	public $displayField = 'description'; 
	public $validate = array(
		'description' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								),
						'isUnique' =>array(
								'rule'=>'isUnique',
								'last' => true
								),
						'maxLength' =>array(
            					'rule'    => array('maxLength', '60'),
								'last' => true
								)  
					)
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed	
    public $hasAndBelongsToMany = array('Marca');	
	
	public $hasMany = array(
		'Agecitacalendario' => array(
			'className' => 'Agecitacalendario',
			'foreignKey' => 'agegrupo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		/*'AgegrupoMarca' => array(
			'className' => 'AgegrupoMarca',
			'foreignKey' => 'agegrupo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)	*/
	);
	
	/**
	 * autor: VENTURA RUEDA, JOSE ANTONIO
	 * @param object $conditions
	 * @return 
	 */
	function obtenerListaGrupos($conditions=array()){
		return $this->find('list',array(
			'conditions'=>$conditions,
			'fields'=>array('Agegrupo.id', 'Agegrupo.description'),
			'order'=>array('Agegrupo.description'=>'ASC')
		));
	}
}
?>