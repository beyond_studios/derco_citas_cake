<?php
/**
 * NOTAS
 * password = DNI
 * username = placa
 * documento_numero = (DNI, Otros)
 * 
 * password_real =  "Password utilizado para el logeo"
 * password = DNI = "Usuario para el logeo"
 * 
 * 
 */
App::uses('AppModel', 'Model');
class Cliente extends AppModel
{
	public $name = 'Cliente';
	public $stdClient = array(
		'PE'=>'Pendiente', 
		'CP'=>'Completo',
		'PA'=>'Parcial'
	);
	
	public $ArrayTipoCliente = array(
		14=>'Corporativo',44=>'Corporativo',
		10=>'PUBLICO',11=>'EXTRANJERO',
		12=>'RELACIONADOS',13=>'COMPAÑÍA DE SEGURO',
		15=>'PROVEEDORES',
		16=>'EMPLEADOS',17=>'TRANSPORTES',
		18=>'GRANDES TIENDAS',19=>'CES',
		20=>'REPUESTEROS',21=>'LUBRICENTRO, TALLER',
		22=>'SERVITECAS',23=>'DISTRIBUIDORES',
		24=>'DISTRIBUIDORES FLOTAS',25=>'DISTR. LUBRICANTES',
		26=>'FLOTISTAS DERCOMAQ',27=>'INTEREMPRESA',
		30=>'VMS CES AUT.',31=>'VMS CES MAQ.',
		32=>'VMS CLTE. FINAL AUT.',33=>'VMS CLTE. FINAL AUT.',
		34=>'AGRICOLA',35=>'FORESTALES',
		36=>'EMPRESA CONSTRUCTORA',37=>'INDUSTRIA',
		38=>'FERRETERIA',39=>'REPUESTO MINORISTA',
		40=>'REPUESTO MAYORISTA',41=>'TALLER MECANICO',
		42=>'RENTA CAR',43=>'ORGANISMO PUBLICO',
		45=>'FED. COOP. DE TRANS.',
		46=>'ZOFRI',47=>'T. MOV DE TIERRA',
		48=>'T. CARGA',49=>'T.PASAJEROS PRIVADO',
		50=>'T. PASAJEROS PUBLICO',51=>'REFERIDOS',
		52=>'FAMILIARES',53=>'DERCO FACIL A',
		54=>'DERCO FACIL B',55=>'CONTRATO JCB LITE',
		56=>'CONTRATO JCB ESTÁNDA',57=>'CONTRATO JCB PLUS',
		58=>'CONTRATO FOTON','P1'=>'PANDERO'
	);
	
   	public $uses = array('Cliente','AgeclientesVehiculo');
	
	public $validate = array(
		'documento_tipo' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								)
					),
		'documento_numero' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								)
					)
    );
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $hasMany = array(
		'Agedetallecita' => array(
			'className' => 'Agedetallecita',
			'foreignKey' => 'cliente_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'AgeclientesVehiculo' => array(
			'className' => 'AgeclientesVehiculo',
			'foreignKey' => 'cliente_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
/**
 * AUTHOR: VENTURA RUEDA, JOSE ANTONIO
 * password = DNI
 * username = placa
 * documento_numero = (DNI, Otros)
 * 
 * password_new =  "Password utilizado para el logeo"
 * password = DNI = "Usuario para el logeo"
 */
	public function clienteAdd($dt){
		$password_new = '';
		$cliente_db = $this->find('first',array(
			'conditions'=>array('documento_numero'=>$dt['Cliente']['documento_numero'])
		));
		
		if(!empty($cliente_db)){
			$dt['Cliente']['id'] = $cliente_db['Cliente']['id'];
			$dt['Cliente']['estado'] = 'AC';
		}else{
			$password_new = $this->makePassword();
			$dt['Cliente']['password_new'] = md5($password_new);
			$newClient = true;
		}
		
		if(isset($dt['Cliente']['id'])){
			if(empty($dt['Cliente']['id'])){
				unset($dt['Cliente']['id']);
			}
		}
		if(!$this->save($dt)){
			return array(false, "ERROR AL GUARDAR EL CLIENTE");
		}
		
		return array(true, "EL DATO FUE GUARDADO", 'password'=>$password_new);
	}
	
	/**AUTHOR: VENTURA RUEDA, JOSE ANTONIO
	 * AUTO GENERA UN PASSWORD DE 10 DIGITOS PARA EL CLIENTE
	 * [4-NUMEROS, 3-LETRAS MAYUSCULAS, 3-LETRAS MINUSCULAS]
	 * @return 
	 */
	function makePassword(){
		$caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //posibles caracteres a usar
		$numerodeletras=9; //numero de letras para generar el texto
		$cadena = ""; //variable para almacenar la cadena generada
		for($i=0;$i<$numerodeletras;$i++){
		    $cadena .= substr($caracteres,rand(0,strlen($caracteres)),1); 
		}
		
		/** agregamos un caracter especial **/
		$a=array("-"=>'a',"*"=>'b',"."=>'c',"_"=>'d');
		$caracterEspecial = array_rand($a,1);
		
		return $cadena.$caracterEspecial[0];
	}
	
	function getConditionsBuscador($dt, $dtLog){
		app::import('Model', 'AgeclientesVehiculo');		$this->AgeclientesVehiculo = new AgeclientesVehiculo();
		
		//CONDICION POR NOMBRE
		$cndPlaque = array();
		if(!empty($dt['bsc']['placa'])){
			$clientIds = $this->AgeclientesVehiculo->find('list',array(
				'conditions'=>array(
					'AgeclientesVehiculo.estado'=>'AC',
					'100'=>str_replace("splacass", $dt['bsc']['placa'],"AgeclientesVehiculo.placa LIKE '%splacass%'")
				),
				'fields'=>array('AgeclientesVehiculo.cliente_id','AgeclientesVehiculo.cliente_id')
			));
			$cndPlaque = empty($clientIds)?array("1=2"):array('100'=>"Cliente.id in (".implode(",", $clientIds).")");
		} 
		
		
		//CONDICION POR NOMBRE
		$cndCrt = array();
		if(!empty($dt['bsc']['nombres'])) $cndCrt = array('101'=>'Cliente.nombres LIKE \'%'.trim($dt['bsc']['nombres']).'%\'');
		
		//CONDICION DE ACUERDO AL ESTADO
		$cndStd = empty($dt['bsc']['std'])?array():array('Cliente.cliente_estado'=>sprintf("%s",$dt['bsc']['std']));
			
		$cndB = array('102'=>"Cliente.estado in('AC')");
		
		return $cndB + $cndCrt + $cndStd + $cndPlaque;
	}
	
	/**RECUPERA LAS CONDICIONES PARA EL GRID DE CLIENTE
	 * 
	 * @param object $params
	 * @param object $filters
	 * @param object $dtLog
	 * @return 
	 */
	function conditionsBuscadorGrid($params, $filters, $dtLog){
		$condicion ="1=1";
		if($params['_search'] == 'true'){
			
			foreach($filters['rules'] as $rule){
				$condicion .= " ".$filters['groupOp']." ".$this->name.'.'.$rule['field']." LIKE ('%".$rule['data']."%')";
			}				
		}
		$condicion .= " AND Cliente.estado in('AC')";
		return $condicion;
	}
	
	public function procesar($data,$dataRequest){
		if(!empty($data) && !empty($dataRequest)){
			if($data['Estado']==0){ //cliente existe
				//consultamos si el cliente existe con esos datos
				$conditions = array('Cliente.username'=>$data['VEHICULO']['Placa'],
									'Cliente.password'=>$data['CLIENTE']['NumeroDoc'],
									'Cliente.codigo_sap'=>$data['CLIENTE']['IdCliente']);
				$clientesRegistradosConAccesos = $this->find('first',array('conditions'=>$conditions));
				//si el cliente ya esta registrado con la misma placa en el sistema web solo muestra un mesaje
				if(!empty($clientesRegistradosConAccesos) && isset($clientesRegistradosConAccesos)){
					return array('Estado'=>'CLIENTEVEHICULOCONACCESSOS','Msg'=>'Ud ya se encuentra registrado');
//					if(!empty($clientesRegistradosConAccesos['Cliente']['password_new'])){
//						return array('Estado'=>'CLIENTEVEHICULOCONACCESSOS','Msg'=>'Ud ya se encuentra registrado');	
//					}else{
//						return array(
//							'Estado'=>'CLIENTEVEHICULOCSINACCESSONEW',
//							'dataWeb'=>$clientesRegistradosConAccesos,
//							'dataWebService'=>$data
//						);
//					}
				}else{
					return array('Estado'=>'CLIENTEVEHICULOREGISTRADOONLYSAP',
								'Msg'=>'Ud no tiene registro web',
								'dataWeb'=>$this->convertClienteSapOnClient($data),
								'dataWebService'=>$data);	
				} 
			}elseif($data['Estado']==1){//cliente existe pero no tiene relacion con placa
				$dataWeb = $this->convertClienteSapOnClient($data);
				$dataWeb['Cliente']['placa'] = $dataRequest['Cliente']['placa'];
				return array('Estado'=>'CLIENTEREGISTRADOONLYSAP',
							'Msg'=>'Cliente Registrado en SAP pero sin accessos en la web',
							'dataWeb'=>$dataWeb,
							'dataWebService'=>$data);
			}elseif($data['Estado']==2){//no existe
				return array('Estado'=>'CLIENTENOEXISTE',
							'Msg'=>'Cliente no Existe',
							'dataWeb'=>$dataRequest);
			}
		}
		return array('Estado'=>'Success');
	}
	
	public function convertClienteSapOnClient($clienteSap){
		$cliente = array();
		//rtineo modificado por casos de cliente pandero
		if(empty($clienteSap['CLIENTE']['TipoCliente']))
			$clienteSap['CLIENTE']['TipoCliente'] = '10';
		//rtineo fin
		$cliente['Cliente']['codigo_sap'] = $clienteSap['CLIENTE']['IdCliente'];
		$cliente['Cliente']['documento_tipo'] = $clienteSap['CLIENTE']['TipoDoc'];
		$cliente['Cliente']['documento_numero'] = $clienteSap['CLIENTE']['NumeroDoc'];
		$cliente['Cliente']['cliente_tipo'] = $clienteSap['CLIENTE']['TipoCliente'];
		$cliente['Cliente']['ciudad'] = $clienteSap['CLIENTE']['Ciudad'];
		$cliente['Cliente']['distrito'] = $clienteSap['CLIENTE']['Distrito'];
		$cliente['Cliente']['direccion'] = $clienteSap['CLIENTE']['Direccion'];
		$cliente['Cliente']['email'] = $clienteSap['CLIENTE']['Correo'];
		$cliente['Cliente']['telefono'] = $clienteSap['CLIENTE']['Telefono'];
		$cliente['Cliente']['str_cliente_tipo'] = $this->getStrTipoCliente($clienteSap['CLIENTE']['TipoCliente']);
		
		if($clienteSap['CLIENTE']['TipoDoc'] == 'RU'){
			$cliente['Cliente']['razonSocial'] = $clienteSap['CLIENTE']['Nombres'];
			$cliente['Cliente']['nombres'] = $clienteSap['CLIENTE']['Nombres'];
		}elseif($clienteSap['CLIENTE']['TipoDoc'] == 'DN'){
			$cliente['Cliente']['apellidoPaterno'] = $clienteSap['CLIENTE']['Apellidos'];
			$cliente['Cliente']['nombres'] = $clienteSap['CLIENTE']['Nombres'];
		}
		$cliente['Cliente']['fechaCreacion'] = $this->fechaHoraActual();
		$cliente['Cliente']['username'] = $clienteSap['VEHICULO']['Placa'];
		$cliente['Cliente']['password'] = $clienteSap['CLIENTE']['NumeroDoc'];
		return $cliente;
	}
	
	public function selectDataClient($dataWeb,$clienteSap,$estado = null){
		$cliente = array();
		//rtineo modificado por casos de cliente pandero
		if(empty($clienteSap['CLIENTE']['TipoCliente']))
			$clienteSap['CLIENTE']['TipoCliente'] = '10';
		//rtineo fin
		if($estado ==0){
			$cliente['Cliente']['codigo_sap'] = $clienteSap['CLIENTE']['IdCliente'];
			$cliente['Cliente']['username'] = $clienteSap['VEHICULO']['Placa'];
			$cliente['Cliente']['placa'] = $clienteSap['VEHICULO']['Placa'];
			$cliente['Cliente']['documento_tipo'] = $clienteSap['CLIENTE']['TipoDoc'];
			$cliente['Cliente']['documento_numero'] = $clienteSap['CLIENTE']['NumeroDoc'];
			$cliente['Cliente']['password'] = $clienteSap['CLIENTE']['NumeroDoc'];
			$cliente['Cliente']['cliente_tipo'] = $clienteSap['CLIENTE']['TipoCliente'];
			if($this->isNotEmpty($dataWeb['Cliente']['email'])){
				$cliente['Cliente']['email'] = $dataWeb['Cliente']['email'];
			}
			if($this->isNotEmpty($dataWeb['Cliente']['telefono'])){
				$cliente['Cliente']['telefono'] = $dataWeb['Cliente']['telefono'];
			}
			if($this->isNotEmpty($dataWeb['Cliente']['celular'])){
				$cliente['Cliente']['celular'] = $dataWeb['Cliente']['celular'];
			}
			$cliente['Cliente']['nombres'] = $clienteSap['CLIENTE']['Nombres'];
			$cliente['Cliente']['apellidoPaterno'] = $clienteSap['CLIENTE']['Apellidos'];			
			if($clienteSap['CLIENTE']['TipoDoc'] == 'RU'){
				$cliente['Cliente']['razonSocial'] = $clienteSap['CLIENTE']['Nombres'].' '.$clienteSap['CLIENTE']['Apellidos'];
			}
			
			$cliente['Cliente']['ciudad'] = $clienteSap['CLIENTE']['Ciudad'];
			$cliente['Cliente']['distrito'] = $clienteSap['CLIENTE']['Distrito'];
			$cliente['Cliente']['direccion'] = $clienteSap['CLIENTE']['Direccion'];
			$cliente['Cliente']['fechaCreacion'] = $this->fechaHoraActual();
			$cliente['Cliente']['cliente_estado'] = 'CP';

			$cliente['AgeclientesVehiculo']['codigo_vehiculo'] = $clienteSap['VEHICULO']['CodUnidad'];
			$cliente['AgeclientesVehiculo']['placa'] = $clienteSap['VEHICULO']['Placa'];
			$cliente['AgeclientesVehiculo']['marca'] = $clienteSap['VEHICULO']['Marca'];
			$cliente['AgeclientesVehiculo']['modelo'] = $clienteSap['VEHICULO']['Modelo'];
		}elseif($estado ==1){
			$cliente['Cliente']['codigo_sap'] = $clienteSap['CLIENTE']['IdCliente'];
			$cliente['Cliente']['username'] = $dataWeb['Cliente']['placa'];
			$cliente['Cliente']['placa'] = $dataWeb['Cliente']['placa'];
			$cliente['Cliente']['marca'] = $dataWeb['Cliente']['marca'];
			$cliente['Cliente']['documento_tipo'] = $clienteSap['CLIENTE']['TipoDoc'];
			$cliente['Cliente']['documento_numero'] = $clienteSap['CLIENTE']['NumeroDoc'];
			$cliente['Cliente']['password'] = $clienteSap['CLIENTE']['NumeroDoc'];
			$cliente['Cliente']['cliente_tipo'] = $clienteSap['CLIENTE']['TipoCliente'];
			if($this->isNotEmpty($dataWeb['Cliente']['email'])){
				$cliente['Cliente']['email'] = $dataWeb['Cliente']['email'];
			}
			if($this->isNotEmpty($dataWeb['Cliente']['telefono'])){
				$cliente['Cliente']['telefono'] = $dataWeb['Cliente']['telefono'];
			}
			if($this->isNotEmpty($dataWeb['Cliente']['celular'])){
				$cliente['Cliente']['celular'] = $dataWeb['Cliente']['celular'];
			}
			
			$cliente['Cliente']['nombres'] = $dataWeb['Cliente']['nombres'];
			if($clienteSap['CLIENTE']['TipoDoc'] == 'RU'){
				$cliente['Cliente']['razonSocial'] = $dataWeb['Cliente']['nombres'];
			}
			$cliente['Cliente']['ciudad'] = $clienteSap['CLIENTE']['Ciudad'];
			$cliente['Cliente']['distrito'] = $clienteSap['CLIENTE']['Distrito'];
			$cliente['Cliente']['direccion'] = $clienteSap['CLIENTE']['Direccion'];
			$cliente['Cliente']['fechaCreacion'] = $this->fechaHoraActual();
			$cliente['Cliente']['cliente_estado'] = 'PA';
			
			$cliente['AgeclientesVehiculo']['placa'] = $dataWeb['Cliente']['placa'];
			$cliente['AgeclientesVehiculo']['marca'] = $dataWeb['Cliente']['marca'];
		}elseif($estado == 2){
			//$cliente['Cliente']['codigo_sap'] = $clienteSap['CLIENTE']['IdCliente'];
			$cliente['Cliente']['username'] = $dataWeb['Cliente']['placa'];
			$cliente['Cliente']['placa'] = $dataWeb['Cliente']['placa'];
			$cliente['Cliente']['marca'] = $dataWeb['Cliente']['marca'];
			$cliente['Cliente']['documento_tipo'] = $dataWeb['Cliente']['documento_tipo'];
			$cliente['Cliente']['documento_numero'] = $dataWeb['Cliente']['documento_numero'];
			$cliente['Cliente']['password'] = $dataWeb['Cliente']['documento_numero'];
			$cliente['Cliente']['cliente_tipo'] = 10;
			if($this->isNotEmpty($dataWeb['Cliente']['email'])){
				$cliente['Cliente']['email'] = $dataWeb['Cliente']['email'];
			}
			if($this->isNotEmpty($dataWeb['Cliente']['telefono'])){
				$cliente['Cliente']['telefono'] = $dataWeb['Cliente']['telefono'];
			}
			if($this->isNotEmpty($dataWeb['Cliente']['celular'])){
				$cliente['Cliente']['celular'] = $dataWeb['Cliente']['celular'];
			}
			$cliente['Cliente']['nombres'] = $dataWeb['Cliente']['nombres'];
			if($dataWeb['Cliente']['documento_tipo'] == 'RU'){
				$cliente['Cliente']['razonSocial'] = $dataWeb['Cliente']['nombres'];
			}
			$cliente['Cliente']['ciudad'] = $dataWeb['Cliente']['ciudad'];
			$cliente['Cliente']['distrito'] = $dataWeb['Cliente']['distrito'];
			$cliente['Cliente']['direccion'] = $dataWeb['Cliente']['direccion'];
			$cliente['Cliente']['fechaCreacion'] = $this->fechaHoraActual();
			$cliente['Cliente']['cliente_estado'] = 'PE';

			$cliente['AgeclientesVehiculo']['placa'] = $dataWeb['Cliente']['placa'];
			$cliente['AgeclientesVehiculo']['marca'] = $dataWeb['Cliente']['marca'];
		}
		return $cliente;
	}
	
	public function saveCliente($client){
		//registramos el cliente en la base de datos
		if(!$this->existsClientVehiculoNew($client)){
			$existsClient =$this->existsClient($client);
			if(!empty($existsClient)){
				$client['Cliente']['id'] = $existsClient['Cliente']['id'];
				unset($client['Cliente']['username']); unset($client['Cliente']['fechaCreacion']);
				$client['AgeclientesVehiculo']['cliente_id'] = $existsClient['Cliente']['id'];
			}
			//convertimos a mayuscula siempre la placa
			if(!empty($client['Cliente']['username']) && isset($client['Cliente']['username'])) $client['Cliente']['username'] = strtoupper($client['Cliente']['username']);
			if(!empty($client['Cliente']['placa']) && isset($client['Cliente']['placa'])) $client['Cliente']['placa'] = strtoupper($client['Cliente']['placa']);
			if(!empty($client['AgeclientesVehiculo']['placa']) && isset($client['AgeclientesVehiculo']['placa'])) $client['AgeclientesVehiculo']['placa'] = strtoupper($client['AgeclientesVehiculo']['placa']); 
			
			/** LE GENERAMOS SU CONTRASEÑA NUEVA **/
			$password_new = $this->makePassword();
			$client['Cliente']['password_new'] = md5($password_new);
			if(!$this->save($client)){
				return array('Estado'=>'Error','Msg'=>'No se puede registrar al Cliente');
			}
			
			if(!$existsClient){
				$client['AgeclientesVehiculo']['cliente_id'] = $this->getLastInsertID();
			}
			
			if(!empty($client['Cliente']['modelo_id'])){
				$client['AgeclientesVehiculo']['modelo_id'] = $client['Cliente']['modelo_id'];
			}
			
			if(!$this->AgeclientesVehiculo->save($client)){
				return array('Estado'=>'Error','Msg'=>'No se puede registrar al Vehiculo del Cliente');
			}else{
				return array('Estado'=>'Success','Msg'=>'Datos registrado satisfactoriamente', 'password_new'=>$password_new);	
			}
		}else{
			return array('Estado'=>'Error','Msg'=>'Cliente ya cuenta con Accesos');
		} 
	}
	
	public function existsClientVehiculo($client){
		$condition = array('Cliente.username'=>trim($client['Cliente']['username']),
							'Cliente.password'=>trim($client['Cliente']['password']),
							'Cliente.estado'=>'AC');
		$cliente = $this->find('all',array('conditions'=>$condition));
		if(!empty($cliente) && isset($cliente)) return true;
		return false;
	}
	
	public function existsClientVehiculoNew($client){
		$condition = array('Cliente.username'=>trim($client['Cliente']['username']),
							'Cliente.password'=>trim($client['Cliente']['password']),
							'Cliente.estado'=>'AC');
		$client_db = $this->find('first',array('conditions'=>$condition, 'recursive'=>'-1'));
//		var_dump($client_db);
		if(empty($client_db)){
			return false;   /** envia false porque no tenemos registrado al cliente -  function saveCliente**/
		} 
		return !empty($client_db['Cliente']['password_new']);  /** verifica si tiene password_new **/
	}
	
	public function existsClient($client){
		$condition = array('Cliente.documento_tipo'=>trim($client['Cliente']['documento_tipo']),
							'Cliente.documento_numero'=>trim($client['Cliente']['documento_numero']),
							'Cliente.estado'=>'AC');
		$cliente = $this->find('first',array('conditions'=>$condition));
		if(!empty($cliente) && isset($cliente)) return $cliente;
		return false;
	}
	
	public function existsAcess($nrodocumento,$placa){
		$condition = array('Cliente.username'=>trim($placa),
							'Cliente.password'=>trim($nrodocumento),
							'Cliente.estado'=>'AC');
		$cliente = $this->find('all',array('conditions'=>$condition));
		if(!empty($cliente) && isset($cliente)) return true;
		return false;
	}	
	
	/**
	 * NOTAS
	 * password = DNI
	 * username = placa
	 * documento_numero = (DNI, Otros)
	 * 
	 * password_new =  "Password utilizado para el logeo"
	 * password = DNI = "Usuario para el logeo"
	 */
	public function existsAcessNew($nrodocumento,$placa){
		$client_db =  $this->find('first',array(
			'conditions'=>array('Cliente.username'=>trim($placa), 'Cliente.password'=>trim($nrodocumento), 'Cliente.estado'=>'AC'),
			'recursive'=>'-1',
			'fields'=>array('Cliente.id', 'Cliente.password_new', 'Cliente.username', 'Cliente.password')
		));
		
		if(empty($client_db)){
			return false;
		} 
		return !empty($client_db['Cliente']['password_new']);
	}	
	
	function enviarEmailConfirmacionCitaTaller($cliente_id, $project_id, $cita_id) {
		app::import('Model', 'Agecitacalendario'); 		$this->Agecitacalendario = new Agecitacalendario;
		app::import('Model', 'Agedetallecita'); 		$this->Agedetallecita = new Agedetallecita;
		app::import('Model', 'Secproject'); 			$this->Secproject = new Secproject;
		
		
		// se obtienen los datos de la cita
		$this->Agedetallecita->recursive = 1;
		$cita = $this->Agedetallecita->findById($cita_id);
		$fecha = !empty($cita) ? date('d-m-Y', strtotime($cita['Agecitacalendariodia']['initDateTime'])) : '-';
		$hora = !empty($cita) ? date('H:i A', strtotime($cita['Agecitacalendariodia']['initDateTime'])) : '-';
		
		$this->Agecitacalendario->recursive=1;
		$this->Agedetallecita->unbindModel(array('hasMany'=>array('Agecitacalendariodia')));
		$servicio=$this->Agecitacalendario->findById($cita['Agecitacalendariodia']['agecitacalendario_id']);
	
		$this->Secproject->recursive=-1;
		$sucursal=$this->Secproject->findById($servicio['Agecitacalendario']['secproject_id']);
		$servicio=$servicio['Agemotivoservicio']['description'];
		$sucursal=$sucursal['Secproject']['name'];
		
		// se obtienen los datos del cliente
		$campos = array('Cliente.apellidoPaterno, Cliente.apellidoMaterno, Cliente.nombres, 
						Cliente.razonSocial, Cliente.tipoPersona, Cliente.correo');
		$cliente = $this->findById($cliente_id, $campos);
		$nombreCompleto = ($cliente['Cliente']['tipoPersona'] == 'N') ? $cliente['Cliente']['nombreCompleto'] : $cliente['Cliente']['razonSocial'];
		$correo = $cliente['Cliente']['correo'];
		
		// se preparan los datos del email a enviar
		if($correo <> null) {
			$asunto = 'Confirmación de Cita de Taller';
			$codigohtml =  "<b>Estimado Cliente: </b> '".$nombreCompleto."' <br><br>
							Le informamos que su cita de taller ha sido registrada con éxito. <br>
							A continuación le mostramos el detalle de la cita. <br><br>
							<b>Datos de la Cita</b> <br><br>
							<b>Sucursal : </b>".$sucursal." <br>
							<b>Servicio : </b>".$servicio." <br>
							<b>Fecha : </b>".$fecha." <br>
							<b>Hora : </b>".$hora." <br><br>
							Lo esperamos. <br><br>
							Atentamente Derco Perú";						
		} else {
			$sql ="SELECT adminEmail FROM secconfigurations";
			$rs = $this->query($sql);			
			$correo = $rs['0']['secconfigurations']['adminEmail'];
			$asunto = 'El cliente carece de email : Confirmación de Cita de Taller';
			$codigohtml = "El cliente: <b>'".$nombreCompleto."'</b> carece de email.";
		}
		return $this->enviarEmail($correo, $asunto, $codigohtml) ? true : false;
	}
	
	/**REGISTRA A UN CLIENTE EN SESSION
	 * AUTOR: VENTURA RUEDA, JOSE ANTONIO
	 * FECHA: 2013-03-18
	 * @param object $dt
	 * @return 
	 */
	function setLogin($dt){
		//echo ConnectionManager::getInstance();
		if(!$this->validates($dt['Cliente'])) return array(false, __('PASSWORD_USUARIO_ERROR'));
		
		if(empty($dt['Cliente']['username']) || empty($dt['Cliente']['password'])) return array(false, __('PASSWORD_USUARIO_CONTRASEÑA_INCOMPLETOS'));
		
		$username = strtoupper(trim($dt['Cliente']['username']));
		$password = $dt['Cliente']['password'];
		$std = 'AC';
		
		$client_db = $this->find('first',array(
			'conditions'=>array(
				'Cliente.estado'=>$std,
				'Cliente.documento_numero'=>$username
			),
			'recursive'=>-1
		));
		echo($client_db); //exit();
		if(empty($client_db)){
			$client_db = $this->find('first',array(
				'conditions'=>array('Cliente.documento_numero'=>$username,'Cliente.estado'=>$std),
				'recursive'=>-1
			));
			
			if(empty($client_db)) return array(false, __('PASSWORD_USUARIO_WEB_NO_ESTA_REGISTRADO'));
			else return array(false, __('PASSWORD_USUARIO_WEB_NO_ESTA_REGISTRADO')); 
		}
		
		// VALIDAMOS EL CLIENTE COMPLETAMENTE
		$client_db = $this->find('first',array(
			'conditions'=>array(
				'Cliente.estado'=>$std, 'Cliente.password_new'=>md5($password),
				'Cliente.documento_numero'=>$username
			),
			'recursive'=>-1
		));
		
		if(empty($client_db)) return array(false, __('PASSWORD_USUARIO_WEB_NO_ESTA_REGISTRADO'));  
			
        return array(true, __('CLIENTE_REGISTRADO'), 'cliente'=>$client_db);
	}
	
	function getClienteDummy(){
		$cliente = $this->findById(0);
		return $cliente;
	}
	
	function getStrTipoCliente($tipoCliente){
		$strTipoCliente = "";
		if(isset($this->ArrayTipoCliente[$tipoCliente])){
			$strTipoCliente = $this->ArrayTipoCliente[$tipoCliente];
		}
		return $strTipoCliente;
	}
	
	function convertClienteSapOnVehiculo($datosSap){
		$ageclientevehiculo = array();
		if(isset($datosSap['VEHICULO']['CodUnidad'])){
			$ageclientevehiculo['codigo_vehiculo'] = $datosSap['VEHICULO']['CodUnidad'];
			$ageclientevehiculo['placa'] = $datosSap['VEHICULO']['Placa'];
			$ageclientevehiculo['marca'] = $datosSap['VEHICULO']['Marca'];
			$ageclientevehiculo['modelo'] = $datosSap['VEHICULO']['Modelo'];
		}
		return $ageclientevehiculo;
	}
}

