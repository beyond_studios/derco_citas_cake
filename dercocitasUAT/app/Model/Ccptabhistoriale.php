<?php
class Ccptabhistoriale extends AppModel{
	public $name='Ccptabhistoriale';
	
	
	//The Associations below have been created with all possible keys, those that are not needed can be removed		
	public $belongsTo = array(
		'Ccpsolicitudservicio' => array(
			'className' => 'Ccpsolicitudservicio',
			'foreignKey' => 'ccpsolicitudservicio_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Ccptabestado' => array(
			'className' => 'Ccptabestado',
			'foreignKey' => 'ccptabestado_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	function verificarPasoDeEstados($estadoInicial,$estadoFinal){
		switch($estadoInicial){
			case 1://pendiente
				if(in_array($estadoFinal, array(2,3)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 2://deposito
				if(in_array($estadoFinal, array(1,3,4,13)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 3://con solicitud
				if(in_array($estadoFinal, array(3,4)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 4:// con solicitud aprobada
				if(in_array($estadoFinal, array(5)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 5://inicio ABP
				if(in_array($estadoFinal, array(6)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 6://fin ABP
				if(in_array($estadoFinal, array(7,12,13)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 7://control de calidad Aprobada
				if(in_array($estadoFinal, array(11,14,21)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 8://control de calidad Rechazada
				if(in_array($estadoFinal, array(12)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
		/*	case 9://lavado
				if(in_array($estadoFinal, array(10)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 10://control de calidad asesor => (entregado,Pendiente Entrega)
				if(in_array($estadoFinal, array(11,21)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;*/
			case 11://entregado
				if(in_array($estadoFinal, array(14)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 12://reprogramado
				if(in_array($estadoFinal, array(6)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 13://ampliacion de ampliacion pasa a estado deposito o con solicitud
				if(in_array($estadoFinal, array(2,3)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 14://entregado file
				if(in_array($estadoFinal, array(15)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 15://file observado
				if(in_array($estadoFinal, array(14)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 21://Pendiente entrega
				if(in_array($estadoFinal, array(22,14)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
			case 22://Salida Unidad => pendiente entrega o entregado
				if(in_array($estadoFinal, array(21,11,14)))return array('respuesta'=>true,'msg'=>'Permitido');
				else return array('respuesta'=>false,'msg'=>'Estado no Permitido');
				break;
						
			default: return array('respuesta'=>false,'msg'=>'Estado no Permitido');break;
		}
	}

	function obtenerEstadoCcpPotNumeroOt($ot_numero){
	//   	$sql="select 
	//			Ccptabestado.id, 
	//			Ccptabestado.descripcion from
	//			(select ot_numero,max(ccptabestado_id) as estadomaximo from ccptabhistoriales group by ot_numero) as Ccptabhistorial
	//			inner join ccptabestados Ccptabestado on(Ccptabhistorial.estadomaximo=Ccptabestado.id)
	//			where Ccptabhistorial.ot_numero='".$ot_numero."' and Ccptabestado.estado='AC' LIMIT 1";
	$sql="SELECT 
		Ccptabestado.id,
		Ccptabestado.descripcion
 		FROM (select max(id) as id,ot_numero from ccptabhistoriales group by ot_numero) as CcptabhistorialTemp
  		INNER JOIN ccptabhistoriales Ccptabhistorial on(CcptabhistorialTemp.id=Ccptabhistorial.id)
  		INNER JOIN ccptabestados Ccptabestado on(Ccptabestado.id=Ccptabhistorial.ccptabestado_id)
 		WHERE Ccptabhistorial.ot_numero='".$ot_numero."' and Ccptabestado.estado='AC' LIMIT 1";
	
	$estadoCCP=$this->query($sql);
	return(!empty($estadoCCP) && isset($estadoCCP))?$estadoCCP:array();
   }
	
	function validarEstadoAnterior($ot_numero,$estado){
		$sql="SELECT id,ccptabestado_id 
			FROM ccptabhistoriales Ccptabhistoriale WHERE ot_numero='".$ot_numero."'
			ORDER BY id DESC";
		$ot=$this->query($sql);
		if(!empty($ot[1]['Ccptabhistoriale']) && isset($ot[1]['Ccptabhistoriale'])){
			if($ot[1]['Ccptabhistoriale']['ccptabestado_id']==$estado){
				return true;
			}else return false;
		}else return false;
	}
	
	function getSolitudServicioId($ot_numero){
		//verificar si tiene un ampliacion para agregar el estado ampliacion		
		app::import('Model','Ccpsolicitudservicio');    		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		$this->Ccpsolicitudservicio->recursive=-1;
		$condition=array('Ccpsolicitudservicio.ot_numero'=>$ot_numero,
						'Ccpsolicitudservicio.ccpsolicitudservicioestado_id'=>2);
		$solicitudServicio= $this->Ccpsolicitudservicio->find('all',array('conditions'=>$condition,'order'=>'Ccpsolicitudservicio.id desc'));
		$comboAmpliacion=array();
		$solicitud_id=null;
		if(!empty($solicitudServicio) && isset($solicitudServicio)){
			$solicitud_id = $solicitudServicio[0]['Ccpsolicitudservicio']['id'];			
		}
		return 	$solicitud_id;
	}
	
	function cambiarEstado($estadoInicial,$estadoFinal,$numero_ot,$comentario=null,$usuario=null,$id_solicitud=null){		
		app::import('Model','Ccppresupuesto');    		$this->Ccppresupuesto = new Ccppresupuesto;
   		$estadoInicial=(!empty($estadoInicial) && isset($estadoInicial))?$estadoInicial:'';
		$estadoFinal=(!empty($estadoFinal) && isset($estadoFinal))?$estadoFinal:'';
		$numero_ot=(!empty($numero_ot) && isset($numero_ot))?$numero_ot:'';
		$usuario=(!empty($usuario) && isset($usuario))?$usuario:'';
		$respuesta=$this->verificarPasoDeEstados($estadoInicial,$estadoFinal);
		//validamos los Estados
		if($respuesta['respuesta']){
			//validamos que este bien ingresado la OT
			if(!empty($numero_ot) && isset($numero_ot)){
				//validamos si existe OT
				if($this->Ccppresupuesto->ValidarOT($numero_ot)){
					//control de concurrencia
					$estadoTemp=$this->bloquearRegistro($numero_ot);
					if($estadoTemp[0]['Ccptabestado']['id']!=$estadoInicial){
						$respuesta['respuesta']=false;
						$respuesta['msg']="El estado actual ha cambiado";	
					}else{
						$data['Ccptabhistorial']['fecha']=$this->fechaHoraActual();
						$data['Ccptabhistorial']['ot_numero']=$numero_ot;
						$data['Ccptabhistorial']['ccptabestado_id']=$estadoFinal;
						$data['Ccptabhistorial']['comentario']=$comentario;
						$data['Ccptabhistorial']['secperson_id']=$usuario;
						$data['Ccptabhistorial']['ccpsolicitudservicio_id']=(!empty($id_solicitud) && isset($id_solicitud))?$id_solicitud:null;
						$this->create();
						if($this->save($data['Ccptabhistorial'])){
							$respuesta['respuesta']=true;
							$respuesta['msg']="El Proceso se actualizó satisfactoriamente";		
						}else{
							$respuesta['respuesta']=false;
							$respuesta['msg']="Error al cambiar estado";								
						}
					}
					//fin control de Concurrencia
				}else{
					$respuesta['respuesta']=false;
					$respuesta['msg']='OT no existe';				
				}
			}else{
				$respuesta['respuesta']=false;
				$respuesta['msg']='Valor invalido para OT';
			}
		}else{
			$respuesta['respuesta']=false;
			$respuesta['msg']='Estado no permitido';
		}
		return $respuesta;
   }

	
	//RTINEO
	//FUNCIONES MODIFICADAS PARA LAS BANDEJAS DEL TABLERO CCP
	function getTalotCarsNew($data, $params, $login, $total = false,$exportar){
		// VALORES NECESARIOS
		$secproject_carscod = $params['params']['carscod'];
		// RECUPERAMOS DATOS PARA LA PAGINACION
		$sord = empty($params['params']['order'])?'':$params['params']['order'];
		$limit = empty($params['params']['limit'])?'':$params['params']['limit'];
		$page = empty($params['params']['page'])?'':$params['params']['page'];
 		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$condicionSolicitud = "";
		$condicionSolicitud = 'Ccpsolicitudservicio.ot_numero is not null';
		$fechaSol = $this->condicionBuscadorFecha(empty($data['fechaIni'])?'':$data['fechaIni'], empty($data['fechaIni'])?'':$data['fechaFin'], $campoModelo ='Ccpsolicitudservicio.fecha_emitido');
		if(!empty($fechaSol)) $condicionSolicitud .= $fechaSol;
		if(!empty($data['ccptiposervicio_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccptiposervicio_id = '.$data['ccptiposervicio_id'];
		if(!empty($data['ccpsolicitudservicioestado_id'])) $condicionSolicitud .= ' AND Ccpsolicitudservicio.ccpsolicitudservicioestado_id = '.$data['ccpsolicitudservicioestado_id'];
	
		//RECUPERO LOS OT DE LAS SOLICITUDES
		$ot_solicitudes = '';
		
		if($condicionSolicitud != 'Ccpsolicitudservicio.ot_numero is not null'){
			$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
			$ot_solicitudes = $this->Ccpsolicitudservicio->generateList($condicionSolicitud,null,null,"{n}.Ccpsolicitudservicio.id","{n}.Ccpsolicitudservicio.ot_numero");
			$ot_solicitudes = empty($ot_solicitudes)?'AND ot.OT IN(\'0\')':'AND ot.OT IN(\''.implode('\',\'', $ot_solicitudes).'\')';
			
		}
		//RECUPERAMOS las ots que se encuentran en estado
		$ot_ccphistorial='';
		if(!empty($data['estadootccp_id']) && isset($data['estadootccp_id'])){
			$this->Ccptabhistoriale = new Ccptabhistorial;
			if(strlen($data['estadootccp_id'])==7){
				if(strpos('1', $data['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistoriale->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($data['estadootccp_id']==1){
					$ot_ccphistorial="";
				}else{				
					$ot_ccphistorial = $this->Ccptabhistoriale->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		//FILTROS DE CARS
		$condicion_cars=" ";
		$condicion_cars="ot.TALLER=".$secproject_carscod;
		if(!empty($data['placa']) && isset($data['placa'])){
			$placa_= " AND ot.PLACA like '%".$data['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;
		$fechaCars = $this->condicionBuscadorFecha(empty($data['fechaIniOt'])?'':$data['fechaIniOt'], empty($data['fechaFinOt'])?'':$data['fechaFinOt'], $campoModelo ='ot.FECHA');
		//pr(array('aca'=>$fechaCars));
		if(!empty($fechaCars)) $condicion_cars .= $fechaCars;
		if(!empty($data['asesor_id'])) $condicion_cars .= " AND  LTRIM(RTRIM(ot.USUARIO_OT)) = '".trim($data['asesor_id'])."'";
		if(!empty($data['carsestado_id'])){
			$estadoCars = $data['carsestado_id']-1; 
			$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		if(!empty($data['preestado_id'])) $condicion_cars .= ' AND ot.OT_PRE_ESTADO = '.$data['preestado_id'];
		if(!empty($data['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$data['nrocars'].'%\'';
		
		// RECUPERAMOS EL TOTAL DE REGISTROS
		//debug("$condicion_cars $ot_solicitudes");
		$total_query = "SELECT COUNT(OT) as total FROM Sistema_Planificador_CCP as ot WHERE $condicion_cars $ot_solicitudes $ot_ccphistorial";
		$total_query = $this->query($total_query);
		
		if($total) return $total_query[0][0]['total'];
		
		$count = $total_query[0][0]['total'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		//paginacion en SQL 
		$offset=empty($page)?0:($page-1)*$limit;
		
		//RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		if($exportar){
			$presupuestos = "SELECT
									ot.OT AS ot_numero,
									ot.MARCA AS ot_marca,
									ot.MODELO AS ot_modelo,
									ot.VERSION AS ot_version,
									ot.DESCRIPCION AS ot_descripcion,
									ot.NOMBRE_ASESOR AS ot_nombre_asesor,
									ot.TIPO_OT AS ot_tipo_ot,
									ot.HORA_RECIBIDA AS ot_hora_recibida,
									ot.HORA_PROMETIDA AS ot_hora_prometida,
									ot.NRO_CONO AS ot_numero_cono,
									ot.PRESUPUESTO AS presupuesto_numero,
									ot.FECHA AS ot_fecha_creacion,
									ot.USUARIO_OT AS ot_asesor,
									ot.FECHA_PROM AS ot_fecha_entrega,
									ot.CLIENTE AS ot_cliente, 
									ot.NRO_SERIE AS chasis_vin,
									ot.PLACA as ot_placa,
									ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,
									ot.OT_ESTADO AS ot_estado,
									ot.OT_PRE_ESTADO as ot_pre_estado
							FROM 
								Sistema_Planificador_CCP AS ot
							WHERE 
								$condicion_cars $ot_solicitudes $ot_ccphistorial
							ORDER BY $sord LIMIT $limit"; 			
		}else{
			$presupuestos = "SELECT
									ot.OT AS ot_numero,
									ot.MARCA AS ot_marca,
									ot.MODELO AS ot_modelo,
									ot.VERSION AS ot_version,
									ot.DESCRIPCION AS ot_descripcion,
									ot.NOMBRE_ASESOR AS ot_nombre_asesor,
									ot.TIPO_OT AS ot_tipo_ot,
									ot.HORA_RECIBIDA AS ot_hora_recibida,
									ot.HORA_PROMETIDA AS ot_hora_prometida,
									ot.NRO_CONO AS ot_numero_cono,
									ot.PRESUPUESTO AS presupuesto_numero,
									ot.FECHA AS ot_fecha_creacion,
									ot.USUARIO_OT AS ot_asesor,
									ot.FECHA_PROM AS ot_fecha_entrega,
									ot.CLIENTE AS ot_cliente,
									ot.NRO_SERIE AS chasis_vin,
									ot.PLACA as ot_placa,
									ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,
									ot.OT_ESTADO AS ot_estado,
									ot.OT_PRE_ESTADO as ot_pre_estado
							FROM 
								Sistema_Planificador_CCP AS ot
							WHERE 
								$condicion_cars $ot_solicitudes $ot_ccphistorial
							ORDER BY ".$sord." LIMIT ".$limit." OFFSET ".$offset; 
		}
		//pr($presupuestos);exit();
		
		$presupuestos = $this->query($presupuestos);
		//pr($presupuestos);		
		$this->Talot = new Talot();
		$this->Ccpsolicitudservicio = new Ccpsolicitudservicio;
		$this->Ccptabhistorial= new Ccptabhistorial;
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['ot']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['ot']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['ot']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['ot']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['ot']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['ot']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['ot']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['ot']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['ot']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['ot']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['ot']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['ot']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['ot']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['ot']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['ot']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['ot']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['ot']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['ot']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['ot']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['ot']['ot_numero_cono'];
			
			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			
			$tooltip = $this->Talot->findByNrocars($presupuesto['ot']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;
			
			//obtenemos el color de la ot
			$presupuestos[$key]['Talot']['color']=$this->Ccptabhistoriale->obtenerDiferenciaDias(date('d-m-Y'),$presupuestos[$key]['Talot']['ot_fecha_creacion']);
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistoriale->getSolicitudServicioAndCcpEstado(trim($presupuesto['ot']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			
			unset($presupuestos[$key]['0']);
		}
		
		return $presupuestos;
	}

	function getAsesoresCars($secproject_carscod){
		$asesores = "SELECT miOt.USUARIO_OT FROM 
						(SELECT USUARIO_OT FROM Sistema_Planificador_CCP 
						WHERE TALLER='$secproject_carscod' AND OT_ESTADO IN(0,2)
						GROUP BY USUARIO_OT ORDER BY USUARIO_OT) as miOt";
		$asesores = $this->query($asesores);
		if(empty($asesores)) return $asesores;
		//pr($asesores);
		foreach($asesores as $asesor)
			$asesor_tmp[$asesor['miOt']['USUARIO_OT']] = $asesor['miOt']['USUARIO_OT'];	
		
		return $asesor_tmp;
	}
/*
 * RTINEO: Bandeja del resposable de deposito
 * @param object $data
 * @param object $params
 * @param object $login
 * @param object $total [optional]
 * @return 
 */
	function getTalotCarsResponsableDeposito($data, $params, $login, $total = false){
		// VALORES NECESARIOS
		$secproject_carscod = $params['params']['carscod'];
		// RECUPERAMOS DATOS PARA LA PAGINACION
		$sord = empty($params['params']['order'])?'':$params['params']['order'];
		$limit = empty($params['params']['limit'])?'':$params['params']['limit'];
		$page = empty($params['params']['page'])?'':$params['params']['page'];
 		
		//FILTROS DE LA SOLICITUD - FILTRO DE FECHAS
		$ot_solicitudes = '';

		//RECUPERAMOS las ots que se encuentran en ccpestado
		$ot_ccphistorial='';
		//pr($data['estadootccp_id']);
		if(!empty($data['estadootccp_id']) && isset($data['estadootccp_id'])){
			$this->Ccptabhistoriale = new Ccptabhistoriale;
			if(strlen($data['estadootccp_id'])==17){
				if(strpos('1', $data['estadootccp_id']) !== false){// si tiene uno en su cadena
					$ot_ccphistorial = "";
				}else{
					$ot_ccphistorial = $this->Ccptabhistoriale->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'';
				}
			}else{
				if($data['estadootccp_id']==1){
					//SI ES ESTADO PENDIENTE DEBE MOSTRAR SOLO ESTADO PENDIENTE
					$estadoCcp="2,3,4,5,6,7,8,9,10,11,12,13,14,15,21,22";//lavado y ccasesor
					$ot_ccphistorial = $this->Ccptabhistoriale->obtenerOTPorEstadoCcp($estadoCcp);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT NOT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT NOT IN(\'0\')';
				}else{				
					$ot_ccphistorial = $this->Ccptabhistoriale->obtenerOTPorEstadoCcp($data['estadootccp_id']);
					$ot_ccphistorial = (!empty($ot_ccphistorial) && isset($ot_ccphistorial))?'AND ot.OT IN(\''.implode('\',\'', $ot_ccphistorial).'\')':'AND ot.OT IN(\'0\')';
				}
			}
		}
		//FILTROS DE CARS
		$condicion_cars=" ";
		$condicion_cars="ot.TALLER=".$secproject_carscod;
		
		if(!empty($data['placa']) && isset($data['placa'])){
			$placa_= " AND ot.PLACA like '%".$data['placa']."%'";
		}else $placa_=" ";
		
		$condicion_cars = $condicion_cars.$placa_;

		//if(!empty($data['asesor_id'])) $condicion_cars .= " AND ot.USUARIO_OT = '".trim($data['asesor_id'])."'";
		if(!empty($data['carsestado_id'])){
			$estadoCars = $data['carsestado_id']-1; 
			//$condicion_cars .= " AND ot.OT_ESTADO = '$estadoCars'";
			$condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		}else  $condicion_cars .= " AND ot.OT_ESTADO IN ('0','2')"; 
		if(!empty($data['chasis'])) $condicion_cars .= " AND ot.NRO_SERIE like '%".$data['chasis']."%'";
		if(!empty($data['nrocars'])) $condicion_cars .= ' AND ot.OT like \'%'.$data['nrocars'].'%\'';
		
		// RECUPERAMOS EL TOTAL DE REGISTROS
		//debug("$condicion_cars $ot_solicitudes");
		$total_query = "SELECT COUNT(OT) as total FROM Sistema_Planificador_CCP as ot WHERE $condicion_cars $ot_solicitudes $ot_ccphistorial";

		$total_query = $this->query($total_query);
		
		if($total) return $total_query[0][0]['total'];
		
		$count = $total_query[0][0]['total'];
		
		if($count>0) $total_pages = ceil($count/$limit); 
		else $total_pages = 0; 
				
		if ($page > $total_pages) $page=$total_pages; 
		
		//paginacion en SQL 
		$offset=empty($page)?0:($page-1)*$limit;

		// RECUPERAMOS LOS DATOS PARA ENVIO A LA VISTA
		$presupuestos = "SELECT
								ot.OT AS ot_numero,
								ot.PRESUPUESTO AS presupuesto_numero,		-- Numero de presupuesto
								ot.FECHA AS ot_fecha_creacion,         		-- Fecha que se gener? el la OT  *
								ot.USUARIO_OT AS ot_asesor,  				-- Usuario que genero la OT(Asesor de Servicio del CCP)
								ot.FECHA_PROM AS ot_fecha_entrega,  		-- Fecha Prometida
								ot.CLIENTE AS ot_cliente,  					-- AQUI SERA EL CLIENTE
								ot.NRO_SERIE AS chasis_vin,					-- Numero de chasis
								ot.PLACA as ot_placa,							-- Nuemero de Placa
								ot.FECHA_APROB_ASEG AS pre_fecha_aprobacion,	-- fecha aprobacion de seguro * cia ceguros
								ot.OT_ESTADO AS ot_estado,						-- Estado de la ot
								ot.OT_PRE_ESTADO as ot_pre_estado,				-- Estado de la ot con respecto al presupuesto
								ot.MARCA AS ot_marca,         				-- Numero de OT generad
								ot.MODELO AS ot_modelo,         				-- Numero de OT generad
								ot.COLOR as ot_color,					-- Color								
								ot.VERSION AS ot_version,         				-- Numero de OT generad
								ot.DESCRIPCION AS ot_descripcion,         				-- Numero de OT generad
								ot.NOMBRE_ASESOR AS ot_nombre_asesor,         				-- Numero de OT generad
								ot.TIPO_OT AS ot_tipo_ot,         				-- Numero de OT generad
								ot.HORA_RECIBIDA AS ot_hora_recibida,         				-- Numero de OT generad
								ot.HORA_PROMETIDA AS ot_hora_prometida,         				-- Numero de OT generad
								ot.NRO_CONO AS ot_numero_cono        				-- Numero de OT generad
						FROM 
							Sistema_Planificador_CCP AS ot
						WHERE 
							$condicion_cars $ot_solicitudes $ot_ccphistorial
						ORDER BY ".$sord." LIMIT ".$limit." OFFSET ".$offset; 
		//DEBUG($presupuestos);
		//pr($presupuestos);
		$presupuestos = $this->query($presupuestos);
				
		$this->Talot = new Talot();
		$this->Ccptabhistoriale= new Ccptabhistoriale();
		$this->Ccpsolicitudservicio= new Ccpsolicitudservicio();
		
		foreach($presupuestos as $key => $presupuesto){
			$presupuestos[$key]['Talot']['ot_numero'] = trim($presupuesto['ot']['ot_numero']);
			$presupuestos[$key]['Talot']['ot_pre_estado'] = $presupuesto['ot']['ot_pre_estado'];
			$presupuestos[$key]['Talot']['presupuesto_numero'] = trim($presupuesto['ot']['presupuesto_numero']);
			$presupuestos[$key]['Talot']['ot_fecha_creacion'] = $this->fecha_Mysql_Php($presupuesto['ot']['ot_fecha_creacion']);
			$presupuestos[$key]['Talot']['ot_asesor'] = trim($presupuesto['ot']['ot_asesor']);
			$presupuestos[$key]['Talot']['chasis_vin'] = trim($presupuesto['ot']['chasis_vin']);
			$presupuestos[$key]['Talot']['ot_cliente'] = trim($presupuesto['ot']['ot_cliente']);
			$presupuestos[$key]['Talot']['ot_placa'] = trim($presupuesto['ot']['ot_placa']);
			$presupuestos[$key]['Talot']['ot_fecha_entrega'] = $this->fecha_Mysql_Php($presupuesto['ot']['ot_fecha_entrega']);
			$presupuestos[$key]['Talot']['pre_fecha_aprobacion'] = $this->fecha_Mysql_Php($presupuesto['ot']['pre_fecha_aprobacion']);
			
			//prueba con tooltip desde cars
			$presupuestos[$key]['Talot']['ot_estado'] = $presupuesto['ot']['ot_estado'];
			$presupuestos[$key]['Talot']['ot_marca'] = $presupuesto['ot']['ot_marca'];
			$presupuestos[$key]['Talot']['ot_modelo'] = $presupuesto['ot']['ot_modelo'];
			$presupuestos[$key]['Talot']['ot_color'] = trim($presupuesto['ot']['ot_color']);
			$presupuestos[$key]['Talot']['ot_version'] = $presupuesto['ot']['ot_version'];
			$presupuestos[$key]['Talot']['ot_descripcion'] = $presupuesto['ot']['ot_descripcion'];
			$presupuestos[$key]['Talot']['ot_nombre_asesor'] = $presupuesto['ot']['ot_nombre_asesor'];
			$presupuestos[$key]['Talot']['ot_tipo_ot'] = $presupuesto['ot']['ot_tipo_ot'];
			$presupuestos[$key]['Talot']['ot_hora_recibida'] = $presupuesto['ot']['ot_hora_recibida'];
			$presupuestos[$key]['Talot']['ot_hora_prometida'] = $presupuesto['ot']['ot_hora_prometida'];
			$presupuestos[$key]['Talot']['ot_numero_cono'] = $presupuesto['ot']['ot_numero_cono'];

			$presupuestos[$key]['tooltip']['Talot'] =$presupuestos[$key]['Talot'];
			$tooltip = $this->Talot->findByNrocars($presupuesto['ot']['ot_numero']);
			if(!empty($tooltip) && isset($tooltip))$presupuestos[$key]['tooltip']['Talot']['acta']=true;
			else $presupuestos[$key]['tooltip']['acta'] = false;

			//recuperamos el comentario de los estados de deposito
			$comentarioDeposito = $this->Ccptabhistoriale->getComentarioDeposito(trim($presupuesto['ot']['ot_numero']));
			if(!empty($comentarioDeposito) && isset($comentarioDeposito)) $presupuestos[$key]['tooltip']['Talot']['comentarioDeposito']=$comentarioDeposito[0][0]['comentario'];
			else $presupuestos[$key]['tooltip']['Talot']['comentarioDeposito'] = null;
			//if(empty($comentarioDeposito[0]['Talot']) && isset($comentarioDeposito[0]['Talot'])) $presupuestos[$key]['tooltip']['Talot']['acta']=true;
			//else $presupuestos[$key]['tooltip']['acta'] = false;
			
			//recuperamos la solicitud de servicio y la ot
			$solicitudServicio=$this->Ccptabhistorial->getSolicitudServicioAndCcpEstado(trim($presupuesto['ot']['ot_numero']));
			//Se agregan todas las solicitudes de la ot
			if(!empty($solicitudServicio) && isset($solicitudServicio)){
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$solicitudServicio['Ccpsolicitudservicio']['fecha_emitido'] = $this->Ccpsolicitudservicio->fecha_Mysql_Php($solicitudServicio[0]['Ccpsolicitudservicio']['fecha_emitido']);
					$presupuestos[$key]['Ccpsolicitudservicio']=$solicitudServicio[0]['Ccpsolicitudservicio'];
					$presupuestos[$key]['Ccptiposervicio']=$solicitudServicio[0]['Ccptiposervicio'];
					$presupuestos[$key]['Ccpsolicitudservicioestado']=$solicitudServicio[0]['Ccpsolicitudservicioestado'];
				}else{
					$presupuestos[$key]['Ccpsolicitudservicio']=array();
					$presupuestos[$key]['Ccptiposervicio']=array();
					$presupuestos[$key]['Ccpsolicitudservicioestado']=array();
				}
				if(!empty($solicitudServicio[0]['Ccpsolicitudservicio']) && isset($solicitudServicio[0]['Ccpsolicitudservicio'])){
					$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];
				}
			}else{
				$solicitudServicio[0]['Ccptabestado']['id']=1;
				$solicitudServicio[0]['Ccptabestado']['descripcion']='Pendiente';
				$presupuestos[$key]['Ccptabestado']=$solicitudServicio[0]['Ccptabestado'];;
			}
			
			unset($presupuestos[$key]['ot']);
		} 
		
		return $presupuestos;
	}

   function obtenerOTPorEstadoCcp($estadoccp_id){
   	$sql="SELECT Ccptabhistorial.ot_numero
 		FROM (SELECT MAX(id) as id,ot_numero FROM ccptabhistoriales GROUP BY ot_numero) as CcptabhistorialTemp
  		INNER JOIN ccptabhistoriales Ccptabhistorial on(CcptabhistorialTemp.id=Ccptabhistorial.id)
   		WHERE Ccptabhistorial.ccptabestado_id IN(".$estadoccp_id.")";
		debug($sql);
	$otCCP=$this->query($sql);
	$lista = array();
	foreach($otCCP as $id=>$item){
		$lista[$id]=$item['Ccptabhistorial']['ot_numero'];
	}
	return (!empty($lista) && isset($lista))?$lista:array();
   }
} 
?>