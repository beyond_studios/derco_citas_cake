<?php
class Agedetallecitaslostsmotive extends AppModel
{
	public $name = 'Agedetallecitaslostsmotive';	
	public $displayField = 'description';   
	public $validate = array(
		'description' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								),
						'maxLength' =>array(
            					'rule'    => array('maxLength', '60'),
								'last' => true
								)   
					)
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $hasMany = array(
		'Agedetallecitaslost' => array(
			'className' => 'Agedetallecitaslosts',
			'foreignKey' => 'agedetallecitaslostsmotive_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	/** ############################################################################
	 * ###################### ACCIONES MODIFICADAS #################################
	 * ########## AUTOR: VENTURA RUEDA, JOSE ANTONIO ###############################
	 * */
	
	
	function getList($conditions=array()){
		return $this->find('list',array(
			'conditions'=>$conditions,
			'fields'=>array('Agedetallecitaslostsmotive.id', 'Agedetallecitaslostsmotive.description'),
			'order'=>array('Agedetallecitaslostsmotive.description'=>'ASC')
		));
	}	
}