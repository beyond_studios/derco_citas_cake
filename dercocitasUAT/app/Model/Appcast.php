<?php
  class Appcast extends AppModel {
	public $name = 'Appcast';	     
   
	public $validate = array(
		'description' => array(
						'notEmpty' =>array(
								'rule'=>'notEmpty',
								'last' => true
								)
					)
    );

	public $hasMany = array(
		'Appcasttime' =>
			array('className' => 'Appcasttime',
				'foreignKey' => 'appcast_id',
				'conditions' => '',
				'fields' => '',
				'order' => '',
				'limit' => '',
				'offset' => '',
				'dependent' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
				),
	);
	
	function buscarTodos($condicion, $order, $limit, $page) {
		$sql = "SELECT
				Appcast.id AS \"Appcast__id\",
				Appcast.description AS \"Appcast__description\",
				Appcast.status AS \"Appcast__status\" FROM
				appcasts Appcast";
		if ($condicion) {
			$sql .= ' WHERE '.$condicion;
		}
		if ($order && $limit && $page) {
			$offset = ($page-1)*$limit;
			$sql .= ' ORDER BY '.$order.' LIMIT '.$limit.' OFFSET '.$offset;
		}
		$items = $this->query($sql);
		return $items;
	}
	
	function buscarTodosTotal($condicion) {
		$sql = "SELECT
				Appcast.id AS \"Appcast__id\",
				Appcast.description AS \"Appcast__description\",
				Appcast.status AS \"Appcast__status\" FROM
				appcasts Appcast";
		if ($condicion) {
			$sql .= ' WHERE '.$condicion;
		}
		$sqlTotal =	"SELECT count(*) as total FROM (".$sql.") as s1";
		$total = $this->query($sqlTotal);
		return $total[0][0]['total'];
	}

	function getCantidadCitasPorPlantilla($cast_id){
		$sql = "select sum(Appcasttime.programed) as suma from appcasttimes
				 Appcasttime where Appcasttime.status='AC' and Appcasttime.appcast_id=".$cast_id." group by (appcast_id)";
		$total = $this->query($sql);

		//$listacombo=0;
		$total=(!empty($total) && isset($total))?$total[0][0]['suma']:0;
		return $total;
		
	}
	
	function generateLista(){
		$sql = "SELECT
				Appcast.id,
				Appcast.description,
				Appcast.status,cantcitas.suma FROM
				appcasts Appcast left JOIN (select appcast_id,sum(Appcasttime.programed) as suma from appcasttimes
				Appcasttime where Appcasttime.status='AC' group by (appcast_id)) as cantcitas on (Appcast.id=cantcitas.appcast_id)
        		where Appcast.status = 'AC'";
		$total = $this->query($sql);
		$listacombo=array();
		if(!empty($total) && isset($total)){
			foreach($total as $item=>$valor){
				$suma=($valor['cantcitas']['suma']!='')?$valor['cantcitas']['suma']:0;
				$listacombo[$valor['Appcast']['id']]=$valor['Appcast']['description'].' CP:'.$suma;
			}
		}
		return $listacombo;
	}
}